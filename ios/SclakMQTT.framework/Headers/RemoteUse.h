//
//  RemoteUse.h
//  SclakApp
//
//  Created by Daniele Poggi on 27/08/2019.
//  Copyright © 2019 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@interface RemoteUse : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *rele;
@property (nonatomic, strong) NSNumber<Optional> *aux;
@property (nonatomic, strong) NSNumber<Optional> *res;
@property (nonatomic, strong) NSString<Optional> *resultMessage;

- (BOOL) isOnError;

@end
