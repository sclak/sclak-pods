#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "RemoteUse.h"
#import "SCKMQTTManager.h"
#import "SclakMQTT.h"

FOUNDATION_EXPORT double SclakMQTTVersionNumber;
FOUNDATION_EXPORT const unsigned char SclakMQTTVersionString[];

