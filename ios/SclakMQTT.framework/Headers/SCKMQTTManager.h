//
//  SCKMQTTManager.h
//  SclakApp
//
//  Created by Daniele Poggi on 27/08/2019.
//  Copyright © 2019 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SclakFacade/SclakFacade.h>
#import <SclakBle/PPLPeripheralUsage.h>
#import "RemoteUse.h"

/**
 * status changed notification. object is the btcode (used also for filtering)
 * userInfo is a dictionary with key "online" and value NSNumber TRUE/FALSE
 */
#define kMQTTManager_OnConnectionStatusChanged_Notification @"kMQTTManager_OnConnectionStatusChanged_Notification"
#define kMQTTManager_OnDoorStatusChanged_Notification       @"kMQTTManager_OnDoorStatusChanged_Notification"
#define kMQTTConnectionStatus                               @"online"
#define kMQTTDoorStatus                                     @"door_status"

@protocol SCKMQTTManagerDelegate <NSObject>

- (void) btcode:(NSString*)btcode onConnectionStatusChanged:(BOOL)online;
- (void) btcode:(NSString*)btcode onDoorStatusChanged:(NSUInteger)doorStatus;

@end

@interface SCKMQTTManager : NSObject

@property (nonatomic, readonly, assign) BOOL connected;

/**
 * dictionary with online states
 * btcode (NSString*) is the key
 * status (NSNumber*) is the value
 * use isOnline: to check for online status
 */
@property (nonatomic, readonly, strong) NSDictionary *onlinePeripheralBtcodes;

/**
 * the delegate. alternatively subscribe to notifications (defined above)
 */
@property (nonatomic, weak) id<SCKMQTTManagerDelegate> delegate;

/**
 * singleton access
 */
+ (instancetype) sharedInstance;

/**
 * make a peripheral announce request by API
 */
+ (void) announce:(NSString*)btcode;

/**
 * retrieve every sclak unit bridge present in SCKFacade peripherals
 * connect to the broker if needed
 * subscribe to every bridge for status changed
 */
- (void) subscribeToAll;

- (void) subscribe:(NSString*)btcode;
- (void) unsubscribe:(NSString*)btcode;

- (BOOL) isOnline:(NSString*)btcode;
- (BOOL) isOnline:(NSArray<NSString*>*)btcodes btcode:(NSString**)btcode;

- (void) requestCommand:(SclakCommand)command forPeripheral:(Peripheral*)peripheral callback:(ResponseObjectCallback)callback;

- (void) publish:(NSString*)message onTopic:(NSString*)topic;

@end
