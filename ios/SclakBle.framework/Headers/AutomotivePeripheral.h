//
//  AutomotivePeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 14/03/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "SclakPeripheral.h"

typedef NS_ENUM(NSUInteger, SclakAMStatus) {
    SclakAMStatusUnknown,
    SclakAMStatusActivated,
    SclakAMStatusDeactivated
};

typedef NS_ENUM(NSUInteger, SclakAMCommand) {
    SclakAMCommandNone,
    SclakAMCommandActivate,
    SclakAMCommandDeactivate
};

// specific commands
extern const uint8_t kPhoneToDevice_SEND_OUT_AM;
extern const uint8_t kDeviceToPhone_RESP_SEND_OUT_AM;
extern const uint8_t kPhoneToDevice_GET_OUT_AM;
extern const uint8_t kDeviceToPhone_RESP_GET_OUT_AM;

@interface AutomotivePeripheral : SclakPeripheral

/**
 * door status
 * 0 = unknown status
 * 1 = Locked: chiusura centralizzata chiusa, indica che l’ultimo comando inviato è stato quello di chiusura centralizzata.
 * 2 = Unlocked: chiusura centralizzata aperta, indica che l’ultimo comando inviato è stato quello di apertura centralizzata.
 */
@property (nonatomic, assign) SclakAMStatus doorStatus;

/**
 * immobilizer status
 * 0 = unknown status
 * 1 = Locked: Relè immobilizer disattivo, avviamento vettura impossibile.
 * 2 = Unlocked: Relè immobilizer attivo, avviamento vettura possibile.
 */
@property (nonatomic, assign) SclakAMStatus immobilizerStatus;

#pragma mark - Actions

/**
 * Comando di apertura centralizzata, attiva l’uscita del relè di apertura centralizzata per il tempo specifico impostato sullo sclak.
 */
- (void) unlockDoorCallback:(BluetoothResponseErrorCallback)callback;

/**
 * Comando di chiusura centralizzata, attiva l’uscita del relè di chiusura centralizzata per il tempo specifico impostato sullo sclak.
 */
- (void) lockDoorCallback:(BluetoothResponseErrorCallback)callback;

/**
 * Comando di attivazione immobilizer (sblocco avviamento vettura), attiva l’uscita del relè immobilizer permettendo il corretto avviamento della vettura.
 */
- (void) unlockImmobilizerCallback:(BluetoothResponseErrorCallback)callback;

/**
 * Comando di disattivazione immobilizer (blocco avviamento vettura), disattiva l’uscita del relè immobilizer impedendo l’ avviamento della vettura.
 */
- (void) lockImmobilizerCallback:(BluetoothResponseErrorCallback)callback;

/**
 * Esito OK Il valore delle uscite rispecchia quello richiesto nel comando
 * 0x01 Esito KO, il codice PIN inserito non è stato trovato nella tabella interna allo Sclak o il codice PUK risulta errato. Lo stato delle uscite rimane invariato.
 * 0x02 Esito KO, il codice PIN inserito è stato trovato ma non risulta valido se inviato da Tastiera/Radiocomando. Lo stato delle uscite rimane invariato.
 * 0x03 Esito KO, il codice PIN inserito è stato trovato ma la validazione temporale basata sull’ora interna dello sclak è fallita. Lo stato delle uscite rimane invariato.
 */
- (void) commandDoor:(SclakAMCommand)command immobilizer:(SclakAMCommand)command callback:(BluetoothResponseErrorCallback)callback;

@end
