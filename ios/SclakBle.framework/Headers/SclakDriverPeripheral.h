//
//  SclakDriverPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 21/09/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "SclakDriverUserConfiguration.h"

@interface SclakDriverPeripheral : SclakPeripheral

@property (nonatomic, strong) SclakDriverUserConfiguration *userConfiguration;

#pragma mark - User Configuration

- (void) sendUserConfiguration:(SclakDriverUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

@end
