//
//  ODLPlantCommand.h
//  SclakBle
//
//  Created by Daniele Poggi on 29/09/15.
//  Copyright © 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ODLGeneralStatus.h"

typedef NS_ENUM(NSUInteger, ODLSystemActivation) {
    ODLSystemActivationOFF,
    ODLSystemActivationON
};

typedef NS_ENUM(NSUInteger, ODLSendCommandError) {
    ODLSendCommandErrorConfig = 0,
    ODLSendCommandErrorMalformedConfiguration = 1,
    ODLSendCommandErrorMalformedSetPress = 2,
    ODLSendCommandErrorMalformedSetFlow = 3,
    ODLSendCommandErrorPlantNotConfigured = 10,
    ODLSendCommandErrorBeerTypeNotConfigured = 11,
    ODLSendCommandErrorMemoryInUse = 12
};

typedef NS_ENUM(NSUInteger, ODLDisplayLanguage) {
    ODLDisplayLanguageEnglish,
    ODLDisplayLanguageItalian,
    ODLDisplayLanguageGerman,
    ODLDisplayLanguageFrench,
    ODLDisplayLanguageSpanish
};

@interface ODLPlantCommand : NSObject

/**
 * Flag operativo e comandi
 */
@property (nonatomic, assign) ODLSystemActivation systemActivation;

/**
 * Scenario di spillatura richiesto
 */
@property (nonatomic, assign) ODLConfigurationType systemConfiguration;

/**
 * Pressione impostata dall’utente, questo valore viene utilizzato solo nello scenario di spillatura Manuale. Valore espresso in mBar in formato MSByte first
 */
@property (nonatomic, strong) NSNumber *setPress;

/**
 * Portata impostata dall’utente, questa definisce il target di portata che si cerca di mantenere nelle modalità Manuale e Semiautomatica. Valore espresso in dl/m.
 */
@property (nonatomic, strong) NSNumber *setFlow;

#pragma mark -

- (NSMutableData*) messageData;

@end
