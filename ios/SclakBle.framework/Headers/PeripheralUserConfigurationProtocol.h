//
//  PeripheralUserConfigurationProtocol.h
//  SclakBle
//
//  Created by Daniele Poggi on 22/10/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

@protocol PeripheralUserConfigurationProtocol <NSObject>

- (instancetype) initWithData:(NSData*)data;
- (NSMutableData*) messageData;

- (void) setInstalled:(BOOL)installed;

- (void) setBuzzerEnabled:(BOOL)enabled;

- (void) setLedEnabled:(BOOL)enabled;
- (void) setLedBrightness:(NSUInteger)powerLevel;

- (void) setAutocloseTime:(NSUInteger)autocloseTime;

- (void) setWrongPinFeedbackEnabled:(BOOL)enabled;

+ (instancetype) defaultConfiguration;

@end
