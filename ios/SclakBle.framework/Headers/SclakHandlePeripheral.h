//
//  SclakHandlePeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 14/11/15.
//  Copyright © 2015 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "SCKHandleUserConfiguration.h"
#import "SclakHandleStatus.h"

@interface SclakHandlePeripheral : SclakPeripheral

#pragma mark - Handle Properties

@property (nonatomic, strong) SclakHandleStatus *status;

/**
 * Conteggio reset I2C effettuati dall’ultimo reset della scheda
 */
@property (nonatomic, assign) NSUInteger countResetI2C;

/**
 * Conteggio reset Tastiera effettuati dall’ultimo reset della scheda
 */
@property (nonatomic, assign) NSUInteger countResetKeyb;

#pragma mark - User Configuration

@property (nonatomic, strong) SCKHandleUserConfiguration *userConfiguration;

/**
 *  send user configuration
 *
 *  @param userConfiguration
 *  @param callback
 */
- (void) sendUserConfiguration:(SCKHandleUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

@end
