//
//  BWGWifiSSID.h
//  SclakBle
//
//  Created by Daniele Poggi on 14/10/2020.
//  Copyright © 2020 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BWGPeripheral;

@interface BWGDebugLog : NSObject

/**
 * Log counter
 */
@property (nonatomic, assign) NSUInteger counter;

/**
 * Log message string
 */
@property (nonatomic, strong) NSString *log;

/**
 * Log timestamp insert time in UTC, string formatted as YYYY-MM-DD HH:mm:ss
 */
@property (nonatomic, strong) NSString *insertTime;

/**
 * TRUE if the log message is detected as relevant by regex matching a series of known issues
 * this is done automatically when calling the designated constructors
 * the property is NOT setted when calling the empty constructor
 */
@property (nonatomic, assign) BOOL important;

/**
 * if the log is important, then has a localizedString 
 */
@property (nonatomic, strong) NSString *localizedString;

- (instancetype) initWithData:(NSData*)data bwg:(BWGPeripheral*)bwg;
- (instancetype) initWithCounter:(NSUInteger)counter log:(NSString*)log bwg:(BWGPeripheral*)bwg;

- (NSString*) toJSONString;

@end
