//
//  GivikPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 05/07/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "SclakGearAPeripheral.h"
#import "GivikStatus.h"
#import "GivikUserConfiguration.h"

extern NSString * const PREFS_AUTO_OPEN;
extern NSString * const PREFS_AUTO_OPEN_NEAR_VALUE;
extern NSString * const PREFS_AUTO_OPEN_FAR_VALUE;

extern NSInteger const AUTO_OPEN_MIN_NEAR_VALUE;
extern NSInteger const AUTO_OPEN_MAX_FAR_VALUE;

extern NSInteger const AUTO_OPEN_DEFAULT_NEAR_VALUE;
extern NSInteger const AUTO_OPEN_DEFAULT_FAR_VALUE;

typedef void(^GivikPollRssiCallback)(NSString *btcode, BOOL farReached);

@interface GivikPeripheral : SclakGearAPeripheral

@property (nonatomic, strong) GivikStatus *status;
@property (nonatomic, assign) GivikLockStatus previousLockStatus;
@property (nonatomic, strong) GivikUserConfiguration *userConfiguration;

/**
 * semaphore that keeps the state of the manual usage
 * true: the peripheral has been manually operated
 * auto-open is the locked since the auto close is performed
 */
@property (nonatomic, assign) BOOL manuallyOperated;

@property BOOL flagInterruptPollingRssi;

- (BOOL) isOpen;
- (BOOL) wasOpen;

+ (void) setNearAutoOpenRange:(NSInteger)value;
+ (void) setFarAutoOpenRange:(NSInteger)value;

#pragma mark - User Configuration

- (void) sendUserConfiguration:(GivikUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

#pragma mark - APIs

+ (GivikPollRssiCallback) pollRssiCallback;

/**
 * callback used by the App to set the polling Business Logic
 */
+ (void) setPollRssiCallback:(GivikPollRssiCallback)callback;

#pragma mark - RSSI Polling

- (void) startPollRssiForOpenCloseCallback:(GivikPollRssiCallback)callback;
- (void) stopPollRssiCallback:(BluetoothReadRSSICallback)callback;

#pragma mark - Shake and open

- (void) startShakeAndOpen;
- (void) stopShakeAndOpen;

@end
