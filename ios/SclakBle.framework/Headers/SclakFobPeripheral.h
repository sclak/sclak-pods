//
//  SclakRemotePeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 20/06/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "SCKFobMode.h"
#import "SCKFobButton.h"

typedef NS_ENUM(NSUInteger, SCKFobType) {
    SCKFobTypeTekoOVO
};

// sclak fob specific commands
extern const uint8_t kPhoneToDevice_SET_MODE_FUNZ;
extern const uint8_t kDeviceToPhone_RESP_SET_MODE_FUNZ;
extern const uint8_t kPhoneToDevice_REQ_MODE_FUNZ;
extern const uint8_t kDeviceToPhone_RESP_REQ_MODE_FUNZ;
extern const uint8_t kPhoneToDevice_SET_GRP_TYPE;
extern const uint8_t kDeviceToPhone_RESP_SET_GRP_TYPE;
extern const uint8_t kPhoneToDevice_REQ_TYPE_KEYFOB;
extern const uint8_t kDeviceToPhone_RESP_REQ_TYPE_KEYFOB;
extern const uint8_t kPhoneToDevice_SET_PRG_KEY;
extern const uint8_t kDeviceToPhone_RESP_SET_PRG_KEY;
extern const uint8_t kPhoneToDevice_REQ_PRG_KEY;
extern const uint8_t kDeviceToPhone_RESP_REQ_PRG_KEY;
extern const uint8_t kPhoneToDevice_REQ_REQ_RESET_VLDY;
extern const uint8_t kDeviceToPhone_RESP_REQ_RESET_VLDY;

@interface SclakFobPeripheral : SclakPeripheral

/**
 *  Tipologia di radiocomando, permette di definire il modello del telecomando in modo da adattare la grafica di programmazione dei
 *  tasti sulla App: 0x00 Radiocomando in case Teko OVO.
 */
@property (nonatomic, assign) SCKFobType fobType;

/**
 *  Numero di tasti programmabili sul radiocomando.
 */
@property (nonatomic, assign) NSUInteger numButtons;
@property (nonatomic, strong) NSMutableDictionary *buttons;

@property (nonatomic, strong) SCKFobMode *mode;
@property (nonatomic, strong) NSString *groupType;

@property (nonatomic, strong) NSString *pairedBtcode;
@property (nonatomic, strong) NSString *pairedSecret;

#pragma mark - Buttons

- (SCKFobButton*) buttonWithId:(NSUInteger)buttonId;

#pragma mark - Legacy Methods

- (void) getPairedBtcodeCallback:(BluetoothResponseCallback)callback;
- (void) sendPairingWithBtcode:(NSString*)btcode secret:(NSString*)secret callback:(BluetoothResponseCallback)callback;

#pragma mark -

- (void) readModeOfOperationCallback:(BluetoothResponseCallback)callback;
- (void) sendModeOfOperation:(SCKFobMode*)mode callback:(BluetoothResponseCallback)callback;

- (void) readGroupTypeCallback:(BluetoothResponseCallback)callback;
- (void) sendGroupType:(NSString*)groupType callback:(BluetoothResponseCallback)callback;

- (void) readTypeCallback:(BluetoothResponseCallback)callback;

- (void) readButtonsCallback:(BluetoothResponseCallback)callback;
- (void) readButtonAtIndex:(NSUInteger)index callback:(BluetoothResponseCallback)callback;

- (void) sendButton:(SCKFobButton*)button callback:(BluetoothResponseCallback)callback;
- (void) sendButtons:(NSArray*)buttons callback:(BluetoothResponseCallback)callback;

- (void) resetTimeValidityCallback:(BluetoothResponseCallback)callback;

@end
