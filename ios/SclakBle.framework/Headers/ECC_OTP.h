//
//  ECC_OTP.h
//  SclakBle
//
//  Created by Daniele Poggi on 17/04/2020.
//  Copyright © 2020 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * ECC OTP Object
 */
@interface ECC_OTP : NSObject

@property (nonatomic, assign) NSUInteger idKey;
@property (nonatomic, strong) NSData *valueKey;

- (instancetype) initWithIdKey:(NSUInteger)idKey valueKey:(NSData*)valueKey;

@end
