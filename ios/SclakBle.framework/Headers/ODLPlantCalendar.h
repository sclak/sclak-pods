//
//  ODLPlantCalendar.h
//  SclakBle
//
//  Created by Daniele Poggi on 05/10/15.
//  Copyright © 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ODLPlantCalendar : NSObject

typedef NS_ENUM(NSUInteger, ODLPlantCalendarWeekday) {
    ODLPlantCalendarWeekdayMonday,
    ODLPlantCalendarWeekdayTuesday,
    ODLPlantCalendarWeekdayWednesday,
    ODLPlantCalendarWeekdayThursday,
    ODLPlantCalendarWeekdayFriday,
    ODLPlantCalendarWeekdaySaturday,
    ODLPlantCalendarWeekdaySunday
};

/**
 * Blocco dell’erogazione birra al di fuori delle fasce orarie impostate.
 */
@property (nonatomic, assign) BOOL lockOutsideCalendar;

/**
 * Abilitazione gestione Fasce orarie
 */
@property (nonatomic, assign) BOOL calendarEnabled;

/**
 * configurazione di 2 fasce orarie per ciascun giorno della settimana
 * e.g.
 *
 * {
 * 0: [ NSDate (00:00) , NSDate (00:00) , NSDate (00:00) , NSDate (00:00) ],      // Lunedì       e.g. 2 fascie orarie non usate
 * 1: [ NSDate (08:00) , NSDate (12:00) , NSDate (00:00) , NSDate (00:00) ],      // Martedì      e.g. 1 fascia usata, la 2 no
 * 2: [ NSDate (08:00) , NSDate (12:00) , NSDate (14:30) , NSDate (08:00) ],      // Mercoledì    e.g. 2 fascie orarie usate
 * 3: [ NSDate (08:00) , NSDate (08:10) , NSDate (08:20) , NSDate (08:30) ],      // Giovedì      minimi tempi 10 minuti
 * 4: [ NSDate (08:00) , NSDate (20:00) , NSDate (00:00) , NSDate (00:00) ],      // Venerdì      uso realistico
 * 5: [ NSDate (00:00) , NSDate (00:00) , NSDate (00:00) , NSDate (00:00) ],      // Sabato       tutto il giorno = non settato (non bloccato)
 * 6: [ NSDate (19:00) , NSDate (05:00) , NSDate (00:00) , NSDate (00:00) ]       // Domenica     passaggio al giorno dopo
 * }
 */
@property (nonatomic, strong) NSDictionary *weekdays;

+ (instancetype) emptyCalendar;

- (instancetype) initWithData:(NSData*)data;

- (NSMutableData*) messageData;

/**
 * check if a time slot can be added to weekday
 */
- (BOOL) canAddTimeSlotToWeekday:(ODLPlantCalendarWeekday)weekday;

/**
 * utility to fill plant calendar with time slots
 * you can call this method max twice for each weekday
 */
- (BOOL) addTimeSlotToWeekday:(ODLPlantCalendarWeekday)weekday begin:(NSDate*)begin end:(NSDate*)end;

/**
 * utility to change time slot to weekday
 */
- (BOOL) updateTimeSlotToWeekday:(ODLPlantCalendarWeekday)weekday timeSlot:(NSInteger)timeSlot begin:(NSDate*)begin end:(NSDate*)end;

/**
 * utility to remove time slots from weekday
 */
- (void) removeTimeSlots:(ODLPlantCalendarWeekday)weekday;

/**
 * utility to remove a specific time slot from weekday
 * valid timeSlot is: 1 or 2
 */
- (BOOL) removeTimeSlot:(ODLPlantCalendarWeekday)weekday timeSlot:(NSInteger)timeSlot;

@end
