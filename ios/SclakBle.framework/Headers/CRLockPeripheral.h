//
//  SclakCabinetPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 14/11/2017.
//  Copyright © 2017 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "CRLockStatus.h"

@interface CRLockPeripheral : SclakPeripheral

@property (nonatomic, strong) CRLockStatus *status;

@end
