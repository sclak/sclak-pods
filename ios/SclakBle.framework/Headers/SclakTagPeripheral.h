//
//  SclakTagPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 21/02/2017.
//  Copyright © 2017 sclak. All rights reserved.
//

#import "SclakFobPeripheral.h"
#import "SCKSclakTagUserConfiguration.h"

@interface SclakTagPeripheral : SclakFobPeripheral

@property (nonatomic, strong) SCKSclakTagUserConfiguration *userConfiguration;

#pragma mark - User Configuration

- (void) sendUserConfiguration:(SCKSclakTagUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

@end
