//
//  SclakPeripheralPin2
//  SclakBle
//
//  Created by Daniele Poggi on 12/11/16.
//  Copyright (c) 2016 sclak. All rights reserved.
//

#import "SclakPinWeekdayTimeSlot.h"
#import "SclakPeripheralPin.h"

#define PIN_REVISION                    @"106"

typedef NS_ENUM(NSUInteger, PinMode) {
    PinMode3Periods,
    PinModeDaily
};

typedef NS_ENUM(NSUInteger, PinTimePeriodMode) {
    PinTimePeriodModeDaily,
    PinTimePeriodModeWeekly
};

@interface SclakPeripheralPin106 : SclakPeripheralPin

+ (SclakPeripheralPin106*) pinWithData:(NSData*)data;

+ (SclakPeripheralPin106*) pinWithJSON:(NSData*)json;
+ (SclakPeripheralPin106*) pinWithJSON:(NSData*)json timezone:(NSTimeZone*)timezone;

+ (SclakPeripheralPin106*) defaultPin;

+ (NSString*) defaultFlags;

#pragma mark - FLAGS

/**
 * Selezione modalità giornaliera (0) oppure a 3 periodi (1)
 * Se il Bit0 è impostato a 1, i registri di validità temporale vengono interpretati secondo lo schema di validità giornaliera. In questa modalità sono presenti 7 fasce orarie la cui validità dipende dalla modalità di utilizzo delle stesse, indicata con il Bit12 (descritto in seguito)
 Se il Bit0 è impostato a 0, i registri di validità temporale vengono interpretati secondo lo schema di validità a 3 periodi. In questa modalità sono presenti 3 periodi di validità, ciascun periodo composto da una data di inizio ed una di fine; le due date possono coincidere per indicare un periodo di un solo giorno. In questa modalità i 3 periodi sono sempre controllati, se abilitati con i rispettivi Bit2, Bit3 e Bit4, ed il primo dei 3 periodi ad essere abilitato e valido nell’ora attuale permette l’apertura.
 *
 */
@property (nonatomic, assign) PinMode pinMode;

/**
 * Abilitazione accesso per un numero limitato di volte, l’accesso viene permesso solo se il valore COUNT è diverso da 0, ad ogni accesso COUNT viene decrementato.
 */
@property (nonatomic, assign) BOOL countEnabled;

/**
 * Abilitazione modalità periodo INIT1 – END1 all’interno del periodo orario indicato da TIME_1
 */
@property (nonatomic, assign) BOOL period1Enabled;

/**
 * Abilitazione modalità periodo INIT2 – END2 all’interno del periodo orario indicato da TIME_2
 */
@property (nonatomic, assign) BOOL period2Enabled;

/**
 * Abilitazione modalità periodo INIT3 – END3 all’interno del periodo orario indicato da TIME_3
 */
@property (nonatomic, assign) BOOL period3Enabled;

/**
 * Selezione modalità fasce orarie giornaliera (0) o settimanale (1)
 */
@property (nonatomic, assign) PinTimePeriodMode timePeriodsMode;

/**
 * Blocco utilizzo pin da parte di tastiera/radiocomando (1 utilizzo pin solo da smartphone, 0 utilizzo pin da tutti i dispositivi).
 */
@property (nonatomic, assign) BOOL denyAccessories;

#pragma mark - COUNT

/**
 * Conteggio numero di accessi ancora utilizzabili, abilitato dal Bi1 del campo Flag, permette di abilitare il codice per un numero limitato di utilizzi, ad ogni utilizzo il valore COUNT viene decrementato, quando raggiunge 0 l’accesso viene inibito, ed il cluster relativo al codice viene cancellato dalla memoria di Sclak. Il valore massimo del numero di accessi è 255.
 */
@property (nonatomic, assign) NSUInteger count;

#pragma mark - MODALITA' GIORNALIERA

/**
 * Maschera di accesso sulla base dei giorno della settimana, la maschera viene abilitata dal Bit0 del campo FLAG, la gestione è a bit (1 accesso abilitato nel giorno indicato):
 ￼Bit7 Non Utilizzato (0).
 Bit6 Domenica. Bit5 Sabato. Bit4 Venerdì. Bit3 Giovedì. Bit2 Mercoledì. Bit1 Martedì. Bit0 Lunedì.
 */
@property (nonatomic, strong) NSArray *weekdays;

/**
 * struttura dati per la modalità giornaliera
 */
@property (nonatomic, strong) NSMutableDictionary *timeSlots;

#pragma mark - 3 PERIODI

/**
 * INIT1 convertito in data
 */
@property (nonatomic, strong) NSDate *period1BeginDate;

/**
 * END1 convertito in data
 */
@property (nonatomic, strong) NSDate *period1EndDate;

/**
 * INIT2 convertito in data
 */
@property (nonatomic, strong) NSDate *period2BeginDate;

/**
 * END2 convertito in data
 */
@property (nonatomic, strong) NSDate *period2EndDate;

/**
 * INIT3 convertito in data
 */
@property (nonatomic, strong) NSDate *period3BeginDate;

/**
 * END3 convertito in data
 */
@property (nonatomic, strong) NSDate *period3EndDate;

/**
 * TIME1 period 1 time slot
 */
@property (nonatomic, strong) SclakPinTimeSlot *period1TimeSlot;

/**
 * TIME2 period 2 time slot
 */
@property (nonatomic, strong) SclakPinTimeSlot *period2TimeSlot;

/**
 * TIME3 period 3 time slot
 */
@property (nonatomic, strong) SclakPinTimeSlot *period3TimeSlot;

/**
 * local time zone passed in pinWithJSON: method
 */
@property (nonatomic, strong) NSTimeZone *timeZone;

#pragma mark - exposed for unit testing

+ (uint16_t) timeSlotToIntBegin:(NSDate*)begin end:(NSDate*)end allDay:(BOOL)allDay disabled:(BOOL)disabled;

+ (SclakPinTimeSlot*) timeSlotFromInt:(uint16_t)time;
+ (SclakPinTimeSlot*) timeSlotFromInt:(uint16_t)time weekday:(NSNumber*)weekday;

+ (uint16_t) dateToInt:(NSDate*)date;
+ (NSDate*) dateFromInt:(uint16_t)date;
+ (NSDate*) dateFromInt:(uint16_t)date seconds:(uint16_t)seconds;

@end
