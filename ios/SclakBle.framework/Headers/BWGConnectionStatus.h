//
//  BWGConnectionStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 14/10/2020.
//  Copyright © 2020 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, BWG_GSM_Status) {
    gsm_not_used = 0,
    gsm_configuring_core = 1,
    gsm_configuring_sim = 2,
    gsm_error_sim = 3,
    gsm_configuring_advanced = 4,
    gsm_accessing_network = 5,
    gsm_attaching_gprs = 10,
    gsm_connecting_mqtt = 11,
    gsm_disconnecting_mqtt = 12,
    gsm_error_connecting_mqtt = 13,
    gsm_connection_ok = 14,
    gsm_disconnecting_change = 15
};

typedef NS_ENUM(NSUInteger, BWG_WiFi_Status) {
    wifi_not_used = 0,
    wifi_resetting = 1,
    wifi_configuring = 2,
    wifi_configured_ok = 3,
    wifi_attaching = 10,
    wifi_connecting = 11,
    wifi_disconnecting = 12,
    wifi_connection_error = 13,
    wifi_connection_ok = 14,
    wifi_disconnecting_change = 15
};

typedef NS_ENUM(NSUInteger, BWG_MQTT_Status) {
    mqtt_connection_inactive,
    mqtt_connection_error,
    mqtt_connecting,
    mqtt_subscribing,
    mqtt_pinging,
    mqtt_connection_ok
};

@interface BWGConnectionStatus : NSObject

@property (nonatomic, assign) BWG_GSM_Status gsmStatus;
@property (nonatomic, assign) NSInteger gsmRssi;
@property (nonatomic, assign) BWG_WiFi_Status wifiStatus;
@property (nonatomic, assign) NSInteger wifiRssi;
@property (nonatomic, assign) BWG_MQTT_Status mqttStatus;

- (instancetype) initWithData:(NSData*)data;

@end
