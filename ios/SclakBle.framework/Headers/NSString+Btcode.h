//
//  NSString+Btcode.h
//  PassePartoutLib
//
//  Created by Daniele Poggi on 27/01/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Btcode)

- (BOOL) isBtcodeValid;
- (NSString*) halfBtcode;

@end
