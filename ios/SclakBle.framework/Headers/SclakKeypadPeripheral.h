//
//  SclakKeyboardPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 12/03/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "SCKKeypadButton.h"
#import "SCKKeypadUserConfiguration.h"

// sclak keyboard specific commands
extern const uint8_t kPhoneToDevice_KEYB_SET_PRG_KEY;
extern const uint8_t kDeviceToPhone_KEYB_RESP_SET_PRG_KEY;
extern const uint8_t kPhoneToDevice_KEYB_REQ_PRG_KEY;
extern const uint8_t kDeviceToPhone_KEYB_RESP_REQ_PRG_KEY;

@interface SclakKeypadPeripheral : SclakPeripheral

@property (nonatomic, strong) NSString *pairedBtcode;
@property (nonatomic, strong) NSString *pairedSecret;

/**
 *  Numero di tasti programmabili sul radiocomando.
 */
@property (nonatomic, assign) NSUInteger numButtons;
@property (nonatomic, strong) NSMutableDictionary *buttons;

@property (nonatomic, strong) SCKKeypadUserConfiguration *userConfiguration;

#pragma mark - User Configurations

- (void) sendUserConfiguration:(SCKKeypadUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

#pragma mark - APIs

- (void) getPairedBtcodeCallback:(BluetoothResponseCallback)callback;
- (void) sendPairingWithBtcode:(NSString*)btcode secret:(NSString*)secret callback:(BluetoothResponseCallback)callback;

#pragma mark - Buttons APIs

- (SCKKeypadButton*) buttonWithId:(NSUInteger)buttonId;

- (void) readButtonsCallback:(BluetoothResponseCallback)callback;
- (void) readButtonAtIndex:(NSUInteger)index callback:(BluetoothResponseCallback)callback;

- (void) sendButton:(SCKKeypadButton*)button callback:(BluetoothResponseCallback)callback;
- (void) sendButtons:(NSArray*)buttons callback:(BluetoothResponseCallback)callback;

@end
