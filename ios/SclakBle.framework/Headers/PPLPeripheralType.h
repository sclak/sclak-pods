//
//  PPLPeripheralType.h
//  SclakBle
//
//  Created by Daniele Poggi on 18/06/2019.
//  Copyright © 2019 sclak. All rights reserved.
//

typedef NS_ENUM(NSUInteger, PeripheralType) {
    
    PeripheralType_Unknown, // IMPORTANT: keep it first element of enum
    
    PeripheralType_Sclak,   // IMPORTANT: keep it second element of enum
    
    PeripheralType_Ulock,
    PeripheralType_SclakSafe,
    PeripheralType_Parkey,
    PeripheralType_Wittkopp,
    PeripheralType_NearSclak,
    PeripheralType_ZTF,
    PeripheralType_SclakFob,
    PeripheralType_SclakKeypad,
    PeripheralType_LDimm,
    PeripheralType_Cemo,
    PeripheralType_SistemaIdea_ODL,
    PeripheralType_SclakHandle,
    PeripheralType_Mifare,
    PeripheralType_SclakBattery,
    PeripheralType_Entrematic,
    PeripheralType_SclakHandleTag,
    PeripheralType_SclakHandleKeyb,
    PeripheralType_SclakTag,
    PeripheralType_SclakLock,
    PeripheralType_Wiegand,
    PeripheralType_SclakCRLock,
    PeripheralType_SclakCylinder,
    PeripheralType_DHand,
    PeripheralType_S2R,
    PeripheralType_SAM,
    PeripheralType_LKR,
    PeripheralType_SclakGear,
    PeripheralType_SclakUFOe,
    PeripheralType_Givik,
    PeripheralType_SclakGearA,
    PeripheralType_SclakGearR,
    PeripheralType_SclakTagPresence,
    PeripheralType_SclakDriver,
    PeripheralType_SclakPadlock,
    PeripheralType_SclakCRLockM,
    PeripheralType_MBolt,
    PeripheralType_KDE,
    PeripheralType_PTS,
    PeripheralType_BWG,
    PeripheralType_SclakCylinderE,
    PeripheralType_SclakReader,
    
    // append new types over this comment
    
    PeripheralType_Collaudo_SHA256 // IMPORTANT: keep it last element of enum
};
