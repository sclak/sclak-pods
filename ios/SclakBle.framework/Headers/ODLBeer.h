//
//  ODLBeer.h
//  SclakBle
//
//  Created by Daniele Poggi on 21/10/15.
//  Copyright © 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ReadBeerError) {
    ReadBeerErrorNone,
    ReadBeerErrorNotFound,
    ReadBeerErrorFormat,
    ReadBeerErrorInvalidMemoryAccess
};

@interface ODLBeer : NSObject

/**
 * Produttore della Birra
 */
@property (nonatomic, strong) NSString *manufacturer;

/**
 * tipologia della birra
 */
@property (nonatomic, strong) NSString *type;

/**
 * nome della birra
 */
@property (nonatomic, strong) NSString *name;

/**
 * errore di lettura dati
 */
@property (nonatomic, assign) ReadBeerError error;

#pragma mark -

- (instancetype) initWithData:(NSData*)data;

@end
