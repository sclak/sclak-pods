//
//  KDEUserConfiguration.h
//  AFNetworking
//
//  Created by Daniele Poggi on 29/10/2019.
//

#import "PPLGenericUserConfiguration.h"

@interface KDEUserConfiguration : PPLGenericUserConfiguration

@end
