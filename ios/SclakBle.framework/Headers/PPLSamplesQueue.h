//
//  PPLSamplesQueue.h
//  PassePartoutLib
//
//  Created by isghe on 02/01/14.
//  Copyright (c) 2014 Daniele Poggi - Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPLSample.h"

@interface PPLSamplesQueue : NSObject

@property (readonly) NSUInteger currentQueueLength;

- (id) initWithLength:(NSUInteger)theQueueLength;
- (void) push:(PPLSample*)theSample;
- (NSNumber*) weightedAverage;
- (NSArray*) samples;
@end
