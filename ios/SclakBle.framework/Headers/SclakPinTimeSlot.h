//
//  SclakPinTimeSlot.h
//  SclakBle
//
//  Created by Daniele Poggi on 29/06/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kAllDayKey                          @"allDay"
#define kDisabledKey                        @"disabled"
#define kBeginTimeKey                       @"beginTime"
#define kEndTimeKey                         @"endTime"

@interface SclakPinTimeSlot : NSObject

@property (nonatomic, assign) BOOL allDay;
@property (nonatomic, assign) BOOL disabled;
@property (nonatomic, strong) NSDate *beginTime;
@property (nonatomic, strong) NSDate *endTime;
@property (nonatomic, strong) NSTimeZone *timeZone;

+ (instancetype) defaultSlot;

- (instancetype) initWithTimeBegin:(NSDate*)initial end:(NSDate*)end;
- (instancetype) initWithTimeBegin:(NSDate*)initial end:(NSDate*)end allDay:(BOOL)allDay disabled:(BOOL)disabled;

- (instancetype) initWithJSONModel:(NSDictionary*)model;
- (instancetype) initWithJSONModel:(NSDictionary*)model timezone:(NSTimeZone*)timezone;

- (NSMutableDictionary*) toJSONModel;

@end
