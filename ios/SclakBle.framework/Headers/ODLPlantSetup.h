//
//  ODLPlantSetup.h
//  SclakBle
//
//  Created by Daniele Poggi on 05/10/15.
//  Copyright © 2015 sclak. All rights reserved.
//

#import "ODLGeneralStatus.h"
#import "ODLPlantCommand.h"

typedef NS_ENUM(NSUInteger, ODLCleanType) {
    ODLCleanTypeKeg,
    ODLCleanTypeWaterSupply
};

@interface ODLPlantSetup : NSObject <NSCopying>

/**
 * type of gas used by the plant
 */
@property (nonatomic, assign) ODLGasCylinderType gasType;

/**
 * Soglia di transizione bombola in fase di esaurimento. Valore espresso in Bar.
 */
@property (nonatomic, strong) NSNumber *cylinderEmptyingThreshold;

/**
 * Soglia di transizione bombola vuota. Valore espresso in Bar.
 */
@property (nonatomic, strong) NSNumber *cylinderEmptyThreshold;

/**
 * Differenza di pressione tra riduttore e compensatore (100mBar ogni metro). Valore espresso in mBar in formato MSByte first.
 */
@property (nonatomic, strong) NSNumber *deltaP;

/**
 * Portata minima ammissibile sull’impianto. Valore espresso in dl/m.
 */
@property (nonatomic, strong) NSNumber *minFlow;

/**
 * Portata massima ammissibile sull’impianto. Valore espresso in dl/m.
 */
@property (nonatomic, strong) NSNumber *maxFlow;

/**
 * Quantità di birra da erogare prima di attivare l’algoritmo di discesa della pressione di spillatura in modalità Auto ed Eco. Valore espresso
 in dl.
 */
@property (nonatomic, strong) NSNumber *qSafe;

/**
 * lingua display
 */
@property (nonatomic, assign) ODLDisplayLanguage language;

// NEW

/**
 * Lunghezza totale serpentina chiller
 */
@property (nonatomic, strong) NSNumber *lenChiller;

/**
 * Lunghezza totale linea pitone
 */
@property (nonatomic, strong) NSNumber *lenLine;

/**
 * Tipo di lavaggio da eseguire sull’impianto
 */
@property (nonatomic, assign) ODLCleanType typeClean;

/**
 * Pressione di lavaggio
 */
@property (nonatomic, strong) NSNumber *pressClean;

/**
 * Pressione di sanificazione
 */
@property (nonatomic, strong) NSNumber *pressSani;

#pragma mark -

- (instancetype) initWithData:(NSData*)data;

- (NSMutableData*) messageData;

@end
