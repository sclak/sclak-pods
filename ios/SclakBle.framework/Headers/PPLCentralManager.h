//
//  PPLCentralManager.h
//  PassePartoutLib
//
//  Created by danielepoggi on 10/12/13.
//  Copyright (c) 2014 Daniele Poggi - Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>
#elif TARGET_OS_MAC
#import <CoreBluetooth/CoreBluetooth.h>
#import <IOBluetooth/IOBluetooth.h>
#endif
#import "PPLPeripheralType.h"
#import "PPLManager.h"
#import "FXKeychain.h"
#import "PPLBluetoothCallbacks.h"
#import "PPLCentralManagerOptions.h"

#define CSTART                                  NSDate *startTime = [NSDate date]
#define CEND(...)                               NSLog(@"%@ - Time: %f", __VA_ARGS__, -[startTime timeIntervalSinceNow])

#define kSclakBleDomain                                             @"com.sclak.SclakBle"
#define kSecondsPeripheralIsLost                                    10.0f
#define kSecondsPeripheralIsLost_slow_announce                      20.0f

// NOTIFICATION CENTER
#define kCentralManagerDidUpdateState                               @"kCentralManagerDidUpdateState"
#define kCentralManagerDidDiscoverPeripheralNotification            @"kCentralManagerDidDiscoverPeripheralNotification"
#define kCentralManagerDidChangeProximityNotification               @"kCentralManagerDidChangeProximityNotification"
#define kCentralManagerDidLostPeripheralsNotification               @"kCentralManagerDidLostPeripheralsNotification"
#define kPeripheralConnectionFailedNotification                     @"kPeripheralConnectionFailedNotification"

//@protocol PPLCentralManagerProtocol;

/**
 *  Bluetooth Central Manager
 *
 *  common initialization: (usually in AppDelegate)
 *
 * PPLCentralManager *wrapper = [PPLCentralManager sharedInstance];
 * wrapper.monitorLostPeripherals = YES;
 * wrapper.deviceIdentifiers = @[PERIPHERAL_NAME_SCLAK];
 * wrapper.devicePreferences = @{
 *    kCentralModeAutoScanOption: @YES,                   // scanner always on if possible (bluetooth turned on)
 *    kCentralModeScannerFrequencyOption: @(0.1),         // scanner update frequency [0-10s] 0 = real-time
 *    kCentralModeSHA256ThresholdVersion: @3.0,
 *    kCentralModeStopScanWhenConnectOption: @(NO),       // should the scanner stop when connecting to a peripheral?
 * };
 *
 *
 */
@interface PPLCentralManager : PPLManager <CBCentralManagerDelegate>

//#pragma mark - Delegate

//@property (nonatomic, strong) id<PPLCentralManagerProtocol> delegate;

#pragma mark - Preferences

@property (nonatomic, readonly, assign) BOOL initialized;

@property (nonatomic, strong) NSString *centralModeIdentifier;
@property (nonatomic, strong) NSArray *deviceIdentifiers;
@property (nonatomic, readonly, strong) NSArray *allDeviceIdentifiers;
@property (nonatomic, strong) NSString *otauDeviceIdentifier;
@property (nonatomic, strong) NSDictionary *devicePreferences;
@property (nonatomic, strong) NSString *btcodeRoot;
@property (nonatomic, strong) NSNumber *scannerFrequency;

@property (nonatomic, assign) BOOL autoScan;
@property (nonatomic, assign) float sha256ThresholdVersion;
@property (nonatomic, assign) BOOL stopScanWhenConnect;

@property (nonatomic, readonly, assign) BOOL autoRestartScan;

#pragma mark - SHA2 Auth Configurations

@property (nonatomic, strong) NSMutableDictionary *commandRequiresEncryption;
/**
 * enables Anti Out Of Sync algorithm
 * default: DEFAULT_ANTI_OUT_OF_SYNC_ENABLED
 */
@property (nonatomic, assign) BOOL antiOutOfSync;

#pragma mark - CentralManager

@property (nonatomic, readonly, assign) CBManagerState state;
@property (nonatomic, readonly, assign) CBManagerState previousState;
@property (nonatomic, readonly, strong) CBCentralManager *centralManager;

#pragma mark - Central Options

// OPTIONS

@property (nonatomic, assign) BOOL monitorLostPeripherals;
@property (nonatomic, assign) BOOL connectUsingQueue;

/**
 * allows the scanner to detect Peripherals that has unacceptable names, for example:
 * - device type is not detected
 * - device version is not detected
 * - device btcode is not detected
 * default: NO
 */
@property (nonatomic, assign) BOOL showUnacceptableDevices;

#pragma mark - Bluetooth Scanner

@property (nonatomic, readonly, assign) BOOL scanning;
@property (nonatomic, readonly, assign) BOOL wasScanning;
@property (nonatomic, assign) BOOL wasScanningBeforeConnection;

/**
 * whitelist mode
 * if not setted (default) every peripheral will be discovered
 * if setted, only peripheral with a btcode contained in white list will be discovered
 */
@property (nonatomic, strong) NSArray *whitelistedBtcodes;

@property (nonatomic, readonly, strong) NSArray *discoveredPeripherals;
@property (nonatomic, readonly, strong) NSDictionary *discoveredBtcodesPeripherals;

@property (nonatomic, readonly, strong) NSArray *discoveredBtcodes;
@property (nonatomic, readonly, strong) NSArray *peripheralsRssi;

@property (nonatomic, strong) NSMutableDictionary *connectedPeripherals;
@property (nonatomic, strong) NSMutableArray *connectedBtcodes;

#pragma mark - General Properties

@property (nonatomic, strong) dispatch_queue_t managerScanQueue;
@property (nonatomic, assign) NSUInteger autoopenSclakTaskIdentifier;

#pragma mark - Centralized Callbacks

@property (nonatomic, strong) BluetoothPeripheralResponseCallback centralizedPeripheralAuthenticatedCallback;
@property (nonatomic, strong) BluetoothPeripheralResponseCallback centralizedPeripheralGetInOutCallback;
@property (nonatomic, strong) BluetoothPeripheralUsageCallback centralizedPeripheralActionCallback;
@property (nonatomic, strong) BluetoothPeripheralResponseCallback centralizedPeripheralDisconnectCallback;

/** @name Singleton */

#pragma mark - Singleton

/**
 * Singleton getter of SCLAK bluetooth central manager
 */
+ (instancetype) sharedInstance;

/** @name Init methods */

/**
 *  initialize an instance of Central Manager. Used only if peripheral mode is required
 *
 *  @param queue   a dispatch_queue created for the central manager to work
 *  @param mode         central or peripheral mode
 *  @param options options
 *
 *  @return instance of central manager
 */
- (id) initWithQueue:(dispatch_queue_t)queue peripheralMode:(BOOL)mode options:(NSDictionary*)options;
- (void) checkState;
- (NSString*) centralManagerStateName;

/**
 * check if bluetooth permission has been granted
 */
+ (BOOL) isBluetoothPermissionGranted;

/**
 * main method that creates the CBCentralManager queue. PPLCentralManager devicePreferences must be setted before
 */
- (void) initialize;

/**
 * reverse the initialize method by destroying the PPLCentralManager
 */
- (void) destroyCentralManager;

/**
 * remove discovered peripherals, leaving cached peripherals intact
 */
- (void) removeDiscoveredPeripherals;

/**
 * remove discovered peripheral with specific btcode, leaving cached peripherals intact
 */
- (void) removeDiscoveredPeripheral:(NSString*)btcode;

/**
 * removes all discovered peripherals from in-memory and cache
 */
- (void) removeAllPeripherals;

/**
 * removes all cached discovered peripherals, leaving in-memory discovered peripherals unaltered
 */
- (void) removeCachedPeripherals;

/**
 * disconnects all connected peripherals
 */
- (void) disconnectAllPeripherals;

/**
 * start BLE scanner
 */
- (void) startScan;

/**
 *  start BLE scanner providing specific services to be discovered
 *
 *  @param services 
 */
- (void) startScan:(NSArray*)services;

/**
 * stop BLE scanner
 */
- (void) stopScan;

/**
 * stop BLE scanner and clear discovered peripherals
 */
- (void) stopScan:(BOOL)clearDiscoveredPeripherals;

#pragma mark - Reset

/**
 * resets the internal states
 * - remove any cached BLE identifiers
 * - remove any blacklisted auto connect peripherals
 */
- (void) reset;

#pragma mark - PPLDiscoveredPeripheral

/**
 * entry point in the lib to get a peripheral
 * requirements: btcode: is the unique peripheral identifier, example is: 4CE2F10A0001
 @return the discovered peripheral for btcode or nil
 */
- (PPLDiscoveredPeripheral*) getOrRestorePeripheralWithBtcode:(NSString*)btcode;

/**
 * get discovered or cached peripheral with identifier UUID
 @return the peripheral or nil
 */
- (PPLDiscoveredPeripheral*) peripheralWithIdentifier: (NSUUID *) theIdentifier;

/**
 * get discovered or cached peripheral with CBPeripheral instance, wich is contained in PPLDiscoveredPeripheral
 @return the peripheral or nil
 */
- (PPLDiscoveredPeripheral*) discoveredPeripheralWithCBPeripheral: (CBPeripheral*)cbPeripheral;

#pragma mark - Peripheral Identifier Management

/**
 *  lazy loading and getter of peripheral identifiers
 *
 *  @return peripheral identifiers (uuids)
 */
- (NSMutableDictionary*) peripheralIdentifiers;

/**
 *  retrieve UUID identifier for peripheral
 *
 *  @param peripheral
 *
 *  @return peripheral identifier
 */
- (NSUUID*) identifierForPeripheral:(CBPeripheral*)peripheral;

/**
 *  add dictionary of btcode -> peripheral identifier to peripheral identifiers
 *
 *  @param identifiers
 *
 *  @return success / failure
 */
- (BOOL) addPeripheralsIdentifierToCache:(NSDictionary*)identifiers;

/**
 *  remove identifier from peripheral cache
 *
 *  @param btcode
 *
 *  @return success / failure
 */
- (BOOL) removeCachedPeripheralWithBtcode:(NSString*)btcode;

#pragma mark - BLE Scanner Whitelist

/**
* add peripheral (btcode) in whitelist
*
* @param btcode
*
* @return true if peripheral has been added, false otherwise
*
*/
- (BOOL) addWhitelistedPeripheral:(NSString*)btcode;

/**
 * remove peripheral (btcode) from whitelist
 *
 * @param btcode
 *
 * @return true if peripheral has been removed, false otherwise
 *
 */
- (BOOL) removeWhitelistedPeripheral:(NSString*)btcode;

/**
 * retrieve all discovered peripherals in BOOT mode (peripheralInBoot = YES)
 *  @param type filter by peripheral type
 */
- (NSArray*) peripheralsInBootMode:(PeripheralType)type;

#pragma mark - Elliptic Curve Cryptography APIs

/**
 * set Server Public Key (SPK) 64 bytes in uncompressed format ( R + S ) Base 16 (hex string)
 */
- (void) setSPK:(NSString*)SPK;

/**
 * get Server Public Key (SPK) 64 bytes Base 16 (hex string)
 */
- (NSString*) getSPK;

/**
 * generated ECK pair with prime 256r1 algorithm, {key: private, cert: public}
 */
- (NSDictionary*) generateECK;

/**
 * retrieve ECK private key
 */
- (NSString*) getECK;

/**
 * set ECK private key, for test purposes only
 */
- (void) setECK:(NSString*)ECK;

/**
 * set Elliptic Curve User Public Key (ECP)
 */
- (void) setECP:(NSString*)ECP;

#pragma mark - onBlocks - generic

@property (nonatomic, copy) void (^onDidUpdateState)(PPLCentralManager *manager, CBManagerState state, CBManagerState previousState);
@property (nonatomic, copy) void (^onDidDiscoverPeripheral)(PPLDiscoveredPeripheral *peripheral, BOOL isNewPeripheral);
@property (nonatomic, copy) void (^onDidChangeProximity)(NSArray *discoveredPeripherals);
@property (nonatomic, copy) void (^onDidLostPeripherals)(NSArray *btcodes);
@property (nonatomic, copy) void (^onDidChangePeripheralName)(PPLDiscoveredPeripheral *peripheral);
@property (nonatomic, copy) void (^onDidRetrievePeripheral)(PPLDiscoveredPeripheral *peripheral);
@property (nonatomic, copy) void (^onDidConnectPeripheral)(PPLDiscoveredPeripheral *peripheral);
@property (nonatomic, copy) void (^onDidFailToConnectPeripheral) (PPLDiscoveredPeripheral *peripheral, NSError *error);
@property (nonatomic, copy) void (^onDidDisconnectPeripheral) (PPLDiscoveredPeripheral *peripheral, NSError *error);
@property (nonatomic, copy) void (^onDidDiscoverServices)(PPLDiscoveredPeripheral *peripheral, NSError *error);
@property (nonatomic, copy) void (^onDidDiscoverCharacteristicsForService) (PPLDiscoveredPeripheral *peripheral, NSError *error); // ready to authenticate

#pragma mark - onBlock - results

@property (nonatomic, copy) void (^onAuthenticateResult) (PPLDiscoveredPeripheral *peripheral, NSDate *date, NSError *error, const PPLAuthenticateResult theAuthenticatedResult);
@property (nonatomic, copy) void (^onDeviceRSSIUpdated) (PPLDiscoveredPeripheral *peripheral, NSNumber *rssi);
@property (nonatomic, copy) void (^onSendSecureDataResult) (PPLDiscoveredPeripheral *peripheral, NSString *key);

#pragma mark - Utils

+ (NSString*) majorWithInt:(NSUInteger)major;
+ (NSString*) majorWithString:(NSString*)btcode;
+ (NSString*) minorWithString:(NSString*)btcode;
+ (uint16_t) majorValueWithString:(NSString*)major;
+ (uint16_t) minorValueWithString:(NSString*)major;
+ (NSString*) halfBtcodeWithMajor:(uint16_t)major minor:(uint16_t)minor;

#pragma mark - Debug

- (void) addDiscoveredPeripheral:(PPLDiscoveredPeripheral*)peripheral;

@end
