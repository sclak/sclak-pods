//
//  SCKBatteryFeature.h
//  SclakBle
//
//  Created by Daniele Poggi on 23/04/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCKBatteryFeature : NSObject

/**
 * Capacità della batteria espressa in mAh, formato MSByte First
 */
@property (nonatomic, assign) NSUInteger capacity;

/**
 *
 Limite inferiore tensione della batteria in mV per rilevare la condizione di scarica, formato MSByte First. Tale valore viene verificato durante il ciclo di movimento caratterizzato dall’assorbimento massimo
 */
@property (nonatomic, assign) NSUInteger lowVoltage;

#pragma mark -

+ (instancetype) defaultFeature;

- (instancetype) initWithData:(NSData*)data;

- (NSMutableData*) messageData;

@end
