//
//  SCKFobButton.h
//  SclakBle
//
//  Created by Daniele Poggi on 25/03/16.
//  Copyright © 2016 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCKFobButton : NSObject

/**
 * Flag validità configurazione tasto (1 configurazione valida, 0 configurazione non valida)
 */
@property (nonatomic, assign) BOOL valid;

/**
 *  Identificativo tasto
 */
@property (nonatomic, assign) NSUInteger buttonId;

/**
 *  Impostazione Tasto per funzionamento con Gruppi (1) o con sclak singolo (0).
 */
@property (nonatomic, assign) BOOL groupMode;

/**
 *  Codice PIN inviato nel comando del dispositivo. Il campo è su tre byte il formato del PIN con lunghezza variabile da 4 a 6 cifre è definito nel
 documento di specifica Sclak.
 */
@property (nonatomic, strong) NSString *pin;

/**
 *  campo variabile a seconda di quento definito dal Bit7 del campo ID_KEY: indirizzo Bluetooth del dispositivo da controllare espresso su
 * 6 byte, oppure codice del Gruppo di dispositivi espresso su 1 byte.
 */
@property (nonatomic, strong) NSString *btcode;

/**
 *  campo variabile a seconda di quento definito dal Bit7 del campo ID_KEY: indirizzo Bluetooth del dispositivo da controllare espresso su
 6 byte, oppure codice del Gruppo di dispositivi espresso su 1 byte.
 */
@property (nonatomic, assign) NSUInteger groupId;

#pragma mark -

- (instancetype) initWithData:(NSData*)data;
- (NSMutableData*) messageData;

- (BOOL) isSetted;

@end
