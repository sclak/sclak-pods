//
//  PPLBluetoothCallbacks.h
//  SclakBle
//
//  Created by Daniele Poggi on 26/01/2014.
//  Copyright © 2014 sclak. All rights reserved.
//

@class PPLDiscoveredPeripheral;
@class SCKPeripheralUsage;

// CALLBACKS
typedef void(^BluetoothResponseCallback)(BOOL success, NSException *ex);
typedef void(^BluetoothResponseErrorCallback)(BOOL success, NSError *error);
typedef void(^BluetoothResponseDataCallback)(BOOL success, NSData *data, NSError *error);
typedef void(^BluetoothReadRSSICallback)(BOOL success, NSInteger RSSI);
typedef void(^BluetoothPeripheralResponseCallback)(PPLDiscoveredPeripheral *discoveredPeripheral, BOOL success);
typedef void(^BluetoothPeripheralUsageCallback)(SCKPeripheralUsage *usage, BOOL success);
typedef BOOL(^BluetoothRequestAutoConnectCallback)(PPLDiscoveredPeripheral *discoveredPeripheral);
