//
//  BWGUserConfiguration.h
//  SclakBle
//
//  Created by Daniele Poggi on 11/01/2020.
//  Copyright © 2020 sclak. All rights reserved.
//

#import "PPLGenericUserConfiguration.h"

typedef NS_ENUM(NSInteger, BWGProgramMode) {
    BWGProgramModePeripheral,   // default mode: device works standalone
    BWGProgramModeCentral,      // central mode: device commands another device
};

typedef NS_ENUM(NSInteger, BWGConnectionMode) {
    BWGConnectionModeGSMOnly,   // only GSM Connection
    BWGConnectionModeWifiOnly,  // only Wifi Connection
};

@interface BWGUserConfiguration : PPLGenericUserConfiguration

/**
 * current program mode. read the property by calling readProgramDeviceCallback: while authenticated
 */
@property (nonatomic, assign) BWGProgramMode programMode;

/**
 * Modalità di funzionamento della connessione dello Sclak Bridge
 */
@property (nonatomic, assign) BWGConnectionMode connectionMode;

/**
 * GSM APN Configuration
 */
@property (nonatomic, strong) NSString *APN;

/**
* GSM Username
*/
@property (nonatomic, strong) NSString *USER_GSM;

/**
* GSM Password
*/
@property (nonatomic, strong) NSString *PWD_GSM;

/**
* Access Point Wifi
*/
@property (nonatomic, strong) NSString *AP_WIFI;

/**
* Password Wifi
*/
@property (nonatomic, strong) NSString *PWD_WIFI;

/**
 * MQTT server URL
 */
@property (nonatomic, strong) NSString *URL_MQTT;

/**
 * MQTT server PORT
 */
@property (nonatomic, assign) NSUInteger PORT_MQTT;

/**
 * MQTT username
 */
@property (nonatomic, strong) NSString *USER_MQTT;

/**
 * MQTT password
 */
@property (nonatomic, strong) NSString *PWD_MQTT;

/**
 * MQTT Ping time in minutes
 */
@property (nonatomic, assign) NSUInteger PING_MQTT;

/**
 * GSM operator
 */
@property (nonatomic, strong) NSString *GSM_OPERATOR;

/**
 * GSM CCID
 */
@property (nonatomic, strong) NSString *GSM_CCID;

- (void) setData:(NSData*)data;
- (void) setParamsData:(NSData*)data;

- (NSData*) paramsData;

@end
