//
//  SHA256Peripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 13/02/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import "PPLDiscoveredPeripheral.h"

@interface PPLDiscoveredPeripheral (SHA256Extensions)

- (void) initKeyGenerationCallback:(KeyGenerationCallback)callback;
- (void) initRemoteGenerationCallback:(RemoteGenerationCallback)callback;

@end
