//
//  SCKKeyboardUserConfiguration.h
//  SclakBle
//
//  Created by Daniele Poggi on 21/04/2017.
//  Copyright © 2017 sclak. All rights reserved.
//

#import "PPLGenericUserConfiguration.h"

@interface SCKKeypadUserConfiguration : PPLGenericUserConfiguration

@end
