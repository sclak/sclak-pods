//
//  DitecPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 26/04/16.
//  Copyright © 2016 sclak. All rights reserved.
//

#import "SclakPeripheral.h"

// sclak specific commands
extern const uint8_t kPhoneToDevice_Entrematic_SEND_COMM;
extern const uint8_t kDeviceToPhone_Entrematic_SEND_RESP;

@interface EntrematicPeripheral : SclakPeripheral

- (void) sendTelegram:(NSData*)data callback:(BluetoothResponseErrorCallback)callback;
- (void) setRespSendCallback:(BluetoothResponseDataCallback)callback;

@end
