//
//  PPLGenericUserConfiguration.h
//  SclakBle
//
//  Created by Daniele Poggi on 23/10/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PeripheralUserConfigurationProtocol.h"

typedef NS_ENUM(NSUInteger, SclakDriverType) {
    DriverTypeEngine,
    DriverTypeSolenoid,
    DriverTypeRelè,
};

@interface PPLGenericUserConfiguration : NSObject <PeripheralUserConfigurationProtocol>

#pragma mark - Common Properties

/**
 *  Abilitazione segnalazione Buzzer su attivazione uscita (0 segnalazione disattiva, 1 segnalazione attiva).
 */
@property (nonatomic, assign) BOOL buzzerOn;

/**
 *  Abilitazione segnalazione LED su attivazione uscita (0 segnalazione disattiva, 1 segnalazione attiva).
 */
@property (nonatomic, assign) BOOL ledOn;

/**
 *  Flag Sclak Feedback Pin Invalido (0 segnalazione disattiva, 1 attiva).
 */
@property (nonatomic, assign) BOOL wrongPinFeedbackOn;

/**
 *  Livello di luminosità richiesto per i led espresso in percentuale (0 led spenti, 100 led accesi al massimo livello).
 */
@property (nonatomic, assign) NSUInteger ledPowerLevel;

/**
 *  Periodo di attivazione uscita in unità di 100ms, permette di impostare la disattivazione automatica dell’uscita dopo un certo periodo, se è a 0 il comando è di tipo bistabile (la disattivazione deve essere effettuata dallo Smartphone). L’utilizzo di questo campo dipende dal valore del campo FLAG nel comando di attivazione uscita.
 */
@property (nonatomic, assign) NSUInteger dtOnOut;

/**
 * Flag periferica installata (0 Non Installato, 1 Installato).
 */
@property (nonatomic, assign) BOOL installedOn;

#pragma mark - Sclak Padlock Properties

/**
 *  Abilitazione segnalazione LED durante standby. 0 segnalazione disattiva, 1 segnalazione attiva.
 */
@property (nonatomic, assign) BOOL ledStandbyOn;

/**
 *  Abilitazione segnalazione LED lucchetto sbloccato. 0 segnalazione disattiva, 1 segnalazione attiva.
 */
@property (nonatomic, assign) BOOL ledUnlockedOn;

/**
 *  Timeout attesa apertura lucchetto dopo lo sblocco in unità di 100ms. Se il lucchetto non viene aperto entro questo tempo torna autonomamente nello stato bloccato.
 */
@property (nonatomic, assign) NSUInteger timeoutOpen;

#pragma mark - Sclak Driver Properties

/**
 * Tipologia di pilotaggio Driver:
 * 0x00 Tipo A, Motore pilotaggio bidirezionale
 * 0x01 Tipo B, Solenoide pilotaggio bidirezionale
 * 0x02 Tipo C, Relè pilotaggio monodirezionale
 */
@property (nonatomic, assign) SclakDriverType driverType;

/**
 * Periodo di attivazione della uscita driver, in unità di 10ms, nel caso di impostazione uscita bidirezionale, in tale caso l’uscita viene attivata per il tempo DT_DRIVER con polarità positiva, poi dopo DT_ON_OUT viene attivata per DT_DRIVER con polarità negativa.
 */
@property (nonatomic, assign) NSUInteger dtDriver;

#pragma mark - Sclak Gear Properties

/**
 * Flag Inversione pulsanti di comando apertura/chiusura (0 Pulsanti normali, 1 Pulsanti invertiti)
 */
@property (nonatomic, assign) BOOL invertRotation;

#pragma mark - common properties with Gear A/R, UFOe, Givik

/**
 *  Abilitazione segnalazione Buzzer su comando di apertura (sblocco serratura, apertura porta). 0 segnalazione disattiva, 1 segnalazione attiva.
 */
@property (nonatomic, assign) BOOL buzzerOnEnabled;

/**
 *  Abilitazione segnalazione Buzzer su comando di chiusura (blocco serratura, porta chiusa). 0 segnalazione disattiva, 1 segnalazione attiva.
 */
@property (nonatomic, assign) BOOL buzzerOnDisabled;

#pragma mark - Sclak Gear A Properties

/**
 *  Abilitazione segnalazione Buzzer Batterie scariche (segnalazione inviata a seguito di comando di apertura o chiusura al posto della normale segnalazione). 0 segnalazione disattiva, 1 segnalazione attiva.

 */
@property (nonatomic, assign) BOOL lowBatteryFeedbackOn;

/**
 *  Flag Attivazione modalità semi-automatica (0 Modalità semiautomatica disattiva, 1 Modalità semiautomatica attiva).
 */
@property (nonatomic, assign) BOOL semiAutomaticMode;

#pragma mark - Sclak Gear R Properties

/**
 * Flag Abilitazione pulsanti di comando apertura/chiusura (0 Pulsanti disattivi, 1 Pulsanti attivi)
 */
@property (nonatomic, assign) BOOL commandButtonsEnabled;

/**
 * Flag Inversione pulsanti di comando apertura/chiusura (0 Pulsanti normali, 1 Pulsanti invertiti)
 */
@property (nonatomic, assign) BOOL commandButtonsInverted;

#pragma mark - Sclak Handle Properties

/**
 * Abilitazione segnalazione Buzzer su attivazione uscita (sblocco maniglia, utilizzo consentito).
 * 0 segnalazione disattiva, 1 segnalazione attiva.
 */
@property (nonatomic, assign) BOOL buzzOnHandleEnabled;

/**
 * Abilitazione segnalazione Buzzer su disattivazione uscita (blocco maniglia, utilizzo non consentito).
 * 0 segnalazione disattiva, 1 segnalazione attiva
 */
@property (nonatomic, assign) BOOL buzzOnHandleDisabled;

/**
 *  Abilitazione segnalazione Buzzer tessera TAG non valida (SK accesso Mifare errata o formattazione PIN errata). 0 segnalazione disattiva, 1 segnalazione attiva.
 */
@property (nonatomic, assign) BOOL wrongTagFeedbackOn;

#pragma mark - Sclak Keypad Properties

/**
 * Flag abilitazione modalità ripetizione tasto (0, modalità non attiva, 1 modalità attiva). Se attivo effettua la ritrasmissione del comando quando il tasto rimane premuto, in caso di rilascio del tasto di disattiva la connessione dopo il tempo definito in DT_DISCONN.
 */
@property (nonatomic, assign) BOOL repeatCommandOn;

/**
 * Tempo di disconnessione dopo invio comando, espresso in decimi di secondo.
 */
@property (nonatomic, assign) NSUInteger dtDisconnect;

#pragma mark - Sclak Lock (DiLo) Properties

/**
 * Abilitazione segnalazione Buzzer su attivazione uscita (sblocco serratura, utilizzo consentito). 0 segnalazione disattiva, 1 segnalazione attiva.
 */
@property (nonatomic, assign) BOOL buzzerOnLockEnabled;

/**
 * Abilitazione segnalazione Buzzer su disattivazione uscita
 (blocco serratura, utilizzo non consentito). 0 segnalazione disattiva, 1 segnalazione attiva.
 */
@property (nonatomic, assign) BOOL buzzerOnLockDisabled;

/**
 *  Flag chiusura con consenso, (0, chiusura automatica, 1 chiusura con consenso). Se attivato permette la chiusura della serratura (blocco manopola) solo dopo la ricezione del comando di chiusura.
 */
@property (nonatomic, assign) BOOL closeWithConsentOn;

#pragma mark - Sclak Tag Reader and Sclak Presence common Properties

/**
 *  Abilitazione segnalazione Buzzer tessera TAG Valida. 0 segnalazione disattiva, 1 segnalazione attiva
 */
@property (nonatomic, assign) BOOL correctTagFeedbackOn;

/**
 *  Abilitazione segnalazione Buzzer tessera TAG non valida (SK accesso Mifare errata o formattazione PIN errata). 0 segnalazione disattiva, 1 segnalazione attiva.
 */
@property (nonatomic, assign) BOOL incorrectTagFeedbackOn;

#pragma mark - Sclak Tag Reader (SclakTagUserConfiguration) Properties

/**
 * Flag funzionamento a batteria (0 Alimentazione da rete, 1 alimentazione da batteria). Modifica le tempistiche di attivazione in modo da ottimizzare i consumi in base allo scenario selezionato.
 */
@property (nonatomic, assign) BOOL batteryModeOn;

/**
 * Abilitazione segnalazione Buzzer codice PIN non valido (codice errato o temporalmente non valido). 0 segnalazione disattiva, 1 segnalazione attiva.
 */
@property (nonatomic, assign) BOOL incorrectPinFeedbackOn;

#pragma mark - Sclak Presence Properties

#pragma mark - Sclak UFOe Properties

#pragma mark - Givik Properties

/**
 * Abilitazione segnalazione Buzzer errore movimento. 0 segnalazione disattiva, 1 segnalazione attiva.
 */
@property (nonatomic, assign) BOOL buzzerOnMovementError;

- (void) setStringDriverTypeWithType:(NSString*)type;
- (NSString*) getStringDriverType;

@end
