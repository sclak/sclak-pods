//
//  SHA256Auth.h
//  PassePartoutLib
//
//  Created by Daniele Poggi on 12/12/14.
//  Copyright (c) 2014 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPLDiscoveredPeripheral.h"
#import "SclakAuth.h"

// auth commands
extern const uint8_t kPhoneToDevice_INIT_AUTH_SHA256;
extern const uint8_t kPhoneToDevice_INIT_AUTH_SHA256_EXT;
extern const uint8_t kPhoneToDevice_SEND_MAC_SHA256;
extern const uint8_t kPhoneToDevice_REQ_INIT_SK_SHA256;
extern const uint8_t kDeviceToPhone_SEND_SEED_SHA256;
extern const uint8_t kDeviceToPhone_SEND_ACK_SHA256;
extern const uint8_t kDeviceToPhone_SEND_INIT_SK_SHA256;

@interface SHA256Auth : SclakAuth

#pragma mark properties

@property (nonatomic, assign) NSUInteger secretPage;

#pragma mark methods

/**
 * initialize secret for Production firmwares
 */
- (void) initRemoteGenerationCallback:(RemoteGenerationCallback)callback;

/**
 * initialize secret for Developer firmwares
 */
- (void) initKeyGenerationCallback:(KeyGenerationCallback)callback;

/**
 * send SEED response after authentication
 */
- (void) sendSeedResponse;

/**
 * encrypt a message with a new OTP and return the encrypted bytes
 */
- (NSData*) encryptData:(NSData*)data;

/**
 * decrypt a message with a new OTP and return the decrypted bytes
 */
- (NSData*) decryptData:(NSData*)aData;

- (void) testIC;
- (void) test0;
- (void) test1;
- (void) test2;
- (void) test3;
- (void) test4;
- (void) test5;

@end
