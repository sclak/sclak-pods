//
//  ODLPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 22/07/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import "PPLDiscoveredPeripheral.h"
#import "ODLGeneralStatus.h"
#import "ODLAdapterStatus.h"
#import "ODLCompensatorStatus.h"
#import "ODLGraftStatus.h"
#import "ODLCountRevisionsStatus.h"
#import "ODLPlantCommand.h"
#import "ODLOperativeCommand.h"
#import "ODLPlantSetup.h"
#import "ODLPlantCalendar.h"
#import "ODLBeer.h"
#import "ODLWashOperationStep.h"

// ODL services
#define kODLServiceCBUUID                 [CBUUID UUIDWithString: @"5C8A2084-6C8E-9714-BD44-63AE525B34D0"]

// ODL characteristics
#define kODLCharacteristicTXCBUUID        [CBUUID UUIDWithString: @"5C8A2084-6C8E-9714-BD44-63AE525B34D1"]
#define kODLCharacteristicRXCBUUID        [CBUUID UUIDWithString: @"5C8A2084-6C8E-9714-BD44-63AE525B34D2"]

// ODL specific commands
extern const uint8_t kPhoneToDevice_SYSTEM_GET_STS;
extern const uint8_t kPhoneToDevice_SYSTEM_STS;
extern const uint8_t kDeviceToPhone_RESP_SYSTEM_STS;
extern const uint8_t kPhoneToDevice_ADAPTER_STS;
extern const uint8_t kDeviceToPhone_RESP_ADAPTER_STS;
extern const uint8_t kPhoneToDevice_COMP_STS;
extern const uint8_t kDeviceToPhone_RESP_COMP_STS;
extern const uint8_t kPhoneToDevice_GRAFT_STS;
extern const uint8_t kDeviceToPhone_RESP_GRAFT_STS;
extern const uint8_t kPhoneToDevice_COUNT_STS;
extern const uint8_t kDeviceToPhone_RESP_COUNT_STS;
extern const uint8_t kPhoneToDevice_SEND_COM;
extern const uint8_t kDeviceToPhone_RESP_SEND_COM;
extern const uint8_t kPhoneToDevice_SEND_SET;
extern const uint8_t kDeviceToPhone_RESP_SEND_SET;
extern const uint8_t kPhoneToDevice_READ_SET;
extern const uint8_t kDeviceToPhone_RESP_READ_SET;
extern const uint8_t kPhoneToDevice_SEND_HOUR;
extern const uint8_t kDeviceToPhone_RESP_SEND_HOUR;
extern const uint8_t kPhoneToDevice_READ_HOUR;
extern const uint8_t kDeviceToPhone_RESP_READ_HOUR;
extern const uint8_t kPhoneToDevice_SEND_OP;
extern const uint8_t kDeviceToPhone_RESP_SEND_OP;
extern const uint8_t kPhoneToDevice_READ_IDEN;
extern const uint8_t kDeviceToPhone_RESP_READ_IDEN;
extern const uint8_t kPhoneToDevice_SEND_SEL;
extern const uint8_t kDeviceToPhone_RESP_SEND_SEL;
extern const uint8_t kPhoneToDevice_READ_BEER;
extern const uint8_t kDeviceToPhone_RESP_READ_BEER;
extern const uint8_t kPhoneToDevice_SEND_IDEA_COMM;
extern const uint8_t kDeviceToPhone_RESP_SEND_IDEA_COMM;

typedef NS_ENUM(NSUInteger, ODLSelectBeerError) {
    ODLSelectBeerErrorNone,
    ODLSelectBeerErrorBadFormat,
    ODLSelectBeerErrorChangeKegRequired
};

typedef NS_ENUM(NSUInteger, ODLStatusType) {
    ODLStatusTypeGeneral,
    ODLStatusTypeAdapter,
    ODLStatusTypeCompensator,
    ODLStatusTypeGraft,
    ODLStatusTypeCountRevisions
};

typedef void(^ODLStepCallback)(BOOL success, NSError *error, ODLWashOperationStep *step);
typedef void(^GetBeerCallback)(BOOL success, NSError *error, ODLBeer *beer);
typedef void(^GetBeersCallback)(BOOL success, NSError *error, NSArray *beers);

@interface ODLPeripheral : PPLDiscoveredPeripheral

@property (nonatomic, strong) ODLGeneralStatus *generalStatus;
@property (nonatomic, strong) BluetoothResponseErrorCallback generalStatusCallback;

@property (nonatomic, strong) ODLAdapterStatus *adapterStatus;
@property (nonatomic, strong) BluetoothResponseErrorCallback adapterStatusCallback;

@property (nonatomic, strong) ODLCompensatorStatus *compensatorStatus;
@property (nonatomic, strong) BluetoothResponseErrorCallback compensatorStatusCallback;

@property (nonatomic, strong) ODLGraftStatus *graftStatus;
@property (nonatomic, strong) BluetoothResponseErrorCallback graftStatusCallback;

@property (nonatomic, strong) NSNumber *selectedBeerIndex;
@property (nonatomic, strong) NSNumber *numBeers;

@property (nonatomic, strong) ODLWashOperationStep *currentOperationStep;
@property (nonatomic, strong) ODLStepCallback currentOperationStepCallback;

/**
 * plant setup, operative conditions and limits
 * setted with sendPlantSetup, read with getPlantSetup
 */
@property (nonatomic, strong) ODLPlantSetup *setup;
@property (nonatomic, strong) BluetoothResponseErrorCallback setupCallback;

/**
 * plant calendar, operative weekday time slots
 * setted with sendPlantCalendar, read with getPlantCalendar
 */
@property (nonatomic, strong) ODLPlantCalendar *calendar;
@property (nonatomic, strong) BluetoothResponseErrorCallback calendarCallback;

/**
 * contatore revisioni
 */
@property (nonatomic, strong) ODLCountRevisionsStatus *countRevisions;
@property (nonatomic, strong) BluetoothResponseErrorCallback countRevisionsCallback;

/**
 * modello della birra letto
 */
@property (nonatomic, strong) ODLBeer *currentBeer;
@property (nonatomic, strong) NSMutableArray *beers;

#pragma mark - Status Messages

- (ODLPlantCommand*) currentPlantStatus;

/*
 Questi messaggi permettono la lettura dello stato del sistema i-dea da parte del dispositivo di controllo. Oltre a poter essere letti su richiesta, tutti i messaggi di stato sono inviati in maniera autonoma al variare delle condizioni del sistema, l’invio non è comunque più frequente di una volta al secondo
 */

- (void) getStatuses:(NSArray*)types;

/**
 * Stato Generale Sistema (General Status)
 * Questi messaggi permettono la lettura e l’invio dello stato generale del sistema i- dea
 */
- (void) getGeneralStatusCallback:(BluetoothResponseErrorCallback)callback;

/**
 * Stato del Riduttore (Adapter Status)
 */
- (void) getAdapterStatusCallback:(BluetoothResponseErrorCallback)callback;

/**
 * Stato del Compensatore (Compensator Status)
 */
- (void) getCompensatorStatusCallback:(BluetoothResponseErrorCallback)callback;

/**
 * Stato dell'innesto (Graft Status)
 */
- (void) getGraftStatusCallback:(BluetoothResponseErrorCallback)callback;

/**
 * Questi messaggi permettono di leggere i parametri impianto attualmente impostati sul sistema.
 */
- (void) getPlantSetupCallback:(BluetoothResponseErrorCallback)callback;

#pragma mark - Commands

- (void) sendPlantCommand:(ODLPlantCommand*)command callback:(BluetoothResponseErrorCallback)callback;
- (void) sendPlantSetup:(ODLPlantSetup*)setup callback:(BluetoothResponseErrorCallback)callback;
- (void) sendOperativeCommand:(ODLOperativeCommand*)command callback:(BluetoothResponseErrorCallback)callback;

#pragma mark - Orari Spillatura

- (void) getPlantCalendarCallback:(BluetoothResponseErrorCallback)callback;
- (void) sendPlantCalendar:(ODLPlantCalendar*)calendar callback:(BluetoothResponseErrorCallback)callback;

#pragma mark - Lettura / Scrittura tipologie Birra

- (void) getAllBeersCallback:(GetBeersCallback)callback;
- (void) getNumBeersCallback:(BluetoothResponseErrorCallback)callback;
- (void) getBeerWithId:(NSNumber*)beerId callback:(GetBeerCallback)callback;
- (void) sendSelectedBeer:(NSUInteger)selectedIndex callback:(BluetoothResponseErrorCallback)callback;

- (void) deleteBeerWithId:(NSNumber*)beerId callback:(BluetoothResponseErrorCallback)callback;
- (void) beginDownloadBeerFile:(NSString*)filename callback:(BluetoothResponseErrorCallback)callback;
- (void) downloadBeerFile:(NSData*)file callback:(BluetoothResponseErrorCallback)callback;
- (void) endDownloadBeerFileCallback:(BluetoothResponseErrorCallback)callback;

#pragma mark - Contatori revisioni

- (void) getCounterRevisionsCallback:(BluetoothResponseErrorCallback)callback;

#pragma mark - Sincronizzazione operazioni di Lavaggio e Sanificazione

- (void) getCurrentOperationStepCallback:(ODLStepCallback)callback;
- (void) nextOperationStepCallback:(ODLStepCallback)callback;

#pragma mark - Error Management

- (BOOL) errorsChanged;


@end
