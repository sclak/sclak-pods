//
//  ODLWashOperationStep.h
//  SclakBle
//
//  Created by Daniele Poggi on 08/01/16.
//  Copyright © 2016 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ODLWashOperationType) {
    ODLWashOperationTypeNone,
    ODLWashOperationTypeKeg,
    ODLWashOperationTypeWaterSystem,
    ODLWashOperationTypeSanification
};

@interface ODLWashOperationStep : NSObject

/**
 * tipologia di operazione di sanificazione o lavaggio in corso
 */
@property (nonatomic, assign) ODLWashOperationType typeOp;

/**
 * Step attuale dell'operazione in corso
 */
@property (nonatomic, strong) NSNumber *stepOp;

/**
 * Numero di step totali dell'operazione in corso
 */
@property (nonatomic, strong) NSNumber *numStep;

/**
 * Presenza tasto di avanzamento step
 */
@property (nonatomic, assign) BOOL nextStepAvailable;

- (instancetype) initWithData:(NSData*)data;

- (BOOL) isOpNone;
- (BOOL) isWashing;
- (BOOL) isSanitizing;
- (BOOL) isFinished;

- (NSString*) typeOpString;

@end
