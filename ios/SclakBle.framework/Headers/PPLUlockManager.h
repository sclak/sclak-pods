//
//  PPLUlockManager.h
//  PassePartoutLib
//
//  Created by Daniele Poggi on 31/07/14.
//  Copyright (c) 2014 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPLPeripheralManager.h"

// notification center
#define kUlockStatusChangedNotification @"kUlockStatusChangedNotification"

@interface PPLUlockManager : PPLPeripheralManager

#pragma mark - status

@property (nonatomic, assign) BOOL buzzerOn;
@property (nonatomic, assign) BOOL identifyOn;
@property (nonatomic, assign) BOOL autoOpenOn;
@property (nonatomic, assign) BOOL autoCloseOn;
@property (nonatomic, assign) NSInteger shakePower;

@end
