//
//  SclakPinWeekdayTimeSlot.h
//  SclakBle
//
//  Created by Daniele Poggi on 12/11/2016.
//  Copyright © 2016 sclak. All rights reserved.
//

#import "SclakPinTimeSlot.h"

#define kWeekdayKey                         @"weekday"

typedef NS_ENUM(NSUInteger, PinWeekday) {
    PinWeekdayMonday,
    PinWeekdayTuesday,
    PinWeekdayWednesday,
    PinWeekdayThursday,
    PinWeekdayFriday,
    PinWeekdaySaturday,
    PinWeekdaySunday
};

@interface SclakPinWeekdayTimeSlot : SclakPinTimeSlot

@property (nonatomic, assign) PinWeekday weekday;

- (instancetype) initWithTimeBegin:(NSDate*)initial end:(NSDate*)end weekday:(PinWeekday)weekday allDay:(BOOL)allDay disabled:(BOOL)disabled;

@end
