//
//  SclakPadlockPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 29/11/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "SclakPadlockUserConfiguration.h"

@interface SclakPadlockPeripheral : SclakPeripheral

@property (nonatomic, strong) SclakPadlockUserConfiguration *userConfiguration;

#pragma mark - User Configuration

- (void) sendUserConfiguration:(SclakPadlockUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

@end
