//
//  HartmannPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 03/03/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import "SclakPeripheral.h"

extern const uint8_t kPhoneToDevice_SET_LOCK;
extern const uint8_t kDeviceToPhone_RESP_SET_LOCK;
extern const uint8_t kPhoneToDevice_SET_CFG_USER;
extern const uint8_t kDeviceToPhone_RESP_SET_CFG_USER;
extern const uint8_t kPhoneToDevice_REQ_CFG_USER;
extern const uint8_t kDeviceToPhone_RESP_REQ_CFG_USER;

typedef NS_ENUM(NSUInteger, SclakSafeCommandResponseStatus) {
    ResponseUnknown,
    ResponseOK,
    ResponseKO_InvalidCode,
    ResponseKO_NoCodeSetted
};

typedef NS_ENUM(NSUInteger, SclakSafeStatus) {
    SafeLocked,
    SafeLocking,
    SafeUnlocked,
    SafeUnlocking
};

@interface SclakSafePeripheral : SclakPeripheral

@property (nonatomic, strong) BluetoothResponseCallback safeStatusChangedCallback;

@property (nonatomic, assign) SclakSafeStatus safeStatus;
@property (nonatomic, assign) SclakSafeCommandResponseStatus safeResponseStatus;

- (void) requestLockStatusWithCode:(NSString*)validationCode6 callback:(BluetoothResponseErrorCallback)callback;

- (void) openLockWithCode:(NSString*)validationCode6 callback:(BluetoothResponseErrorCallback)callback;
- (void) closeLockWithCode:(NSString*)validationCode6 callback:(BluetoothResponseErrorCallback)callback;

- (void) sendUserCode:(NSString*)userCode12 validationCode:(NSString*)validationCode6 callback:(BluetoothResponseCallback)callback;

- (void) sendResetCodes:(NSString*)userCode12 callback:(BluetoothResponseCallback)callback;

@end
