//
//  SCKKeyboardButton.h
//  SclakBle
//
//  Created by Daniele Poggi on 27/06/2017.
//  Copyright © 2017 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCKKeypadButton : NSObject

/**
 * Flag validità configurazione tasto (1 configurazione valida, 0 configurazione non valida)
 */
@property (nonatomic, assign) BOOL valid;

/**
 *  Identificativo tasto
 */
@property (nonatomic, assign) NSUInteger buttonId;

/**
 *  campo variabile a seconda di quento definito dal Bit7 del campo ID_KEY: indirizzo Bluetooth del dispositivo da controllare espresso su
 * 6 byte, oppure codice del Gruppo di dispositivi espresso su 1 byte.
 */
@property (nonatomic, strong) NSString *btcode;

/**
 *  campo variabile a seconda di quento definito dal Bit7 del campo ID_KEY: indirizzo Bluetooth del dispositivo da controllare espresso su
 6 byte, oppure codice del Gruppo di dispositivi espresso su 1 byte.
 */
@property (nonatomic, assign) NSUInteger groupId;

#pragma mark -

- (instancetype) initWithData:(NSData*)data;
- (NSMutableData*) messageData;

- (BOOL) isSetted;

@end
