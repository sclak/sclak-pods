//
//  WittkoppPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 03/02/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "SHA256Peripheral.h"

// wittkopp specific commands
extern const uint8_t kPhoneToDevice_SEND_COMM;
extern const uint8_t kDeviceToPhone_RESP_SEND_COMM;

typedef NS_ENUM(NSUInteger, LockStatus) {
    StatusUnknown,          // unknown status
    BoltOpened,             // Bolt completely retracted - open
    BoltLocking,            // Blocking mechanism in locking position, bolt is in “latch mode” prepared for spring loaded autolocking
    BoltLocked,             // Lock is secured
    BoltBlocked,            // Opening blocked by external signal
    OperationRunning,       // Operation running (bolt movement)
    TimeDelayRunning,       // Time delay running
    LowBattery,             // Low battery
    OpenedEmergency         // Opened by mechanical emergency override
};

@interface WittkoppPeripheral : SclakPeripheral

@property (nonatomic, readonly, assign) LockStatus lockStatus;

@property (nonatomic, readonly, assign) NSUInteger timeDelayRemainingTime;
@property (nonatomic, readonly, assign) NSUInteger openingWindowRemainingTime;

@property (nonatomic, readonly, assign) NSUInteger autocloseTime;
@property (nonatomic, readonly, assign) NSUInteger timeDelay;
@property (nonatomic, readonly, assign) NSUInteger openingWindow;

/**
 The lock will open and close automatically after the defined autoclosing time (default 5s).
 */
- (void) openAutocloseCallback:(BluetoothResponseCallback)callback;

/**
 The lock will open and stay in open position until a close command has been sent.
 */
- (void) openHoldModeCallback:(BluetoothResponseCallback)callback;

/**
 The lock will close immediately. Independent from a running “autoclosing time”.
 */
- (void) closeCallback:(BluetoothResponseCallback)callback;

/**
 It’s a command for changing the default “autoclosing time”. The new value will be stored permanently. The default value is 5s.
 @param autocloseTime
 */
- (void) sendAutocloseTime:(NSUInteger)autocloseTime callback:(BluetoothResponseCallback)callback;

/**
 To set up a time delay function a delay time value (1st payload byte) in a range of 1 up to 99 minutes has to be set. The second payload byte defines the possible opening window in a range of 1 up to 15 minutes. If the opening window is set to 0 minutes, the lock opens automatically after the delay time is elapsed. Otherwise the lock could be opened by sending an open command during the opening window.
 To switch off the time delay function set the 1st byte to 0. In this case the value of the 2nd byte doesn’t matter.
 @param timeDelay
 @param openingWindow
 */
- (void) sendTimeDelay:(uint8_t)timeDelay openingWindow:(uint8_t)openingWindow callback:(BluetoothResponseCallback)callback;

/**
 The lock status is stored in 3 bytes. When a lock status is requested Bit0 up to Bit5 of the first byte are updated before responding to the module. Bit 6 and 7 are updated after the answer is given to take care that for example a detected “low battery” signal will be read by the module.
 Every bit indicates a determined lock condition.
 Especially the 3 least significant bits define the locking state. There can only one of these
 bits been set at time. For undefined or not valid conditions the lock will answer for a status
 request with NAK.
 The second and third bytes display the remaining time of the time delay or (if elapsed) of the
 opening window. If the value is 0, no time delay is active. To know if a time delay is
 configured use the hC1 command.
 */
- (void) requestLockStatusCallback:(BluetoothResponseCallback)callback;

/**
 In the same manner of the byte arrangement which are used to setup the time delay function the values are responded to the module.
 */
- (void) readTimeDelayConfigurationCallback:(BluetoothResponseCallback)callback;

/**
 Return value is the stored "autoclose time"
 */
- (void) readAutocloseTimeCallback:(BluetoothResponseCallback)callback;

/**
 These commands should us stay well prepared for future applications. Without specifying the arrangement of bytes and so on, it is possible to transmit packages of max. 255 Byte of payload.
 */
- (void) uploadDataset:(uint8_t*)dataset length:(int)length callback:(BluetoothResponseCallback)callback;

/**
 These commands should us stay well prepared for future applications. Without specifying the arrangement of bytes and so on, it is possible to transmit packages of max. 255 Byte of payload.
 */
- (void) downloadDatasetCallback:(BluetoothResponseCallback)callback;

@end
