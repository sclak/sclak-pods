//
//  ODLCompensatorStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 28/09/15.
//  Copyright © 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ODLCompensatorStatus : NSObject

/**
 * Pressione attuale istantanea in ingresso al compensatore. Valore espresso in mBar in formato MSByte first.
 */
@property (nonatomic, strong) NSNumber *inPress;

/**
 * Pressione attuale istantanea in uscita dal compensatore. Valore espresso in mBar in formato MSByte first.
 */
@property (nonatomic, strong) NSNumber *outPress;

/**
 * Temperatura istantanea attuale birra in uscita dal compensatore (temperatura in colonna), espressa in gradi celsius con offset -100°.
 */
@property (nonatomic, strong) NSNumber *tempGlass;

/**
 * Temperatura mediata nell’ultima spillatura (temperatura media spillatura), espressa in gradi celsius con offset -100°.
 */
@property (nonatomic, strong) NSNumber *tempMean;

#pragma mark -

- (instancetype) initWithData:(NSData*)data;

@end
