//
//  SCKHandleUserConfiguration.h
//  SclakBle
//
//  Created by Daniele Poggi on 06/05/16.
//  Copyright © 2016 sclak. All rights reserved.
//

#import "PPLGenericUserConfiguration.h"

@interface SCKHandleUserConfiguration : PPLGenericUserConfiguration

@end
