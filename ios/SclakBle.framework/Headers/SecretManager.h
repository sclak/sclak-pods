//
//  SecretManager.h
//  SclakBle
//
//  Created by Daniele Poggi on 24/04/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SecretManager : NSObject

@property (nonatomic, assign) float sha256ThresholdVersion;

+ (instancetype) getInstance;

#pragma mark - SHA2 Secret Management

- (BOOL) hasSecretForBtcode:(NSString*)btcode;
- (BOOL) hasSecretForBtcode:(NSString*)btcode version:(float)version;
- (BOOL) setSecretForBtcode:(NSString*)btcode secret:(NSString*)secret;
- (BOOL) removeSecretForBtcode:(NSString*)btcode;
- (NSString*) secretForBtcode:(NSString*)btcode;
- (void) removeAllSecrets;

#pragma mark - Temporary Secret

/**
 *  stored in memory, to be used only in specific cases where it is known when
 *
 *  @param btcode
 *  @param secret
 *
 *  @return
 */
- (BOOL) setTemporarySecretForBtcode:(NSString*)btcode secret:(NSString*)secret;

/**
 *  remove stored secret in memory if found
 *
 *  @param btcode
 *
 *  @return
 */
- (BOOL) removeTemporarySecretForBtcode:(NSString*)btcode;

#pragma mark - Secret validation

+ (BOOL) isSecretValid:(NSString*)secret;
+ (BOOL) isAES128SecretValid:(NSString*)secret;
+ (BOOL) isSHA256SecretValid:(NSString*)secret;
+ (BOOL) isENTRSecretValid:(NSString*)secret;
+ (BOOL) isEDCCSecretValid:(NSString*)secret;

#pragma mark - Secret Clone validation

+ (BOOL) isSecretCloneNeeded:(NSString*)slaveSecret masterSecret:(NSString*)masterSecret;

@end
