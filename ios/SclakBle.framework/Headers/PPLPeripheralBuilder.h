//
//  PPLPeripheralBuilder.h
//  PassePartoutLib
//
//  Created by Daniele Poggi on 11/01/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPLPeripheralType.h"
#import "PPLDiscoveredPeripheral.h"
#import "PPLCentralManager.h"

#define PERIPHERAL_OTA_IDENTIFIER   @"OTA"

#define PERIPHERAL_NAME_SCLAK       @"Sclak"
#define PERIPHERAL_NAME_ULOCK       @"Ulock"
#define PERIPHERAL_NAME_SCLAKSAFE   @"Safe"
#define PERIPHERAL_NAME_PARKEY      @"Parkey"
#define PERIPHERAL_NAME_WITTKOPP    @"Witt"
#define PERIPHERAL_NAME_NEAR        @"Near"
#define PERIPHERAL_NAME_ZTF         @"ZTF"
#define PERIPHERAL_NAME_KEYB        @"Keyb"
#define PERIPHERAL_NAME_KEYFOB      @"KeyFob"
#define PERIPHERAL_NAME_LDIMM       @"L-Dimm"
#define PERIPHERAL_NAME_CEMO        @"Cemo"
#define PERIPHERAL_NAME_ODL         @"ODL"
#define PERIPHERAL_NAME_HANDLE      @"Handle"
#define PERIPHERAL_NAME_HANDLE_TAG  @"H_Tag"
#define PERIPHERAL_NAME_HANDLE_KEYB @"H_Keyb"
#define PERIPHERAL_NAME_MIFARE      @"Mifare"
#define PERIPHERAL_NAME_SCLAKB      @"SclakB"
#define PERIPHERAL_NAME_ENTREMATIC  @"Ditec"
#define PERIPHERAL_NAME_SCLAK_TAG   @"S_Tag"
#define PERIPHERAL_NAME_ENTR        @"ENTR"
#define PERIPHERAL_NAME_SCLAK_LOCK  @"S_Lock"
#define PERIPHERAL_NAME_WIEGAND     @"Wieg"
#define PERIPHERAL_NAME_CABINET     @"SCab_L"
#define PERIPHERAL_NAME_CRLOCK      @"CRlock"
#define PERIPHERAL_NAME_CYLINDER    @"SCyl"
#define PERIPHERAL_NAME_CYLINDER_E  @"CYE"
#define PERIPHERAL_NAME_DHAND       @"DHand"
#define PERIPHERAL_NAME_S2R         @"S2R"
#define PERIPHERAL_NAME_SAM         @"SAM"
#define PERIPHERAL_NAME_LKR         @"LKR"
#define PERIPHERAL_NAME_GEAR        @"GEAR"
#define PERIPHERAL_NAME_UFOe        @"UFOe"
#define PERIPHERAL_NAME_GIVI        @"GIVI"  // correct one
#define PERIPHERAL_NAME_GIVIK       @"GIVIK" // legacy
#define PERIPHERAL_NAME_GEAR_A      @"GRA"
#define PERIPHERAL_NAME_GEAR_R      @"GRR"
#define PERIPHERAL_NAME_TAG_P       @"TAGP"
#define PERIPHERAL_NAME_S_DRIVER    @"SDR"
#define PERIPHERAL_NAME_PADLOCK     @"PLK"
#define PERIPHERAL_NAME_CRLOCK_M    @"YAZ"
#define PERIPHERAL_NAME_MBOLT       @"ARL"
#define PERIPHERAL_NAME_KDE         @"KDE"
#define PERIPHERAL_NAME_PTS         @"PTS"
#define PERIPHERAL_NAME_BWG         @"BWG"
#define PERIPHERAL_NAME_RDR         @"RDR"

#define PERIPHERAL_NAME_COLLAUDO_SHA256   @"Coll_SHA256"

@interface PPLPeripheralBuilder : NSObject

+ (PPLDiscoveredPeripheral*) buildPPLPeripheralWithCBPeripheral:(CBPeripheral*)peripheral
                                                           type:(PeripheralType)type
                                                           name:(NSString*)name
                                                         btcode:(NSString*)btcode
                                                           RSSI:(NSNumber*)RSSI
                                                        manager:(PPLCentralManager*)manager;

+ (PeripheralType) peripheralTypeWith:(NSString*)name;
+ (PeripheralType) peripheralTypeWithPeripheral:(PPLBasePeripheral*)peripheral;

+ (NSDictionary*) peripheralTypes;

@end
