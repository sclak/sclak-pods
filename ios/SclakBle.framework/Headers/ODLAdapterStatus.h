//
//  ODLAdapterStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 28/09/15.
//  Copyright © 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ODLAdapterStatus : NSObject

/**
 * Pressione attuale richiesta in uscita dal riduttore, corrisponde a quella impostata in caso di funzionamento manuale, oppure a quella calcolata dal sistema in caso di funzionamento automatico o ECO. Valore espresso in mBar in formato MSByte first.
 */
@property (nonatomic, strong) NSNumber *usedPress;

/**
 * Portata attuale utilizzata dall’impianto, corrisponde a quella impostata in caso di funzionamento manuale, oppure a quella calcolata dal sistema in caso di funzionamento automatico o ECO. Valore espresso in dl/m.
 */
@property (nonatomic, strong) NSNumber *usedFlow;

/**
 * Pressione attuale istantanea in uscita dal riduttore. Valore espresso in mBar in formato MSByte first.
 */
@property (nonatomic, strong) NSNumber *outPress;

/**
 * Pressione attuale istantanea in ingresso al riduttore. Valore espresso in decimi di Bar in formato MSByte first.
 */
@property (nonatomic, strong) NSNumber *inPress;

#pragma mark -

- (instancetype) initWithData:(NSData*)data;

@end
