//
//  SclakPadlockUserConfiguration.h
//  SclakBle
//
//  Created by Daniele Poggi on 29/11/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "PPLGenericUserConfiguration.h"

@interface SclakPadlockUserConfiguration : PPLGenericUserConfiguration

@end
