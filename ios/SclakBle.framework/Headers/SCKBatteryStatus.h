//
//  SCKBatteryStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 22/04/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCKBatteryStatus : NSObject

/**
 * Percentuale capacità residua delle batterie valori ammessi 0-100
 */
@property (nonatomic, assign) NSUInteger batteryPercentage;

/**
 * Conteggio manovre effettuate dall’ultima sostituzione delle batterie, formato MSByte First
 */
@property (nonatomic, assign) NSUInteger countOpen;

/**
 * Valore di capacità residua della batteria espresso in mAh, formato MSByte First
 */
@property (nonatomic, assign) NSUInteger residualCapacity;

/**
 * Valore di tensione della batteria misurato durante la fase di StandBy, formato MSByte First
 */
@property (nonatomic, assign) NSUInteger standbyVoltage;

/**
 * Valore di tensione della batteria misurato durante la fase operativa di movimento (consumo massimo), formato MSByte First
 */
@property (nonatomic, assign) NSUInteger lowVoltage;

/**
 * Flag non validità dei dati batteria (1 Dati batteria non validi, 0 Dati batteria validi), questo flag viene utilizzato per permettere la gestione con lo stesso firmware di versioni HW diverse (con e senza la gestione consumo batterie).
 */
@property (nonatomic, assign) BOOL batteryDataInvalid;

/**
 * Reset dei dati gestione batterie dall’ultima lettura dello stato consumo Batterie, questo flag viene resettato dopo la lettura. La percentuale di carica viene impostata al 10% o al 100% a seconda del valore di Standby-Voltage.
 */
@property (nonatomic, assign) BOOL batteryDataResetted;

/**
 * Reset sistema dall’ultima lettura dello stato consumo Batterie, questo flag viene resettato
 * dopo la lettura.
 */
@property (nonatomic, assign) BOOL systemResetDetected;

/**
 * Flag sostituzione autonomo batteria rilevato (1 Sostituzione rilevata, 0 Non rilevata), questo flag viene resettato automaticamente dal comando di cambio batterie
 */
@property (nonatomic, assign) BOOL batteryReplacedDetected;

#pragma mark -

+ (instancetype) defaultBatteryStatus;

- (instancetype) initWithData:(NSData*)data;

- (NSMutableData*) messageData;

@end
