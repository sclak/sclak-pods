//
//  SCKPinAccessLog.h
//  SclakBle
//
//  Created by Daniele Poggi on 10/05/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SCKPinAccessLogType) {
    SCKPinAccessLogType_OK,
    SCKPinAccessLogType_KO_PIN_NOT_FOUND,
    SCKPinAccessLogType_KO_INVALID_FROM_ACCESSORY,
    SCKPinAccessLogType_KO_INVALID_TIME,
};

@interface SCKPinAccessLog : NSObject

/**
* Codice valido (0) o non Valido (1)
*/
@property (nonatomic, assign) BOOL invalidCode;

/**
* Bit3-0 Tipo di evento loggato:
* 0 Evento di accesso con esito positivo.
* 1 Evento di accesso negato per validità temporale PIN.
* 2 Evento di accesso negato errore codice PIN.
*/
@property (nonatomic, assign) SCKPinAccessLogType logType;

/**
 * Data LOG evento costruita unendo i campi date e time con la funzione di SclakPeripheralPin106
 */
@property (nonatomic, strong) NSDate *logDate;

/**
 * codice di accesso ricevuto ed utilizzato per la validazione accesso.
 */
@property (nonatomic, strong) NSString *pinCode;

- (instancetype) initWithData:(NSData*)data;

@end
