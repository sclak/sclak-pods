//
//  NearSclakPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 08/02/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import "SclakPeripheral.h"

#define NEAR_SCLAK_USE_SSK          NO

// near sclak specific commands
extern const uint8_t kPhoneToDevice_SET_NFC;
extern const uint8_t kDeviceToPhone_RESP_SET_NFC;

@interface NearSclakPeripheral : SclakPeripheral

@property (nonatomic, assign) BOOL patch_disconnect_3s;
@property (nonatomic, assign) BOOL waitingForNFCResponse;
@property (nonatomic, assign) BOOL setNFCReceived;
@property (nonatomic, assign) BOOL NFCActive;
@property (nonatomic, assign) BOOL isTransmitter;
@property (nonatomic, assign) NSUInteger transmissionInterval;
@property (nonatomic, assign) NSUInteger numPacketsReceived;

@property (nonatomic, strong) NearSclakPeripheral *pairedPeripheral;

@property (atomic, strong) NSData *NFCTransmittedData;

@property (atomic, assign) BOOL isNear;
@property (atomic, assign) BOOL isNearAndSafe;
@property (atomic, assign) uint8_t receivedStatus;
@property (atomic, strong) NSData *receivedData;

@property (nonatomic, strong) BluetoothResponseCallback responseNFCCallback;

- (void) pairWithTransmitterPeripheral:(NearSclakPeripheral*)nearPeripheral;

- (void) activate:(BOOL)activate asTransmitter:(BOOL)transmitter callback:(BluetoothResponseCallback)callback;
- (void) activate:(BOOL)activate asTransmitter:(BOOL)transmitter transmissionInterval:(NSUInteger)dtx callback:(BluetoothResponseCallback)callback;
- (void) activate:(BOOL)activate asTransmitter:(BOOL)transmitter transmissionInterval:(NSUInteger)dtx enableA:(BOOL)enableA enableB:(BOOL)enableB callback:(BluetoothResponseCallback)callback;

- (void) generateSecret;

@end
