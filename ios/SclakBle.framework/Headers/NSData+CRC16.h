//
//  NSData+CRC16.h
//  SclakBle
//
//  Created by Daniele Poggi on 08/06/2020.
//  Copyright © 2020 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (CRC16)

- (unsigned short) crc16Checksum;

@end
