//
//  NSDate+TimeZoneUtils.h
//  SclakUnit
//
//  Created by Daniele Poggi on 17/01/2018.
//  Copyright © 2018 SCLAK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (TimeZoneUtils)

- (NSDate*) toLocalTime:(NSTimeZone*)timezone;
- (NSDate*) toGlobalTime:(NSTimeZone*)timezone;

@end
