//
//  PPLUtil.h
//  PassePartoutLib
//
//  Created by isghe on 10/12/13.
//  Copyright (c) 2013 Daniele Poggi - Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef PPL_DIM_OF_ARRAY
#error "PPL_DIM_OF_ARRAY already defined"
#endif

// theArray must be a real C array for example:
// int aArray [] = {1, 2, 3};
#define PPL_DIM_OF_ARRAY(theArray) (sizeof (theArray)/sizeof (*theArray))

@interface PPLUtil : NSObject

+ (void) test;

@end
