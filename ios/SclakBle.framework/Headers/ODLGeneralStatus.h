//
//  ODLGeneralStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 27/09/15.
//  Copyright © 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ODLOperationGeneralStatus) {
    ODLOperationStatusOff,
    ODLOperationStatusOnDraft,      // acceso e in spillatura
    ODLOperationStatusOnStorage,    // acceso e in stoccaggio
    ODLOperationStatusOnError,      // acceso e in errore
};

typedef NS_ENUM(NSUInteger, ODLConfigurationType) {
    ODLConfigurationTypeManual,
    ODLConfigurationTypeSemiautomatic,
    ODLConfigurationTypeAutomatic,
    ODLConfigurationTypeEcoC02,
    ODLConfigurationTypePower,
    ODLConfigurationTypeWashing,    // lavaggio
    ODLConfigurationTypeSanitation  // sanificazione
};

typedef NS_ENUM(NSUInteger, ODLGasCylinderType) {
    ODLGasCylinderTypeCO2,
    ODLGasCylinderTypeN2,
    ODLGasCylinderTypeMixed,
};

typedef NS_ENUM(NSUInteger, ODLGasCylinderStatus) {
    ODLGasCylinderStatusFull,
    ODLGasCylinderStatusLow,
    ODLGasCylinderStatusEmpty,
};

typedef NS_ENUM(NSUInteger, ODLTapStatus) {
    ODLTapStatusClosed,
    ODLTapStatusOpened
};

typedef NS_ENUM(NSUInteger, ODLSystemError) {
    ODLSystemError_NoPower,
    ODLSystemError_LowPressureInputCO2,
    ODLSystemError_AdapterSecurityValve,
    ODLSystemError_CO2LeakDetected,
    ODLSystemError_KegLowPressure,
    ODLSystemError_ExceededMaximumAdjustmentAdapter,
    ODLSystemError_ExceededMinimumAdjustmentAdapter,
    ODLSystemError_KegEmpty
};

typedef NS_ENUM(NSUInteger, ODLSystemCustomError) {
    ODLSystemCustomError_ColdKeg,
    ODLSystemCustomError_HotKeg
};

typedef NS_ENUM(NSUInteger, ODLSystemFailure) {
    ODLSystemFailure_CommunicationGraft,
    ODLSystemFailure_CommunicationCompensator,
    ODLSystemFailure_PressureSensorInputCO2Adapter,
    ODLSystemFailure_PressureSensorOutputCO2Adapter,
    ODLSystemFailure_PressureSensorInputCompensator,
    ODLSystemFailure_PressureSensorOutputCompensator,
    ODLSystemFailure_TemperatureSensorGlass,
    ODLSystemFailure_TemperatureSensorKeg,
    ODLSystemFailure_TemperatureSensorAmbient
};

typedef NS_ENUM(NSUInteger, ODLSystemWarning) {
    ODLSystemWarning_IcingReducer,
    ODLSystemWarning_MemoryCardNotInsertedOrFull,
    ODLSystemWarning_PossibleFoam,
    ODLSystemWarning_PossibleLeak,
    ODLSystemWarning_WashingLimit,
    ODLSystemWarning_SanitationLimit,
    ODLSystemWarning_SealsRevisionLimit,
    ODLSystemWarning_PlantRevisionLimit,
};

@interface ODLGeneralStatus : NSObject <NSCopying>

// operation status and configuration type

@property (nonatomic, assign) ODLOperationGeneralStatus operationStatus;
@property (nonatomic, assign) ODLConfigurationType configurationType;

// gas cylinder type and statuses

@property (nonatomic, assign) ODLGasCylinderType gasCylinderType;
@property (nonatomic, assign) ODLGasCylinderStatus gasCylinderStatus;

/**
 * Pressione impostata dall’utente, questo valore viene utilizzato solo nello scenario di spillatura Manuale. Valore espresso in mBar in formato MSByte first
 */
@property (nonatomic, strong) NSNumber *setPress;

/**
 * Portata impostata dall’utente, questa definisce il target di portata che si cerca di mantenere modalità Manuale e Semiautomatica. Valore
 espresso in dl/m.
 */
@property (nonatomic, strong) NSNumber *setFlow;

/**
 * System status - tap
 */
@property (nonatomic, assign) ODLTapStatus tapStatus;

/**
 * Errori presenti nel sistema, gestito a Bit (1 errore presente, 0 errore non presente), la presenza di uno o più errori modifica lo stato
 impianto; alcuni guasti sono autoripristinabile altri devono essere rispristinati con una sequenza spegnimento/accensione impianto, formato MSByte first
 */
@property (nonatomic, strong) NSArray *systemErrors;

/**
 * errori calcolati dall'App
 */
@property (nonatomic, strong) NSMutableArray *systemCustomErrors;

/**
 * Guasti presenti nel sistema, gestito a Bit (1 guasto presente, 0 guasto non presente), formato MSByte first
 */
@property (nonatomic, strong) NSArray *systemFaults;

/**
 * Anomalie presenti nel sistema, gestito a Bit (1 anomalia presente, 0 anomalia non presente), formato MSByte first
 */
@property (nonatomic, strong) NSArray *systemWarnings;

/**
 * Numero di birre medie erogate dall'ultimo cambio fusto
 */
@property (nonatomic, strong) NSNumber *numBeer;

/**
 * Numero di ore trascorse dall’ultimo cambio Fusto
 */
@property (nonatomic, strong) NSNumber *hoursChange;

#pragma mark - Error Management

- (NSInteger) totalErrorsCount;

#pragma mark -

- (instancetype) initWithData:(NSData*)data;

@end
