//
//  ODLOperativeCommand.h
//  SclakBle
//
//  Created by Daniele Poggi on 20/10/15.
//  Copyright © 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ODLOPerativeCommandType) {
    ODLOPerativeCommandTypeChangeKeg                = 1,
    ODLOPerativeCommandTypeStartWashing             = 2,
    ODLOPerativeCommandTypeStartSanitation          = 3,
    ODLOPerativeCommandTypeResetSealingsCounter     = 4,
    ODLOPerativeCommandTypeResetPlantCounter        = 5,
};

@interface ODLOperativeCommand : NSObject

@property (nonatomic, assign) ODLOPerativeCommandType command;

#pragma mark -

- (NSMutableData*) messageData;

@end
