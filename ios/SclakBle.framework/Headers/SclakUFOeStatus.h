//
//  SclakUFOeStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 30/05/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SclakUFOeStatus : NSObject

/**
 * Valore attuale della serratura (uscita attiva (serratura sbloccate) -> 0x01, uscita disattiva (serratura bloccata) -> 0x00).
 */
@property (nonatomic, assign) BOOL lockStatus;

/**
 * Segnale di stato switch Carrello arretrato (Serratura sbloccata, 0 -> Switch disattivo, 1 -> Switch attivo)
 */
@property (nonatomic, assign) BOOL cartStatus;

/**
 * Segnale di stato switch Carrello in blocco porta (Serratura bloccata, 0 -> Switch disattivo, 1 -> Switch attivo).
 */
@property (nonatomic, assign) BOOL cartLockStatus;

/**
 * Segnale di stato switch stato Porta (Porta chiusa, 0 -> Switch disattivo, 1 -> Switch attivo).
 */
@property (nonatomic, assign) BOOL doorStatus;

- (instancetype) initWithData:(NSData*)data;

@end
