//
//  SclakLockUserConfiguration.h
//  SclakBle
//
//  Created by Daniele Poggi on 02/05/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "PPLGenericUserConfiguration.h"

@interface SclakLockUserConfiguration : PPLGenericUserConfiguration

@end
