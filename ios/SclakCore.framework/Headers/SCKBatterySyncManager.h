//
//  SCKBatteryManager.h
//  SclakApp
//
//  Created by Daniele Poggi on 01/09/2018.
//  Copyright © 2018 Sclak S.p.A. All rights reserved.
//

#import <SclakFacade/SCKFacade.h>
#import <SclakBle/PPLBluetoothCallbacks.h>

@class Peripheral;
@class SclakPeripheral;

@interface SCKBatterySyncManager : NSObject

+ (instancetype) sharedInstance;

/**
 * sync battery data with server
 * @param model the peripheral model to sync
 * @param peripheral the BLE peripheral model to sync
 * @param disconnect YES disconnect from BLE peripheral when done, NO otherwise
 * @param callback called with success or failure status
 */
- (void) syncBattery:(Peripheral*)model
          peripheral:(SclakPeripheral*)peripheral
          disconnect:(BOOL)disconnect
            callback:(BluetoothResponseErrorCallback)callback;

@end
