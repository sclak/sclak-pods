//
//  SCKAuthCallbacks.h
//  Pods
//
//  Created by Developer on 30/11/18.
//

#import "SCKAuthCommon.h"

@class ResponseObject;

typedef void(^LoginFinishedCallback)(BOOL finished);
typedef void(^RegisterFinishedCallback)(BOOL finished);
typedef void(^ChangePasswordFinishedCallback)(BOOL success, id result);
typedef void(^ResetPasswordFinishedCallback)(BOOL success, id result);
typedef void(^SendActivationCodeFinishedCallback)(BOOL success, id result);
typedef void(^GenerateResetPasswordCodeFinishedCallback)(BOOL success, id result);
typedef void(^EditEmailFinishedCallback)(BOOL success, id result, ResponseObject *responseObject);
typedef void(^TwoFactorChallengeFinishedCallback)(BOOL success);
typedef void(^CodeVerificationChallengeFinishedCallback)(BOOL success);
