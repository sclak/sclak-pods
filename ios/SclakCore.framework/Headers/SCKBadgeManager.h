//
//  SCKBadgeManager.h
//  SclakCore
//
//  Created by Daniele Poggi on 30/10/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCKManagerProtocol.h"
#import <SclakFacade/Badge.h>

typedef void (^WriteBadgeCallback)(BOOL cancelled, BOOL success, Badge *result);

@interface SCKBadgeManager : NSObject <SCKManagerProtocol>

#pragma mark - Shared Instance

+ (SCKBadgeManager*) sharedInstance;

#pragma mark - Activate Badge

- (void) activateBadge:(Badge*)badge overwrite:(BOOL) overwrite callback:(WriteBadgeCallback)callback;

@end
