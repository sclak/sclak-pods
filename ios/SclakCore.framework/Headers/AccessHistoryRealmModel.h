//
//  AccessHistoryRealmModel.h
//  SclakCore
//
//  Created by Daniele Poggi on 30/05/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import <Realm/Realm.h>

@interface AccessHistoryRealmModel : RLMObject

@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, assign) int usageType;
@property (nonatomic, strong) NSNumber<RLMInt> *peripheralId;
@property (nonatomic, strong) NSNumber<RLMDouble> *insertTime;

@end
