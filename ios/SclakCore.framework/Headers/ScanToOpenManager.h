//
//  ScanToOpenManager.h
//  SclakApp
//
//  Created by Daniele Poggi on 06/11/2018.
//  Copyright © 2018 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ScanToOpenDelegate;

@class ResponseObject, PrivilegeRequest, ScanToOpen, Peripheral;

@interface ScanToOpenManager : NSObject

@property ScanToOpen *lastScan;

+ (instancetype) getInstance;

- (void) startRequest:(NSString*)qrcodeContent delegate:(id<ScanToOpenDelegate>)delegate;
- (void) startRequestWithCode:(NSString*)code delegate:(id<ScanToOpenDelegate>)delegate;

- (void) proceedWithRequest:(ScanToOpen*)request;
- (void) proceedWithRequest:(ScanToOpen*)request selectedPeripheral:(Peripheral*)peripheral;

- (void) acceptPrivilegeRequest:(NSNumber*)privilegeRequestId;
- (void) denyPrivilegeRequest:(NSNumber*)privilegeRequestId;

@end

@protocol ScanToOpenDelegate <NSObject>

- (void) requestCanOpen:(Peripheral*)peripheral scan:(ScanToOpen*)model;
- (void) requestRequiresRegistration:(ScanToOpen*)model;
- (void) requestRequiresDetails:(ScanToOpen*)model;
- (void) requestAlreadyTaken:(ScanToOpen*)model;
- (void) requestLimitReached:(ScanToOpen*)model;
- (void) requestError:(ResponseObject*)error;

@optional

- (void) qrcodeDetectionError:(ResponseObject*)response;
- (void) beginPrivilegeGeneration;
- (void) endPrivilegeGeneration:(ResponseObject*)response;
- (void) endPrivilegeGenerationWithError:(ResponseObject*)error;

@end
