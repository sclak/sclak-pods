//
//  SCKLocationManager.h
//  SclakCore
//
//  Created by Francesco Mantello on 11/07/2018.
//  Copyright © 2018 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "SCKManagerProtocol.h"

typedef void (^LocationBlock)(BOOL success, CLLocation *location, NSError *error);
typedef void (^ReverseGeocodeBlock)(BOOL success, NSString *address, CLPlacemark *placemark, NSError *error);

@protocol SCKLocationManagerDelegate;

@interface SCKLocationManager : CLLocationManager <SCKManagerProtocol>

#pragma mark - Shared Instance

+ (instancetype) sharedInstance;

#pragma mark - Setup

- (void) setupWithDelegates:(NSArray<id<SCKLocationManagerDelegate>>*)delegates;

#pragma mark - Authorization

- (BOOL) isAuthorized;

#pragma mark - Delegate Accessories Methods (solution: #1)

- (void) addDelegate:(id<SCKLocationManagerDelegate>)delegate;
- (void) removeDelegate:(id<SCKLocationManagerDelegate>)delegate;
- (BOOL) hasDelegate:(id<SCKLocationManagerDelegate>)delegate;
    
#pragma mark - Location Utils
    
- (void) getCurrentLocationCallback:(LocationBlock)callback;
- (void) reverseGeocode:(CLLocation *)location callback:(ReverseGeocodeBlock)callback;
- (void) geocodeAddressString:(NSString *)addressString callback:(LocationBlock)callback;

@end

@protocol SCKLocationManagerDelegate<NSObject>
@optional
- (void) locationManager:(CLLocationManager*)manager didFailWithError:(NSError*)error;
- (void) locationManager:(CLLocationManager*)manager monitoringDidFailForRegion:(CLRegion*)region withError:(NSError*)error;
- (void) locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status;
- (void) locationManager:(CLLocationManager*)manager didStartMonitoringForRegion:(CLRegion*)region;
- (void) locationManager:(CLLocationManager*)manager didEnterRegion:(CLRegion*)region;
- (void) locationManager:(CLLocationManager*)manager didExitRegion:(CLRegion*)region;
#if defined SCLAK_CORE_APP
- (void) locationManager:(CLLocationManager*)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion*)region;
- (void) locationManager:(CLLocationManager*)manager didRangeBeacons:(NSArray*)beacons inRegion:(CLBeaconRegion*)beaconRegion;
- (void) locationManager:(CLLocationManager*)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion*)region withError:(NSError*)error;
#endif
@end
