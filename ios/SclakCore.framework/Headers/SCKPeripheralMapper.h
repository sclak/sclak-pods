//
//  SCKPeripheralMapper.h
//  SclakCore
//
//  Created by Daniele Poggi on 29/04/2019.
//

#import <SclakBle/PPLPeripheralBuilder.h>

@class Peripheral;

NS_ASSUME_NONNULL_BEGIN

@interface SCKPeripheralMapper : NSObject

/**
 * from Peripheral model retrieve BLE Peripheral type
 */
+ (PeripheralType) peripheralTypeWithPeripheral:(Peripheral*)peripheral;

/**
 * from Peripheral type, retrieve SCKFacade's SCKPeripheralType
 */
+ (NSString*) facadePeripheralTypeWith:(PeripheralType)type;

@end

NS_ASSUME_NONNULL_END
