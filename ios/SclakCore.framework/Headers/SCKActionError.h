//
//  SCKActionError.h
//  SclakCore
//
//  Created by Daniele Poggi on 09/07/2018.
//  Copyright © 2018 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SclakActionError) {
    GenericError,
    NoPrivilegeForPeripheral,
    PrivilegeDisabled,
    PrivilegePendingAcceptance,
    PrivilegeForceScanEnabled,
    PrivilegeAppDisabled,
    PrivilegeExpired,
    DisabledByAdmin,
    CalendarRestriction,
    CalendarRestrictionEnded,
    GettingData,
    ConnectionInsecure,
    SclakNotNearby,
    SclakCommunicationError,
    SecurityLevelCheckNotPassed,
    OneTimeAccess,
    EntryphoneInvalid,
    EntryphoneDenied,
    BackgroundCannotProceed,
    GPSNeededNotAvailable,
};

@interface SCKActionError : NSObject

+ (NSString*) localizedErrorName:(SclakActionError)error;

@end
