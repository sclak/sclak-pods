//
//  SCKPhonePrefixManager.h
//  SclakCore
//
//  Created by Daniele Poggi on 01/08/2019.
//  Copyright © 2019 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCKPhonePrefixManager : NSObject

@property (nonatomic, strong) NSArray *countries;
@property (nonatomic, strong) NSArray *phoneNumberPrefixes;

+ (instancetype) sharedInstance;

- (NSString*) phoneNumberContainsPrefix:(NSString*)phoneNumber;

- (NSString*) countryCodeWithPhonePrefix:(NSString*)phonePrefix;

- (NSString*) internationalNumber:(NSString*)phoneNumber;

- (NSString*) internationalNoPlusWithPrefix:(NSString*)prefix phoneNumber:(NSString*)phoneNumber;

- (NSDictionary*) prefixAndNumber:(NSString*)phoneNumber;

- (NSString*) findPrefixNumber:(NSString*)myCountryCode;

- (NSString*) numberWithoutAlphaCharacters:(NSString*)phoneNumber;

- (void) testNumberLib;

@end
