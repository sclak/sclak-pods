//
//  SCKManagerProtocol.h
//  SclakCore
//
//  Created by Francesco Mantello on 06/07/18.
//  Copyright © 2018 Sclak. All rights reserved.
//

#ifndef SCKManagerProtocol_h
#define SCKManagerProtocol_h

@protocol SCKManagerProtocol <NSObject>

+ (instancetype) sharedInstance;

- (void) setup;

@end

#endif /* SCKManagerProtocol_h */
