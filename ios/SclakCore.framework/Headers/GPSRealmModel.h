//
//  AccessCounterRealmModel.h
//  SclakCore
//
//  Created by Daniele Poggi on 25/03/2019.
//

#import <Realm/Realm.h>

@interface GPSRealmModel : RLMObject

@property (nonatomic, strong) NSString *btcode_datetime;
@property (nonatomic, strong) NSString *btcode;
@property (nonatomic, strong) NSNumber<RLMDouble> *datetime;
@property (nonatomic, strong) NSNumber<RLMDouble> *longitude;
@property (nonatomic, strong) NSNumber<RLMDouble> *latitude;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, assign) BOOL synced;

@end

