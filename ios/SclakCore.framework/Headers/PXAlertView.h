//
//  PXAlertView.h
//  PXAlertViewDemo
//
//  Created by Alex Jarvis on 25/09/2013.
//  Copyright (c) 2013 Panaxiom Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PXAlertViewCompletionBlock)(BOOL cancelled, NSInteger buttonIndex);

@interface PXAlertView : UIViewController

@property (nonatomic, readonly) UILabel *messageLabel;
@property (nonatomic, readonly) UIView *contentView;

@property (nonatomic, getter = isVisible) BOOL visible;
@property (nonatomic, getter = isDismissed) BOOL dismissed;

+ (PXAlertView*) showProgressAlertViewWithTitle:(NSString *)title message:(NSString *)message;
+ (PXAlertView*) showProgressAlertViewWithTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString*)cancelTitle;
+ (PXAlertView*) showProgressAlertViewWithTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString*)cancelTitle completion:(PXAlertViewCompletionBlock)completion;

+ (void) showErrorAlertViewForResult:(id)result;
+ (void) showErrorAlertViewForResult:(id)result andDispatchProgressAlert:(PXAlertView*)progressAlert;
+ (void) showErrorAlertViewForResult:(id)result andDispatchProgressAlert:(PXAlertView*)progressAlert completion:(PXAlertViewCompletionBlock)completion;

+ (instancetype) showAlertWithTitle:(NSString *)title;

+ (instancetype) showAlertWithTitle:(NSString *)title
                            message:(NSString *)message;

+ (instancetype) showAlertWithTitle:(NSString *)title
                            message:(NSString *)message
                         completion:(PXAlertViewCompletionBlock)completion;

+ (instancetype) showAlertWithTitle:(NSString *)title
                            message:(NSString *)message
                        cancelTitle:(NSString *)cancelTitle;

+ (instancetype) showAlertWithTitle:(NSString *)title
                            message:(NSString *)message
                        cancelTitle:(NSString *)cancelTitle
                         completion:(PXAlertViewCompletionBlock)completion;

+ (instancetype) showAlertWithTitle:(NSString *)title
                            message:(NSString *)message
                        cancelTitle:(NSString *)cancelTitle
                         otherTitle:(NSString *)otherTitle
                         completion:(PXAlertViewCompletionBlock)completion;

/**
 * @param otherTitles Must be a NSArray containing type NSString, or set to nil for no otherTitles.
 */
+ (instancetype) showAlertWithTitle:(NSString *)title
                            message:(NSString *)message
                        cancelTitle:(NSString *)cancelTitle
                        otherTitles:(NSArray *)otherTitles
                         completion:(PXAlertViewCompletionBlock)completion;


+ (instancetype) showAlertWithTitle:(NSString *)title
                            message:(NSString *)message
                        cancelTitle:(NSString *)cancelTitle
                         otherTitle:(NSString *)otherTitle
                        contentView:(UIView *)view
                         completion:(PXAlertViewCompletionBlock)completion;
/**
 * @param otherTitles Must be a NSArray containing type NSString, or set to nil for no otherTitles.
 */
+ (instancetype) showAlertWithTitle:(NSString *)title
                            message:(NSString *)message
                        cancelTitle:(NSString *)cancelTitle
                        otherTitles:(NSArray *)otherTitles
                        contentView:(UIView *)view
                         completion:(PXAlertViewCompletionBlock)completion;

/**
 * Adds a button to the receiver with the given title.
 * @param title title of the new button
 * @return The index of the new button. Button indices start at 0 and increase in the order they are added.
 */
- (NSInteger) addButtonWithTitle:(NSString *)title;

/**
 * change alert message
 */
- (void) setMessage:(NSString*)message;

- (void) hide;
- (void) show;

@end

@interface PXAlertViewStack : NSObject

@property (nonatomic, strong) NSMutableArray *alertViews;

+ (PXAlertViewStack *) sharedInstance;

- (void) push:(PXAlertView *) alertView;
- (void) pop:(PXAlertView *) alertView;

@end



