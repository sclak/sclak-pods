//
//  SCKSyncAccessManager.h
//  SclakUnit
//
//  Created by Daniele Poggi on 10/05/2018.
//  Copyright © 2018 SCLAK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SclakFacade/LogUsage.h>
#import "SCKManagerProtocol.h"
#import <SclakBle/PPLBluetoothCallbacks.h>
#import "AccessControlRealmModel.h"

#if !TARGET_OS_WATCH
#import <UIKit/UIKit.h>
#endif

@class Peripheral;
@class SclakPeripheral;

// Notification Center
#define SYNC_ACCESS_MANAGER_STARTED        @"SYNC_ACCESS_MANAGER_STARTED"
#define SYNC_ACCESS_MANAGER_PROGRESS       @"SYNC_ACCESS_MANAGER_PROGRESS"
#define SYNC_ACCESS_MANAGER_FINISHED       @"SYNC_ACCESS_MANAGER_FINISHED"

// FM: to be renamed in SCKAccessSyncManager
@interface SCKSyncAccessManager : NSObject<SCKManagerProtocol>

#pragma mark - Shared Instance

+ (SCKSyncAccessManager*) sharedInstance;

#pragma mark - SMARTPHONE ACCESS LOGS

- (AccessControlRealmModel*) getLastCheckInOut:(Peripheral*)peripheral type:(LogUsageType)type;
- (int) countCheckInOut:(Peripheral*)peripheral;
/**
 * save a usage log for the selected peripheral, with usage type. callback when finished
 * save locally and then try to sync to server immediately if possible
 */
- (void) logUsageForPeripheral:(Peripheral*)peripheral usageType:(LogUsageType)usageType callback:(BluetoothResponseCallback)callback;

/**
 * sync smartphone access logs if possible
 */
- (void) syncAccessLogsIfPossible:(BluetoothResponseCallback)callback;
- (void) syncAccessLogsMulti:(BluetoothResponseCallback)callback;
- (void) syncAccessLogsRecursive;

/**
 * @deprecated delete all models in realm database. Use specific delete functions
 */
- (void) deleteRealm;

/**
 * remove realm access logs models
 */
- (void) deleteAccessLogs;

#pragma mark - PIN ACCESS LOGS

#if defined SCLAK_CORE_APP

/**
 * start the sync process using model and peripheral to retrive pin codes to update/remove from server
 * and then sync peripheral with bluetooth APIs
 * update server when sync is complete (if updateServer is YES)
 */
- (void) syncAccessPins:(Peripheral*)model
             peripheral:(SclakPeripheral*)peripheral
                visible:(BOOL)visible
           updateServer:(BOOL)updateServer
                context:(UIViewController*)context
               callback:(BluetoothResponseErrorCallback)callback;

#else

/**
 * start the sync process using model and peripheral to retrive pin codes to update/remove from server
 * and then sync peripheral with bluetooth APIs
 * update server when sync is complete (if updateServer is YES)
 */
- (void) syncAccessPins:(Peripheral*)model
             peripheral:(SclakPeripheral*)peripheral
           updateServer:(BOOL)updateServer
               callback:(BluetoothResponseErrorCallback)callback;

#endif

/**
 * read all pins from Realm database and send them to server
 */
- (void) syncAccessPinsToServerCallback:(BluetoothResponseErrorCallback)callback;

#pragma mark - Access Counter

/**
 * read from device the access counter, save on local database
 */
- (void) syncAccessCounter:(Peripheral*)model
                peripheral:(SclakPeripheral*)peripheral
              updateServer:(BOOL)updateServer
                  callback:(BluetoothResponseErrorCallback)callback;

/**
 * read from local database access counters and send to sclak server
 */
- (void) syncAccessCountersToServerCallback:(BluetoothResponseErrorCallback)callback;

- (void) interruptSync;

@end
