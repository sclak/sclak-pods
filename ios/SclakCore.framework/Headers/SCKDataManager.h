//
//  SCKDataManager.h
//  SclakCore
//
//  Created by Francesco Mantello on 18/07/2018.
//  Copyright © 2018 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCKManagerProtocol.h"
#import <SclakFacade/SCKFacadeCallbacks.h>

// STANDARD USER DEFAULTS
#define kApplicationInactiveTime                @"ApplicationInactiveTime"

// NOTIFICATIONS
#define kGetDataStartedNotification             @"kGetDataStartedNotification"
#define kGetDataFinishedNotification            @"kGetDataFinishedNotification"

@protocol SCKDataManagerDelegate;

@interface SCKDataManager : NSObject<SCKManagerProtocol>

#pragma mark - Properties

@property (nonatomic, readonly) BOOL hasInactiveTimeSetted;
@property (nonatomic, readonly) BOOL gettingData;
@property (nonatomic, readonly) BOOL isDifferentialLastUsefulUpdate;

#pragma mark - Delegate

@property (nonatomic, weak) id<SCKDataManagerDelegate> delegate;

#pragma mark - Shared Instance

+ (SCKDataManager*) sharedInstance;

#pragma mark - Setup
    
- (void) setupWithDelegate:(id<SCKDataManagerDelegate>)delegate;

#pragma mark - Get Data Accessors
    
- (void) getData:(BOOL)differentially callback:(ResponseObjectCallback)callback;
- (void) getData:(BOOL)differentially forceUpdate:(BOOL)forceUpdate callback:(ResponseObjectCallback)callback;

#pragma mark - Reset Accessors

- (void) interrupt;
- (void) reset;

@end

@protocol SCKDataManagerDelegate<NSObject>
@optional
- (void) didGetDataSuccess;
- (void) didGetDataFailWithError:(NSError*)error;
@end
