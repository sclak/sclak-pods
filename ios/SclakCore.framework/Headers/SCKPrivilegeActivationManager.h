//
//  SCKPrivilegeActivationManager.h
//  SclakApp
//
//  Created by Daniele Poggi on 06/02/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCKManagerProtocol.h"
#import <SclakBle/PPLBluetoothCallbacks.h>

@protocol SCKPrivilegeActivationDelegate;

typedef NS_ENUM(NSUInteger, PrivilegeActivationRequestType) {
    PrivilegeActivationRequestInstallation,
    PrivilegeActivationRequestUsage
};

@class Coupon;
@class PeripheralActivation, PrivilegeActivation;

typedef void(^PrivilegeActivationCallback)(NSString *activationCode);

@interface SCKPrivilegeActivationManager : NSObject<SCKManagerProtocol>

#pragma mark - Properties

// current peripheral activation model
@property (nonatomic, strong) PeripheralActivation *peripheralActivation; // FM: bad readwrite here, use readonly and make accessory methods to manage association ... !!!

@property (nonatomic, strong) Coupon *coupon;
@property (nonatomic, strong) NSString *activationCode;
@property (nonatomic, assign) PrivilegeActivationRequestType type;
@property (nonatomic, strong) NSString *pukCode;
@property (nonatomic, strong) NSString *pinCode;
@property (nonatomic, strong) NSDictionary *pukCodes;
@property (nonatomic, strong) NSDictionary *pinCodes;
@property (nonatomic, strong) NSString *groupTag;
@property (nonatomic, strong) NSString *groupPin;

#pragma mark - Shared Instance

+ (SCKPrivilegeActivationManager*) sharedInstance;

#pragma mark - Methods

- (void) evaluateUrlForPrivilegeActivation:(NSURL*)url callback:(PrivilegeActivationCallback)callback;
- (void) backupPukPinCodesCallback:(ResponseObjectCallback)callback;

- (void) submitWithCouponCode:(NSString*)activationCode type:(PrivilegeActivationRequestType)type;
- (void) submitWithCouponCode:(NSString*)activationCode
                         type:(PrivilegeActivationRequestType)type
                     delegate:(id<SCKPrivilegeActivationDelegate>)delegate
               fromController:(id)controller;

- (BOOL) hasPendingActivation;

/**
 * request again last activation using again the property "activationCode"
 * get coupon will be requested
 */
- (void) retryPendingActivation;

/**
 * request again privilege activation using again the properties "coupon" and "activationCode"
 * get coupon will NOT be requested
 */
- (void) resumePrivilegeActivation;

- (void) reset;

@end

@protocol SCKPrivilegeActivationDelegate <NSObject>

- (void) privilegeActivationBegin:(PrivilegeActivationRequestType)type;
- (void) privilegeActivationInstantKeyDetected;
- (void) privilegeActivationDidEnd:(BOOL)success error:(ResponseObject*)error;
- (void) privilegeActivationRequiresUserLogin:(NSString*)username name:(NSString*)name randomPassword:(BOOL)randomPassword;
- (void) privilegeActivationRequiresUserRegistration:(BOOL)asInstaller instantKey:(BOOL)instantKey;
- (void) privilegeActivationRequiresDismissTutorial;
- (void) privilegeActivationRequiresInstallNewPeripheral:(id)controller;

@end
