//
//  SCKSyncManagerProtocol.h
//  SclakCore
//
//  Created by Francesco Mantello on 06/07/18.
//  Copyright © 2018 Sclak. All rights reserved.
//

#ifndef SCKSyncManagerProtocol_h
#define SCKSyncManagerProtocol_h

@protocol SCKSyncManagerProtocol <NSObject>

// - (void) syncPeripheralSecret:(Peripheral*)peripheral
//                   peripheral:(PPLDiscoveredPeripheral*)discoveredPeripheral
//                     callback:(BluetoothResponseErrorCallback)callback;

@end

#endif /* SCKSyncManagerProtocol_h */
