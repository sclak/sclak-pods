//
//  SCKOpenButtonProtocol.h
//  SclakCore
//
//  Created by Daniele Poggi on 03/07/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#ifndef SCKOpenButtonProtocol_h
#define SCKOpenButtonProtocol_h

@protocol SCKOpenButtonProtocol <NSObject>

@property (nonatomic, strong) Peripheral *peripheral;

- (void) reloadButtonUI;

@end

#endif /* SCKOpenButtonProtocol_h */
