#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "SclakCore+Constants.h"
#import "SclakCore.h"
#import "PXAlertView.h"
#import "SCKPeripheralMapper.h"
#import "SCKEventLogger.h"
#import "CLLocationManager+blocks.h"
#import "NSCalendar+Utils.h"
#import "SCKAuthCallback.h"
#import "SCKAuthCommon.h"
#import "SCKAuthManager.h"
#import "SCKDataManager.h"
#import "SCKPeripheralInstallationManager.h"
#import "SCKPrivilegeActivationLinkManager.h"
#import "SCKPrivilegeActivationManager.h"
#import "SCKBatterySyncManager.h"
#import "SCKGPSManager.h"
#import "SCKPinSyncManager.h"
#import "SCKSyncAccessManager.h"
#import "EntrematicSyncManager.h"
#import "SCKPeripheralSecretSyncManager.h"
#import "SCKPeripheralUsageManager.h"
#import "SCKPeripheralUsageManagerProtocols.h"
#import "SCKWatchManager.h"
#import "ScanToOpenManager.h"
#import "SCKAlertManager.h"
#import "SCKBadgeManager.h"
#import "SCKBleScanManager.h"
#import "SCKLocationManager.h"
#import "SCKPasswordManager.h"
#import "SCKPhonePrefixManager.h"
#import "SCKSecurityManager.h"
#import "SCKActionError.h"
#import "SCKPeripheralUsage.h"
#import "SCKManagerProtocol.h"
#import "SCKOpenButtonProtocol.h"
#import "SCKSyncManagerProtocol.h"
#import "AccessControlRealmModel.h"
#import "AccessCounterRealmModel.h"
#import "AccessHistoryRealmModel.h"
#import "GPSRealmModel.h"
#import "RLMPinAccessLog.h"
#import "SCKRealm.h"
#import "SCKSettings.h"

FOUNDATION_EXPORT double SclakCoreVersionNumber;
FOUNDATION_EXPORT const unsigned char SclakCoreVersionString[];

