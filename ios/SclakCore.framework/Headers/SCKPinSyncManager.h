//
//  SCKSyncManager.h
//  SclakCore
//
//  Created by Daniele Poggi on 23/05/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SCKManagerProtocol.h"
#import <SclakBle/PPLBluetoothCallbacks.h>

typedef void(^PeripheralPUKBlock) (BOOL setted, NSString *PUK);

@class Peripheral;
@class SclakPeripheral;

// Notification Center
#define PIN_SYNC_MANAGER_STARTED        @"PIN_SYNC_MANAGER_STARTED"
#define PIN_SYNC_MANAGER_PROGRESS       @"PIN_SYNC_MANAGER_PROGRESS"
#define PIN_SYNC_MANAGER_FINISHED       @"PIN_SYNC_MANAGER_FINISHED"

@interface SCKPinSyncManager : NSObject<SCKManagerProtocol>

#pragma mark - Properties

@property (nonatomic, assign) BOOL interrupt;

#pragma mark - Shared Instance

+ (SCKPinSyncManager*) sharedInstance;

#pragma mark - PUK

/**
 * set the PUK on the peripheral
 * if the PUK is already present on server, it will be verified
 * if the PUK is already setted and the provided PUK doesn't match, it will be requested to the user with an input alert
 * when operation is completed, callback is called
 *
 * @param visible show a PXAlertView while operation is in progress
 * @param callback a Completion Block that is called on operation finished. YES: PUK has been setted, NO otherwise
 */
- (void) setPeripheralPUK:(SclakPeripheral*)peripheral
                  visible:(BOOL)visible
                 callback:(PeripheralPUKBlock)callback;

#pragma mark - Sync

#if defined SCLAK_CORE_APP

/**
 * start the sync process using model and peripheral to retrive pin codes to update/remove from server
 * and then sync peripheral with bluetooth APIs
 * update server when sync is complete
 */
- (void) syncPinCodes:(Peripheral*)model
           peripheral:(SclakPeripheral*)peripheral
              visible:(BOOL)visible
              context:(UIViewController*)context
             callback:(BluetoothResponseErrorCallback)callback;

#else

/**
 * start the sync process using model and peripheral to retrive pin codes to update/remove from server
 * and then sync peripheral with bluetooth APIs
 * update server when sync is complete
 */
- (void) syncPinCodes:(Peripheral*)model
           peripheral:(SclakPeripheral*)peripheral
             callback:(BluetoothResponseErrorCallback)callback;

#endif

- (void) interruptSync;

#pragma mark - Unit Tests

- (void) generateRandomPins:(NSUInteger)number btcode:(NSString*)btcode;

@end
