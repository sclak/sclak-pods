//
//  SCKSettings.h
//  SclakApp
//
//  Created by Daniele Poggi on 07/02/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCKSettings : NSObject

@property (nonatomic, assign) BOOL debugServer;
@property (nonatomic, assign) BOOL moreLogs;
@property (nonatomic, strong) NSString *appVersion;

@property (nonatomic, assign) BOOL stopScanWhenConnect;
@property (nonatomic, assign) float scannerFrequency;

+ (SCKSettings*) defaultSettings;

@end
