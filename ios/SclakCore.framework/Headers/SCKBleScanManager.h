//
//  SCKBleScanManager.h
//  Pods
//
//  Created by Daniele Poggi on 15/12/2018.
//

#import <Foundation/Foundation.h>

@class Peripheral;
@class PPLDiscoveredPeripheral;

@interface SCKBleScanManager : NSObject

+ (instancetype) sharedInstance;

- (NSArray*) discoveredPeripherals;

- (BOOL) isScanning;
- (BOOL) isScanningForBtcode:(NSString*)btcode;

- (void) startScan;
- (void) startScan:(BOOL)myPeripheralsOnly;
- (void) startScanPeripheral:(NSString*)btcode;
- (void) restartScan;
- (void) stopScan;
- (void) stopScan:(BOOL)clearDiscoveredPeripherals;

/**
 * to be called after the user changes its privileges
 * - after a privilege activation
 * - after a privilege delete
 */
- (void) reloadMyPeripheralsFilter;

- (void) setScanMyPeripheralsOnly:(BOOL)myPeripheralsOnly;
- (void) setBtcodeFilter:(NSString*)btcode;

- (void) removeDiscoveredPeripherals;
- (void) removeDiscoveredPeripheral:(NSString*)btcode;

- (BOOL) isPeripheralAvailable:(Peripheral*)peripheral discoveredPeripheral:(PPLDiscoveredPeripheral*)discoveredPeripheral;

@end
