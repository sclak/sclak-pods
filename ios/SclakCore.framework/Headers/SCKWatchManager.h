//
//  SCKWatchManager.h
//  SclakApp
//
//  Created by Daniele Poggi on 25/12/2016.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCKManagerProtocol.h"

extern NSString *const SCKWatchSettings;
extern NSString *const SCKWatchSettingsSecurityEnabled;
extern NSString *const SCKWatchSettingsSecurityLevel;
extern NSString *const SCKWatchSettingsLastSyncDate;

typedef void (^WatchUpdatedBlock) (BOOL success, NSError *error);

@interface SCKWatchManager : NSObject<SCKManagerProtocol>

#pragma mark - Shared Instance

+ (SCKWatchManager*) sharedInstance;

#pragma mark - Get Watch Settings

/**
 * a dictionary containing iWatch Settings setted from SCLAK APP:
 * - Security enabled: if security level is turned on
 * - Security level: level of security (different time values)
 */
+ (NSDictionary*) getWatchSettings;

#pragma mark - Sync Watch Data

/**
 * to be called when peripherals are updated
 * so that Watch data is updated
 */
- (void) sync;

/**
 * to be called when peripherals are updated
 * so that Watch data is updated
 * callback called when operation is finished, with success / error
 */
- (void) syncCallback:(WatchUpdatedBlock)callback;

@end
