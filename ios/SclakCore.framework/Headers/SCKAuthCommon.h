//
//  SCKAuthCommon.h
//  Pods
//
//  Created by Developer on 30/11/18.
//

typedef NS_ENUM(NSUInteger, AuthReferrer) {
    AuthReferrerTutorial = 1,
    AuthReferrerInvitation = 2,
    AuthReferrerInstallation = 3
};

typedef NS_ENUM(NSUInteger, AuthControllerType) {
    AuthControllerTypeChangePassword = 1,
    AuthControllerTypePasswordForgot = 2,
    AuthControllerTypeAccountVerification = 3
};

//typedef NS_ENUM(NSUInteger, ChangePasswordControllerType) {
//    ChangePassword = 1,
//    PasswordForgot = 2
//};

//typedef NS_ENUM(NSUInteger, PasswordForgotControllerType) {
//    ControllerTypeAccountVerification = 1,
//    ControllerTypePasswordForgot = 2
//};
