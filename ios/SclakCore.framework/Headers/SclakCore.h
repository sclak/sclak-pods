//
//  SclakCore.h
//  SclakCore
//
//  Created by Francesco Mantello on 02/07/18.
//  Copyright (c) 2018 Sclak. All rights reserved.
//

// Constants
#import "SclakCore+Constants.h"

#if defined SCLAK_CORE_APP

// Alert View
#import "PXAlertView.h"

#endif

// Extensions
#import "NSCalendar+Utils.h"
#import "CLLocationManager+blocks.h"

// Access Sync Manager
#import "SCKSyncAccessManager.h"

// Managers (installation)
#import "SCKPeripheralInstallationManager.h"

// Realm configuration
#import "SCKRealm.h"

// GPS Manager
#import "SCKGPSManager.h"

// phone prefix manager
#import "SCKPhonePrefixManager.h"

// bluetooth
#import "SCKPeripheralMapper.h"

// Models
#import "SCKPeripheralUsage.h"

// Alert Manager
#import "SCKAlertManager.h"

// Badge Manager
#import "SCKBadgeManager.h"

// Location Manager
#import "SCKLocationManager.h"

// Password Manager
#import "SCKPasswordManager.h"

// Security Manager
#import "SCKSecurityManager.h"

// Managers (auth)
#import "SCKAuthManager.h"

// Managers (data)
#import "SCKDataManager.h"

// Managers (sync)
#import "SCKPinSyncManager.h"

// Managers (usage)
#import "SCKPeripheralUsageManager.h"

// Managers (privilege)
#import "SCKPrivilegeActivationManager.h"
#import "SCKPrivilegeActivationLinkManager.h"

// Managers (watch)
#import "SCKWatchManager.h"

// Settings
#import "SCKSettings.h"

// Ble Scanner Manager
#import "SCKBleScanManager.h"

// Battery Sync Manager
#import "SCKBatterySyncManager.h"

// Event Logger
#import "SCKEventLogger.h"
