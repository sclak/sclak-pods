//
//  SclakSDK.h
//  SclakSDK
//
//  Created by Daniele Poggi on 24/01/2017.
//  Copyright © 2017 SCLAK S.p.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SclakFoundation/SclakFoundation.h>
#import <SclakCore/SclakCore.h>
#import <SclakBle/SclakBle.h>
#import <SclakFacade/SclakFacade.h>

@class ResponseObject;
@class Peripheral;
@class Peripherals;
@class User;

// API CALLBACKS
typedef void(^LoginCallback)(BOOL success, User *user, ResponseObject *failureObject);
typedef void(^PeripheralsCallback)(BOOL success, NSArray<Peripheral*> *peripherals, ResponseObject *failureObject);

/**
 * SclakSDK main interface. It's a Singleton Class
 */
@interface SclakSDK : NSObject

/*!
 @abstract
 retrieve the SclakSDK singleton shared instance
 @return the SclakSDK shared instance
 */
+ (SclakSDK*) sharedInstance;

/*!
 @abstract
 Call this method from the [UIApplicationDelegate application:didFinishLaunchingWithOptions:]
 Initialize using project id, api token, secret.
 
 @param key The API key can be found in Project Settings under API Keys & Usage on Sclak Admin.
 @param secret The API secret can be found in Project Settings under API Keys & Usage on Sclak Admin.
 */
- (void) provideAPIKey:(NSString *)key APISecret:(NSString *)secret;

/*!
 @abstract
 log sclak user in with its username (email or phone number) and password
 if user is logged in, SDK statefully remembers the user and keep using its data
 
 @param username username (email or phone number)
 @param password password
 @param callback API callback, may return success or error with result
 */
- (void) loginWithUsername:(NSString *)username password:(NSString *)password callback:(LoginCallback)callback;

/*!
 @return the SclakSDK User obtained with the API call loginWithUsername:password:callback
 */
- (User*) getUser;

/*!
 @abstract
 retrieve logged user peripherals (keys)
 
 @param callback API callback, may return success with data or error with error_code and error_message (ResponseObject model)
 if the callback returns a success, the "id" result contains a "Peripherals" model
 if the callback returns a failure, the "id" result contains a "ResponseObject" model with error_code and error_message
 */
- (void) getPeripheralsCallback:(PeripheralsCallback)callback;

/*!
 @abstract
 check if an access can be done on peripheral at specified time
 
 @param peripheral one of the models received with getKeysCallback: API
 @param date specific opening time, can be in the past or future. pass "nil" if to use current timestamp
 @param callback API callback, may return success or error and response that specifies why the access cannot be done at specified time
 */
- (void) accessControlCheckForPeripheral:(Peripheral*)peripheral customDate:(NSDate*)date callback:(PrivilegeEvaluationCallback)callback;

/*!
 @abstract
 request an open command on peripheral
 before the request is sent, an access control check with current timestamp is performed. if the access control check fails,
 the open command is not issued.
 
 @param peripheral one of the models received with getKeysCallback: API
 @param evaluationCallback API callback, may return success or error and response that specifies why the access cannot be done at specified time
 @param responseCallback API callback, called after evaluationCallback, may return success or error and response that specifies the BLE communication error
 */
- (void) openPeripheral:(Peripheral*)peripheral
     evaluationCallback:(PrivilegeEvaluationCallback)evaluationCallback
       responseCallback:(BluetoothResponseErrorCallback)responseCallback;

/*!
@abstract
request a close command on peripheral
before the request is sent, an access control check with current timestamp is performed. if the access control check fails,
the close command is not issued.

@param peripheral one of the models received with getKeysCallback: API
@param evaluationCallback API callback, may return success or error and response that specifies why the access cannot be done at specified time
@param responseCallback API callback, called after evaluationCallback, may return success or error and response that specifies the BLE communication error
*/
- (void) closePeripheral:(Peripheral*)peripheral
      evaluationCallback:(PrivilegeEvaluationCallback)evaluationCallback
        responseCallback:(BluetoothResponseErrorCallback)responseCallback;

#pragma mark - SDK usage without user

/*!
 @abstract
 initialize the SDK without a User
 */
- (void) initializeWithoutUser;

/*!
 @return the list of locks currently managed by the SDK
 */
- (NSArray<Peripheral*>*) getPeripherals;

/*!
 @return the peripheral that has this specific btcode
 */
- (Peripheral*) getPeripheralWithBtcode:(NSString*)btcode;

/*!
 @param peripherals the object containing the new list of Peripherals to be loaded inside the SDK
 */
- (void) setPeripherals:(Peripherals*)peripherals;

@end

