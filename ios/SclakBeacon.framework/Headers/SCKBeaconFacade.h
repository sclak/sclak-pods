//
//  SCKBeaconManager.m
//  SclakBeacon
//
//  Created by Daniele Poggi on 11/11/13.
//  Copyright (c) 2013 Toodev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AudioToolbox/AudioToolbox.h>

#import <SclakBle/PPLBluetoothCallbacks.h>
#import <SclakCore/CLLocationManager+blocks.h>
#import <SclakCore/SCKLocationManager.h>

#import "SCKMotionGesturesController.h"

// iBeacon features
#define ENABLE_GPS_REGION                               NO
#define ENABLE_GENERIC_REGION                           NO

#define MORE_LOGS_HELLO                                 NO
#define MORE_LOGS_START                                 NO
#define MORE_LOGS_RANGING                               NO
#define MORE_LOGS_AUTO                                  NO

#define DEBUG_DISABLE_FIRED_CHECK                       NO
#define DEBUG_OPEN_BYPASS_1_MINUTE                      NO

#define gpsRegionIdentifier                             @"sck_gps_region"
#define gpsRegionRadius                                 100 // meters
#define maxThresholdCounter                             5   // counter
#define regionAccuracyThreshold                         1.5 // meters
#define autoOpenResetTimeDifference                     10  // seconds

#define ENABLE_OPEN_IN_DETERMINE_STATE                  YES
#define ENABLE_RESET_BADGING                            YES
#define ENABLE_RESET_NEAR                               YES

// USER DEFAULTS
#define kUserDefaultsKnockSoundOn                       @"knock_sound_on"

@class Peripheral;
@class SCKPeripheralUsage;
@class PeripheralBeacon;

@interface SCKBeaconFacade : NSObject<SCKManagerProtocol, SCKLocationManagerDelegate> {
    
}

#pragma mark - Properties

@property (nonatomic, assign) BOOL openWithHomeButton;
@property (nonatomic, strong) NSMutableDictionary *peripheralOpenCache;
@property (nonatomic, assign) UIBackgroundTaskIdentifier backgroundTaskIdentifier;
@property (nonatomic, copy) void (^expirationHandler)(void);

@property (nonatomic, readonly, strong) SCKMotionGesturesController *motionGesturesController;

#pragma mark - Shared Instance

+ (SCKBeaconFacade*) sharedInstance;

- (void) setLocationManager:(CLLocationManager*)manager;

#pragma mark - Peripheral Beacon Accessories Methods

- (PeripheralBeacon*) getOrCreatePeripheralBeacon:(NSString*)btcode;

#pragma mark -

- (void) start;
- (void) stop;

#pragma mark -

- (BOOL) needToShowLocationAlert;
- (void) showLocationAlertOpenSettingsIfNeededCallback:(void(^)(BOOL enabled))callback;
- (void) showLocationAlertIfNeededCallback:(DidChangeAuthorizationStatusBlock)callback;
- (void) requestLocationManagerAuthorizationIfNeeded;

- (BOOL) needToShowBackgroundRefreshAlert;
- (void) showBackgroundRefreshAlertIfNeededCallback:(void(^)(BOOL enabled))callback;

#pragma mark - Alerts Methods (FM: ???)

- (void) alert:(NSString*)message;
- (void) errorSettingsAlert:(NSString*)message;

- (void) performAutoopenToPeripheral:(Peripheral*)peripheral;
- (void) performAutoopenToPeripheral:(Peripheral*)peripheral usage:(SCKPeripheralUsage*)usage callback:(BluetoothResponseErrorCallback)responseCallback;

@end
