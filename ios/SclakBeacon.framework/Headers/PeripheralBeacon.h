//
//  PeripheralBeacon.h
//  SclakApp
//
//  Created by Daniele Poggi on 08/10/2016.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PeripheralBeacon : NSObject

@property (nonatomic, assign) BOOL manualOpenFired;
@property (nonatomic, strong) NSDate *manualOpenTime;

@property (nonatomic, assign) BOOL autoOpenFired;
@property (nonatomic, strong) NSDate *autoOpenTime;

@property (nonatomic, assign) BOOL knockKnockFired;
@property (nonatomic, strong) NSDate *knockKnockTime;

@end
