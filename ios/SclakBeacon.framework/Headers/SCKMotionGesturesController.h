//
//  KnockController.h
//  SclakApp
//
//  Created by albi on 16/09/15.
//  Copyright © 2015 Sclak. All rights reserved.
//

#define MORE_LOGS_KNOCK                 YES
#define DEFAULT_KNOCK_FREQUENCY         15.0f
#define KNOCK_DETECTOR_FREQUENCY_KEY    @"KNOCK_DETECTOR_FREQUENCY_KEY"

@class SCKBeaconFacade;
@class Peripheral;

@interface SCKMotionGesturesController : NSObject

#pragma mark - Properties

@property (nonatomic, readonly, assign) BOOL started;
@property (nonatomic, strong) Peripheral *nearestPeripheral;

#pragma mark - Methods

- (void) stop;
- (void) clearSemaphores;

#pragma mark - Shake and open

- (BOOL) isShakeAndOpenStarted;
- (void) startShakeAndOpenCallback:(void (^)(void))callback;
- (void) stopShakeAndOpen;

#pragma mark - Hook Delegate Methods

- (void) didDetermineState:(CLRegionState)state forRegion:(CLRegion*)region;
- (void) didEnterRegion:(CLRegion*)region;
- (void) didExitRegion:(CLRegion*)region;
- (void) didRangeBeacons:(NSArray*)beacons inRegion:(CLBeaconRegion*)region;
- (void) rangingBeaconsDidFailForRegion:(CLBeaconRegion*)region withError:(NSError*)error;

@end
