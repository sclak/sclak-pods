//
//  SCKKnockAnalyzer.h
//  SclakApp
//
//  Created by Daniele Poggi on 25/04/16.
//  Copyright (c) 2016 SCLAK s.r.l. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>

#define kKnockDetectedNotification @"kKnockDetectedNotification"
#define kKnockKnockDetectedNotification @"kKnockKnockDetectedNotification"

@interface KnockAnalyzer : NSObject

+ (KnockAnalyzer*) sharedAnalyzer;

- (void) startAnalyzerActivityCallback:(void (^)(BOOL ready))activityCallback;
- (void) startKnockEngineCallback:(void (^)(BOOL finished))callback;
- (void) stopAnalyzing;
- (void) stopAnalyzingKnock;

@end