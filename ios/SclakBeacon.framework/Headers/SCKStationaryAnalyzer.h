//
//  SCKStationaryAnalyzer.h
//  SclakApp
//
//  Created by Daniele Poggi on 27/07/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import <CoreMotion/CoreMotion.h>

@interface SCKStationaryAnalyzer : NSObject

#pragma mark - Shared Instance

+ (SCKStationaryAnalyzer*) sharedAnalyzer;

#pragma mark - 

- (void) startAnalyzerActivityCallback:(void (^)(BOOL ready))activityCallback;
- (void) stopAnalyzing;

@end
