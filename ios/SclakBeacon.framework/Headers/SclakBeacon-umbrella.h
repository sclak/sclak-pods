#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "SclakBeacon.h"
#import "AccelerometerFilter.h"
#import "HTKnockDetector.h"
#import "KnockAnalyzer.h"
#import "SCKMotionGesturesController.h"
#import "SCKStationaryAnalyzer.h"
#import "SCKToneGeneratorController.h"
#import "SCKBeaconFacade.h"
#import "PeripheralBeacon.h"

FOUNDATION_EXPORT double SclakBeaconVersionNumber;
FOUNDATION_EXPORT const unsigned char SclakBeaconVersionString[];

