//
//  NSObject+AssociatedObjectSupport.h
//  SclakApp
//
//  Created by Daniele Poggi on 04/03/2018.
//  Copyright © 2018 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (AssociatedObjectSupport)

- (id) associatedObject:(char*)key;
- (void) setAssociatedObject:(id)object forKey:(char*)key;

@end
