//
//  NSNumber+Helper.h
//  SCLAK
//
//  Created by Daniele Poggi on 17/10/14.
//  Copyright (c) 2014 SCLAK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Helper)

- (NSString*) priceValue;

- (NSTimeInterval)NSTimeIntervalValue;

- (id)initWithNSTimeInterval:(NSTimeInterval)value;

+ (NSNumber *)numberWithNSTimeInterval:(NSTimeInterval)value;


@end
