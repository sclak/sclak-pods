//
//  Base62.h
//  SclakFoundation
//
//  Created by Daniele Poggi on 31/07/2019.
//  Copyright © 2019 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Base62 : NSObject

+ (NSString*) formatNumber:(NSUInteger)n usingAlphabet:(NSString*)alphabet;

+ (NSString*) formatNumber:(NSUInteger)n toBase:(NSUInteger)base;

@end
