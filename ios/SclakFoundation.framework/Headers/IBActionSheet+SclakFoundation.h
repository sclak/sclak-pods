//
//  IBActionSheet+SclakFoundation.h
//  AFNetworking
//
//  Created by Developer on 12/11/18.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol SCKActionSheetThemeDescriptor<NSObject>

+ (UIFont*) font;
+ (UIColor*) backgroundColor;

+ (UIFont*) titleFont;
+ (UIColor*) titleTextColor;
+ (UIColor*) titleBackgroundColor;

+ (float) buttonHeight;
+ (float) buttonPadding;
+ (UIColor*) buttonTextColor;
+ (UIColor*) buttonBackgroundColor;
+ (UIColor*) buttonHighlightTextColor;
+ (UIColor*) buttonHighlightBackgroundColor;
+ (CGFloat) buttonBorderWidth;
+ (UIColor*) buttonBorderColor;

+ (UIColor*) cancelButtonTextColor;
+ (UIColor*) cancelButtonBackgroundColor;
+ (UIColor*) cancelButtonHighlightTextColor;
+ (UIColor*) cancelButtonHighlightBackgroundColor;
+ (UIColor*) cancelButtonBorderColor;

+ (UIColor*) transparentViewColor;

@end

//@interface IBActionSheet (SclakFoundation) <SCKActionSheetThemeDescriptor>
//@end
