//
//  iOSBlocks.h
//  iOS Blocks
//
//  Created by Ignacio Romero Zurbuchen on 2/12/13.
//  Copyright (c) 2011 DZN Labs.
//  Licence: MIT-Licence
//

#if TARGET_OS_WATCH || TARGET_IS_WATCH_EXTENSION

// CoreLocation
#import "CLLocationManager+iOSBlocks.h"

#else

// UIKit
#import "UIPickerView+iOSBlocks.h"
#import "UIPopoverController+iOSBlocks.h"
#import "UINavigationController+iOSBlocks.h"

// MessageUI
#import "MFMailComposeViewController+iOSBlocks.h"
#import "MFMessageComposeViewController+iOSBlocks.h"

// Foundation
#import "NSURLConnection+iOSBlocks.h"

// CoreLocation
#import "CLLocationManager+iOSBlocks.h"

#endif


