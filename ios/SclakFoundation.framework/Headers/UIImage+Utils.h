//
//  UIImage+Utils.h
//  SclakApp
//
//  Created by albi on 11/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utils)

+ (UIImage *)imageWithColor:(UIColor *)color cornerRadius:(CGFloat)cornerRadius;

@end
