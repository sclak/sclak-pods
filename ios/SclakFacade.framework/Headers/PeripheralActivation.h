//
//  PeripheralActivation.h
//  SclakFacade
//
//  Created by Daniele Poggi on 19/09/15.
//  Copyright © 2015 Sclak. All rights reserved.
//

#import "Coupon.h"
#import "Peripheral.h"

@interface PeripheralActivation : ResponseObject

@property (nonatomic, strong) Coupon *coupon;
@property (nonatomic, strong) Peripheral *peripheral;

@end