//
//  PeripheralBaseModel.h
//  SclakFacade
//
//  Created by Daniele Poggi on 01/08/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "Privilege.h"

@interface PeripheralBaseModel : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *externalId;
@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, strong) NSArray<Optional> *groupTags;
@property (nonatomic, strong) NSMutableArray<Privilege, Optional> *sentPrivileges;
@property (nonatomic, strong) NSMutableArray<Privilege, Optional> *privileges;

// SCAN TO OPEN QRCODES
@property (nonatomic, strong) NSMutableArray<ScanToOpen, Optional> *qrcodes;

- (BOOL) isPrivilegeGuest;
- (BOOL) isPrivilegeOwner;
- (BOOL) isPrivilegeAdmin;
- (BOOL) isPrivilegeInstaller;
- (BOOL) isPrivilegeSuperAdmin;
- (BOOL) isPrivilegesPendingReset;
- (BOOL) isEligibleForQrConfiguration;

- (BOOL) isOnlyInstaller;
- (BOOL) isOnlyGuest;

- (BOOL) hasAdminInvited;
- (BOOL) allPrivilegesDeleted;
- (BOOL) isDeleted;

- (BOOL) hasQrcodeConfigurations;

- (void) reloadGroupTags;

- (void) addPrivilege:(Privilege*)privilege;
- (BOOL) replacePrivilege:(Privilege*)privilege;

- (Privilege*) getPrivilegeForId:(NSNumber*)privilegeId;
- (Privilege*) getSentPrivilegeForId:(NSNumber*)privilegeId;
- (Privilege*) getPrivilegeForGroupTag:(NSString*)groupTag;

- (BOOL) can:(NSString*)action;
- (BOOL) can:(NSString*)action withData:(Constraint*)data;
- (BOOL) can:(NSString*)action withData:(Constraint*)data privilege:(Privilege**)privilege;

@end
