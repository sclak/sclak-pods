//
//  BwgGsmPreset.h
//  SclakFacade
//
//  Created by Daniele Poggi on 01/02/2020.
//  Copyright © 2020 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@protocol BwgGsmPreset

@end

@interface BwgGsmPresets : ResponseObject

@property (nonatomic, strong) NSArray<BwgGsmPreset> *list;

@end

@interface BwgGsmPreset : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *apn;
@property (nonatomic, strong) NSString<Optional> *username;
@property (nonatomic, strong) NSString<Optional> *password;

+ (instancetype) presetWithName:(NSString*)name
                            apn:(NSString*)apn
                       username:(NSString*)username
                       password:(NSString*)password;

@end
