//
//  PrivilegesMulti.h
//  SclakFacade
//
//  Created by Stefano Demattè on 23/05/2019.
//  Copyright © 2019 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "Privilege.h"
#import "PeripheralProtocols.h"

NS_ASSUME_NONNULL_BEGIN

@interface PrivilegesMulti : ResponseObject

@property (nonatomic, strong) NSArray<Privilege, Optional> *privileges;
@property (nonatomic, strong) NSArray<Peripheral, Optional> *peripherals;

@end

NS_ASSUME_NONNULL_END
