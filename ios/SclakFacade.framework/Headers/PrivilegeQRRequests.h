//
//  Peripherals.h
//  Sclak2
//
//  Created by albi on 06/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "PrivilegeQRRequest.h"

@protocol PrivilegeQRRequest
@end

@interface PrivilegeQRRequests : ResponseObject

@property (nonatomic, strong) NSArray *sectionNames;
@property (nonatomic, strong) NSDictionary *sections;


@end
