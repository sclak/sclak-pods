//
//  MemorySetting.h
//  SclakFacade
//
//  Created by Daniele Poggi on 04/10/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@interface MemorySetting : ResponseObject

@property (nonatomic, strong) NSNumber *maxRegistryEntries;
@property (nonatomic, strong) NSNumber *maxPinEntries;
@property (nonatomic, strong) NSNumber *maxAccessLogEntries;

@end
