//
//  GetPeripheralsOptions.h
//  SclakApp
//
//  Created by Daniele Poggi on 09/06/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface GetPeripheralsOptions : JSONModel

@property (nonatomic, strong) NSArray<Optional> *btcodes;
@property (nonatomic, strong) NSString<Optional> *groupTags;

@property (nonatomic, strong) NSString<Optional> *lot;
@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, strong) NSNumber<Optional> *userId;
@property (nonatomic, strong) NSNumber<Optional> *pageNumber;
@property (nonatomic, strong) NSNumber<Optional> *pageSize;

@property (nonatomic, strong) NSNumber<Optional> *editTime;

@property (nonatomic, strong) NSNumber<Optional> *exportPrivileges;
@property (nonatomic, strong) NSNumber<Optional> *exportFeatures;
@property (nonatomic, strong) NSNumber<Optional> *exportSettings;
@property (nonatomic, strong) NSNumber<Optional> *exportFirmware;
@property (nonatomic, strong) NSNumber<Optional> *exportStatistics;
@property (nonatomic, strong) NSNumber<Optional> *exportFirmwareUpdate;

@property (nonatomic, strong) NSString<Optional> *peripheralTypeCode;

@end
