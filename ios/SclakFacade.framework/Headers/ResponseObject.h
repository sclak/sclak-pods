//
//  ResponseObject.h
//  Sclak2
//
//  Created by albi on 05/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ResponseObject : JSONModel

@property (nonatomic, strong) NSNumber <Optional> *errorCode;
@property (nonatomic, strong) NSString <Optional> *errorMessage;
@property (nonatomic, strong) NSNumber <Optional> *responseTime;

+ (instancetype) responseWithErrorCode:(NSUInteger)code message:(NSString*)message;

@end
