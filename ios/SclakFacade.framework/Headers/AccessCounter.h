//
//  AccessCounter.h
//  SclakFacade
//
//  Created by Daniele Poggi on 25/03/2019.
//  Copyright © 2019 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "SCKFacadeCallbacks.h"

@interface AccessCounter : JSONModel

@property (nonatomic, strong) NSString *btcode;
@property (nonatomic, strong) NSNumber *datetime;
@property (nonatomic, strong) NSNumber *accessCounter;

+ (void) syncToServer:(NSArray*)logs callback:(ResponseObjectCallback)callback;

@end
