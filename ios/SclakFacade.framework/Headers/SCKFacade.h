//
//  SCKFacade.h
//  Sclak2
//
//  Created by albi on 04/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "SCKServerDate.h"
#import "SCKPinManager.h"
#import "SCKNetworkFacade.h"

// Callbacks
#import "SCKFacadeCallbacks.h"

// Models
#import "Common.h"
#import "SCKFacadeHeaders.h"

#define F                                       [SCKFacade sharedFacade]

#define authTokensUrl                           @"/auth_tokens"
#define groupsUrl                               @"/groups"
#define updateUserUrl                           @"/users"
#define userEmailsUrl                           @"/users_emails"
#define activateUserEmailUrl                    @"/users_emails/activate"
#define sendActivationCodeUrl                   @"/users_emails/send_activation_code"
#define generateResetPasswordUrl                @"/users/generate_reset_password_code"
#define changePasswordUrl                       @"/users/change_password"
#define resetPasswordUrl                        @"/users/reset_password"
#define check_operative_code_post               @"/users/check_operative_code_sha"
#define activatePeripheralUrl                   @"/peripherals/%@/activate"
#define peripheralsUrl                          @"/peripherals"
#define peripheralUrl                           @"/peripherals/%@"
#define peripheralSecretCodeUrl                 @"/peripherals/%@/get_secret_code"
#define peripheralAvailableBtcodeUrl            @"/peripherals/available_btcode"
#define peripheralSetSecretCodeUrl              @"/peripherals/%@/set_secret_code"
#define peripheralGroupsUrl                     @"/peripherals_groups"
#define privilegesIdUrl                         @"/privileges/%@"
#define privilegesUrl                           @"/privileges"
#define privilegeUsersUrl                       @"/privileges/sent"
#define couponUrl                               @"/coupons"
#define devicesUrl                              @"/devices"
#define privilegeConfirm                        @"/privileges/%@/confirm"
#define privilegeDeny                           @"/privileges/%@/deny"
#define privilegeSetInviteExpireTime            @"/privileges/%@/set_invite_expire_time"
#define requireConfirm                          @"/privileges/%@/require_confirm"
#define purchasesUrl                            @"/purchases"
#define privilegesDisable                       @"/privileges/%@/disable"
#define peripheralsLog                          @"/peripherals/%@/log_usage"
#define peripheralsUsagesLog                    @"/peripherals/log_usages"
#define peripheralsUsages                       @"/peripherals/%@/usages"
#define firmwaresUrl                            @"/firmwares"
#define latestfirmwareUrl                       @"/firmwares/last_version_for_peripheral_type/"
#define peripheralStatusUrl                     @"/peripherals/%@/status"

// pin codes
#define peripheralPinCodes                      @"/peripherals/%@/pin_codes"
#define pinCodesUrl                             @"/pin_codes"
#define resetPinCodesUrl                        @"/pin_codes/%@/reset"
#define pinCodeBulkUrl                          @"/pin_codes/bulk"
#define peripheralPinCodeAvailable              @"/peripherals/%@/available_pin_code"

// pair unpair
#define pairPeripheral                          @"/peripherals/%@/pair/%@"
#define unpairPeripheral                        @"/peripherals/%@/unpair"

// template
#define privilegesEmailTemplate                 @"/privileges/email_template"

// tiny url
//#define tinyUrl                                 @"/urls/tiny_activation_code"
#define tinyUrlService                          @"https://tinyurl.com/api-create.php?url=%@"

#define requestResetOperativeCodeUrl            @"/users/request_reset_operative_code"
#define resetOperativeCodeUrl                   @"/users/reset_operative_code"

#define privilegesActivateResetUrl              @"/privileges/%@/activate_reset"

#define timeUrl                                 @"/time"

// ERRORS
#define ERROR_INVALID_TOKEN                     2
#define ERROR_INVALID_CREDENTIALS               5
#define ERROR_DEVICE_NOT_FOUND                  30
#define ERROR_ATTEMPTS_USED                     32
#define ERROR_RESET_PASSWORD_ATTEMPTS_USED      44
#define ERROR_DEVICE_DISABLED                   45
#define ERROR_USER_EMAIL_NOT_ACTIVE             47
#define ERROR_PIN_CODES_NOT_AVAILABLE           71
#define ERROR_PUK_CODE_EXIST                    75
#define ERROR_LOGIN_FORCE_LOGOUT                64
#define ERROR_PIN_CODE_EXIST                    67

// USER DEFAULTS
#define PREFS_AUTH_USERID                        @"PREFS_AUTH_USERID"
#define PREFS_PUSH_TOKEN                         @"PREFS_PUSH_TOKEN"
#define PREFS_PUSH_ENABLED                       @"PREFS_PUSH_ENABLED"
#define kStandardUserDefaultsAlreadyLoggedInOnce @"kStandardUserDefaultsAlreadyLoggedInOnce"
#define kStandardUserDefaultsAskCredentialBackup @"kStandardUserDefaultsAskCredentialBackup"
#define kSettingsDictKey                         @"settingsDictKey"
#define kPasscode                                @"passcode"
#define kLockList                                @"lockList"
#define kVirtualKeypad                           @"kVirtualKeyboard"
#define kTouchID                                 @"touchID"
#define kShowHints                               @"showHints"
#define kShowLowBattery                          @"kShowLowBattery"
#define kHideDisabledSclak                       @"kHideDisabledSclak"
#define kActivationCode                          @"activationCode"
#define kPrivilegeId                             @"privilegeId"
#define kSettingsHideSclakAddresses              @"kSettingsHideSclakAddresses"
#define kCanShowQr                               @"kCanShowQr"
// this is a dictionary saved in Standard User Defaults with this structure:
// {"btcode1": "1", "btcode2": "1"}
// for each btcode you can find a boolean NSNumber that represents a usage has been already done
// the btcode will not be present for a peripheral never used
// the btcode is removed when the privilege is deleted
#define kPeripheralUsageFirstUseCheck            @"peripheral_usage_first_use_check"

// NOTIFICATIONS
#define kFacadeDidLoginNotification                  @"kFacadeDidLoginNotification"
#define kFacadeDidLogoutNotification                 @"kFacadeDidLogoutNotification"
#define kFacadePeripheralsUpdatedNotification        @"kFacadePeripheralsUpdatedNotification"
#define kFacadeCleanNotification                     @"kFacadeCleanNotification"
#define kDeviceDisabledNotification                  @"kDeviceDisabledNotification"
#define kDeviceLoginForceLogoutNotification          @"kDeviceLoginForceLogoutNotification"

#define kFacadePeripheralsCacheChangedNotification   @"kFacadePeripheralsCacheChangedNotification"

typedef NS_ENUM(NSUInteger, TinyUrlType) {
    TinyUrlTypeActivationCode,
    TinyUrlTypeResetKey,
};

// KVO constants
static void *KVOGettingData = &KVOGettingData;
static void *KVOPeripherals = &KVOPeripherals;
static void *KVOPeripheralGroups = &KVOPeripheralGroups;

@interface SCKFacade : SCKNetworkFacade

#pragma mark - Facade

+ (instancetype) sharedFacade;

#define VERIFICATION_CODE_FIELD_LENGTH 1
#define VERIFICATION_CODE_NUMBER_FIELD 4
#define VERIFICATION_CODE_LENGTH VERIFICATION_CODE_FIELD_LENGTH * VERIFICATION_CODE_NUMBER_FIELD

/**
 * check if deserializeCaches has been already called
 */
@property (nonatomic, assign) BOOL ready;

// CACHES
@property (nonatomic, strong) NSArray *users;
    
@property (nonatomic, strong) NSMutableArray *peripherals;
@property (nonatomic, strong) NSMutableDictionary *peripheralsCache;
    
@property (nonatomic, strong) NSMutableArray *peripheralGroups;
@property (nonatomic, strong) NSMutableDictionary *peripheralGroupsCache;

@property (nonatomic, strong) NSMutableArray<SCKPeripheralType*> *peripheralTypes;
@property (nonatomic, strong) NSMutableDictionary<NSNumber<Optional>*, SCKPeripheralType*> *peripheralTypesCache;

@property (nonatomic, strong) NSMutableArray *privileges;

@property (nonatomic, strong) NSMutableArray *user_privileges;

@property (nonatomic, strong) NSMutableArray *places;

@property (nonatomic, strong) NSDictionary *settings;
@property (nonatomic, strong) NSDictionary *iapProducts;
@property (nonatomic, strong) Firmware *latestFirmware;

@property (nonatomic, strong) Project *project;

// INSTALLER
@property (nonatomic, strong) NSMutableArray *installerEnabledIndexes;

// LOGIN ERROR CALLBACK
@property (nonatomic, strong) LoginErrorCallback loginErrorCallback;

#pragma mark - Caches APIs

/**
 * clear the facade completely, back to initial state
 */
- (void) clear;

/**
 *  deserialize User, Profile, Peripherals, User Devices, Firmware caches
 */
- (void) deserializeCaches;
- (void) clearCaches;

#pragma mark User APIs

- (void) getUserProfileCallback:(ResponseObjectCallback)callback;
- (void) updateUserProfile:(User*)user callback:(ResponseObjectCallback)callback;
- (void) addUserEmail:(NSString*)email callback:(ResponseObjectCallback)callback;
- (void) deleteUserEmail:(NSString*)email callback:(ResponseObjectCallback)callback;
- (void) activateUser:(NSString*)email activationCode:(NSString*)code callback:(ResponseObjectCallback)callback;
- (void) sendActivationCodeForEmail:(Email*)email callback:(ResponseObjectCallback)callback;
- (void) requestInviteExpireTimeForPrivilegeId:(NSNumber*)privilegeId callback:(ResponseObjectCallback)callback;
- (void) logoutCallback:(ResponseObjectCallback)callback;

#pragma mark Peripheral APIs

- (void) getPeripheralsCallback:(ResponseObjectCallback)callback;
- (void) getPeripheralsCallback:(ResponseObjectCallback)callback notify:(BOOL)notify;
- (void) getPeripheralsWithEditTime:(NSNumber*)editTime callback:(ResponseObjectCallback)callback notify:(BOOL)notify;
- (void) getPeripheralsWithOptions:(GetPeripheralsOptions*)options callback:(ResponseObjectCallback)callback;

- (BOOL) hasPeripheralWithBtcode:(NSString*)btcode;

- (Peripheral*) getPeripheralWithId:(NSNumber*)pid;
- (Peripheral*) getPeripheralWithBtcode:(NSString*)btcode;
- (Peripheral*) getPeripheralWithHexCode:(NSString*)hexCode;

/**
 * override cache of peripherals with objects from Peripherals model
 * used when facade is initialized to be used without a User
 */
- (void) setFacadePeripherals:(Peripherals*)peripherals;

- (void) getPeripheralWithBtcode:(NSString*)btcode enableCache:(BOOL)enableCache callback:(ResponseObjectCallback)callback;
- (void) getPeripheralWithBtcode:(NSString*)btcode andConfirmCode:(NSString*)code enableCache:(BOOL)enableCache callback:(ResponseObjectCallback)callback;
- (void) getSecretForPeripheral:(Peripheral*)peripheral sequence:(NSNumber*)sequence callback:(ResponseObjectCallback)callback;
    
/**
 * activate a peripheral with a code
 *
 * @param btcode - the peripheral btcode
 * @param code - the coupon code
 * @return model of kind Activation if success, ResponseObject otherwise
 */
- (void) activatePeripheralWithBtcode:(NSString*)btcode andActivationCode:(NSString*)code callback:(ResponseObjectCallback)callback;
- (void) updatePeripheral:(Peripheral*)peripheral callback:(ResponseObjectCallback)callback;
- (void) updatePeripheral:(Peripheral*)peripheral writeCache:(BOOL)writeCache callback:(ResponseObjectCallback)callback;

- (Peripheral*) getPeripheralWithPrivilegeId:(NSNumber*)privilegeId;

/**
 * btcode: la periferica da mettere in pair con lo sclak (e.g. tastiera)
 * destinationBtcode: il btcode dello sclak
 */
- (void) pairPeripheralBtcode:(NSString*)btcode withDestinationBtcode:(NSString*)destinationBtcode callback:(ResponseObjectCallback)callback;
- (void) unpairPeripheralBtcode:(NSString*)btcode callback:(ResponseObjectCallback)callback;
- (void) getAvailablePinCodeForBtCode:(NSString*)btcode callback:(ResponseObjectCallback)callback;

- (void) getPeripheralTypesWithOptions:(NSDictionary*)options callback:(ResponseObjectCallback)callback;

/**
* write current F.peripheralGroups cache to file
*/
- (void) writePeripheralGroupsCache;

/**
 * write current F.peripherals cache to file
 */
- (void) writePeripheralsCache;

/**
 * write current F.peripheralTypes cache to file
 */
- (void) writePeripheralTypesCache;

/**
 * set new peripheral cache. KVO observers are called once at the end
 */
- (void) setPeripheralsCache:(NSArray<Peripheral>*)peripheralList writeCache:(BOOL)write;

/**
 * update the peripheral cache by adding or updating the peripherals in list
 */
- (void) updatePeripheralListCache:(NSArray<Peripheral>*)peripheralList writeCache:(BOOL)write;

/**
 * updates the peripheral cache by adding or updating peripheral model
 */
- (BOOL) updatePeripheralsCache:(Peripheral*)peripheral;
- (BOOL) updatePeripheralsCache:(Peripheral*)peripheral writeCache:(BOOL)write;


/**
 Retrieve the full SCKPeripheralType with the given id from the cache.
 */
- (SCKPeripheralType*) getPeripheralTypeWithId:(NSNumber*)peripheralTypeId;

/**
 * set new peripheral types cache. KVO observers are called once at the end
 */
- (void) setPeripheralTypesCache:(NSArray<SCKPeripheralType*>*)peripheralTypesList writeCache:(BOOL)write;

/**
 * updates the peripheral types cache.
 */
- (BOOL) updatePeripheralTypesCache:(SCKPeripheralType*)peripheralType;
- (BOOL) updatePeripheralTypesCache:(SCKPeripheralType*)peripheralType writeCache:(BOOL)write;

/**
 * remove the peripheral with btcode from the cache, returns YES if the peripheral is successfully removed
 */
- (BOOL) removePeripheralFromCache:(Peripheral*)peripheral;

/**
 * removes from the cache all the peripherals that are in current cache but NOT in *all* array
 * if *all* array is null nothing is removed.
 */
- (void) updatePeripheralsCacheRemoveDeleted:(NSArray*)allPeripherals;

/**
 * Returns the status of the peripheral with the given btcode.
 */
- (void) getPeripheralStatus:(NSString*)btcode callback:(ResponseObjectCallback)callback;

#pragma mark Logout

/**
 * clear caches and post a notification that the user has been logged out
 */
- (void) logoutUser;
- (void) logoutUser:(NSUInteger)errorCode;

#pragma mark Peripheral Group APIs
    
- (PeripheralGroup*) getPeripheralGroupWithId:(NSNumber*)peripheralGroupId;
- (PeripheralGroup*) getPeripheralGroupWithTag:(NSString*)tag;
- (void) updatePeripheralGroupCache:(PeripheralGroup*)peripheralGroup;
- (BOOL) updatePeripheralGroupCache:(PeripheralGroup*)peripheralGroup writeCache:(BOOL)write;
- (void) removePeripheralGroupFromCache:(PeripheralGroup*)peripheralGroup;
    
#pragma mark Operative code APIs

- (void) checkOperativeCode:(NSString*)cypheredOperativeCode callback:(ResponseObjectCallback)callback;
- (void) postRequestResetOperativeCodeWithHash:(NSString*)opcodeHash email:(NSString*)email disableEmail:(BOOL)disableEmail disablePush:(BOOL)disablePush callback:(ResponseObjectCallback)callback;
- (void) postResetOperativeCodeWithHash:(NSString*)opcodeHash resetCode:(NSString*)resetCode disableMassEmail:(BOOL)disableMassEmail disableMassPush:(BOOL)disableMassPush callback:(ResponseObjectCallback)callback;

#pragma mark Coupons APIs

/**
 * get coupon model with code
 *
 * @param coupon - the coupon code
 * @return model of kind Coupon if success, ResponseObject otherwise
 */
- (void) getCoupon:(NSString*)coupon callback:(ResponseObjectCallback)callback;
- (void) createCoupon:(NSString*)couponType callback:(ResponseObjectCallback)callback;

#pragma mark - Privileges APIs

- (Privilege*) getPrivilegeWithId:(NSNumber*)privilegeId;

- (BOOL) insertPrivilegeInCache:(Privilege*)privilege;
- (BOOL) insertPrivilegeInCache:(Privilege*)privilege writeCache:(BOOL)serialize;

- (void) updatePrivilegeCache:(Privilege*)privilege;
- (void) updatePrivilegeCache:(Privilege*)privilege writeCache:(BOOL)serialize;
- (void) updatePrivilegesCache:(NSArray*)privileges writeCache:(BOOL)serialize;

/**
 * remove the privilege from local cache
 */
- (BOOL) removePrivilegeFromCache:(Privilege*)privilege;

- (void) getPrivilegeWithId:(NSNumber*)privilegeId callback:(ResponseObjectCallback)callback;
- (void) privilegeREST:(Privilege*)privilege action:(ActionREST)action callback:(ResponseObjectCallback)callback;
- (void) disablePrivilege:(Privilege*)privilege callback:(ResponseObjectCallback)callback;
- (void) putActivateReserForPrivilegeId:(NSNumber*)privilegeId callback:(ResponseObjectCallback)callback;

#pragma mark - Group APIs

- (void) manageGroupREST:(Group*)group action:(ActionREST)action callback:(ResponseObjectCallback)callback;

#pragma mark - Devices APIs

- (void) getDevicesCallback:(ResponseObjectCallback)callback;

#pragma mark - Entryphone

- (void) requireConfirmForPrivilegeId:(NSNumber*)privilegeId callback:(ResponseObjectCallback)callback;
- (void) requireConfirmForPrivilegeId:(NSNumber*)privilegeId btcode:(NSString*)btcode callback:(ResponseObjectCallback)callback;
- (void) actionForPrivilege:(PrivilegeAction)action forPrivilegeId:(NSNumber*)privilegeId callback:(ResponseObjectCallback)callback;

#pragma mark - Purchase

- (void) addPurchase:(PurchaseReceipt*)purchase callback:(ResponseObjectCallback)callback;
- (void) postPurchaseCoupon:(PostPurchaseCoupon*)model callback:(ResponseObjectCallback)callback;
- (void) getPurchasesCallback:(ResponseObjectCallback)callback;

#pragma mark - Firmware

- (void) getFirmwaresCallback:(ResponseObjectCallback)callback;
- (void) getFirmwaresWithCode:(NSString*)code callback:(ResponseObjectCallback)callback;
- (void) downloadFirmware:(Firmware*)model progress:(DownloadProgressBlock)downloadProgressBlock callback:(ResponseObjectCallback)callback;
- (void) getLatestFirmware:(NSString*)peripheralTypeCode callback:(ResponseObjectCallback)callback;

#pragma mark - Collaudo

- (void) getAvailableBtcodeWithFirmwareType:(NSNumber*)firmwareType callback:(ResponseObjectCallback)callback;
- (void) remoteInitSkWithBtode:(NSString*)btcode sequence:(NSNumber*)sequence romId:(NSString*)romId initialCode:(NSString*)initialCode callback:(ResponseObjectCallback)callback;

#pragma mark - Pin Code

- (void) getPinCodesForBtCode:(NSString*)btcode callback:(ResponseObjectCallback)callback;
- (void) putPinCode:(PinCode*)pinCode callback:(ResponseObjectCallback)callback;
- (void) postPinCode:(PinCode*)pinCode callback:(ResponseObjectCallback)callback;
- (void) postPinCodes:(NSArray*)codes forPeripheralId:(NSNumber*)peripheralId flags:(NSString*)flags command:(NSString*)command callback:(ResponseObjectCallback)callback;
- (void) resetPinCodes:(NSString*)btcode callback:(ResponseObjectCallback)callback;

#pragma mark - Email Template

- (void) getPrivilegeTemplateEmailWithCallback:(ResponseObjectCallback)callback;

#pragma mark - Tiny Url

- (void) tinyUrlForCouponCode:(NSString*)code
                     clearPuk:(NSString*)puk
                     clearPin:(NSString*)pin
                    clearPuks:(NSDictionary*)clearPuks
                    clearPins:(NSDictionary*)clearPins
                     pinGroup:(NSDictionary*)pinGroup
                         type:(TinyUrlType)type
                 webUrlNeeded:(BOOL)webUrlNeeded
                   webUrlTiny:(BOOL)webUrlTiny
                     callback:(ResponseObjectCallback)callback;

#pragma mark - Time

- (void) getTimeCallback:(ResponseObjectCallback)callback;

#pragma mark - Error Management

- (void) manageError:(ResponseObject*)responseObject task:(NSURLSessionDataTask*)task callback:(ResponseObjectCallback)callback;

#pragma mark - Log on Server

- (void) logServer:(NSString*)topic data:(NSDictionary*)data;
- (void) logServer:(NSString*)topic data:(NSDictionary*)data forceEnable:(BOOL)forceEnable;

@end
