//
//  PeripheralSettings.h
//  SclakFacade
//
//  Created by Daniele Poggi on 02/08/19.
//  Copyright © 2019 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "PeripheralSetting.h"

@interface PeripheralSettings : ResponseObject

@property (nonatomic, strong) NSArray<PeripheralSetting> *list;

@end
