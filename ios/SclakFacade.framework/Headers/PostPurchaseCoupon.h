//
//  PostPurchaseCoupon.h
//  SclakFacade
//
//  Created by Daniele Poggi on 25/01/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface PostPurchaseCoupon : JSONModel

@property (nonatomic, strong) NSString *btcode;
@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSString *couponCode;
@property (nonatomic, strong) NSNumber <Optional> *renewPurchaseId;

@end
