//
//  PeripheralReplacement.h
//  SclakFacade
//
//  Created by Daniele Poggi on 18/05/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "SCKFacadeCallbacks.h"

@class Peripheral;

@interface PeripheralReplacement : ResponseObject

@property(nonatomic, strong) NSNumber *id;
@property(nonatomic, strong) NSNumber<Optional> *installerId;
@property(nonatomic, strong) NSNumber<Optional> *replacedPeripheralId;
@property(nonatomic, strong) NSNumber<Optional> *replacementPeripheralId;
@property(nonatomic, strong) NSNumber<Optional> *status;
@property(nonatomic, strong) NSNumber<Optional> *beginTime;
@property(nonatomic, strong) NSNumber<Optional> *acceptTime;
@property(nonatomic, strong) NSNumber<Optional> *doneTime;

- (BOOL) isRequested;
- (BOOL) isAccepted;
- (BOOL) isCompleted;

+ (void) request:(Peripheral*)peripheral callback:(ResponseObjectCallback)callback;
+ (void) cancel:(Peripheral*)peripheral callback:(ResponseObjectCallback)callback;
+ (void) accept:(Peripheral*)peripheral callback:(ResponseObjectCallback)callback;
+ (void) replacePeripheral:(Peripheral*)source withPeripheral:(Peripheral*)destination callback:(ResponseObjectCallback)callback;

@end
