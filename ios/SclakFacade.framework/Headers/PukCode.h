//
//  PukCode.h
//  SclakFacade
//
//  Created by albi on 01/07/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "SCKFacadeCallbacks.h"

@protocol PukCode
@end

@interface PukCodes : ResponseObject

@property (nonatomic, strong) NSArray <PukCode, Optional> *list;

@end

@interface PukCode : ResponseObject

@property (nonatomic, strong) NSNumber <Optional> *id;
@property (nonatomic, strong) NSNumber <Optional> *userId;
@property (nonatomic, strong) NSNumber <Optional> *peripheralId;
@property (nonatomic, strong) NSString <Optional> *code;
@property (nonatomic, strong) NSString <Optional> *clearCode;
@property (nonatomic, strong) NSString <Optional> *btcode;

#pragma mark - Puk Code APIs

+ (void) getPuksCallback:(ResponseObjectCallback)callback;
+ (void) getPuks:(NSArray*)btcodes callback:(ResponseObjectCallback)callback;
+ (void) postPukCode:(PukCode*)pukCode clearCode:(NSString*)clearCode callback:(ResponseObjectCallback)callback;
+ (void) putPukCode:(PukCode*)pukCode callback:(ResponseObjectCallback)callback;
+ (void) deletePukCode:(NSNumber*)peripheralId callback:(ResponseObjectCallback)callback;

@end
