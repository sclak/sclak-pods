//
//  PrivilegeOption.h
//  SclakFacade
//
//  Created by albi on 02/12/15.
//  Copyright © 2015 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface PrivilegeOption : JSONModel

// instant key
@property (nonatomic, strong) NSNumber<Optional> *instantKey;

// security
@property (nonatomic, strong) NSNumber<Optional> *securityEnabled;
@property (nonatomic, strong) NSNumber<Optional> *securityLevel;

// push enabled
@property (nonatomic, strong) NSNumber<Optional> *pushNotifyEnabled;

// remote open/close enabled (bridge)
@property (nonatomic, strong) NSNumber<Optional> *remoteEnabled;

@end
