//
//  CodeBackup.h
//  SclakFacade
//
//  Created by Daniele Poggi on 04/02/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "SCKFacadeCallbacks.h"

@class Privilege;
@class Peripheral;

@protocol CodeBackup

@end

@interface CodeBackup : JSONModel

@property (nonatomic, strong) NSString *btcode;
@property (nonatomic, strong) NSString *group_tag;
@property (nonatomic, strong) NSNumber *privilege_id;

@property (nonatomic, strong) NSString *pin;
@property (nonatomic, strong) NSString *puk;

+ (CodeBackup*) createGroupPinCodeBackup:(NSNumber*)privilegeId groupTag:(NSString*)groupTag groupPin:(NSString*)groupPin;
+ (CodeBackup*) createPeripheralPinCodeBackup:(NSNumber*)privilegeId btcode:(NSString*)btcode pin:(NSString*)pin;
+ (CodeBackup*) createPeripheralPukCodeBackup:(NSNumber*)privilegeId btcode:(NSString*)btcode puk:(NSString*)puk;

+ (Privilege*) queryPrivilegeForBackup:(Peripheral*)peripheral;

#pragma mark - Server API

+ (void) backupCodes:(NSArray*)codes callback:(ResponseObjectCallback)callback;

@end
