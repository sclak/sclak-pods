//
//  TDNetworkFacade.h
//  SCLAK
//
//  Created by Daniele Poggi on 04/09/14.
//  Copyright (c) 2014 SCLAK. All rights reserved.
//

#import "SCKGenericFacade.h"
#import "SCKFacadeCallbacks.h"
#if !TARGET_OS_WATCH
#import "AFNetworkReachabilityManager.h"
#endif
#import "User.h"
#import "AuthToken.h"

// NETWORK
#define DEFAULT_DEBUG_POST_REQUESTS             YES
#define DEFAULT_DEBUG_POST_RESPONSES            YES
#define ERROR_DOMAIN                            @"com.sclak.error"

#define API_DOMAIN                              @"api.sclak.com"
#define API_DOMAIN_REACHABLE                    @"api.sclak.com"
#define API_DEVELOPMENT                         @"https://api-test.sclak.com"
#define API_INTEGRATION                         @"https://api-intgr.sclak.com"
#define API_PRODUCTION                          @"https://api.sclak.com"

#define DEFAULT_TIMEOUT                         60

// request headers
#define HEADER_AUTHORIZATION                    @"AUTHORIZATION"
#define HEADER_DEVICE_ID                        @"DEVICE-UNIQUE-ID"
#define HEADER_PROJECT_API_KEY                  @"project-api-key"

#if defined USE_DEVELOPMENT
    #define DEFAULT_API_URL                     API_DEVELOPMENT
#elif defined USE_INTEGRATION
    #define DEFAULT_API_URL                     API_INTEGRATION
#else
    #define DEFAULT_API_URL                     API_PRODUCTION
#endif

#define kAuthToken                              @"kAuthToken"
#define kAuthTokenExpiryDate                    @"kAuthTokenExpiryDate"

#define IS_LOGGED                               @"IS_LOGGED"

@interface SCKNetworkFacade : SCKGenericFacade

@property (nonatomic, assign) ApiConfig apiConfig;

// LOGGING
@property (assign, nonatomic) BOOL debugPostRequests;
@property (assign, nonatomic) BOOL debugPostResponses;

// FIELDS
@property (nonatomic, strong) NSString *apiKey;
@property (strong, nonatomic) NSString *apiUrl;
@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSNumber *userid;
@property (strong, nonatomic) User *user;

@property (strong, nonatomic) AFHTTPSessionManager *manager;
@property (strong, nonatomic) AFHTTPSessionManager *stringManager;
#if !TARGET_OS_WATCH
@property (nonatomic, strong) AFNetworkReachabilityManager *reachability;
#endif

- (void) setupManager;

/**
 * verifica se c'è la possibilità di fare una auto-autenticazione (login effettuata precedentemente)
 */
- (BOOL) canAuthenticate;

/**
 * verifica se il token è presente in facade, pertanto la login è stata effettuata
 */
- (BOOL) isAuthenticated;

/**
 * verifica se c'è un profilo utente in facade e se questo non possiede email e numero di telefono (ghost)
 */
- (BOOL) isGhost;

/**
 * set api key and secret. PROJECT_API_KEY header will be setted for each request after this call
 */
- (void) setApiKey:(NSString*)apiKey apiSecret:(NSString*)apiSecret;

/**
 *  Check for internet connection
 *
 *  @return
 */
- (ResponseObject*) checkForInternetConnection;

/**
 *  Generic REST API call
 *
 *  @param url
 *  @param method
 *  @param params
 *  @param callback
 */
- (void) restCall:(NSString*)url method:(NSString*)method params:(NSDictionary*)params callback:(ResponseObjectCallback)callback;

/**
 *  Generic REST API call with response object of class "ResponseClass" and API error management
 *
 *  This is the preferred method for making API calls to SCLAK server
 *
 *  @param url web API url controller to call, e.g. "peripherals"
 *  @param method web API method to use, possible params are GET, POST, PUT, DELETE, HEAD
 *  @param params JSON params to send stored in a NSDictionary object
 *  @param class the class of the ResponseObject retrieved in the callback
 *  @param callback API callback
 */
- (void) restCall:(NSString*)url
           method:(NSString*)method
           params:(NSDictionary*)params
            class:(Class)responseClass
         callback:(ResponseObjectCallback)callback;

/**
 *  Generic REST API call with response object of class "ResponseClass" and API error management
 *
 *  @param url
 *  @param method
 *  @param params
 *  @param class the class of the ResponseObject retrieved in the callback
 *  @param timeout timeout of the call in seconds, default 60 seconds
 *  @param callback
 */
- (void) restCall:(NSString*)url
           method:(NSString*)method
           params:(NSDictionary*)params
            class:(Class)responseClass
          timeout:(NSNumber*)timeout
         callback:(ResponseObjectCallback)callback;

/**
*  Generic REST API call with response object of class "ResponseClass" and API error management
*  @param domain a valid SCLAK server domain e.g. https://api.sclak.com
*  @param url
*  @param method
*  @param params
*  @param class the class of the ResponseObject retrieved in the callback
*  @param timeout timeout of the call in seconds, default 60 seconds
*  @param callback
*/
- (void) restCall:(NSString*)domain
              url:(NSString*)url
           method:(NSString*)method
           params:(NSDictionary*)params
            class:(Class)responseClass
          timeout:(NSNumber*)timeout
         callback:(ResponseObjectCallback)callback;

/**
 *  Generic String API call
 *
 *  @param url
 *  @param method
 *  @param params
 *  @param callback
 */
- (void) stringCall:(NSString*)url method:(NSString*)method params:(NSDictionary*)params callback:(ResponseStringCallback)callback;

/**
 *  Generic Stream API call
 *
 *  @param url
 *  @param method
 *  @param params
 *  @param callback
 */
- (void) streamCall:(NSString*)url method:(NSString*)method params:(NSDictionary*)params callback:(ResponseStreamCallback)callback;

/**
 * Handle Generic Response Success
 */
- (void) handleResponseSuccess:(NSURLSessionDataTask*)task
                responseObject:(id)responseObject
                         model:(ResponseObject*)responseObjectModel
                      callback:(ResponseObjectCallback)callback;

/**
 *  Handle Generic Response Error inside Api call with result
 *
 *  @param task
 *  @param result
 *  @param callback
 */
- (void) handleResponseError:(NSURLSessionDataTask*)task result:(id)result callback:(ResponseObjectCallback)callback;

/**
 *  Handle Generic Response Error inside Api call with result and ResponseObject
 *
 *  @param task
 *  @param result
 *  @param responseObject
 *  @param callback
 */
- (void) handleResponseError:(NSURLSessionDataTask*)task result:(id)result responseObject:(ResponseObject*)responseObject callback:(ResponseObjectCallback)callback;

/**
 *  Handle Generic Response Error
 *
 *  @param task
 *  @param error
 *  @param callback
 */
- (void) handleResponseError:(NSURLSessionDataTask*)task error:(NSError*)error callback:(ResponseObjectCallback)callback;
- (void) handleResponseError:(NSURLSessionDataTask*)task error:(NSError*)error objectCallback:(ResponseObjectCallback)callback;

/**
 * cancel all enqueued requests
 */
- (void) cancelAllRequests;

@end
