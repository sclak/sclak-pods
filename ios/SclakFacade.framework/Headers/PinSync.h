//
//  PinSync.h
//  SclakFacade
//
//  Created by Daniele Poggi on 05/04/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@interface PinSync : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *available;
@property (nonatomic, strong) NSNumber<Optional> *used;
@property (nonatomic, strong) NSNumber<Optional> *total;
@property (nonatomic, strong) NSNumber<Optional> *totalInMemory;
@property (nonatomic, strong) NSNumber<Optional> *availableOnSclak;
@property (nonatomic, strong) NSNumber<Optional> *availableInMemory;
@property (nonatomic, strong) NSNumber<Optional> *pinSyncNeeded;
@property (nonatomic, strong) NSNumber<Optional> *pinSyncMandatory;

@end
