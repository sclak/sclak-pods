//
//  SCKFacadeCallbacks.h
//  SclakFacade
//
//  Created by Daniele Poggi on 27/06/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import "ResponseObject.h"

// CALLBACKS
typedef void(^ResponseObjectCallback)(BOOL success, id result, id responseObject);
typedef void(^ResponseStringCallback)(BOOL success, NSString *result);
typedef void(^ResponseStreamCallback)(BOOL success, NSData *streamData);
typedef void(^LoginErrorCallback)(BOOL success, NSNumber *errorCode);
typedef void(^DownloadProgressBlock)(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite);
