//
//  PeripheralBattery.h
//  SclakFacade
//
//  Created by Daniele Poggi on 31/08/2018.
//  Copyright © 2018 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "SCKFacadeCallbacks.h"

@interface PeripheralBattery : ResponseObject

/**
 * Percentuale capacità residua delle batterie valori ammessi 0-100
 */
@property (nonatomic, strong) NSNumber<Optional> *percentage;

/**
 * Conteggio manovre effettuate dall’ultima sostituzione delle batterie, formato MSByte First
 */
@property (nonatomic, strong) NSNumber<Optional> *countOpen;

/**
 * Valore di capacità residua della batteria espresso in mAh, formato MSByte First
 */
@property (nonatomic, strong) NSNumber<Optional> *residualCapacity;

/**
 * Valore di tensione della batteria misurato durante la fase di StandBy, formato MSByte First
 */
@property (nonatomic, strong) NSNumber<Optional> *standbyVoltage;

/**
 * Valore di tensione della batteria misurato durante la fase operativa di movimento (consumo massimo), formato MSByte First
 */
@property (nonatomic, strong) NSNumber<Optional> *lowVoltage;

/**
 * Flag sostituzione autonomo batteria rilevato (1 Sostituzione rilevata, 0 Non rilevata), questo flag viene resettato automaticamente dal comando di cambio batterie
 */
@property (nonatomic, strong) NSNumber<Optional> *batteryReplacedDetected;

/**
 * Reset dei dati gestione batterie dall’ultima lettura dello stato consumo Batterie, questo flag viene resettato dopo la lettura. La percentuale di carica viene impostata al 10% o al 100% a seconda del valore di Standby-Voltage.
 */
@property (nonatomic, strong) NSNumber<Optional> *batteryDataResetted;

/**
 * Flag non validità dei dati batteria (1 Dati batteria non validi, 0 Dati batteria validi), questo flag viene utilizzato per permettere la gestione con lo stesso firmware di versioni HW diverse (con e senza la gestione consumo batterie).
 */
@property (nonatomic, strong) NSNumber<Optional> *batteryDataInvalid;

@property (nonatomic, strong) NSNumber<Optional> *insertTime;

+ (void) getBatteryStatus:(NSString*)btcode callback:(ResponseObjectCallback)callback;
+ (void) postBatteryStatus:(NSString*)btcode battery:(PeripheralBattery*)battery callback:(ResponseObjectCallback)callback;

@end
