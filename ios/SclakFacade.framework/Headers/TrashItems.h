//
//  PrivilegesMulti.h
//  SclakFacade
//
//  Created by Stefano Demattè on 23/05/2019.
//  Copyright © 2019 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "Privilege.h"

NS_ASSUME_NONNULL_BEGIN

@interface TrashItems : ResponseObject

@property (nonatomic, strong) NSArray<Privilege, Optional> *list;

@end

NS_ASSUME_NONNULL_END
