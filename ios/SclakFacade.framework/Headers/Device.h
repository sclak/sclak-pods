//
//  Device.h
//  Sclak2
//
//  Created by albi on 04/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "ResponseObject.h"
#import "SCKFacadeCallbacks.h"

@protocol Device
@end

@interface Devices : ResponseObject

@property (nonatomic, strong) NSArray<Device, Optional> *list;

@end

@interface Device : ResponseObject

@property (nonatomic, strong) NSNumber <Optional> *id;
@property (nonatomic, strong) NSString <Optional> *uniqueId;
@property (nonatomic, strong) NSString <Optional> *name;
@property (nonatomic, strong) NSString <Optional> *brand;
@property (nonatomic, strong) NSString <Optional> *model;
@property (nonatomic, strong) NSString <Optional> *os;
@property (nonatomic, strong) NSString <Optional> *osVersion;
@property (nonatomic, strong) NSString <Optional> *userAgent;
@property (nonatomic, strong) NSString <Optional> *browser;
@property (nonatomic, strong) NSString <Optional> *browserVersion;
@property (nonatomic, strong) NSString <Optional> *language;
@property (nonatomic, strong) NSNumber <Optional> *userDeviceId;
@property (nonatomic, strong) NSNumber <Optional> *enabled;
@property (nonatomic, strong) NSNumber <Optional> *signedOut;
@property (nonatomic, strong) NSNumber <Optional> *dataChanged;
@property (nonatomic, strong) NSNumber <Optional> *dataChangedTime;
@property (nonatomic, strong) NSNumber <Optional> *lastGetTokenTime;
@property (nonatomic, strong) NSNumber <Optional> *lastUsageTime;

- (NSDate*) getLastTokenDate;
- (NSDate*) getLastUsageDate;
- (NSDate*) getDataChangedDate;

#pragma mark - Devices API

- (void) setEnabled:(BOOL)enabled callback:(ResponseObjectCallback)callback;

+ (void) updateDeviceWithPushToken:(NSString*)pushToken callback:(ResponseObjectCallback)callback;
+ (void) logoutWithUserDeviceCallback:(ResponseObjectCallback)callback;
+ (void) deleteUserDeviceCallback:(ResponseObjectCallback)callback;
+ (void) deleteDevice:(Device*)device callback:(ResponseObjectCallback)callback;

@end
