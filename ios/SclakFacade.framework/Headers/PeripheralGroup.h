//
//  PeripheralGroup.h
//  SclakFacade
//
//  Created by Daniele Poggi on 27/03/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "User.h"
#import "PeripheralBaseModel.h"
#import "PeripheralProtocols.h"
#import "SCKFacadeCallbacks.h"

@interface PeripheralGroupParams : JSONModel

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *shared;
@property (nonatomic, strong) NSNumber *preloadedPins;
@property (nonatomic, strong) NSArray *btcodes;
@property (nonatomic, strong) NSString *password;

@end

@interface PeripheralGroups : ResponseObject

@property (nonatomic, strong) NSArray<PeripheralGroup, Optional> *list;

@end

@interface PeripheralGroup : PeripheralBaseModel

@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *tag;
@property (nonatomic, strong) NSNumber<Optional> *shared;

@property (nonatomic, strong) NSNumber<Optional> *userId;
@property (nonatomic, strong) User<Optional> *owner;

@property (nonatomic, strong) NSNumber<Optional> *fwGroup;
@property (nonatomic, strong) NSNumber<Optional> *fwGroupWritten;

@property (nonatomic, strong) NSArray<Peripheral, Optional> *peripherals;

@property (nonatomic, strong) NSNumber<Optional> *page;
@property (nonatomic, strong) NSNumber<Optional> *pageSize;

@property (nonatomic, strong) NSNumber<Optional> *canInviteOwner;
@property (nonatomic, strong) NSNumber<Optional> *canInviteGuest;
@property (nonatomic, strong) NSNumber<Optional> *monoButton;

- (NSString*) presentationName;

- (BOOL) ownerPostPrivilegeEnabled;
- (BOOL) isPinPukSupported;

- (BOOL) hasPairedKeypad;
- (BOOL) hasPairedTagReader;
- (BOOL) hasPairedRemote;

- (BOOL) canModify;

- (Peripheral*) peripheralWithLowestIdInGroup;

- (NSArray*) peripheralModels;
- (NSArray*) peripheralBtcodes;

- (BOOL) isScanToOpenType:(ScanToOpenType)type;
- (BOOL) isScanToOpenForceScan;

- (BOOL) supportRemoteUse;
- (BOOL) privilegeRemoteUseEnabled;

#pragma mark - Rest APIs

+ (void) all:(ResponseObjectCallback)callback;
+ (void) allWithOptions:(NSDictionary*)options callback:(ResponseObjectCallback)callback;
+ (void) one:(NSString*)idOrTag callback:(ResponseObjectCallback)callback;
+ (void) create:(PeripheralGroupParams*)params callback:(ResponseObjectCallback)callback;
+ (void) update:(PeripheralGroup*)model params:(PeripheralGroupParams*)params callback:(ResponseObjectCallback)callback;
+ (void) delete:(PeripheralGroup*)model callback:(ResponseObjectCallback)callback;

@end
