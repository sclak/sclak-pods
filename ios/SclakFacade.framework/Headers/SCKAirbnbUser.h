//
//  SCKAirbnbUser.h
//  SclakFacade
//
//  Created by Daniele Poggi on 19/05/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@interface SCKAirbnbUser : ResponseObject

@property (nonatomic, strong) NSString<Optional> *firstName;
@property (nonatomic, strong) NSString<Optional> *lastName;
@property (nonatomic, strong) NSString<Optional> *pictureUrl;

@end
