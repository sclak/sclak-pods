//
//  SCKPeripheralTypes.h
//  SclakFacade
//
//  Created by Rico Crescenzio on 10/03/2020.
//

#import <Foundation/Foundation.h>
#import "ResponseObject.h"
#import "SCKPeripheralType.h"

@interface SCKPeripheralTypes : ResponseObject

@property (nonatomic, strong) NSArray<SCKPeripheralType*><SCKPeripheralType, Optional> *list;
@property (nonatomic, strong) NSNumber <Optional> *totalCount;

@end
