//
//  Firmware.h
//  ULockUnit
//
//  Created by Daniele Poggi on 27/01/15.
//  Copyright (c) 2015 SCLAK. All rights reserved.
//

#import "ResponseObject.h"

typedef NS_ENUM(NSUInteger, FirmwarePeripheralType) {
    FirmwarePeripheralTypeUnknown,
    FirmwarePeripheralTypeUlock,
    FirmwarePeripheralTypeSclak,
    FirmwarePeripheralTypeParkey,
    FirmwarePeripheralTypeHartmann
};

@protocol Firmware

@end

@interface Firmwares : ResponseObject

@property (nonatomic, strong) NSMutableArray<Firmware> *list;

@end

@interface Firmware : ResponseObject

@property (nonatomic, strong) NSString <Optional> *id;
@property (nonatomic, strong) NSString <Optional> *version;
@property (nonatomic, strong) NSNumber <Optional> *supportPin;
@property (nonatomic, strong) NSNumber <Optional> *supportUpgrade;
@property (nonatomic, strong) NSNumber <Optional> *supportBattery;
@property (nonatomic, strong) NSNumber <Optional> *upgradeMandatory;
@property (nonatomic, strong) NSString <Optional> *lastUpgradeVersion;
@property (nonatomic, strong) NSString <Optional> *checksum;
@property (nonatomic, strong) NSString <Optional> *notes;
@property (nonatomic, strong) NSNumber <Optional> *insertTime;
@property (nonatomic, strong) NSNumber <Optional> *editTime;
@property (nonatomic, strong) NSString <Optional> *peripheralTypeCode;

- (FirmwarePeripheralType) firmwareType;
- (NSString*) filename;
- (BOOL) updateRequiredForPeripheralVersion:(NSString*)peripheralVersion;

- (BOOL) greater:(NSString*)firmwareCode;
- (BOOL) greaterOrEqualTo:(NSString*)firmwareCode;

- (BOOL) lesser:(NSString*)firmwareCode;
- (BOOL) lesserOrEqualTo:(NSString*)firmwareCode;

- (BOOL) isMandatoryUpgrade;

@end
