//
//  LogUsage.h
//  SclakFacade
//
//  Created by Daniele Poggi on 23/08/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "SCKFacadeCallbacks.h"

typedef NS_ENUM(NSUInteger, LogUsageType) {
    Access,                         // 0
    AutoOpen,                       // 1
    TocToc,                         // 2
    Checkin,                        // 3
    KeypadPin,                      // 4
    SMSPin,                         // 5
    CRLock_Job_Begin,               // 6
    CRLock_Door_Status_Invalid,     // 7
    CRLock_Lock_Status_Invalid,     // 8
    CRLock_Unlock,                  // 9
    CRLock_Lock_Open,               // 10
    CRLock_Door_Open,               // 11
    CRLock_Door_Close,              // 12
    CRLock_Lock_Close,              // 13
    CRLock_Job_Close_Invalid_Door,  // 14
    CRLock_Job_Close_Invalid_Lock,  // 15
    CRLock_Job_Close,               // 16
    Close,                          // 17
    ReaderPin,                      // 18
    TwistAndOpen,                   // 19
    ShakeAndOpen,                   // 20
    ScanAndOpen,                    // 21
    SiriOpen,                       // 22
    Checkout,                       // 23
    CheckOutError,                  // 24
    RemoteOpen,                     // 25
    RemoteClose,                    // 26
    AutoClose,                      // 27
    ShakeAndClose,                  // 28
    AttendanceStart,                // 29
    AttendanceStop                  // 30
};

@class User;

@protocol LogUsage
@end

@interface LogUsage : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) User<Optional> *user;
@property (nonatomic, strong) NSNumber<Optional> *anonymous;
@property (nonatomic, strong) NSNumber<Optional> *insertTime;
@property (nonatomic, strong) NSNumber<Optional> *usageType;

#pragma mark - Log Usages Access Control APIs

+ (void) logUsages:(NSArray*)usages disableNotification:(BOOL)disableNotification callback:(ResponseObjectCallback)callback;

+ (void) logUsageForBtcode:(NSString*)btcode usageType:(LogUsageType)usageType callback:(ResponseObjectCallback)callback;

+ (void) logUsageForBtcode:(NSString*)btcode
                 anonymous:(BOOL)anonymous
                   logTime:(NSNumber*)logTime
                 usageType:(LogUsageType)usageType
       disableNotification:(BOOL)disableNotification
                  callback:(ResponseObjectCallback)callback;

+ (void) getLogUsageForBtcode:(NSString*)btcode page:(NSInteger)page userID:(NSNumber*)userID callback:(ResponseObjectCallback)callback;

@end

@interface LogUsages : ResponseObject

@property (nonatomic, strong) NSArray <Optional, LogUsage> *list;
@property (nonatomic, strong) NSNumber <Optional> *nextPage;

@end

@interface LogUsagePost : JSONModel

@property (nonatomic, strong) NSString *btcode;
@property (nonatomic, strong) NSNumber *logTime;
@property (nonatomic, strong) NSNumber *usageType;
/**
 * used to log the pin code usage
 */
@property (nonatomic, strong) NSString *pinCode;
/**
 * needed when logging the pinCode, if peripheral has no "simple puk" setted
 */
@property (nonatomic, strong) NSString *puk;
/**
 * type of access. value may vary depending on usageType
 * Usage type PIN:
 * - "0" access OK with PIN
 * - "1" access KO, good PIN but wrong time
 * - "2" access KO, wrong PIN
 */
@property (nonatomic, strong) NSString *accessType;

@end
