//
//  SCKFacadeErrors.h
//  SclakFacade
//
//  Created by Daniele Poggi on 10/02/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

typedef NS_ENUM(NSInteger, SCKFacadeErrors) {
    ERROR_AUTH_REQUIRED =                         1,
    ERROR_INVALID_TOKEN =                         2,
    ERROR_INVALID_DEVICE_UNIQUE_ID =              3,
    ERROR_INVALID_EMAIL =                         4,
    ERROR_INVALID_USER =                          5,
    ERROR_NO_PRIVILEGE_FOR_ACTION =               6,
    ERROR_NO_PARAMS =                             7,
    ERROR_INVALID_LOT =                           8,
    ERROR_INVALID_PERIPHERALS_NUMBER =            9,
    ERROR_INVALID_GROUP =                        10,
    ERROR_INVALID_ACTIONS =                      11,
    ERROR_INVALID_USER_ID =                      12,
    ERROR_INVALID_EMAIL_ACTIVATION_CODE =        13,
    ERROR_EMAIL_USED =                           14,
    ERROR_DELETE_LAST_EMAIL =                    15,
    ERROR_INVALID_RESET_CODE =                   16,
    ERROR_INVALID_PASSWORD =                     17,
    ERROR_INVALID_TARGET =                       18,
    ERROR_INVALID_PERIPHERAL =                   19,
    ERROR_BTCODE_USED =                          20,
    ERROR_INVALID_PERIPHERAL_ACTIVATION_CODE =   21,
    ERROR_PERIPHERAL_ALREADY_ACTIVE =            22,
    ERROR_INVALID_PRIVILEGE =                    23,
    ERROR_GENERIC_REST =                         24,
    ERROR_INVALID_PERIPHERAL_TYPE =              25,
    ERROR_INVALID_PRIVILEGE_ACTIVATION_CODE =    26,
    ERROR_INVALID_COUPON_ID =                    27,
    ERROR_INVALID_COUPON_TYPE =                  28,
    ERROR_INVALID_COUPON_CODE =                  29,
    ERROR_INVALID_DEVICE =                       30,
    ERROR_PRIVILEGE_EXISTS =                     31,
    ERROR_ATTEMPTS_USED =                        32,
    ERROR_INVALID_PRODUCT_IDENTIFIER =           33,
    ERROR_INVALID_IOS_RECEIPT =                  34,
    ERROR_INVALID_ANDROID_PURCHASE_TOKEN =       35,
    ERROR_PURCHASE_EXISTS =                      36,
    ERROR_INVALID_FIRMWARE =                     37,
    ERROR_INVALID_FIRMWARE_VERSION =             38,
    ERROR_INVALID_FIRMWARE_DATA =                39,
    ERROR_FIRMWARE_EXISTS =                      40,
    ERROR_SECRET_CODE_EXISTS =                   41,
    ERROR_INVITE_EXPIRED =                       42,
    ERROR_INVALID_PERIPHERAL_VERSION =           43,
    ERROR_RESET_PASSWORD_ATTEMPTS_USED =         44,
    ERROR_DEVICE_DISABLED =                      45,
    ERROR_NO_PERIPHERAL_AVAILABLE =              46,
    ERROR_EMAIL_NOT_ACTIVE =                     47,
    ERROR_SELF_PRIVILEGE_NOT_ALLOWED =           48,
    ERROR_INVALID_SEQUENCE =                     49,
    ERROR_INVALID_INITIAL_CODE =                 50,
    ERROR_INVALID_ROM_ID =                       51,
    ERROR_INVALID_WEB_PURCHASE_TOKEN =           52,
    ERROR_INVALID_PURCHASE =                     53,
    
    ERROR_INVALID_LATITUDE =                     54,
    ERROR_INVALID_LONGITUDE =                    55,
    ERROR_INVALID_AUTOCLOSE_TIME =               56,
    
    ERROR_PERIPHERAL_ACTIVE =                    57,
    
    ERROR_OWNER_CREATE_PRIVILEGE_DISABLED =      58,
    ERROR_INVALID_PERIPHERAL_STATUS =            59,
    ERROR_INVALID_PRIVILEGE_STATUS =             60,
    ERROR_INVALID_PRIVILEGE_USER =               61,
    ERROR_CANNOT_DELETE_ADMIN_PRIVILEGE =        62,
    
    ERROR_INVALID_PIN_CODE =                     65,
    ERROR_INVALID_PIN_CODE_CODE =                66,
    ERROR_PIN_CODE_EXISTS =                      67,
    ERROR_PERIPHERAL_ALREADY_PAIRED =            68,
    ERROR_PERIPHERAL_NOT_PAIRABLE =              69,
    ERROR_PERIPHERAL_NOT_PAIRED =                70,
    ERROR_NO_PIN_CODE_AVAILABLE =                71,
    ERROR_NO_PIN_CODE_USED =                     72,
    ERROR_INVALID_PUK_CODE =                     73,
    ERROR_INVALID_PUK_CODE_CODE =                74,
    ERROR_PUK_CODE_EXISTS =                      75,
    ERROR_PUK_CODE_NOT_SET =                     76,
    ERROR_PIN_CODE_OUTDATED =                    77,
    ERROR_INVALID_PIN_CODE_FW_POSITION =         78,
    ERROR_COUPON_ALREADY_USED =                  79,
    ERROR_INVALID_PIN_GROUP =                    80,
    
    ERROR_INVALID_VERSION_CODE =                 85,
    ERROR_OPERATIVE_CODE_CHECK_FAILED =          86,
    ERROR_OPERATIVE_CODE_NOT_SETTED =            87,
    
    ERROR_INVALID_LOCALIZED_STRING =             90,
    
    ERROR_INVALID_TIMEZONE =                     100,
    
    ERROR_INVALID_CUSTOMER =                     110,
    
    ERROR_INVALID_PRODUCT_LOT =                  120,
    
    ERROR_INVALID_PRODUCT_ORDER =                130,
    
    ERROR_INVALID_BLUETOOTH_LOG =                140,
    
    ERROR_INVALID_USER_GROUP =                   150,
    ERROR_INVALID_USER_GROUP_NAME =              151,
    ERROR_INVALID_PERIPHERAL_GROUP =             152,
    ERROR_INVALID_PERIPHERAL_GROUP_NAME =        153,
    ERROR_PERIPHERAL_GROUP_ALREADY_EXISTS =      154,
    ERROR_PERIPHERAL_GROUP_CANNOT_MODIFY =       155,
    
    ERROR_INVALID_ACCESSORY =                    160,
    ERROR_ACCESSORY_ALREADY_EXISTS =             161,
    ERROR_INVALID_ACCESSORY_OWNER =              162,
    ERROR_INVALID_PAIRED_PERIPHERALS =           163,
    
    ERROR_ACTIVATE_PRIVILEGE_EMAIL_INVALID =     170,
    ERROR_INVALID_PASSWORD_FORMAT =              171,
    
    ERROR_INVALID_EMAIL_ALREADY_USED =           180,
    ERROR_INVALID_PHONE_NUMBER_ALREADY_USED =    181,
    
    ERROR_DATA_NOT_FOUND =                       190,
    
    ERROR_SOME_PUSH_NOTIFICATIONS_NOT_SENT =     200,
    
    ERROR_INVALID_INSTALLATION =                 210,
    ERROR_INVALID_INSTALLATION_CODE =            211,
    ERROR_INVALID_INSTALLATION_INSTALLER =       212,
    ERROR_INVALID_INSTALLATION_ADMIN =           213,
    
    ERROR_INVALID_JSON_INPUT =                   250,
    
    GENERIC_ERROR =                              300,
    ERROR_MODEL_ALREADY_EXISTS =                 301,
    
    ERROR_ACCESS_CONTROL_DENIED =                350,
    
    ERROR_INVALID_PROJECT_ID =                   370,
    ERROR_INVALID_PROJECT_API_KEY =              371,
    ERROR_INVALID_PROJECT_API_SECRET =           372,
};
