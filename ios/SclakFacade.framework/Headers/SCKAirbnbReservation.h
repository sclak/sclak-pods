//
//  SCKAirbnbReservation.h
//  SclakFacade
//
//  Created by Daniele Poggi on 19/05/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import "ResponseObject.h"

extern NSString * const AIRBNB_DATETIME_FORMAT;

@class SCKAirbnbListing;

@protocol SCKAirbnbReservation

@end

@interface SCKAirbnbReservations : ResponseObject

@property (nonatomic, strong) NSArray<SCKAirbnbReservation> *list;

@end

@interface SCKAirbnbReservation : ResponseObject

/**
 * from 01/07/2020, id has become the confirmation code
 */
@property (nonatomic, strong) NSString<Optional> *confirmationCode;
@property (nonatomic, strong) NSString<Optional> *guestAliasEmail;
@property (nonatomic, strong) NSNumber<Optional> *numberOfGuests;
@property (nonatomic, strong) NSString<Optional> *startDate;
@property (nonatomic, strong) NSString<Optional> *endDate;
@property (nonatomic, strong) NSString<Optional> *checkInDatetime;
@property (nonatomic, strong) NSString<Optional> *checkOutDatetime;
@property (nonatomic, strong) NSString<Optional> *statusType;

@property (nonatomic, strong) SCKAirbnbListing<Optional> *listing;

@property (nonatomic, strong) NSNumber<Optional> *privilegeId;

@property (nonatomic, strong) NSNumber<Optional> *lastUsageTime;

- (NSString*) guestUsernameFromEmail;

- (NSString*) checkInTime;
- (NSString*) checkOutTime;

@end
