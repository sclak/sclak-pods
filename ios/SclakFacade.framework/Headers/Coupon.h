//
//  Coupon.h
//  SclakApp
//
//  Created by Daniele Poggi on 19/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@class User;
@class Peripheral;
@class PeripheralGroup;
@class Privilege;

#define kCouponTypePeripheral   @"peripheral"
#define kCouponTypePrivilege    @"privilege"
#define kCouponTypeRegister     @"register"

@interface Coupon : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *code;
@property (nonatomic, strong) NSString<Optional> *type;
@property (nonatomic, strong) NSNumber<Optional> *used;
@property (nonatomic, strong) NSNumber<Optional> *groupId;
@property (nonatomic, strong) NSString<Optional> *groupTag;
@property (nonatomic, strong) NSNumber<Optional> *privilegeId;

@property (nonatomic, strong) Peripheral<Optional> *peripheral;
@property (nonatomic, strong) PeripheralGroup<Optional> *peripheralGroup;

@property (nonatomic, strong) Privilege<Optional> *privilege;

@property (nonatomic, strong) User<Optional> *account;

- (BOOL) isTypePeripheral;
- (BOOL) isTypePrivilege;
- (BOOL) isTypeRegister;

- (BOOL) isGuest;
- (BOOL) isOwner;
- (BOOL) isAdmin;
- (BOOL) isInstaller;
- (BOOL) isSuperAdmin;

@end
