//
//  PeripheralProtocols.h
//  SclakFacade
//
//  Created by Daniele Poggi on 20/09/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

@protocol Peripheral <NSObject>
@end

@protocol PeripheralGroup <NSObject>
@end
