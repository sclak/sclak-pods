//
//  AccessCode.h
//  SclakApp
//
//  Created by albi on 22/06/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "ResponseObject.h"
#import "SCKFacadeCallbacks.h"

@protocol PinCode
@end

@interface PinCodes : ResponseObject

@property (nonatomic, strong) NSArray <Optional, PinCode> *list;

@end

@interface PinCode : ResponseObject

@property (nonatomic, strong) NSNumber <Optional> *id;
@property (nonatomic, strong) NSString <Optional> *code;
@property (nonatomic, strong) NSString <Optional> *clearCode;
@property (nonatomic, strong) NSNumber <Optional> *peripheralId;
@property (nonatomic, strong) NSString <Optional> *btcode;
@property (nonatomic, strong) NSNumber <Optional> *fwPosition;
@property (nonatomic, strong) NSNumber <Optional> *fwUpdated;
@property (nonatomic, strong) NSNumber <Optional> *fwDelete;
@property (nonatomic, strong) NSNumber <Optional> *editTime;
@property (nonatomic, strong) NSString <Optional> *command;
@property (nonatomic, strong) NSString <Optional> *flags;

#pragma mark Properties of User Pin Group Code

@property (nonatomic, strong) NSNumber <Optional> *privilegeId;
@property (nonatomic, strong) NSNumber <Optional> *peripheralGroupId;
@property (nonatomic, strong) NSString <Optional> *peripheralGroupTag;

+ (void) getUserPinCodeForId:(NSNumber*)userPinCodeId callback:(ResponseObjectCallback)callback;
+ (void) getUserPinCodes:(NSArray*)btcodes callback:(ResponseObjectCallback)callback;
+ (void) postUserPinCode:(NSString*)userPinCode  privilegeId:(NSNumber*)privilegeId callback:(ResponseObjectCallback)callback;
+ (void) postUserPinCodes:(NSArray*)userPinCodes privilegeId:(NSNumber*)privilegeId callback:(ResponseObjectCallback)callback;
+ (void) putUserPinCode:(PinCode*)userPinCode callback:(ResponseObjectCallback)callback;
+ (void) deleteUserPinCode:(NSString*)cipheredUserPin callback:(ResponseObjectCallback)callback;

@end
