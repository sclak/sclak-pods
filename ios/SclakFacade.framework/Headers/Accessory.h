//
//  Accessory.h
//  SclakFacade
//
//  Created by Daniele Poggi on 16/03/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "Peripheral.h"
#import "PeripheralGroup.h"
#import "RemoteButton.h"

@interface Accessories : ResponseObject

@property (nonatomic, strong) NSArray<Accessory> *list;

@end

@interface Accessory : ResponseObject

@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSNumber *peripheralId;
@property (nonatomic, strong) NSNumber<Optional> *masterPeripheralId;
@property (nonatomic, strong) NSString<Optional> *btcode;
@property (nonatomic, strong) Peripheral<Optional> *peripheral;
@property (nonatomic, strong) Peripheral<Optional> *masterPeripheral;
@property (nonatomic, strong) SCKPeripheralType<Optional> *peripheralType;
@property (nonatomic, strong) NSNumber<Optional> *ownerId;
@property (nonatomic, strong) NSNumber<Optional> *userId;
@property (nonatomic, strong) NSString<Optional> *userName;
@property (nonatomic, strong) NSString<Optional> *userSurname;
@property (nonatomic, strong) NSNumber *insertTime;
@property (nonatomic, strong) NSNumber *editTime;

@property (nonatomic, strong) NSArray<Peripheral, Optional> *pairedPeripherals;
@property (nonatomic, strong) NSArray<Optional> *pairedPeripheralsBtcodes;
@property (nonatomic, strong) NSArray<PeripheralGroup, Optional> *pairedGroups;
@property (nonatomic, strong) NSArray<RemoteButton, Optional> *remoteButtons;

- (BOOL) isPending;
- (NSString*) userDescription;

@end
