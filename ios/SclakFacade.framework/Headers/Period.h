//
//  Period.h
//  SclakFacade
//
//  Created by Daniele Poggi on 16/05/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "TimeRange.h"

@protocol Period <NSObject>
@end

@interface Period : JSONModel

@property (nonatomic, strong) NSDate *fromDate;
@property (nonatomic, strong) NSDate *toDate;
@property (nonatomic, strong) TimeRange *timeRange;

/**
 * used in app (not saved on server) to check if the period is temporarily created in the calendar
 * and has not been saved on server
 */
@property (nonatomic, assign) NSNumber<Ignore> *unsaved;

- (BOOL) hasSameTimeRange:(Period*)object;
- (BOOL) isCheckin:(Day*)day;
- (BOOL) isCheckout:(Day*)day;
- (BOOL) containsDay:(Day*)day;

- (NSString*) description;

- (Day*) beginDay;
- (TimeRange*) beginTimeRange;

/**
 * time interval representing the sum of fromDate and checkin time (timeRange.fromHour)
 */
- (NSTimeInterval) beginTime:(NSTimeZone*)timeZone;

- (Day*) endDay;
- (TimeRange*) endTimeRange;

- (TimeRange*) intermediateTimeRange;

@end

