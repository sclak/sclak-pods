//
//  PeripheralSetting.h
//  SclakFacade
//
//  Created by Daniele Poggi on 04/02/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>

// REMINDER:
// KEYS are limited to 30 chars

#define PeripheralSettingBuzzer                                 @"buzzer"
#define PeripheralSettingLed                                    @"led"
#define PeripheralSettingAutocloseTime                          @"autoclose_time"
#define PeripheralSettingPeripheralMode                         @"peripheral_mode"
#define PeripheralSettingOwnerPostPrivilegeEnabled              @"owner_post_privilege_enabled"
#define PeripheralSettingOwnerPostPrivilegePush                 @"owner_post_privilege_push"
#define PeripheralSettingAdminManageOwnerPrivilegesEnabled      @"admin_manage_privs_enabled"
#define PeripheralSettingBuzzOnHandleEnabled                    @"buzz_on_handle_enabled"
#define PeripheralSettingBuzzOnHandleDisabled                   @"buzz_on_handle_disabled"
#define PeripheralSettingSupportOwnerPostPrivilege              @"support_owner_post_privilege"
#define PeripheralSettingSupportAutoOpen                        @"support_auto_open"
#define PeripheralSettingSupportTocToc                          @"support_toc_toc"
#define PeripheralSettingVirtualKeyboard                        @"vkeyboard"
#define PeripheralSettingCheckInOut                             @"support_attendance"
#define PeripheralSettingSupportAttendanceOpen                  @"support_attendance_open"
#define PeripheralSettingImmobilizerInstalled                   @"immobilizer_installed"
#define PeripheralSettingImmobilizerStatus                      @"immobilizer_status"
#define PeripheralSettingDoorStatus                             @"door_status"
#define PeripheralSettingLockStatus                             @"lock_status"
#define PeripheralSettingDoorSensorInstalled                    @"door_sensor_installed"
#define PeripheralSettingRemoteUseEnabled                       @"remote_use_enabled"
#define PeripheralSettingRemoteUsePinEnabled                    @"remote_use_pin_enabled"
#define PeripheralSettingRemoteUsePushStatusEnabled             @"remote_use_push_status_enabled"
#define PeripheralSettingRingBellRequiredToOpen                 @"ring_bell_required_to_open"
#define PeripheralSettingReleInstalled                          @"rele_installed"
#define PeripheralSettingDefaultMQTTPing                        @"mqtt_ping"

#define PERIPHERAL_MODE_DOOR                        @"door"
#define PERIPHERAL_MODE_GATE                        @"gate"
#define PERIPHERAL_MODE_GARAGE                      @"garage"

@protocol PeripheralSetting
@end

@interface PeripheralSetting : JSONModel

@property (nonatomic, strong) NSString *setting;
@property (nonatomic, strong) NSString *value;

@end
