//
//  SCKAirbnbListing.h
//  SclakFacade
//
//  Created by Daniele Poggi on 19/05/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@class SCKAirbnbListingKey;
@class Peripheral;
@class PeripheralGroup;

@protocol SCKAirbnbListing

@end

@interface SCKAirbnbListings : ResponseObject

@property (nonatomic, strong) NSArray<SCKAirbnbListing> *list;

@end

@interface SCKAirbnbListing : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSNumber<Optional> *userId;

@property (nonatomic, strong) NSString<Optional> *apt;
@property (nonatomic, strong) NSNumber<Optional> *bathrooms;
@property (nonatomic, strong) NSNumber<Optional> *bedrooms;
@property (nonatomic, strong) NSNumber<Optional> *beds;
@property (nonatomic, strong) NSNumber<Optional> *lat;
@property (nonatomic, strong) NSNumber<Optional> *lng;
@property (nonatomic, strong) NSNumber<Optional> *personCapacity;
@property (nonatomic, strong) NSString<Optional> *pictureUrl;
@property (nonatomic, strong) NSString<Optional> *propertyTypeCategory;

@property (nonatomic, strong) NSString<Optional> *name;

@property (nonatomic, strong) NSString<Optional> *street;
@property (nonatomic, strong) NSString<Optional> *city;
@property (nonatomic, strong) NSString<Optional> *zipcode;
@property (nonatomic, strong) NSString<Optional> *country;
@property (nonatomic, strong) NSString<Optional> *state;
@property (nonatomic, strong) NSString<Optional> *timeZoneName;

@property (nonatomic, strong) SCKAirbnbListingKey<Optional> *sclakListing;

- (NSString*) fullAddress;

- (Peripheral*) peripheral;
- (PeripheralGroup*) peripheralGroup;

@end
