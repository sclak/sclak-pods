Pod::Spec.new do |s|
  
  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.name         = "SclakFoundation"
  s.version      = "2.0.109"
  s.summary      = "Sclak Foundation Framework"
  s.homepage     = "http://www.sclak.com"
  
  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.license = { :type => "MIT", :file => "LICENSE" }
  
  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.author = { "Daniele Poggi" => "daniele.poggi@sclak.com" }
  #s.social_media_url   = "www.linkedin.com/in/danielepoggi"
  
  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.ios.deployment_target = "9.0"
  s.osx.deployment_target = "10.7"
  s.watchos.deployment_target = "2.0"
  
  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.source = { :git => "ssh://git@bitbucket.org/sclak/sclak-pods.git", :tag => s.version }
  
  # ――― Vendored Frameworks ―――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.ios.vendored_frameworks     = "ios/SclakFoundation.framework"
  s.watchos.vendored_frameworks = "watchos/SclakFoundation.framework"
  
  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.frameworks = 'Foundation', 'UIKit'
  
  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.requires_arc = true
  
  # ――― Common dependencies ―――――――――――――――――――――――――――――――――――――――――――――――――――――― #
    
  s.dependency 'CocoaLumberjack'
  
end
