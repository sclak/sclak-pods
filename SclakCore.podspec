Pod::Spec.new do |s|
  
  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.name         = "SclakCore"
  s.version      = "2.0.109"
  s.summary      = "Sclak Core Framework"
  s.homepage     = "http://www.sclak.com"
  
  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.license = { :type => "MIT", :file => "LICENSE" }
  
  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.author = { "Daniele Poggi" => "daniele.poggi@sclak.com" }
  #s.social_media_url   = "www.linkedin.com/in/danielepoggi"
  
  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.ios.deployment_target = "9.0"
  s.osx.deployment_target = "10.7"
  s.watchos.deployment_target = "2.0"
  
  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.source = { :git => "ssh://git@bitbucket.org/sclak/sclak-pods.git", :tag => s.version }
  
  # ――― Vendored Frameworks ―――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.ios.vendored_frameworks     = "ios/SclakCore.framework"
  s.watchos.vendored_frameworks = "watchos/SclakCore.framework"
  
  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.frameworks = 'Foundation', 'CoreBluetooth', 'CoreLocation'
  
  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.requires_arc = true
  
  # ――― Common Dependencies――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.dependency 'AFNetworking'

  # ――― Supspecs --------――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.default_subspec = 'App'
  
  # ――― Supspecs: App ---――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.subspec 'App' do |app|      
        
      # Dependencies: Sclak
      app.dependency 'SclakFoundation'
      app.dependency 'SclakFacade'
      app.dependency 'SclakBle'
      
      # Dependencies: Vendors
      app.dependency 'Realm'
      app.dependency 'GBDeviceInfo'    
  end
  
  # ――― Supspecs: SclakWatch Extension ――――――――――-―――――――――――――――――――――――――――――――― #
  
  s.subspec 'Watch' do |watchos|          
      
      # Dependencies: Sclak
      watchos.dependency 'SclakFoundation'
      watchos.dependency 'SclakFacade'
      watchos.dependency 'SclakBle'
      
      # Dependencies: Vendors
      watchos.dependency 'Realm'
  end
  
end
