Pod::Spec.new do |s|
  
  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.name         = "SclakMQTT"
  s.version      = "2.0.109"
  s.summary      = "Sclak MQTT Framework"
  s.homepage     = "http://www.sclak.com"
  
  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.license = { :type => "MIT", :file => "LICENSE" }
  
  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.author = { "Daniele Poggi" => "daniele.poggi@sclak.com" }
  
  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.ios.deployment_target = "9.0"
  s.osx.deployment_target = "10.7"
  s.watchos.deployment_target = "2.0"
  
  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.source = { :git => "ssh://git@bitbucket.org/sclak/sclak-pods.git", :tag => s.version }
  
  # ――― Vendored Frameworks ―――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.vendored_frameworks     = "ios/SclakMQTT.framework"  
  
  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.frameworks = 'Foundation'
  
  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
  s.requires_arc = true
  
  # ――― Common dependencies ―――――――――――――――――――――――――――――――――――――――――――――――――――――― #
    
  # Sclak Pods
  s.dependency 'SclakFoundation'
  s.dependency 'SclakFacade'
  s.dependency 'SclakBle'
  
  s.dependency 'MQTTClient/MinL'
  s.dependency 'MQTTClient/ManagerL'
  s.dependency 'MQTTClient/WebsocketL'
  
end
