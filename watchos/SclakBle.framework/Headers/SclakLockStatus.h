//
//  SclakLockStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 08/05/2017.
//  Copyright © 2017 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SclakLockStatus : NSObject

/**
 * Valore attuale della serratura (uscita attiva (serratura sbloccate) -> 0x01, uscita disattiva (serratura bloccata) -> 0x00)
 */
@property (nonatomic, assign) BOOL lockStatus;

/**
 * Segnale di stato switch Carrello arretrato (Serratura sbloccata, 0 -> Switch disattivo, 1 -> Switch attivo).
 */
@property (nonatomic, assign) BOOL knobBackStatus;

/**
 * Segnale di stato switch Carrello in blocco manopola (Serratura bloccata, 0 -> Switch disattivo, 1 -> Switch attivo).
 */
@property (nonatomic, assign) BOOL knobForwardStatus;

/**
 * Segnale di stato switch Manopola retratta (Manopola bloccata, 0 -> Switch disattivo, 1 -> Switch attivo).
 */
@property (nonatomic, assign) BOOL knobRetractedStatus;

#pragma mark -

+ (instancetype) defaultConfiguration;
- (instancetype) initWithData:(NSData*)data;

@end
