//
//  SclakUfoPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 25/05/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "SclakUFOeStatus.h"
#import "SCKUFOeUserConfiguration.h"

@interface SclakUFOePeripheral : SclakPeripheral

@property (nonatomic, strong) SclakUFOeStatus *status;
@property (nonatomic, strong) SCKUFOeUserConfiguration *userConfiguration;

#pragma mark - User Configuration

- (void) sendUserConfiguration:(SCKUFOeUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

@end
