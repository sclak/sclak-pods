//
//  CemoPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 17/06/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import "SclakPeripheral.h"

typedef NS_ENUM(NSUInteger, CemoLogicStatus) {
    LogicStatusNotInitialized,
    LogicStatusNotReset,
    LogicStatusDayMode,
    LogicStatusNightMode,
};

typedef NS_ENUM(NSUInteger, CemoInitStatus) {
    InitStatusNotInitialized,
    InitStatusReady,
    InitStatusInitializing,
    InitStatusErrorDoorNotClosed,
    InitStatusErrorKeyInserted,
    InitStatusErrorMoving,
    InitStatusErrorLimitReached
};

@interface CemoPeripheral : SclakPeripheral

@property (nonatomic, assign) CemoLogicStatus logicStatus;
@property (nonatomic, assign) CemoInitStatus initStatus;

@property (nonatomic, assign) NSUInteger timeoutClose;

@property (nonatomic, assign) BOOL latchOn;     // scrocco
@property (nonatomic, assign) BOOL deadlockOn;  // nasello

@property (nonatomic, assign) BOOL doorOpen;
@property (nonatomic, assign) BOOL keyInside;

@property (nonatomic, assign) BOOL installedOn;

// Diagnostics
@property (nonatomic, assign) NSUInteger countOpen;

// Status Callbacks
@property (nonatomic, strong) BluetoothResponseCallback logicStatusCallback;
@property (nonatomic, strong) BluetoothResponseCallback initStatusCallback;

#pragma mark - Actions

- (void) getLogicStatusCallback:(BluetoothResponseCallback)callback;
- (void) openAndSetDayModeCallback:(BluetoothResponseCallback)callback;
- (void) openAndSetNightModeCallback:(BluetoothResponseCallback)callback;
- (void) setDayModeCallback:(BluetoothResponseCallback)callback;
- (void) setNightModeCallback:(BluetoothResponseCallback)callback;

- (void) getInitLockStatusCallback:(BluetoothResponseCallback)callback;
- (void) initLockCallback:(BluetoothResponseCallback)callback;

- (void) getUserConfigurationCallback:(BluetoothResponseCallback)callback;
- (void) sendUserConfigurationTimeoutClose:(NSUInteger)tmoClose installed:(BOOL)installed callback:(BluetoothResponseCallback)callback;

- (void) readDiagnosticsCallback:(BluetoothResponseCallback)callback;

@end
