//
//  SCKUserConfiguration.h
//  SclakBle
//
//  Created by Daniele Poggi on 23/04/16.
//  Copyright © 2016 sclak. All rights reserved.
//

#import "PPLGenericUserConfiguration.h"

@interface SCKUserConfiguration : PPLGenericUserConfiguration

@end
