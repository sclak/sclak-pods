//
//  NSData+PPLAESAdditions.h
//  PassePartoutLib
//
//  Created by isghe on 10/12/13.
//  Copyright (c) 2013 Daniele Poggi - Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (PPLAESAdditions)

- (NSData *)PPLAES128EncryptWithData:(NSString*)secret;
- (NSData *)PPLAES128DecryptWithData:(NSString*)secret;

@end
