//
//  SCKPinAccessLogCount.h
//  SclakBle
//
//  Created by Daniele Poggi on 10/05/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCKPinAccessLogCount : NSObject

/**
 * Numero totale di PIN gestibili sul dispositivo.
 */
@property (nonatomic, assign) NSUInteger pinTotal;

/**
 * Numero totale di PIN utilizzati sul dispositivo.
 */
@property (nonatomic, assign) NSUInteger pinUsed;

/**
 * Numero totale di LOG gestibili sul dispositivo.
 */
@property (nonatomic, assign) NSUInteger logTotal;

/**
 * Numero totale di LOG utilizzati sul dispositivo.
 */
@property (nonatomic, assign) NSUInteger logUsed;

- (instancetype) initWithData:(NSData*)data;

@end
