//
//  SclakPeripheralCalendarEntry.h
//  SclakBle
//
//  Created by Daniele Poggi on 20/04/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

@interface SclakPeripheralPin : NSObject

+ (instancetype*) pinWithData:(NSData*)data;
+ (instancetype*) pinWithJSON:(NSData*)json;
+ (instancetype*) defaultPin;

/**
 * Codice di accesso di lunghezza variabile tra 4 e 6 cifre, la lunghezza del codice è definita nella configurazione dello Sclak, non è possibile utilizzare codici di lunghezza diversa sullo stesso Sclak. Ogni byte contiene due cifre con valore compreso tra 0 e 9.
 */
@property (nonatomic, strong) NSString *code;

/**
 * Codice valido (0) o non Valido (1)
 */
@property (nonatomic, assign) BOOL invalidCode;

- (NSData*) toData;
- (NSString*) toJSON;

#pragma mark - exposed for unit testing

+ (NSData*) pinCodeWithString:(NSString*)pin;
+ (NSString*) pinCodeFromData:(NSData*)data;

#pragma mark - Revision

+ (NSString*) revision;

@end
