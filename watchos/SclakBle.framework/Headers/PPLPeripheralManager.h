//
//  PPLPeripheralManager.h
//  PassePartoutLib
//
//  Created by Daniele Poggi on 31/07/14.
//  Copyright (c) 2014 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

#if TARGET_OS_IPHONE

#import <CoreBluetooth/CoreBluetooth.h>

#elif TARGET_OS_MAC

#import <IOBluetooth/IOBluetooth.h>

#endif

// peripheral mode preferences
#define kPeripheralModeBeaconDataOption                             @"kPeripheralModeBeaconDataOption"
#define kPeripheralModeSecretCodeOption                             @"kPeripheralModeSecretCodeOption"

// notification center
#define kPeripheralModeReceivedCommandNotification                  @"kPeripheralReceivedCommandNotification"
#define kPeripheralModeReceivedDataNotification                     @"kPeripheralModeReceivedDataNotification"
#define kPeripheralModeReceivedIdentifyNotification                 @"kPeripheralModeReceivedIdentifyNotification"

@interface PPLPeripheralManager : NSObject <CBPeripheralManagerDelegate>

@property (nonatomic, assign) BOOL peripheralMode;
@property (nonatomic, assign) BOOL peripheralAuthenticated;
@property (nonatomic, assign) dispatch_queue_t managerQueue;
@property (nonatomic, readonly, strong) CBPeripheralManager *peripheralManager;
@property (nonatomic, strong) CBMutableCharacteristic *writeCharacteristic;
@property (nonatomic, strong) CBMutableCharacteristic *readNotifyCharacteristic;
@property (nonatomic, strong) CBMutableCharacteristic *identifyCharacteristic;
@property (nonatomic, strong) NSData *peripheralCOD;
@property (nonatomic, strong) NSDictionary *peripheralAnnouncementData;

@property (nonatomic, assign) uint8_t currentCommand;
@property (nonatomic, assign) uint8_t *currentBuffer;

- (id) initWithQueue:(dispatch_queue_t)theQueueIf andOptions:(NSDictionary*)theOptionsIf;

#pragma mark - peripheral events

@property (nonatomic, copy) void (^onReceiveCommand) (PPLPeripheralManager *manager, NSNumber *command);
@property (nonatomic, copy) void (^onReceiveIdentify) (PPLPeripheralManager *manager);

- (void) rebuildPeripheralManager;

- (void) respondCommand:(const uint8_t)command toCentral:(CBCentral*)central;
- (void) respondCommand:(const uint8_t)command value:(const uint8_t)value toCentral:(CBCentral*)central;
- (void) respondCommand:(const uint8_t)command values:(const uint8_t[])values length:(size_t)len toCentral:(CBCentral*)central;

@end
