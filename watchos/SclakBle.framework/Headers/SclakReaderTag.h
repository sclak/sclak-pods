//
//  SclakReaderTag.h
//  SclakBle
//
//  Created by Daniele Poggi on 23/01/2020.
//  Copyright © 2020 sclak. All rights reserved.
//

typedef enum : NSUInteger {
    Tag_undetected,
    Tag_detected,
    Tag_detected_invalid_code
} SclakTagPresenceStatus;

@interface SclakReaderTag : NSObject

@property (nonatomic, assign) SclakTagPresenceStatus status;
@property (nonatomic, strong) NSString *pinCode;

@end
