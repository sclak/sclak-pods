//
//  ODLGraftStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 29/09/15.
//  Copyright © 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ODLPumpLineStatus) {
    ODLPumpLineStatusUnknown,
    ODLPumpLineStatusEmpty,
    ODLPumpLineStatusFull,
    ODLPumpLineStatusPressurized,
    ODLPumpLineStatusEmptying,
    ODLPumpLineStatusFilling
};

@interface ODLGraftStatus : NSObject

/**
 * Stato pompa di messa in pressione linea
 */
@property (nonatomic, assign) ODLPumpLineStatus pumpLineStatus;

/**
 * Posizione del pistone di messa in pressione linea birra, 0 corrisponde a pompa piena. Valore espresso in um in formato MSByte first.
 */
@property (nonatomic, strong) NSNumber *pompPos;

/**
 * Temperatura attuale birra nel fusto, espressa in gradi celsius con offset -100°.
 */
@property (nonatomic, strong) NSNumber *tempKeg;

/**
 * Temperatura ambiente misurate sull’innesto, espressa in gradi celsius con offset -100°
 */
@property (nonatomic, strong) NSNumber *tempAmb;

/**
 * Portata attuale Birra in ml/min.
 */
@property (nonatomic, strong) NSNumber *flowBeer;

#pragma mark -

- (instancetype) initWithData:(NSData*)data;

@end
