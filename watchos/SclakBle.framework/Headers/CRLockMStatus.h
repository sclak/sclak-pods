//
//  CRLockMStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 09/02/2019.
//

#import "CRLockStatus.h"

@interface CRLockMStatus : CRLockStatus

/**
 * Ingresso stato serratura meccanico (1 chiuso, 0 aperto)
 */
@property (nonatomic, assign) BOOL mechanicalLockStatus;

/**
 * Ingresso stato serratura magnetico (1 chiuso, 0 aperto)
 */
@property (nonatomic, assign) BOOL magneticLockStatus;

/**
 * Ingresso finecorsa serratura bloccata (1 chiuso, 0 aperto)
 */
@property (nonatomic, assign) BOOL limitLockLocked;

/**
 * Ingresso finecorsa serratura sbloccata (1 chiuso, 0 aperto)
 */
@property (nonatomic, assign) BOOL limitLockUnlocked;

/**
 * Ingresso pulsante esterno (1 chiuso, 0 aperto)
 */
@property (nonatomic, assign) BOOL externalButtonStatus;

- (void) updateWithData:(NSData*)data;

@end
