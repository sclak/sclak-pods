//
//  BWGPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 10/07/2019.
//  Copyright © 2019 sclak. All rights reserved.
//

#import "BWGWiFiSSID.h"
#import "BWGDebugLog.h"
#import "BWGConnectionStatus.h"
#import "SclakPeripheral.h"
#import "BWGUserConfiguration.h"

#define kBWGConnectionStatusChanged @"kBWGConnectionStatusChanged"
#define kBWGDebugLogReceived @"kBWGDebugLogReceived"

@interface BWGPeripheral : SclakPeripheral

@property (nonatomic, strong) BWGUserConfiguration *userConfiguration;

@property (nonatomic, strong) NSString *bwgFirmwareVersion;
@property (nonatomic, strong) NSString *bwgFirmwareDate;
@property (nonatomic, strong) NSString *bwgFirmwareCRC;

#pragma mark - Debug Logs

@property (nonatomic, readonly, strong) NSDateFormatter *logsFormatter;
@property (atomic, readonly, strong) NSArray<NSDictionary*>* debugLogs;
@property (nonatomic, readonly, strong) NSDictionary* debugLogsRegexFilters;

#pragma mark - Connection Status

@property (nonatomic, readonly, strong) BWGConnectionStatus *connectionStatus;

#pragma mark - Connection Status History

@property (atomic, readonly, strong) NSArray<BWGConnectionStatus*>* connectionStatusHistory;

#pragma mark - WiFi SSID List

@property (atomic, readonly, strong) NSMutableArray<BWGWifiSSID*>* wifiList;

#pragma mark - GSM

- (NSInteger) getGsmPowerLevel;

#pragma mark - Program Device

- (void) readProgramDeviceCallback:(BluetoothResponseCallback)callback;

/**
 * change program device configuration
 */
- (void) sendProgramDevice:(BWGProgramMode)mode callback:(BluetoothResponseCallback)callback;

#pragma mark - GSM Configuration

- (void) sendAPN:(NSString*)APN callback:(BluetoothResponseCallback)callback;
- (void) readAPNCallback:(BluetoothResponseCallback)callback;

- (void) sendUserGSM:(NSString*)userGSM callback:(BluetoothResponseCallback)callback;
- (void) readUserGSMCallback:(BluetoothResponseCallback)callback;

- (void) sendPwdGSM:(NSString*)pwdGSM callback:(BluetoothResponseCallback)callback;
- (void) readPwdGSMCallback:(BluetoothResponseCallback)callback;

#pragma mark - WiFi Configuration

- (void) sendAccessPointWiFi:(NSString*)apWifi callback:(BluetoothResponseCallback)callback;
- (void) readAccessPointWiFiCallback:(BluetoothResponseCallback)callback;

- (void) sendPwdWiFi:(NSString*)pwdWifi callback:(BluetoothResponseCallback)callback;
- (void) readPwdWiFiCallback:(BluetoothResponseCallback)callback;

#pragma mark - MQTT

- (void) sendUrlMQTT:(NSString*)urlMQTT callback:(BluetoothResponseCallback)callback;
- (void) readUrlMQTTCallback:(BluetoothResponseCallback)callback;

- (void) sendPortMQTT:(NSUInteger)portMQTT callback:(BluetoothResponseCallback)callback;
- (void) readPortMQTTCallback:(BluetoothResponseCallback)callback;

- (void) sendUserMQTT:(NSString*)userMQTT callback:(BluetoothResponseCallback)callback;
- (void) readUserMQTTCallback:(BluetoothResponseCallback)callback;

- (void) sendPwdMQTT:(NSString*)pwdMQTT callback:(BluetoothResponseCallback)callback;
- (void) readPwdMQTTCallback:(BluetoothResponseCallback)callback;

- (void) sendPingMQTT:(NSUInteger)pingMQTT callback:(BluetoothResponseCallback)callback;
- (void) readPingMQTTCallback:(BluetoothResponseCallback)callback;

#pragma mark - Configuration Params

- (void) sendConfigurationParams:(BWGUserConfiguration*)configuration callback:(BluetoothResponseCallback)callback;
- (void) readConfigurationParamsCallback:(BluetoothResponseCallback)callback;

#pragma mark - GSM

- (void) readGsmOperatorCallback:(BluetoothResponseCallback)callback;
- (void) readGsmCCIDCallback:(BluetoothResponseCallback)callback;

#pragma mark - User Configuration

/**
 *  send user configuration
 *
 *  @param userConfiguration
 *  @param callback
 */
- (void) sendUserConfiguration:(BWGUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

#pragma mark - BWG Firmware Version

- (void) readBWGFirmwareVersionCallback:(BluetoothResponseCallback)callback;

#pragma mark - GSM / WiFi / MQTT Connection status

- (void) readConnectionStatusCallback:(BluetoothResponseCallback)callback;

#pragma mark - Scan WiFi

- (void) requestScanWifiCallback:(BluetoothResponseCallback)callback;

#pragma mark - Debug Logs

- (void) sendDebugLogs:(NSUInteger)channel level:(NSUInteger)level clearLogs:(BOOL)clearLogs callback:(BluetoothResponseCallback)callback;

@end
