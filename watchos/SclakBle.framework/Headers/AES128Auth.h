//
//  Auth2Utils.h
//  PassePartoutLib
//
//  Created by Daniele Poggi on 01/12/14.
//  Copyright (c) 2014 sclak. All rights reserved.
//

#import "SclakAuth.h"

@interface AES128Auth : SclakAuth

// auth commands
extern const uint8_t kPhoneToDevice_INIT_AUTH;
extern const uint8_t kDeviceToPhone_SEND_SEED;
extern const uint8_t kPhoneToDevice_SEND_AUTH;
extern const uint8_t kDeviceToPhone_AUTH_ACK;

@end
