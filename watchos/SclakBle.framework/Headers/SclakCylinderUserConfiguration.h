//
//  SclakCylinderUserConfiguration.h
//  SclakBle
//
//  Created by Daniele Poggi on 20/03/2020.
//  Copyright © 2020 sclak. All rights reserved.
//

#import "PPLGenericUserConfiguration.h"

@interface SclakCylinderUserConfiguration : PPLGenericUserConfiguration

@end
