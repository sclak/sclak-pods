//
//  SclakDriverUserConfiguration.h
//  SclakBle
//
//  Created by Daniele Poggi on 21/09/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "PPLGenericUserConfiguration.h"

@interface SclakDriverUserConfiguration : PPLGenericUserConfiguration

@end
