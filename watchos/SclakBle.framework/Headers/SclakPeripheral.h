//
//  SclakPeripheral.h
//  PassePartoutLib
//
//  Created by Daniele Poggi on 07/01/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import "PPLDiscoveredPeripheral.h"
#import "SCKPinAccessLog.h"
#import "SCKPowerProfile.h"
#import "SCKBatteryStatus.h"
#import "SHA256Peripheral.h"
#import "SCKBatteryFeature.h"
#import "PPLPeripheralUsage.h"
#import "SclakPeripheralPin.h"
#import "SclakPeripheralPin1.h"
#import "SCKPinAccessLogCount.h"
#import "SCKUserConfiguration.h"
#import "SclakPeripheralPin106.h"

#define kUSER_CONFIGURATION_MAX_LED_POWER       50
#define kSET_OUT_DEFAULT_PIN                    @"FFFFFF"

typedef NS_ENUM(NSUInteger, SclakCommandStatus) {
    SclakCommand_OK,
    SclakCommand_KO_PIN_OR_PUK_NOT_FOUND,
    SclakCommand_KO_PIN_OR_PUK_NOT_VALID,
    SclakCommand_KO_PIN_OR_PUK_VALID_WRONG_TIME
};

typedef NS_ENUM(NSUInteger, SclakMemoryPartition) {
    SclakMemoryPartition_Internal,
    SclakMemoryPartition_Ext64kb_2000pin_0log,
    SclakMemoryPartition_Ext64kb_1500pin_2000log,
    SclakMemoryPartition_Ext64kb_1000pin_4000log,
    SclakMemoryPartition_Ext64kb_500pin_6000log
};

// pin / puk error codes
#define kPIN_OR_PUK_NOT_FOUND                   1
#define kPIN_OR_PUK_NOT_VALID                   2
#define kPIN_OR_PUK_VALID_BUT_WRONG_TIME        3

// sclak specific commands
extern const uint8_t kPhoneToDevice_GET_IN_OUT;
extern const uint8_t kDeviceToPhone_RESP_GET_IN_OUT;
extern const uint8_t kPhoneToDevice_SET_CFG_USER;
extern const uint8_t kDeviceToPhone_RESP_SET_CFG_USER;
extern const uint8_t kPhoneToDevice_REQ_CFG_USER;
extern const uint8_t kDeviceToPhone_RESP_REQ_CFG_USER;
extern const uint8_t kPhoneToDevice_REQ_DIAGN;
extern const uint8_t kDeviceToPhone_RESP_REQ_DIAGN;
extern const uint8_t kPhoneToDevice_GET_PART_PIN_LOG;
extern const uint8_t kDeviceToPhone_RESP_GET_PART_PIN_LOG;
extern const uint8_t kPhoneToDevice_SET_PART_PIN_LOG;
extern const uint8_t kDeviceToPhone_RESP_SET_PART_PIN_LOG;
extern const uint8_t kPhoneToDevice_GET_COUNT_PIN_LOG;
extern const uint8_t kDeviceToPhone_RESP_GET_COUNT_PIN_LOG;
extern const uint8_t kPhoneToDevice_GET_RECORD_LOG;
extern const uint8_t kDeviceToPhone_RESP_GET_RECORD_LOG;
extern const uint8_t kPhoneToDevice_SET_PRM_BATT;
extern const uint8_t kDeviceToPhone_RESP_SET_PRM_BATT;
extern const uint8_t kPhoneToDevice_GET_PRM_BATT;
extern const uint8_t kDeviceToPhone_RESP_GET_PRM_BATT;
extern const uint8_t kPhoneToDevice_REQ_STS_BATT;
extern const uint8_t kDeviceToPhone_RESP_REQ_STS_BATT;
extern const uint8_t kPhoneToDevice_REQ_RESET_CFG;
extern const uint8_t kDeviceToPhone_RESP_REQ_RESET_CFG;
extern const uint8_t kPhoneToDevice_SET_PRM_ADV;
extern const uint8_t kDeviceToPhone_RESP_SET_PRM_BATT;
extern const uint8_t kPhoneToDevice_REQ_SET_PROF;
extern const uint8_t kDeviceToPhone_RESP_SET_PROF;

typedef void(^GetPinCallback)(BOOL success, SclakPeripheralPin *pin, NSException *ex);

typedef void(^GetAccessLogCallback)(BOOL success, NSError *error, SCKPinAccessLog *accessLog);
typedef void(^GetAccessLogCountCallback)(BOOL success, NSError *error, SCKPinAccessLogCount *accessLogCount);

@interface SclakPeripheral : PPLDiscoveredPeripheral

#pragma mark - Properties

/**
 *  status of relè port 1.
 *  if nil, status has not been read
 *  YES: sclak relè ON (sclak "opened") (closed circuit)
 *  NO: sclak relè OFF (sclak "closed") (opened circuit). default OFF
 */
@property (nonatomic, strong) NSNumber *port1Status;

/**
 * status of input port 1
 * if nil, status has not been read
 */
@property (nonatomic, strong) NSNumber *input1Status;

/**
 *  user configuration
 */
@property (nonatomic, strong) SCKUserConfiguration *userConfiguration;

/**
 * battery status (when available)
 */
@property (nonatomic, strong) SCKBatteryStatus *batteryStatus;

/**
 * battery feature (when available)
 */
@property (nonatomic, strong) SCKBatteryFeature *batteryFeature;

/**
 *  Date / Time to be set on sclak at first connection
 */
@property (nonatomic, strong) NSDate *dateTime;

@property (nonatomic, assign) NSUInteger pinCount;
@property (nonatomic, assign) NSUInteger registryCount;

@property (nonatomic, assign) NSUInteger groupId;
@property (nonatomic, strong) NSString *groupSecret;

// Diagnostics
@property (nonatomic, assign) NSUInteger countOpen;

/**
 * memory partition from firmware >= 4.55
 */
@property (nonatomic, assign) SclakMemoryPartition memoryPartition;

/**
 * profilazione del rapporto Prestazioni/Consumi per i dispositivi Sclak, principalmente per quelli alimentati a batteria
 */
@property (nonatomic, strong) SCKPowerProfile *powerProfile;

/**
 * callback called when RESP_GET_IN_OUT is received autonomously
 */
@property (nonatomic, readonly, strong) BluetoothResponseErrorCallback inputOutputChangedCallback;

#pragma mark - User Configuration

/**
 * Abilitazione segnalazione LED su attivazione uscita (0 segnalazione disattiva, 1 segnalazione attiva).
 */
- (void) setLed:(BOOL)enabled;

/**
 * Livello di luminosità richiesto per i led espresso in percentuale (0 led spenti, 100 led accesi al massimo livello).
 */
- (void) setLedBrightness:(NSUInteger)brightness;

/**
 * Abilitazione segnalazione Buzzer su attivazione uscita (0 segnalazione disattiva, 1 segnalazione attiva).
 */
- (void) setBuzzer:(BOOL)enabled;

/**
 * Flag Sclak Feedback Pin Invalido (0 segnalazione disattiva, 1 attiva).
 */
- (void) setWrongPinFeedbackEnabled:(BOOL)enabled;

/**
 * Periodo di attivazione uscita in unità di 100ms, permette di impostare la disattivazione automatica dell’uscita dopo un certo periodo, se è a 0 il comando è di tipo bistabile (la disattivazione deve essere effettuata dallo Smartphone).
 * L’utilizzo di questo campo dipende dal valore del campo FLAG nel comando di attivazione uscita.
 */
- (void) setAutoCloseTime:(NSUInteger)time;

/**
 * Flag periferica installata (0 Non Installato, 1 Installato).
 */
- (void) setInstalled:(BOOL)installed;

#pragma mark - Authenticate and upload user configuration

- (void) connectAndSendUserConfigurationCallback:(BluetoothResponseCallback)callback;

#pragma mark - Execute PPLPeripheralUsage

- (void) executeUsage:(PPLPeripheralUsage* _Nonnull)usage
      connectCallback:(BluetoothResponseCallback)callback
         authCallback:(BluetoothAuthCallback)authCallback
     responseCallback:(BluetoothResponseErrorCallback)responseCallback
   disconnectCallback:(BluetoothResponseCallback)disconnectCallback;

#pragma mark - Methods

/**
 *  read port in and port out statuses
 *  sets properties port1Status, input1Status
 *
 *  @param callback when finished
 */
- (void) getInOutCallback:(BluetoothResponseCallback)callback;

/**
 * set callback for autonomous getInOut response
 *
 *  @param callback when finished
 */
- (void) setGetInOutCallback:(BluetoothResponseErrorCallback)callback;

/**
 * unset callback for autonomous getInOut response
 */
- (void) unsetGetInOutCallback;

/**
 *  read user configuration
 *
 *  @param callback
 */
- (void) getUserConfigurationCallback:(BluetoothResponseCallback)callback;

/**
 * send current user configuration
 *  @param callback
 */
- (void) sendUserConfigurationCallback:(BluetoothResponseCallback)callback;

/**
 *  send user configuration
 *
 *  @param userConfiguration
 *  @param callback
 */
- (void) sendUserConfiguration:(SCKUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

/**
 *  send a generic user configuration
 *
 *  @param userConfiguration
 *  @param callback
 */
- (void) sendGenericUserConfiguration:(PPLGenericUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

/**
 *  read diagnostics (actually, read countOpen property)
 *
 *  @param callback
 */
- (void) readDiagnosticsCallback:(BluetoothResponseCallback)callback;

#pragma mark - Sclak Mask Commands

/**
 * check if SET_OUT command contains the mask byte
 * default YES for sclak command
 * varies from product to product and NO is a legacy option
 */
- (BOOL) setOutContainsMask;

/**
 *  read relè port status. When callback is called, this.port1Status will be proper setted.
 *
 *  @param command
 *  @param callback
 */
- (void) readPortStatusCallback:(BluetoothResponseErrorCallback)callback;

/**
 *  sclak command legacy method (without PIN and autoclose time)
 *
 *  @param command
 *  @param callback
 */
- (void) sclakCommand:(SclakCommand)command callback:(BluetoothResponseErrorCallback)callback;

/**
 *  sclak command legacy method (without PIN)
 *
 *  @param command
 *  @param time     autoclose time (tenth of second)
 *  @param callback
 */
- (void) sclakCommand:(SclakCommand)command autocloseTime:(NSUInteger)time callback:(BluetoothResponseErrorCallback)callback;

/**
 *  sclak command with PIN
 *
 *  @param command
 *  @param pin      PIN code
 *  @param isPuk    true if PIN code is PUK, false otherwise
 *  @param callback
 */
- (void) sclakCommand:(SclakCommand)command withPin:(NSString*)pin isPuk:(BOOL)isPuk callback:(BluetoothResponseErrorCallback)callback;

/**
 *  sclak command with PIN and autoclose time
 *
 *  @param command
 *  @param time
 *  @param pin
 *  @param isPuk
 *  @param callback
 */
- (void) sclakCommand:(SclakCommand)command autocloseTime:(NSUInteger)time pin:(NSString*)pin isPuk:(BOOL)isPuk callback:(BluetoothResponseErrorCallback)callback;

/**
 *  sclak command with PIN and autoclose time, simulation of keyboard
 *
 *  @param command
 *  @param time
 *  @param pin
 *  @param isKeyb
 *  @param isPuk
 *  @param dtOn     Flag utilizzo tempo di attivazione uscite impostato su Sclak o presente nel comando (1 utilizzo tempo impostato su Sclak, 0 utilizzo tempo presente nel comando). from spec >= 109
 *  @param callback
 */
- (void) sclakCommand:(SclakCommand)command autocloseTime:(NSUInteger)time pin:(NSString*)pin isKeyboard:(BOOL)isKeyb isPuk:(BOOL)isPuk dtOn:(BOOL)dtOn callback:(BluetoothResponseErrorCallback)callback;

/**
 * sclak command all features
 */
- (void) sclakCommand:(SclakCommand)command
        autocloseTime:(NSUInteger)time
                  pin:(NSString*)pin
           isKeyboard:(BOOL)isKeyb
                isPuk:(BOOL)isPuk
                 dtOn:(BOOL)dtOn
                 mask:(const uint8_t)mask
             callback:(BluetoothResponseErrorCallback)callback;

/**
 * SEND_OUT command used by sclak command all features
 */
- (void) sendOutWithAction:(uint8_t)cmd_out
                  duration:(uint8_t)dt_on
                       pin:(NSString*)pin
                isKeyboard:(BOOL)isKeyb
                     isPuk:(BOOL)isPuk
                      dtOn:(BOOL)dtOn
                      mask:(const uint8_t)mask
                  callback:(BluetoothResponseErrorCallback)callback;

#pragma mark - Date / Time

- (void) readDateTimeCallback:(BluetoothResponseCallback)callback;
- (void) sendDateTime:(NSDate*)dateTime callback:(BluetoothResponseCallback)callback;

#pragma mark - PUK

- (void) sendPUK:(NSString*)puk callback:(BluetoothResponseErrorCallback)callback;
- (void) setPUK:(NSString*)puk callback:(BluetoothResponseErrorCallback)callback;
- (void) unsetPUK:(NSString*)puk callback:(BluetoothResponseErrorCallback)callback;

#pragma mark - Reset to factory defaults

- (void) resetWithPUK:(NSString*)puk deleteAllPins:(BOOL)deleteAllPins callback:(BluetoothResponseErrorCallback)callback;

#pragma mark - PIN

- (void) readPinCountCallback:(BluetoothResponseCallback)callback;

- (void) setPinAtIndex:(NSUInteger)index pin:(SclakPeripheralPin*)pin callback:(BluetoothResponseCallback)callback;
- (void) cancelSetPin;

- (void) getPinAtIndex:(NSUInteger)index callback:(GetPinCallback)callback;
- (void) deletePinAtIndex:(NSUInteger)index callback:(BluetoothResponseCallback)callback;

#pragma mark - Group Management

- (NSString*) clonedSecret:(NSString*)secretCodeToClone;

/**
 *  Use this method only when managing a peripheral group
 *  
 *
 *  @param secretCode   secret of ancestor
 *  @param callback     callback
 */
- (void) setSecretCode:(NSString*)secretCode callback:(BluetoothResponseCallback)callback;

- (void) readGroupIdCallback:(BluetoothResponseCallback)callback;
- (void) sendGroupId:(NSUInteger)groupId callback:(BluetoothResponseCallback)callback;

- (void) readMemoryPartitionCallback:(BluetoothResponseCallback)callback;
- (void) sendMemoryPartition:(SclakMemoryPartition)parition callback:(BluetoothResponseCallback)callback;

#pragma mark - Battery Management

/**
 * set parameters for battery consumption
 * command not available inside SCLAK app or in SDK mode. Available only on SCLAK UNIT
 */
- (void) setBatteryFeature:(SCKBatteryFeature*)feature callback:(BluetoothResponseCallback)callback;
- (void) readBatteryFeatureCallback:(BluetoothResponseCallback)callback;

/**
 * read battery status. when callback is called, batteryStatus variable will be available
 */
- (void) readBatteryStatusCallback:(BluetoothResponseCallback)callback;

/**
 * Comando di sostituzione delle batterie reimposta il livello di carica al massimo
 */
- (void) sendBatteryReplaceCommandCallback:(BluetoothResponseCallback)callback;

#pragma mark - Access Logs

/**
 * read number of pin total/used and access logs total/used
 */
- (void) readPinLogCountCallback:(GetAccessLogCountCallback)callback;

/**
 * read next access log
 * Tramite questo comando è possibile leggere in maniera sequenziale (dal più vecchio) i record di LOG presenti sul dispositivo, i record sono cancellati una volta trasmessi e tornano disponibili per la memorizzazione.
 */
- (void) readNextAccessLogCallback:(GetAccessLogCallback)callback;

#pragma mark - Power Profile

/**
 * Messaggio di lettura parametri advertising attuali:
 */
- (void) readPowerProfileCallback:(BluetoothResponseCallback)callback;

/**
 * Messaggio di richiesta Impostazione parametri advertising
 */
- (void) setPowerProfile:(SCKPowerProfile*)profile callback:(BluetoothResponseCallback)callback;

/**
 * Messaggio di richiesta Impostazione profilo consumi
 */
- (void) setSelectedPowerProfile:(SclakPowerProfile)profile callback:(BluetoothResponseCallback)callback;

#pragma mark - Reset to factory defaults

- (void) requestResetFactoryDefaultsCallback:(BluetoothResponseCallback)callback;

#pragma mark - Legacy Methods

- (void) getPairedBtcodeCallback:(BluetoothResponseCallback)callback;
- (void) sendPairingWithBtcode:(NSString*)btcode secret:(NSString*)secret callback:(BluetoothResponseCallback)callback;

@end
