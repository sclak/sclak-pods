//
//  GivikStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 01/11/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, GivikLockStatus) {
    GivikStatusUnknown,
    GivikStatusUnlocked,
    GivikStatusUnlocking,
    GivikStatusLocked,
    GivikStatusLocking,
    GivikStatusError
};

@interface GivikStatus : NSObject

@property (nonatomic, assign) GivikLockStatus lockStatus;

/**
 * Ingresso sensore serratura Aperta
 */
@property (nonatomic, assign) BOOL sensorlockOpenStatus;

/**
 * Ingresso sensore serratura Chiusa
 */
@property (nonatomic, assign) BOOL sensorlockCloseStatus;

/**
 * ingresso sensore di stato di apertura/chiusura del corpo del bauletto
 */
@property (nonatomic, assign) BOOL hullOpenStatus;

- (instancetype) initWithData:(NSData*)data;

@end
