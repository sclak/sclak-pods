//
//  SclakAuth.h
//  PassePartoutLib
//
//  Created by Daniele Poggi on 12/12/14.
//  Copyright (c) 2014 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PPLDiscoveredPeripheral;
@class PPLCentralManager;
@class CBCharacteristic;

enum ecc_auth_req_res {
    /*
     * Autenticazione terminata OK, nella risposta sono presenti i campi CERT e RANDOM.
     */
    ecc_auth_ok = 0x00,
    /*
     * Errore mancanza dati identificativi; sul dispositivo Sclak non sono satti caricati
     * il certificato EDSC o la chiave pubblica della CA.
     */
    ecc_auth_missing_data = 0x10,
    /*
     * Errore identificativo destinatario; il codice BTCode o l’identificativo di gruppo
     * presenti nel certificato inviato non corrispondono a quelli del dispositivo Sclak
     */
    ecc_auth_wrong_btcode_or_group_id = 0x11,
    /*
     * Errore validazione temporale; la validazione temporale basata sui campi contenuti
     * nel certificato è fallita
     */
    ecc_auth_datetime_validity_error = 0x12,
    /*
     * Errore contesto; sul dispositivo sclak non ci sono più contesti disponibili,
     * è stato superato il massimo numero di connessioni autenticate contemporanee
     */
    ecc_auth_no_more_contexts_error = 0x13,
    /*
     * Errore formato certificato EDCC; all’interno del certificato inviato non è stato
     * trovato il campo contenente la chiave pubblica
     */
    ecc_auth_public_key_not_found = 0x14,
    /*
     * Errore validazione certificato EDCC; la validazione del certificato tramite
     * ECDSA utilizzando la chiave pubblica della CA Sclak è fallita
     */
    ecc_auth_ecdsa_failed = 0x20,
    /*
     * Errore generazione chiave ECDH; si è riscontrato un problema nella generazione
     * della chiave con ECDH
     */
    ecc_auth_ecdh_failed = 0x21
    
};

typedef void(^BluetoothAuthCallback)(BOOL success, PPLDiscoveredPeripheral *peripheral, NSError *error);
typedef void(^KeyGenerationCallback)(BOOL success, NSString *secret, NSException *ex);
typedef void(^RemoteGenerationCallback)(BOOL success, NSNumber *sequence, NSString *romId, NSString *initialCode, NSException *ex);

@protocol SclakAuthProtocol <NSObject>

- (void) initKeyGenerationCallback:(KeyGenerationCallback)callback;

- (NSData*) didUpdateValueForCharacteristic:(CBCharacteristic*)characteristic error:(NSError*)error data:(NSData*)data;

/**
 * encrypt a message with a new OTP and return the encrypted bytes
 */
- (NSData*) encryptData:(NSData*)data;

/**
 * decrypt a message with a new OTP and return the decrypted bytes
 */
- (NSData*) decryptData:(NSData*)aData;

- (void) sendSecureCommand:(NSData*)data;
- (void) sendSecureCommand:(uint8_t[])commandBytes length:(int)length;

- (void) authenticateDateTime:(NSDate*)dateTime
        inhibitAutoDisconnect:(BOOL)inhibitAutoDisconnect
                     callback:(BluetoothAuthCallback)callback;

@end

@interface SclakAuth : NSObject <SclakAuthProtocol>

@property (nonatomic, readonly, strong) PPLDiscoveredPeripheral *peripheral;
@property (nonatomic, readonly, strong) PPLCentralManager *manager;
@property (nonatomic, assign) BOOL authResponseReceived;

- (instancetype) initWithPeripheral:(PPLDiscoveredPeripheral*)peripheral;

- (void) print:(NSString*)varName data:(NSData*)data;
- (void) print:(NSString*)varName var:(uint8_t[])var length:(int)length;

@end
