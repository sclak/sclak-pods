//
//  NSData+PPLStringGenerator.h
//  PassePartoutLib
//
//  Created by Daniele Poggi on 10/01/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (PPLStringGenerator)

/**
@deprecated name of function, use dataToHexString
*/
- (NSString*) hexDataToString;
- (NSString*) dataToHexString;
- (NSString*) MD5String;

@end
