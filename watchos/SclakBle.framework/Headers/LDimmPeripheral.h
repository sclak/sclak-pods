//
//  LDimmPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 08/04/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import "SclakPeripheral.h"

@interface LDimmPeripheral : SclakPeripheral

@property (nonatomic, assign) NSUInteger dimmingLevel;

- (void) sendDimming:(NSUInteger)dimming callback:(BluetoothResponseCallback)callback;

@end
