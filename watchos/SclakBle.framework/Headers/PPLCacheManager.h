//
//  PPLPeripheralCacheManager.h
//  SclakBle
//
//  Created by Daniele Poggi on 29/05/16.
//  Copyright © 2016 sclak. All rights reserved.
//

#import "PPLDiscoveredPeripheral.h"

@interface PPLCacheManager : NSObject

/**
 * directory where the cache is stored
 * if not customized, NSDocumentDirectory will be used
 */
@property (nonatomic, strong) NSURL *directory;

/**
 *  singleton build
 *
 *  @return singleton instance of class
 */
+ (instancetype) getInstance;

- (id) parseFile:(NSString*)file;
- (BOOL) writeDictionary:(NSDictionary*)model toFile:(NSString*)file;
- (BOOL) deleteFile:(NSString*)file;

/**
 * test: write a cache file
 */
- (BOOL) writeTestFile;

@end
