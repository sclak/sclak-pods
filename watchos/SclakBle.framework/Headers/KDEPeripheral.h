//
//  SclakCabinetPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 14/11/2017.
//  Copyright © 2017 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "KDEStatus.h"
#import "KDEUserConfiguration.h"

@interface KDEPeripheral : SclakPeripheral

@property (nonatomic, strong) KDEUserConfiguration *userConfiguration;

@property (nonatomic, strong) KDEStatus *status;
@property (nonatomic, assign) BOOL hallStatus;

#pragma mark - User Configuration

- (void) sendUserConfiguration:(KDEUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

@end
