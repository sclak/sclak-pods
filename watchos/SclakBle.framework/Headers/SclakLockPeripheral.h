//
//  SclakLockPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 12/04/2017.
//  Copyright © 2017 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "SclakLockStatus.h"
#import "SclakLockUserConfiguration.h"

@interface SclakLockPeripheral : SclakPeripheral

/**
 * Lock Statuses
 */
@property (nonatomic, strong) SclakLockStatus *status;

#pragma mark - User Configuration

/**
 *  user configuration
 */
@property (nonatomic, strong) SclakLockUserConfiguration *userConfiguration;

/**
 * write user configuration API
 */
- (void) sendUserConfiguration:(SclakLockUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

@end
