//
//  PPLBasePeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 11/02/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

#if TARGET_OS_IPHONE

#import <CoreBluetooth/CoreBluetooth.h>

#elif TARGET_OS_MAC

#import <IOBluetooth/IOBluetooth.h>

#endif

#import "PCM.h"
#import "PPLPeripheralType.h"
#import "BluetoothBenchmark.h"
#import "PPLBluetoothCallbacks.h"

// CONSTANTS
#define kMaxRssiQueueLength                      10                   // default: 10
#define kConnectionRetryLimit                    5                    // default: 10

/****************************************************************************/
/*               Ble Delegate identifier                                    */
/****************************************************************************/
#define kDidDiscoverServices                            @"didDiscoverServices"
#define kDidDiscoverCharacteristicsForService           @"didDiscoverCharacteristicsForService"
#define kDidUpdateNotificationStateForCharacteristic    @"didUpdateNotificationStateForCharacteristic"
#define kDidWriteValueForCharacteristic                 @"didWriteValueForCharacteristic"
#define kDidUpdateValueForCharacteristic                @"didUpdateValueForCharacteristic"
#define kPeripheralDidUpdateName                        @"peripheralDidUpdateName"
#define kDidModifyServices                              @"didModifyServices"
#define kBleDidDiscoverPeripheral                       @"BleDidDiscoverPeripheral"
#define kBleDidConnectPeripheral                        @"BleDidConnectPeripheral"
#define kBleDidDiscoverServices                         @"BleDidDiscoverServices"
#define kBleDidInvalidateServices                       @"BleDidInvalidateServices"
#define kBleDidDisconnectPeripheral                     @"BleDidDisconnectPeripheral"
// made by Daniele Poggi
#define kDidAuthenticatePeripheral                      @"kDidAuthenticatePeripheral"
#define kDidDisconnectPeripheral                        @"kDidDisconnectPeripheral"

/****************************************************************************/
/*						OTAU Service for Boot & Application					*/
/****************************************************************************/
// service UUID exposed by the Boot module
#define     serviceBootOtauUuid     @"00001010-D102-11E1-9B23-00025B00A5A5"

/****************************************************************************/
/*				 Boot Loader Service and Characteristics                    */
/****************************************************************************/
// Service
#define characteristicVersionUuid   @"00001011-D102-11E1-9B23-00025B00A5A5"

//  - Characteristic : Application number to be updated
// Write 0 to switch Application to Boot Loader
// Write 1 for Application 1, 2 for Application 2 etc.
#define characteristicCurrentAppUuid    @"00001013-D102-11E1-9B23-00025B00A5A5"


// Data transfer char UUID
// Set up this char for notifications
#define characteristicDataTransferUuid  @"00001014-D102-11E1-9B23-00025B00A5A5"


// Transfer control char UUID
// Setup this for Notifications
// 1 - Ready
// 2-Transfer In Progress
// 3-Transfer Paused
// 4-Transfer Complete
// 5-Transfer failed
// 6-Transfer Aborted
#define characteristicTransferControlUuid @"00001015-D102-11E1-9B23-00025B00A5A5"

/****************************************************************************/
/* Application Service and characteristics related to OTAU                  */
/****************************************************************************/
// This service exposes Over-the-Air update capabilities of CSR μEnergy applications.
#define serviceApplicationOtauUuid  @"00001016-D102-11E1-9B23-00025B00A5A5"


// Read CS-key char UUID
// Subscribe to kOtaDataTransCharUuid
// Write 1 for Bluetooth Mac address
// Write 2 for Crystal Trim
// Notification will be received on kOtaDataTransCharUuid
#define characteristicGetKeysUuid @"00001017-D102-11E1-9B23-00025B00A5A5"

// Read CS-key block char UUID
// Only used to see if the application supports OTAU v5 library
// This way we determine if we expect to see a OTAU v5 bootloader
// which can be directly queried for relevant CS Keys
#define characteristicGetKeyBlockUuid @"00001018-D102-11E1-9B23-00025B00A5A5"


/****************************************************************************/
/*                  Error reporting typedefs                                */
/****************************************************************************/
typedef enum OtauErrorTypes {
    OTAUErrorFailedConnect = 1000,                          // 1000
    OTAUErrorFailedDiscoverServices,                        // 1001
    OTAUErrorFailedRequestChallengeValue,                   // 1002
    OTAUErrorFailedObtainChallengeValue,                    // 1003
    OTAUErrorFailedSubscribeForNotifications,               // 1004
    OTAUErrorFailedDiscoverApplicationCharacteristics,      // 1005
    OTAUErrorFailedGetBTMacAddress,                         // 1006
    OTAUErrorFailedGetCrystalTrim,                          // 1007
    OTAUErrorFailedEnterBoot,                               // 1008
    OTAUErrorFailedQueueAppTransfer,                        // 1009
    OTAUErrorFailedApplicationUpdate,                       // 1010
    OTAUErrorFailedDisconnected,                            // 1011
    OTAUErrorFailedReadBootloaderVersion,                   // 1012
    OTAUErrorFailedDiscoverBootloaderCharacteristics,       // 1013
    OTAUErrorFailedInvalidConfiguration,                    // 1014
    OTAUErrorFailedToStartOTAUTransfer,                     // 1015
    OTAUErrorFailedAppImgFragmentTransfer,                  // 1016
    OTAUErrorFailedAppImgCRCCheck,                          // 1017
    OTAUErrorFailedNotInitialised,                          // 1018
} OtauErrors;

/****************************************************************************/
/*						OTAU Class Delegates                                */
/****************************************************y************************/
@protocol OTAUDelegate <NSObject>
@optional
- (void) didUpdateProgress: (uint8_t) percent;
- (void) completed:(NSString *) message :(NSError *) error;
- (void) statusMessage:(NSString *)message;
- (void) otauError:(NSError *) error;
- (void) initComplete: (uint8_t) otauVersion;
- (void) didGetCsKeyValue: (NSData *) value;
- (void) didChangeConnectionState: (bool) isConnected;
- (void) didChangeMode:(BOOL)isBootMode;
@end

@class PPLCentralManager;

@interface PPLBasePeripheral : NSObject <CBPeripheralDelegate>

#pragma mark - Generic Properties

@property (nonatomic, strong) NSString *name;
@property (nonatomic, copy) NSString *btcode;
@property (nonatomic, strong) NSString *firmwareVersion;
@property (nonatomic, assign) PeripheralType type;
@property (nonatomic, assign) BOOL hasRandomBtcode;

#pragma mark - connection status properties

- (BOOL) isIdle;

- (BOOL) isConnecting;
- (BOOL) isConnected;

@property (nonatomic, assign) BOOL isAuthenticating;
@property (nonatomic, assign) BOOL isAuthenticated;
@property (nonatomic, assign) BOOL requiresAuthentication;

@property (nonatomic, assign) BOOL isDisconnecting;

@property (nonatomic, assign) BOOL waitForNotifyStateChange;
@property (nonatomic, assign) BOOL waitForDisconnect;

@property (nonatomic, assign) BOOL forceDiscoverAllServices;
@property (nonatomic, assign) BOOL forceDiscoverAllCharacteristics;

#pragma mark - operation management

@property (nonatomic, assign) BOOL shouldIdentify;  // used to store state while identifying
@property (nonatomic, assign) BOOL shouldOTAU;      // used to store state while OTAU

#pragma mark - readonly properties

@property (nonatomic, readonly, strong) PPLCentralManager *manager;
@property (nonatomic, readonly, strong) dispatch_queue_t commandsQueue;
@property (nonatomic, strong) CBPeripheral *fPeripheral;

#pragma mark - OTAU properties

@property (atomic, assign) BOOL peripheralInBoot;
@property (nonatomic, assign) BOOL delegateHit;

// benchmark
@property (strong, nonatomic) BluetoothBenchmark *bluetoothBenchmark;

// PUBLIC FIELDS
@property (nonatomic, strong) NSURL *urlImageFile;
@property (nonatomic, strong) id<OTAUDelegate> OTAUDelegate;
@property (nonatomic, strong) BluetoothReadRSSICallback read_RSSI_callback;

- (instancetype) initWithPeripheral:(CBPeripheral*)thePeripheral
                               name:(NSString*)name
                             btcode:(NSString*)btcode
                               RSSI:(NSNumber*)theRSSI
                            manager:(PPLCentralManager*)manager;

- (NSString*) halfBtcode;

#pragma mark - RSSI

- (void) readRSSICallback:(BluetoothReadRSSICallback)callback;

#pragma mark - Generic Methods

- (BOOL) canAuthenticate;

- (void) cancelConnection;
- (void) forceCancelConnection;

- (void) updatePeripheral:(CBPeripheral*)peripheral;

+ (NSString*) extractBtcodeFrom:(NSString*)string btcodeRoot:(NSString*)btcodeRoot;
+ (NSString*) extractFirmwareVersionFrom:(NSString*)string;
- (NSString*) stateString;

#pragma mark - Public Actions

- (PCM*) waitForDelegate:(NSString *)delegate;
- (PCM*) waitForDelegate:(NSString *)delegate failInSeconds:(NSUInteger)seconds;
- (void) deleteDelegateQueue;

- (void) initOTAU;
- (BOOL) startOTAU:(NSString*)firmwareFilepath;
- (void) abortOTAU;

- (void) getCsKey:(uint8_t)csKeyIndex;
- (BOOL) parseCsKeyJson:(NSString*)csKeyJsonFile;
- (BOOL) parseCsKeyJson:(NSString*)csKeyJsonFile inBundle:(NSBundle*)bundle;

- (BOOL) enterAppMode:(int)appMode;
- (BOOL) enterAppMode2;

#pragma mark - Private Class Methods

- (void) onDisconnectWithError:(NSError *)error;

#pragma mark - Utils

- (void) print:(NSString*)varName data:(NSData*)data;
- (void) print:(NSString*)varName var:(uint8_t[])var length:(int)length;

#pragma mark - DEBUGGING

- (void) setDebugName:(NSString*)name;

@end
