//
//  SCKFobMode.h
//  SclakBle
//
//  Created by Daniele Poggi on 25/03/16.
//  Copyright © 2016 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCKFobMode : NSObject

/**
 *  Attivazione validità temporale (1). Se abilitata questa modalità permette la disabilitazione del radiocomando dopo un numero di ore prefissato definito dal campo TIME_VLDY.
 */
@property (nonatomic, assign) BOOL timeValidity;

/**
 *  Tempo di validità del telecomando espresso in ore formato MSByte first, il contatore di validità viene resettato alla ricezione del comando RESET_TIME_VLDY (limite massimo circa 2700gg).
 */
@property (nonatomic, assign) NSUInteger numHoursTimeValidity;

#pragma mark -

- (instancetype) initWithData:(NSData*)data;
- (NSMutableData*) messageData;

@end
