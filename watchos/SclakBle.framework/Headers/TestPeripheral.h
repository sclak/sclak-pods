//
//  TestPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 28/05/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import "PPLDiscoveredPeripheral.h"
#import "SHA256Auth.h"

// sclak specific commands
extern const uint8_t kPhoneToDevice_SET_DIG_OUT;
extern const uint8_t kDeviceToPhone_RESP_DIG_OUT;

@interface TestPeripheral : PPLDiscoveredPeripheral

- (void) initRemoteGenerationCallback:(RemoteGenerationCallback)callback;

@end
