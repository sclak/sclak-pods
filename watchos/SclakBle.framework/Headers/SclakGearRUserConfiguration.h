//
//  SclakGearRUserConfiguration.h
//  SclakBle
//
//  Created by Daniele Poggi on 11/06/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "PPLGenericUserConfiguration.h"

@interface SclakGearRUserConfiguration : PPLGenericUserConfiguration

@end
