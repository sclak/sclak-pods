//
//  SclakGearAInit.h
//  SclakBle
//
//  Created by Daniele Poggi on 25/09/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SclakGearAInitStatus) {
    Uninitialized,
    Initialized,
    Initializing,
    Failure_movement_below_limit
};

typedef NS_ENUM(NSUInteger, SclakGearARotationConfig) {
    HingeOnLeft,
    HingeOnRight
};

@interface SclakGearAInit : NSObject

/**
 * Stato di inizializzazione della serraura:
 * 0x00 Serratura non inizializzata.
 * 0x01 Serratura inizializzata correttamente pronta per l’utilizzo.
 * 0x02 Serratura in fase di inizializzazione.
 */
@property (nonatomic, assign) SclakGearAInitStatus status;

/**
 * Configurazione verso di rotazione (1 cerniera a destra, 0 cerniera a sinistra).
 */
@property (nonatomic, assign) SclakGearARotationConfig rotationConfig;

/**
 * Tempo totale ciclo di movimento in ms.
 */
@property (nonatomic, assign) NSUInteger totalMovementTime;

- (instancetype) initWithData:(NSData*)data;

@end
