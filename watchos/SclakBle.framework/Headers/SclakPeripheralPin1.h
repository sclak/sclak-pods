//
//  SclakPeripheralCalendarEntry.h
//  SclakBle
//
//  Created by Daniele Poggi on 20/04/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import "SclakPinWeekdayTimeSlot.h"
#import "SclakPeripheralPin.h"

#define kValidCodeKey                   @"validCode"
#define kDenyKeyboard                   @"denyKeyboard"
#define kAccessModeKey                  @"accessMode"
#define kAccessTimeSlotsKey             @"accessTimeSlots"
#define kAccessTillEndKey               @"accessTillEnd"
#define kAccessFromStartKey             @"accessFromStart"
#define kAccessCountLimitKey            @"accessCountLimit"
#define kAccessWeeks                    @"accessWeeks"

#define kTimeSlotsKey                   @"timeSlots"
#define kCountKey                       @"count"
#define kBeginDateKey                   @"beginDate"
#define kEndDateKey                     @"endDate"
#define kWeeksKey                       @"weeks"

typedef NS_ENUM(NSUInteger, PinAccessMode) {
    PinAccessModeDaily,
    PinAccessModeWeekly
};

@interface SclakPeripheralPin1 : SclakPeripheralPin

+ (SclakPeripheralPin1*) pinWithData:(NSData*)data;
+ (SclakPeripheralPin1*) pinWithJSON:(NSData*)json;
+ (SclakPeripheralPin1*) defaultPin;

@property (nonatomic, assign) PinAccessMode accessMode;

@property (nonatomic, assign) BOOL accessTimeSlots;
@property (nonatomic, assign) BOOL accessTillEnd;
@property (nonatomic, assign) BOOL accessFromStart;
@property (nonatomic, assign) BOOL accessCountLimit;
@property (nonatomic, assign) BOOL accessWeeks;
@property (nonatomic, assign) BOOL denyKeyboard;

/**
 * Maschera di accesso sulla base dei giorno della settimana, la maschera viene abilitata dal Bit0 del campo FLAG, la gestione è a bit (1 accesso abilitato nel giorno indicato):
 ￼Bit7 Non Utilizzato (0).
 Bit6 Domenica. Bit5 Sabato. Bit4 Venerdì. Bit3 Giovedì. Bit2 Mercoledì. Bit1 Martedì. Bit0 Lunedì.
 */
@property (nonatomic, strong) NSArray *weekdays;

@property (nonatomic, strong) NSMutableDictionary *timeSlots;

/**
 * Flag di abilitazione accesso su base settimanale, è possibile abilitare/disabilitare l’accesso per una specifica settima dell’anno, sono disponibili 7 byte contenenti 53 flag gestiti a bit
 uno per ogni settimana. Il Bit7 del primo byte abilita la prima settimana dell’anno, il Bit3 dell’ultimo byte abilita l’ultima settimana dell’anno.
 */
@property (nonatomic, strong) NSMutableArray *weeks;

/**
 * Conteggio numero di accessi ancora utilizzabili, abilitato dal Bi1 del campo Flag, permette di abilitare il codice per un numero limitato di utilizzi, ad ogni utilizzo il valore COUNT viene decrementato, quando raggiunge 0 l’accesso viene inibito, ed il cluster relativo al codice viene cancellato dalla memoria di Sclak. Il valore massimo del numero di accessi è 255.
 */
@property (nonatomic, assign) NSUInteger count;

@property (nonatomic, strong) NSDate *beginDate;
@property (nonatomic, strong) NSDate *endDate;

#pragma mark - exposed for unit testing

+ (uint16_t) timeSlotToIntBegin:(NSDate*)begin end:(NSDate*)end allDay:(BOOL)allDay disabled:(BOOL)disabled;
+ (SclakPinWeekdayTimeSlot*) timeSlotFromInt:(uint16_t)time weekday:(int)weekday;

- (uint16_t) dateToInt:(NSDate*)date;
- (NSDate*) dateFromInt:(uint16_t)date;

@end
