//
//  BWGWifiSSID.h
//  SclakBle
//
//  Created by Daniele Poggi on 14/10/2020.
//  Copyright © 2020 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BWGWifiSSID : NSObject

/**
 * Indice dell’SSID presente nella lista ottenuta con l’ultima scansione.
 */
@property (nonatomic, assign) NSUInteger index;

/**
 * Numero di SSID rilevati con l’ultima scansione.
 */
@property (nonatomic, assign) NSUInteger count;

/**
 * Indicazione livello del segnale WiFi, valori con segno range - 128dBm..127dBm
 */
@property (nonatomic, assign) NSInteger RSSI;

/**
 * Stringa contenente l’SSID coriispondente a ID_SSID nella lista ottenuta con l’ultima scansione effettuata.
 */
@property (nonatomic, strong) NSString *SSID;

- (instancetype) initWithData:(NSData*)data;

@end
