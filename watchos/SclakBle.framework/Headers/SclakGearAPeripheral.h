//
//  SclakGearAPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 25/07/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "SclakGearAInit.h"
#import "SclakGearAUserConfiguration.h"

// sclak gear A specific commands
extern const uint8_t kPhoneToDevice_INIT_GEAR;
extern const uint8_t kDeviceToPhone_RESP_INIT_GEAR;

extern const uint8_t COMMAND_READ_STATUS;
extern const uint8_t COMMAND_OPEN;
extern const uint8_t COMMAND_CLOSE_AUTO;
extern const uint8_t COMMAND_CLOSE_1_TIME;
extern const uint8_t COMMAND_CLOSE_FULL;

typedef NS_ENUM(NSUInteger, SclakGearALockStatus) {
    Unknown,
    Opened,
    Closed_latch_only,
    Closed_1_time,
    Closed_all,
    Opening,
    Closing,
    Failure_Uninitialized,
    Failure_Init
};

@interface SclakGearAPeripheral : SclakPeripheral

@property (nonatomic, assign) SclakGearALockStatus lockStatus;
@property (nonatomic, strong) SclakGearAInit *initialization;
@property (nonatomic, strong) SclakGearAUserConfiguration *userConfiguration;

#pragma mark - User Configuration

- (void) sendUserConfiguration:(SclakGearAUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

#pragma mark - initialization

/**
 * utilizzato per conoscere lo stato di inizializzazione della serratura
 */
- (void) readInitializationCallback:(BluetoothResponseCallback)callback;

/**
 * gestire l’inizializzazione della serratura, necessaria per definire i limiti di movimento per l’attivazione dello scrocco e la movimentazione del nasello; Il messaggio di risposta viene anche inviato in maniera autonoma dalla serratura a fronte di un cambio di stato. Questi messaggi sono scambiati in modaIità cifrata.
 */
- (void) initializeCallback:(BluetoothResponseCallback)callback;

@end
