//
//  CRLockStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 22/05/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRLockStatus : NSObject

/**
 * Ingresso stato serratura (1 chiusa, 0 aperta)
 */
@property (nonatomic, assign) BOOL lockStatus;

/**
 * Ingresso ausiliario stato porta (1 chiusa, 0 aperta)
 */
@property (nonatomic, assign) BOOL doorStatus;

// CHANGED STATUSES

@property (nonatomic, assign) BOOL lockStatusChanged;
@property (nonatomic, assign) BOOL doorStatusChanged;

- (instancetype) initWithData:(NSData*)data;

@end
