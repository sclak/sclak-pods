//
//  SclakTagPresenceUserConfiguration.h
//  SclakBle
//
//  Created by Daniele Poggi on 25/07/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "PPLGenericUserConfiguration.h"

@interface SclakTagPresenceUserConfiguration : PPLGenericUserConfiguration

@end
