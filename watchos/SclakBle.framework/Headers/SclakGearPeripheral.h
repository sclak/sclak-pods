//
//  SclakGearPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 25/07/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "SclakGearUserConfiguration.h"

@interface SclakGearPeripheral : SclakPeripheral

@property (nonatomic, strong) SclakGearUserConfiguration *userConfiguration;

#pragma mark - User Configuration

- (void) sendUserConfiguration:(SclakGearUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

@end
