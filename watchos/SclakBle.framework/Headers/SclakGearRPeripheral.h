//
//  SclakGearRPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 25/05/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "SclakGearAPeripheral.h"
#import "SclakGearRUserConfiguration.h"

@interface SclakGearRPeripheral : SclakGearAPeripheral

@property (nonatomic, strong) SclakGearRUserConfiguration *userConfiguration;

#pragma mark - User Configuration

- (void) sendUserConfiguration:(SclakGearRUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

@end
