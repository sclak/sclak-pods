//
//  ECCAuth.h
//  SclakBle
//
//  Created by Daniele Poggi on 17/04/2020.
//  Copyright © 2020 sclak. All rights reserved.
//

#import "SclakAuth.h"
#import "ECC_OTP.h"
#import "GMEllipticCurveCrypto.h"
#import "GMEllipticCurveCrypto+hash.h"

typedef enum : NSUInteger {
    Certificate,
    PublicKey,
    ECPrivateKey
} PEM_Type;

// constants
extern const int DIM_RANDOM_ECDH;
extern const int DIM_BSS_KEY;
extern const int NUM_OTP_KEY;

// auth commands
extern const uint8_t kPhoneToDevice_INIT_AUTH_ECC;
extern const uint8_t kDeviceToPhone_RESP_INIT_AUTH_ECC;

@interface ECCAuth : SclakAuth

- (ECC_OTP*_Nullable) currentOTP;

- (void) setServerPublicKey:(NSString* _Nonnull)PEM;
- (void) setPrivateKey:(NSString* _Nonnull)PEM;

- (BOOL) verifyAuthCert:(NSData* _Nonnull)authCert;
- (ECC_OTP* _Nullable) generateSharedKey:(NSData* _Nonnull)pubKeyDevice randomECDH:(NSData* _Nonnull)random;
- (ECC_OTP* _Nullable) generateNewKeyECDH:(NSData* _Nonnull)pubKeyDevice;

#pragma mark - Encryption

- (NSData* _Nullable) encryptData:(NSData* _Nonnull)data OTP:(ECC_OTP* _Nonnull)OTP;

- (NSData* _Nullable) decryptData:(NSData* _Nonnull)data OTP:(ECC_OTP* _Nonnull)selectedOTP;
- (NSData* _Nullable) decryptData:(NSData* _Nonnull)data OTP:(ECC_OTP* _Nonnull)selectedOTP forceSelected:(BOOL)force;

#pragma mark - Formatting and Segmentation

- (NSData* _Nullable) formatMessage:(uint8_t)idm payload:(NSData* _Nonnull)payload;
- (NSData* _Nullable) unformatMessage:(NSData* _Nonnull)message;

- (NSArray* _Nullable) segmentMessage:(NSData* _Nonnull)message;
- (NSData* _Nullable) reassembleMessage:(NSArray* _Nonnull)segments;

#pragma mark - Utils

- (NSData* _Nullable) PEM2Data:(NSString* _Nonnull)PEM;
- (NSString* _Nullable) Data2PEM:(NSData* _Nonnull)data type:(PEM_Type)type;

@end
