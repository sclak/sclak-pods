//
//  CBUUID+StringExtraction.h
//  PassePartoutLib
//
//  Created by isghe on 02/12/13.
//  Copyright (c) 2013 Daniele Poggi - Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface CBUUID (StringExtraction)
- (NSString *) representativeString;
@end
