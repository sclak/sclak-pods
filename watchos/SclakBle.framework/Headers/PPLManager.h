//
//  PPLManager.h
//  PassePartoutLib
//
//  Created by Daniele Poggi on 31/07/14.
//  Copyright (c) 2014 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	PPLAuthenticateResultFailed = 0,
	PPLAuthenticateResultSucceded = 1
} PPLAuthenticateResult;

@class PPLDiscoveredPeripheral;

typedef void(^onRequestConfigResult) (PPLDiscoveredPeripheral *peripheral, NSInteger major, NSInteger minor, int8_t power, NSInteger timeoutAuth);

@interface PPLManager : NSObject

@end
