//
//  PPLCentralManagerOptions.h
//  SclakBle
//
//  Created by Daniele Poggi on 21/10/15.
//  Copyright © 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPLCentralManagerPeripheralOptions.h"

// central mode preferences
#define kCentralModeIdentifier                                      @"centralModeIdentifier"
#define kCentralModeStopScanWhenConnectOption                       @"stopScanWhenConnectOption"
#define kCentralModeAutoConnectOption                               @"autoConnect"
#define kCentralModeShouldRetryConnectionOption                     @"shouldRetryConnection"
#define kCentralModeShouldRetryAuthenticationOption                 @"shouldRetryAuthentication"
#define kCentralModeShouldRetryDisconnectionOption                  @"shouldRetryDisconnection"
#define kCentralModeAutoAuthenticateOption                          @"autoAuthenticate"
#define kCentralModeBtcodeRootOption                                @"btcodeRoot"
#define kCentralModeScannerFrequencyOption                          @"scannerFrequency"
#define kCentralModeAutoScanOption                                  @"autoScan"
#define kCentralModeRestartScanOption                               @"restartScan"
#define kCentralModeSHA256ThresholdVersion                          @"sha256ThresholdVersion"
#define kCentralModeReadRSSIWhileConnectedOption                    @"readRSSIWhileConnectedOption"

@interface PPLCentralManagerOptions : NSObject

@property (nonatomic, strong) NSString *centralModeIdentifier;

@property (nonatomic, strong) NSArray *peripheralOptions;

@end
