//
//  ODLCountRevisionsStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 20/10/15.
//  Copyright © 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ODLCountRevisionsStatus : NSObject

/**
 * Contatore giorni lavaggio
 */
@property (nonatomic, strong) NSNumber *countDayWashing;

/**
 * Soglia giorni lavaggio
 */
@property (nonatomic, strong) NSNumber *thresholdDayWashing;

/**
 * Contatore giorni sanificazione
 */
@property (nonatomic, strong) NSNumber *countDaySanitation;

/**
 * Soglia giorni sanificazione
 */
@property (nonatomic, strong) NSNumber *thresholdDaySanitation;

/**
 * Contatore mesi manutenzione tenute
 */
@property (nonatomic, strong) NSNumber *countMonthSealings;

/**
 * Soglia mesi manutenzione tenute
 */
@property (nonatomic, strong) NSNumber *thresholdMonthSealings;

/**
 * Contatore mesi manutenzione impianto
 */
@property (nonatomic, strong) NSNumber *countMonthPlant;

/**
 * Soglia mesi manutenzione impianto
 */
@property (nonatomic, strong) NSNumber *thresholdMonthPlant;

#pragma mark -

- (instancetype) initWithData:(NSData*)data;

@end
