//
//  WiegandPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 22/07/2017.
//  Copyright © 2017 sclak. All rights reserved.
//

#import "SclakPeripheral.h"

// wittkopp specific commands
extern const uint8_t kPhoneToDevice_WIEG_SEND_COMM;
extern const uint8_t kDeviceToPhone_WIEG_RESP_SEND_COMM;

@interface WiegandPeripheral : SclakPeripheral

- (void) sendSiteCode:(NSString*)siteCode cardCode:(NSString*)cardCode callback:(BluetoothResponseCallback)callback;

#pragma mark - Wiegand Data Formats

+ (NSData*) wiegandH10306_34bit:(NSString*)siteCode cardCode:(NSString*)cardCode;

@end
