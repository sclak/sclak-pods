//
//  PPLDiscoveredPeripheral.h
//  PassePartoutLib
//
//  Created by danielepoggi on 12/02/14.
//  Copyright (c) 2014 Daniele Poggi - Sclak. All rights reserved.
//

#import "PPLBasePeripheral.h"

#import <CoreLocation/CoreLocation.h>

#import "SclakAuth.h"
#import "PPLSamplesQueue.h"
#import "PPLCentralManager.h"

// CALLBACKS
typedef void(^RequestSecretCallback)(NSNumber *sequence);

#define kSclakMaskAll                       0xFF                    // mask all channels
#define kSclakMaskFirst                     0x01                    // mask first channel

#define kPeripheralAuthenticatedNotification                        @"kPeripheralAuthenticatedNotification"
#define kPeripheralDisconnectedNotification                         @"kPeripheralDisconnectedNotification"
#define kPeripheralGetInOutNotification                             @"kPeripheralGetInOutNotification"
#define kPeripheralUpdateRSSINotification                           @"kPeripheralUpdateRSSINotification"

// This service exposes Over-the-Air update capabilities of CSR μEnergy applications.
#define kAppOtauServiceCBUUID               [CBUUID UUIDWithString: @"00001016-d102-11e1-9b23-00025b00a5a5"]

// characteristics for app otau
#define kCharacteristicGetKeysUuid          [CBUUID UUIDWithString: @"00001017-d102-11e1-9b23-00025b00a5a5"]
#define kCharacteristicGetKeyBlockUuid      [CBUUID UUIDWithString: @"00001018-d102-11e1-9b23-00025b00a5a5"]

// service UUID exposed by the Boot module
#define kBootOtauServiceCBUUID              [CBUUID UUIDWithString: @"00001010-d102-11e1-9b23-00025b00a5a5"]

// characteristics for boot otau
#define kCharacteristicVersionUuid          [CBUUID UUIDWithString: @"00001011-d102-11e1-9b23-00025b00a5a5"]
#define kCharacteristicCurrentAppUuid       [CBUUID UUIDWithString: @"00001013-d102-11e1-9b23-00025b00a5a5"]
#define kCharacteristicDataTransferUuid     [CBUUID UUIDWithString: @"00001014-d102-11e1-9b23-00025b00a5a5"]
#define kCharacteristicTransferControlUuid  [CBUUID UUIDWithString: @"00001015-d102-11e1-9b23-00025b00a5a5"]

// sclak services
#define kSclakServiceCBUUID                 [CBUUID UUIDWithString: @"25E1452D-6402-4182-9AAA-C64936B7C487"]
#define kUlockServiceCBUUID                 [CBUUID UUIDWithString: @"88783fe1-67cf-42fb-b4b1-21de18e3262b"]
#define kTastierinoCBUUID                   [CBUUID UUIDWithString: @"00000000-0000-002b-504c-554747002003"]
#define kIdentifyServiceCBUUID              [CBUUID UUIDWithString: @"71BE24EE-5E7A-4D18-B65A-FD936B7BF69E"]

// characteristics (common to services)
#define kWriteCBUUID                        [CBUUID UUIDWithString: @"00000000-0000-002B-504C-554747010000"]
#define kReadNotifyCBUUID                   [CBUUID UUIDWithString: @"00000000-0000-002B-504C-554747020000"]
#define kIdentifyCharacteristicCBUUID       [CBUUID UUIDWithString: @"9DF402E4-9D41-4C23-ACF0-A57CCA5F05C2"]

// NORDIC

// service
#define kNordicSclakServiceCBUUID           [CBUUID UUIDWithString: @"3C960001-AF02-4948-8177-BCA45CD36BE6"]

// characteristics
#define kNordicWriteCBUUID                  [CBUUID UUIDWithString: @"3C960002-AF02-4948-8177-BCA45CD36BE6"]
#define kNordicReadNotifyCBUUID             [CBUUID UUIDWithString: @"3C960003-AF02-4948-8177-BCA45CD36BE6"]


extern const uint8_t kPhoneToDevice_SET_OUT;
extern const uint8_t kDeviceToPhone_RESP_OUT;
extern const uint8_t kPhoneToDevice_REQ_RSSI;
extern const uint8_t kDeviceToPhone_RESP_RSSI;
extern const uint8_t kPhoneToDevice_DISCONNECT;
extern const uint8_t kPhoneToDevice_SET_CFG;
extern const uint8_t kDeviceToPhone_RESP_SET_CFG;
extern const uint8_t kPhoneToDevice_REQ_CFG;
extern const uint8_t kDeviceToPhone_RESP_REQ_CFG;
extern const uint8_t kPhoneToDevice_SEND_DATA;
extern const uint8_t kDeviceToPhone_REC_DATA;
extern const uint8_t kDeviceToPhone_UNKNOWN_COMMAND;
extern const uint8_t kPhoneToDevice_REQ_INFO;
extern const uint8_t kDeviceToPhone_RESP_REQ_INFO;
extern const uint8_t kDeviceToPhone_RESP_OUT_OF_SYNC;

// unknown command sent response
extern const uint8_t kDeviceToPhone_RESP_UNKNOWN;

#pragma mark Sclak Specific commands
extern const uint8_t kSclakCommandOpen;

#pragma mark - Generic Peripheral Defines

#define kDefaultRetryPeriod                             0.5
#define kDefaultRSSIScanPeriod                          0.125 // quick: 0.01 -- slow: 1.0
#define kDefaultReadRSSIWhileConnectedTimeInterval      1.0

// Standard User Defaults
#define kStandardUserDefaultsCalibrationThresholds @"averages"
#define kStandardUserDefaultsCalibrationThresholdNear @"near"
#define kStandardUserDefaultsCalibrationThresholdFar @"far"

typedef NS_ENUM(NSInteger, SCKBleErrorCodes) {
    SCKBleAuthFailedWrongSecret
};

typedef NS_ENUM(NSInteger, SCKProximity) {
    SCKProximityUnknown,
    SCKProximityImmediate,
    SCKProximityNear,
    SCKProximityFar
};

@interface PPLDiscoveredPeripheral : PPLBasePeripheral

#pragma mark - Command

@property (nonatomic, assign) BOOL commandInProgress;

#pragma mark - Characteristics

@property (nonatomic, strong) CBCharacteristic *writeCharacteristic;
@property (nonatomic, strong) CBCharacteristic *readCharacteristic;
@property (nonatomic, strong) CBCharacteristic *identifyCharacteristic;

#pragma mark - Authentication Protocol

@property (nonatomic, strong) SclakAuth *authProtocol;
@property (nonatomic, assign) BOOL shouldCommandReplica;
/**
 * data e ora da impostare sul dispositivo durante l'autenticazione
 */
@property (nonatomic, strong) NSDate *authDateTime;

/**
 * set this option before connecting to inhibit auto disconnection after 10 seconds of inactivity to happen
 * from firmware 4.56 on (lib 2.20 on)
 * does nothing on firmware before 4.56
 */
@property (nonatomic, assign) BOOL inhibitAutoDisconnect;

#pragma mark - Device Information Properties

/**
 * Returns the periodic advertising interval in units of 1.25ms.
 * Valid range is 6 (7.5ms) to 65536 (81918.75ms).
 */
@property (nonatomic, readonly, assign) NSTimeInterval periodicAdvertisingInterval;

@property (nonatomic, strong) NSNumber *deviceClass;
@property (nonatomic, readonly, strong) NSNumber *deviceFirmwareVersion;
@property (nonatomic, readonly, strong) NSString *deviceAddress;
@property (nonatomic, readonly) NSString *romId;

#pragma mark - Connection Properties

@property (nonatomic, readonly, assign) NSUInteger timeoutAuth;
@property (nonatomic, readonly, assign) CLBeaconMajorValue major;
@property (nonatomic, readonly, assign) CLBeaconMajorValue minor;
@property (nonatomic, readonly, assign) int8_t powerLevel;

#pragma mark - Scanner Properties

@property (nonatomic, strong) NSNumber *fRSSI;
@property (nonatomic, assign) NSInteger rssiIndex;
@property (nonatomic, assign) NSInteger previousRssiAverage;
@property (nonatomic, assign) BOOL isPollingRssi;
@property (atomic, assign) double lastDiscoveredTime;

/**
 * option to request and update RSSI value with readRSSI() automatically when connected
 */
@property (nonatomic, assign) BOOL requestRSSIWhileConnected;

#pragma mark - Preferences

/**
 * when TRUE, preferences have been setted from Central Manager to this peripheral
 * preferences are setted when peripheral is not in boot mode, the first time is has been seen
 */
@property (nonatomic, assign) BOOL preferencesSetted;

/**
 * when TRUE, a connection will be established as soon as possible
 */
@property (nonatomic, assign) BOOL autoConnect;

/**
 * when TRUE, after an auto connection has been made, an "open" command will be automatically triggered
 */
@property (nonatomic, assign) BOOL autoConnectOpen;

/**
 * when TRUE, the connection will be closed after a peripheral "open" or "close" command has been made
 */
@property (nonatomic, assign) BOOL autoDisconnect;

/**
 * when TRUE, the connection will be closed when an app background state is detected. default is TRUE
 */
@property (nonatomic, assign) BOOL autoDisconnectInBackground;

@property (nonatomic, assign) BOOL shouldRetryConnection;
@property (nonatomic, assign) BOOL shouldRetryAuthentication;
@property (nonatomic, assign) BOOL shouldRetryDisconnection;
@property (nonatomic, assign) BOOL shouldRetryCharacteristicWrite;

@property (nonatomic, assign) BOOL slowBluetoothAnnounce;
@property (nonatomic, assign) BOOL useDisconnectCommand;

@property (nonatomic, assign) BOOL autoAuthenticate;

@property (nonatomic, assign) NSInteger totalConnectRetryCounter;
@property (nonatomic, assign) NSInteger totalAuthenticationRetryCounter;
@property (nonatomic, assign) NSInteger totalCharacteristicRetryRetryCounter;

#pragma mark - Connection status

@property (nonatomic, readonly, assign) BOOL isRetryingConnection;

#pragma mark - Proximity RSSI

@property (nonatomic, readonly) NSDate *rssiNotificationDate;
@property (nonatomic, readonly) PPLSamplesQueue *phoneRssiSamplesQueue;

#pragma mark Command to be sent automatically

@property (nonatomic, assign) uint8_t commandToSend;
@property (nonatomic, assign) uint8_t authCommandToSend;
@property (nonatomic, strong) NSData *dataToSend;
@property (nonatomic, assign) BOOL commandToSendRequiresAuthentication;
@property (nonatomic, assign) BOOL commandReceivedInvertedAuthentication;
@property (nonatomic, assign) uint8_t expectedActionResponse;

#pragma mark - Callbacks

@property (nonatomic, strong) BluetoothResponseCallback identifyCallback;
@property (nonatomic, strong) BluetoothResponseCallback configsCallback;
@property (nonatomic, strong) BluetoothResponseCallback connectCallback;
@property (nonatomic, strong) BluetoothAuthCallback     authCallback;
@property (nonatomic, strong) RequestSecretCallback     requestSecretCallback;
@property (nonatomic, strong) BluetoothResponseCallback disconnectCallback;
@property (nonatomic, strong) BluetoothAuthCallback timeoutCallback;

@property (nonatomic, strong) BluetoothResponseCallback responseCommandCallback;
@property (nonatomic, strong) BluetoothResponseErrorCallback responseCommandErrorCallback;

#pragma mark - Authentication Timeout

@property (nonatomic, assign) NSUInteger authenticationTimeout;

#pragma mark - Authentication Test

@property (nonatomic, assign) BOOL checkingAuthInLowRssiCondition;
@property (nonatomic, strong) NSNumber *authInLowRssiConditionResult;

#pragma mark - Beacon Proximity (SCKProximity)

/**
 * variable setted by PPLCentralManager while scanning
 * this is the N'th proximity calculated while scanning
 */
@property (nonatomic, assign) SCKProximity currentProximity;
/**
 * variable setted by PPLCentralManager while scanning
 * this is the N'th - 1 proximity calculated while scanning
 */
@property (nonatomic, assign) SCKProximity previousProximity;

#pragma mark - Elliptic Curve Cryptography Properties

@property (nonatomic, assign) enum ecc_auth_req_res ECCauthResult;

// METHODS

- (instancetype) initWithPeripheral:(CBPeripheral*)peripheral;
- (NSString*) description;

#pragma mark - bluetooth scanner discovery

- (BOOL) isDiscovered;
- (BOOL) isDiscovered:(CFAbsoluteTime)time;
- (NSTimeInterval) timeIntervalSinceLastDiscovered;
- (void) updatePeriodicAdvertisingInterval:(CFAbsoluteTime)time;

#pragma mark - Connection and Preferences

- (void) connectCallback:(BluetoothResponseCallback)callback;
- (void) connectAuthCallback:(BluetoothAuthCallback)callback;
- (void) connectCallback:(BluetoothResponseCallback)callback authCallback:(BluetoothAuthCallback)authCallback;
- (void) connectCallback:(BluetoothResponseCallback)callback authCallback:(BluetoothAuthCallback)authCallback disconnectCallback:(BluetoothResponseCallback)disconnectCallback;
- (void) disconnectCallback:(BluetoothResponseCallback)disconnectCallback;
- (void) disconnect;

- (void) cancelTimeoutCallback;

- (void) setPreferences:(NSDictionary*)preferences;

#pragma mark - Authentication

- (void) authenticate;
- (void) authenticateCallback:(BluetoothAuthCallback)callback;

#pragma mark - RSSI

- (void) requestRssiCallback:(uint8_t)DT_TX callback:(BluetoothReadRSSICallback)callback;
- (void) pollRssiCallback:(BluetoothReadRSSICallback)callback;
- (void) stopPollRssi;
/**
 * protocol event
 */
- (void) didReceiveRssiUpdate:(NSInteger)RSSI;

#pragma mark - RSSI Proximity

/**
 * calculate beacon proximity based on rssi average value
 */
- (SCKProximity) calculateBeaconProximity;

+ (NSString*) proximityName:(SCKProximity)proximity;

#pragma mark - Commands

- (void) sendIdentify;
- (void) sendIdentifyCallback:(BluetoothResponseCallback)callback;

- (void) sendSecureValue:(NSObject*)value forKey:(NSString*)key;

- (BOOL) sendClearCommand:(NSData*)clearCommand;
- (void) sendSecureCommand:(const uint8_t)command action:(const uint8_t)action;

- (void) sendCommand:(const uint8_t)command action:(const uint8_t)action secure:(BOOL)secure callback:(BluetoothResponseCallback)callback;
- (void) sendCommand:(const uint8_t)command action:(const uint8_t)action mask:(const uint8_t)mask duration:(const uint8_t)duration secure:(BOOL)secure callback:(BluetoothResponseCallback)callback;

#pragma mark - callback management

- (void) resetCallbacks;

- (BluetoothResponseCallback) callbackForResultCommandResponse:(const uint8_t)response;
- (void) setCallbackForResultCommandResponse:(const uint8_t)response callback:(BluetoothResponseCallback)callback;
- (void) removeCallbackForResultCommandResponse:(const uint8_t)response;

- (BluetoothResponseErrorCallback) callbackForResponseErrorResponse:(const uint8_t)response;
- (void) setCallbackForResponseErrorResponse:(const uint8_t)response callback:(BluetoothResponseErrorCallback)callback;
- (void) removeCallbackForResponseErrorResponse:(const uint8_t)response;

#pragma mark - Send Commands and receive responses

- (void) sendResultCommand:(const uint8_t)command response:(const uint8_t)response callback:(BluetoothResponseCallback)callback;
- (void) sendResultCommand:(const uint8_t)command data:(NSMutableData*)data response:(const uint8_t)response callback:(BluetoothResponseCallback)callback;

#pragma mark - Device Configuration

- (void) getDeviceInformationCallback:(BluetoothResponseCallback)callback;

#pragma mark - Config

- (void) getConfigurationCallback:(BluetoothResponseCallback)callback;
- (void) setConfigurationPower:(int8_t)powerLevel timeoutAuth:(NSUInteger)timeout callback:(BluetoothResponseCallback)callback;

// DELEGATE METHODS

- (void) pushRssi:(PPLSample*)theRssi;
- (int) rssiAverage;

#pragma mark - Business Logic Events called from Wrapper

- (void) onConnect;
- (BOOL) onDiscoverCharacteristics;
- (void) onDisconnectWithError:(NSError*)error;

- (BOOL) isAntiOutOfSyncAlgorithmAvailable;
- (BOOL) isBootloaderStarted;

- (void) didReceiveCommand:(uint8_t)command data:(NSData*)data buffer:(uint8_t[])buffer;

#pragma mark - managed events

- (void) onAuthenticateResult:(PPLAuthenticateResult)result error:(NSError* _Nullable)error;

#pragma mark - Equality

- (BOOL) isEqualToPeripheral:(PPLDiscoveredPeripheral *)aPeripheral;

#pragma mark - Beacon UUID

- (CBUUID*) beaconUUID;

#pragma mark - Logging

- (NSDictionary*) logModel;

@end
