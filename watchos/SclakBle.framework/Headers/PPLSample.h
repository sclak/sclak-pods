//
//  PPLSample.h
//  PassePartoutLib
//
//  Created by isghe on 02/01/14.
//  Copyright (c) 2014 Daniele Poggi - Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PPLSample : NSObject

@property (nonatomic, strong) NSNumber *value;
@property (nonatomic, strong) NSDate *date;

- (id) initWithValue:(NSNumber *)theValue andDate:(NSDate *)theDate;

@end
