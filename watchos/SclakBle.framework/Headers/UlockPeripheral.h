//
//  UlockPeripheral.h
//  PassePartoutLib
//
//  Created by Daniele Poggi on 07/01/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import "SclakPeripheral.h"

#pragma mark Ulock Specific commands

extern const uint8_t kPhoneToDevice_SET_FEATURE;
extern const uint8_t kDeviceToPhone_RESP_SET_FEATURE;
extern const uint8_t kPhoneToDevice_REQ_FEATURE;
extern const uint8_t kDeviceToPhone_RESP_REQ_FEATURE;
extern const uint8_t kPhoneToDevice_SET_AUTOOPEN;
extern const uint8_t kDeviceToPhone_RESP_SET_AUTOOPEN;

extern const uint8_t kUlockCommandOpen;
extern const uint8_t kUlockCommandClose;
extern const uint8_t kUlockCommandStatus;

extern const uint8_t kUlockCommandEnableAutoopen;
extern const uint8_t kUlockCommandDisableAutoopen;

extern const uint8_t kUlockStatusLockUnknown;
extern const uint8_t kUlockStatusLockEnabled;
extern const uint8_t kUlockStatusLockEnabling;
extern const uint8_t kUlockStatusLockDisabled;
extern const uint8_t kUlockStatusLockDisabling;

typedef NS_ENUM(NSUInteger, UlockStatus) {
    UlockStatusUnknown,
    UlockLocked,
    UlockLocking,
    UlockUnlocked,
    UlockUnlocking
};

@interface UlockPeripheral : SclakPeripheral

// actual properties
@property (nonatomic, assign) UlockStatus ulockStatus;

@property (nonatomic, strong) BluetoothResponseCallback ulockStatusChangedCallback;

@property (nonatomic, assign) BOOL buzzer;
@property (nonatomic, assign) BOOL identify;
@property (nonatomic, assign) BOOL autoOpen;
@property (nonatomic, assign) BOOL autoClose;
@property (nonatomic, assign) NSInteger shakePower;

- (BOOL) isOpened;
- (BOOL) isClosed;

- (void) getStatus;
- (void) getStatusCallback:(BluetoothResponseCallback)callback;

- (void) openCallback:(BluetoothResponseCallback)callback;
- (void) closeCallback:(BluetoothResponseCallback)callback;

- (void) getUlockFeaturesCallback:(BluetoothResponseCallback)callback;
- (void) setUlockFeaturesCallback_buzzerOn:(BOOL)buzzer
                        identifyOn:(BOOL)identify
                        autoOpenOn:(BOOL)shake
                       autoCloseOn:(BOOL)proximity
                        shakePower:(NSInteger)power
                          callback:(BluetoothResponseCallback)callback;

- (void) sendEnableAutoopenCallback:(BluetoothResponseCallback)callback;
- (void) sendDisableAutoopenCallback:(BluetoothResponseCallback)callback;

@end
