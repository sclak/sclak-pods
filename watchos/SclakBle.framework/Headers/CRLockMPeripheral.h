//
//  CRLockMPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 08/02/2019.
//

#import "CRLockPeripheral.h"
#import "CRLockMStatus.h"

@interface CRLockMPeripheral : CRLockPeripheral

@property (nonatomic, strong) CRLockMStatus *status;

@end
