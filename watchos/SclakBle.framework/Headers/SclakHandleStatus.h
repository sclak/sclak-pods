//
//  SclakHandleStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 05/06/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SclakHandleStatus : NSObject

/**
 * Valore attuale della uscita (uscita attiva (maniglia attiva) -> 0x01, uscita disattiva (maniglia bloccata) -> 0x00).
 */
@property (nonatomic, assign) BOOL handleStatus;

/**
 *  Segnale di stato del blocco di sicurezza interno (utilizzato per la segnalazione privacy).
 */
@property (nonatomic, assign) BOOL internalSecurityLockStatus;

/**
 *  Segnale di stato di maniglia abbassata, permette il blocco della maniglia non appena questa viene rilasciata.
 */
@property (nonatomic, assign) BOOL handleLowered;

#pragma mark -

- (instancetype) initWithData:(NSData*)data;

@end
