//
//  PPLPeripheralUsage.h
//  Pods
//
//  Created by Daniele Poggi on 16/09/2019.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SclakCommand) {
    SclakCommandNone,
    SclakCommandGetStatus,
    SclakCommandImpulse,
    SclakCommandActivate,
    SclakCommandDeactivate,
    SclakCommandOpen,
    SclakCommandCloseAuto,
    SclakCommandClose1Time,
    SclakCommandCloseFull
};

@interface PPLPeripheralUsage : NSObject

/**
 * the command to execute
 */
@property (nonatomic, assign) SclakCommand command;

/**
 * custom command mask, as required by "sclak command" in BLE library on SclakPeripheral class
 */
@property (nonatomic, assign) uint8_t customCommandMask;

/**
 * custom autoclose time, if nil default autoclose time of 1 second is used
 */
@property (nonatomic, strong) NSNumber *autocloseTime;

/**
 * sclak fleet option: immobilizer installed (read from Peripheral)
 */
@property (nonatomic, assign) BOOL immobilizerInstalled;

/**
 * disconnect at the end of procedure
 */
@property (nonatomic, assign) BOOL disconnect;

/**
 * usage requested from app in background mode
 */
@property (nonatomic, assign) BOOL background;

@end
