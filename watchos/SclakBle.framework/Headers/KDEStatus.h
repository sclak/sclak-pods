//
//  CRLockStatus.h
//  SclakBle
//
//  Created by Daniele Poggi on 22/05/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, KDELockStatus) {
    KDELockStatusUnknown,
    KDELockStatusUnlocked,
    KDELockStatusLocked,
    KDELockStatusError
};

@interface KDEStatus : NSObject

/**
 * Ingresso stato serratura (1 chiusa, 0 aperta)
 */
@property (nonatomic, assign) KDELockStatus lockStatus;

/**
 * Ingresso ausiliario stato porta (1 chiusa, 0 aperta)
 */
@property (nonatomic, assign) BOOL hallStatus;

- (instancetype) initWithData:(NSData*)data;

@end
