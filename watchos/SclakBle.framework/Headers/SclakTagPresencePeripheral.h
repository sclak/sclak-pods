//
//  SclakTagPresencePeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 25/07/2018.
//  Copyright © 2018 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "SclakReaderTag.h"
#import "SclakTagPresenceUserConfiguration.h"

// sclak tag presence specific commands
extern const uint8_t kPhoneToDevice_REQ_TAG_STS;
extern const uint8_t kDeviceToPhone_RESP_TAG_STS;

@interface SclakTagPresencePeripheral : SclakPeripheral

@property (nonatomic, assign) SclakTagPresenceStatus status;
@property (nonatomic, strong) SclakTagPresenceUserConfiguration *userConfiguration;

@property (nonatomic, strong) NSString *pinCode;

#pragma mark - User Configuration

- (void) sendUserConfiguration:(SclakTagPresenceUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

#pragma mark - APIs

- (void) readTagCallback:(BluetoothResponseCallback)callback;

@end
