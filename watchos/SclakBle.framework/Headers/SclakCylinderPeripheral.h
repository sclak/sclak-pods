//
//  SclakCylinderPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 14/11/2017.
//  Copyright © 2017 sclak. All rights reserved.
//

#import "SclakPeripheral.h"
#import "SclakCylinderUserConfiguration.h"

@interface SclakCylinderPeripheral : SclakPeripheral

@property (nonatomic, strong) SclakCylinderUserConfiguration *userConfiguration;

#pragma mark - User Configuration

/**
 *  send user configuration
 *
 *  @param userConfiguration
 *  @param callback
 */
- (void) sendUserConfiguration:(SclakCylinderUserConfiguration*)userConfiguration callback:(BluetoothResponseCallback)callback;

@end
