//
//  BluetoothBenchmark.h
//  SclakBle
//
//  Created by Daniele Poggi on 20/05/15.
//  Copyright (c) 2015 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BluetoothBenchmark : NSObject

@property (nonatomic, assign) long beginTime;
@property (nonatomic, assign) long connectTime;
@property (nonatomic, assign) long servicesDiscoveredTime;
@property (nonatomic, assign) long notificationStateTime;
@property (nonatomic, assign) long authenticationTime;
@property (nonatomic, assign) long sclakTime;
@property (nonatomic, assign) long disconnectTime;

@property (nonatomic, assign) long connectionDelta;
@property (nonatomic, assign) long servicesDiscoveredDelta;
@property (nonatomic, assign) long notificationStateDelta;
@property (nonatomic, assign) long authenticationDelta;
@property (nonatomic, assign) long sclakDelta;
@property (nonatomic, assign) long disconnectDelta;

+ (long) now;
- (long) benchmarkSclak;

@end
