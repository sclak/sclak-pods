//
//  SCKPowerProfile.h
//  SclakBle
//
//  Created by Daniele Poggi on 23/03/2019.
//  Copyright © 2019 sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SclakPowerProfile) {
    SclakPowerProfile_Custom,
    SclakPowerProfile_Performance,
    SclakPowerProfile_Balanced,
    SclakPowerProfile_PowerSaving
};

@interface SCKPowerProfile : NSObject

/**
 * Profilo Prestazioni/Consumi attualmente selezionato:
 * 0x00 Nessun profilo, i parametri utilizzati sono quelli impostati
 * con il comandi di set.
 * 0x01 Profilo Performance.
 * 0x02 Profilo Balanced.
 * 0x03 Profilo PowerSaving.
 */
@property (nonatomic, assign) SclakPowerProfile profile;

/**
 * Intervallo di advertising FastAdvInt espresso in ms del profilo attuale, formato MSByte First.
 */
@property (nonatomic, assign) NSUInteger fastAdvertising;

/**
 * Intervallo di advertising NormAdvInt espresso in ms del profilo attuale, formato MSByte First.
 */
@property (nonatomic, assign) NSUInteger normalAdvertising;

/**
 * Parametro TmoFastAdv espresso in secondi del profilo attuale, durata advertising con intervallo FastAdvInt prima di passare a NormAdvInt, formato MSByte First.
 */
@property (nonatomic, assign) NSUInteger timeoutFastAdvertising;

/**
 * Livello di potenza RF del profilo attuale, utilizzato in trasmissione valori ammessi 0..7.
 */
@property (nonatomic, assign) NSUInteger power;

#pragma mark -

+ (instancetype) defaultFeature;

- (instancetype) initWithData:(NSData*)data;

- (NSMutableData*) messageData;
- (NSMutableData*) messageData:(BOOL)appendProfile;

@end
