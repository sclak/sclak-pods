//
//  SCKGivikBackgroundPolling.h
//  SclakBle
//
//  Created by Daniele Poggi on 10/11/2020
//  Copyright © 2020 sclak. All rights reserved.
//

#import "GivikPeripheral.h"

@interface SCKGivikBackgroundPolling : NSObject

@property (nonatomic, strong) GivikPeripheral *peripheral;

- (void) heartBeat;
- (void) stop;

@end
