//
//  NSString+PPLDataGenerator.h
//  PassePartoutLib
//
//  Created by isghe on 05/12/13.
//  Copyright (c) 2013 Daniele Poggi - Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (PPLDataGenerator)

- (NSData*) hexStringToData;

@end