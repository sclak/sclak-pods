//
//  SclakReaderPeripheral.h
//  SclakBle
//
//  Created by Daniele Poggi on 23/01/2020.
//  Copyright © 2020 sclak. All rights reserved.
//

#import "SclakTagPresencePeripheral.h"

@interface SclakReaderPeripheral : SclakTagPresencePeripheral

@property (nonatomic, strong) NSMutableArray *tags;

@end
