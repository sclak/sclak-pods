//
//  NSString+Utils.h
//  Pods
//
//  Created by Daniele Poggi on 01/02/2019.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

- (BOOL) greater:(NSString*)firmwareCode;
- (BOOL) greaterOrEqualTo:(NSString*)firmwareCode;
- (BOOL) lesser:(NSString*)firmwareCode;
- (BOOL) lesserOrEqualTo:(NSString*)firmwareCode;

@end
