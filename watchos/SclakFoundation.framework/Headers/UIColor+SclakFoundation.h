//
//  UIColor+SclakFoundation.h
//  SclakFoundation
//
//  Created by albi on 22/11/13.
//  Copyright (c) 2013 bluethings. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SCKColorThemeDescriptor<NSObject>

+ (UIColor*) mainColor;
+ (UIColor*) mainAlertBtnColor;
+ (UIColor*) lightMainColor;
+ (UIColor*) mainTintColor;
+ (UIColor*) navigationBackgroundColor;
+ (UIColor*) navigationTintColor;
+ (UIColor*) mainBackgroundColor;
+ (UIColor*) blackGrayTextColor;
+ (UIColor*) accentTextColor;
+ (UIColor*) softGrayColor;
+ (UIColor*) hardGrayColor;
+ (UIColor*) errorColor;
+ (UIColor*) menuIconColor;
+ (UIColor*) menuLabelColor;
+ (UIColor*) menuSelectedLabelColor;
+ (UIColor*) placeholderTextColor;
+ (UIColor*) separatorNavigationColor;
+ (UIColor*) carouselBackgroundColor;
+ (UIColor*) carouselBackgroundOutlinedColor;
+ (UIColor*) carouselAddNewLockTextColor;
+ (UIColor*) separatorColor;
+ (UIColor*) grayBorderColor;
+ (UIColor*) grayButtonColor;
+ (UIColor*) sclakGreenColor;
+ (UIColor*) grayButtonInstallerColor;
+ (UIColor*) grayButtonInstallerDisabledColor;
+ (UIColor*) disabledColor;
+ (UIColor*) statusColor;
+ (UIColor*) pageControlDotColor;
+ (UIColor*) pageControlSlectedDotColor;
+ (UIColor*) lightVioletColor;
+ (UIColor*) lightOrangeColor;

@end

@interface UIColor (Hex)

+ (UIColor *) colorFromHexString:(NSString *)hexString;
+ (UIColor *) getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha;
+ (unsigned int) intFromHexString:(NSString *)hexStr;

@end

@interface UIColor (SclakFoundation) <SCKColorThemeDescriptor>

+ (UIColor *) lighterColorForColor:(UIColor *)c;
+ (UIColor *) darkerColorForColor:(UIColor *)c;

@end
