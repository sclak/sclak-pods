//
//  NSBundle+Language.h
//  SclakFoundation
//
//  Created by Daniele Poggi on 17/12/20.
//  Copyright © 2020 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Language)

+ (void) setLanguage:(NSString*)language;

@end
