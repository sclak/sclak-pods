//
//  SclakFoundation.h
//  SclakFoundation
//
//  Created by Francesco Mantello on 02/07/18.
//  Copyright (c) 2018 Sclak. All rights reserved.
//

// Base
#if defined SCLAK_CORE_APP
#import "iOSBlocks.h"
#endif

// Extensions
//#import "CLLocationManager+blocks.h"
#import "SCKStack.h"
#import "Base62.h"
#import "NSDate+Helper.h"
#import "NSNumber+Helper.h"
#import "NSObject+AssociatedObjectSupport.h"
#import "UIImage+Utils.h"
#import "NSString+Utils.h"
#import "NSBundle+Language.h"
#if defined SCLAK_CORE_APP
#import "UIAlertController+Block.h"
#import "UINavigationBar+CustomHeight.h"
#import "UITableView+Reload.h"
#import "UIView+Borders.h"
#endif

// Theme
#import "UIColor+SclakFoundation.h"
#import "IBActionSheet+SclakFoundation.h"

// IOS VERSIONS
#define IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 && [[[UIDevice currentDevice] systemVersion] floatValue] < 8.5)
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

// DEVICES
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
#define IS_IPHONE4 (([[UIScreen mainScreen] bounds].size.height-480)?NO:YES)
#define IS_STANDARD_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0  && IS_OS_8_OR_LATER && [UIScreen mainScreen].nativeScale == [UIScreen mainScreen].scale)
#define IS_ZOOMED_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0 && IS_OS_8_OR_LATER && [UIScreen mainScreen].nativeScale > [UIScreen mainScreen].scale)
#define IS_STANDARD_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_ZOOMED_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0 && IS_OS_8_OR_LATER && [UIScreen mainScreen].nativeScale < [UIScreen mainScreen].scale)

#define UIControlStateAll UIControlStateNormal & UIControlStateSelected & UIControlStateHighlighted
