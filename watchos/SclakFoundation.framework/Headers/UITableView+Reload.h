//
//  UITableView+Reload.h
//  SclakApp
//
//  Created by albi on 13/10/15.
//  Copyright © 2015 Sclak. All rights reserved.
//

#if TARGET_OS_WATCH || TARGET_IS_WATCH_EXTENSION
#else

#import <UIKit/UIKit.h>

@interface UITableView (Reload)

- (void) realoadAllVisibleRowsAndSections;

@end

#endif
