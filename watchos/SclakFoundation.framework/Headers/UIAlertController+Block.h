//
//  UIAlertViewController+Block.h
//  SclakUnit
//
//  Created by Daniele Poggi on 14/05/16.
//  Copyright © 2016 SCLAK. All rights reserved.
//

#import <Foundation/Foundation.h>

#if TARGET_OS_WATCH || TARGET_IS_WATCH_EXTENSION
#else

#import <UIKit/UIKit.h>
#import "iOSBlocksProtocol.h"

@interface UIAlertController (Block)

/*
 * Shorter method to Display an alert view with titles and description message, but with no blocks.
 *
 * @param title The string that appears in the receiver’s title bar.
 * @param message Descriptive text that provides more details than the title.
 */
+ (UIAlertController*) alertViewWithTitle:(NSString *)title
                                  message:(NSString *)message
                           fromController:(UIViewController *)viewController;

/*
 * Shorter method to Display an alert view with titles, description message and cancel button title, but with no blocks.
 *
 * @param title The string that appears in the receiver’s title bar.
 * @param message Descriptive text that provides more details than the title.
 * @param cancelButtonTitle The title of the cancel button or nil if there is no cancel button.
 */
+ (UIAlertController*) alertViewWithTitle:(NSString *)title
                                  message:(NSString *)message
                        cancelButtonTitle:(NSString *)cancelButtonTitle
                           fromController:(UIViewController *)viewController;

/*
 * Displays an alert view filled with titles and buttons, and with update blocks to notify when the user dismisses or cancels the alert.
 *
 * @param title The string that appears in the receiver’s title bar.
 * @param message Descriptive text that provides more details than the title.
 * @param cancelButtonTitle The title of the cancel button or nil if there is no cancel button.
 * @param otherButtonTitles The title of aditional buttons.
 * @param dismissed A block object to be executed after the alert view is dismissed from the screen. Returns the pressed button's index and title.
 * @param cancelled A block object to be executed when the alert view is cancelled by the user.
 */
+ (UIAlertController*) alertViewWithTitle:(NSString *)title
                                  message:(NSString *)message
                        cancelButtonTitle:(NSString *)cancelButtonTitle
                        otherButtonTitles:(NSArray *)otherButtonTitles
                                onDismiss:(DismissBlock)dismissed
                                 onCancel:(VoidBlock)cancelled
                           fromController:(UIViewController*)viewController;

/**
 *  add cancel button
 *
 *  @param alert
 *  @param title
 *  @param cancelled
 *
 *  @return
 */
+ (UIAlertController*) alert:(UIAlertController*)alert addCancelButtonTitle:(NSString*)title onCancel:(VoidBlock)cancelled;

/**
 *  add button with title to alert
 *
 *  @param alert
 *  @param title
 *  @param dismissed
 *
 *  @return
 */
+ (UIAlertController*) alert:(UIAlertController*)alert addButtonTitle:(NSString*)title onDismiss:(DismissBlock)dismissed;

/**
 *  add textfield
 *
 *  @param alert       alert
 *  @param placeholder text field placeholder
 *  @param secure      text field secure entry
 *
 *  @return
 */
+ (UIAlertController*) alert:(UIAlertController*)alert addTextField:(NSString*)placeholder secure:(BOOL)secure;

/**
 *  add textfield with prefilled text
 *
 *  @param alert
 *  @param placeholder
 *  @param secure
 *  @param text
 *
 *  @return
 */
+ (UIAlertController*) alert:(UIAlertController*)alert addTextField:(NSString*)placeholder secure:(BOOL)secure text:(NSString*)text;

/**
 * dismiss the alert
 */
- (void) dismiss;

/**
 * dismiss the alert with or without animation
 */
- (void) dismiss:(BOOL)animated;

@end

#endif
