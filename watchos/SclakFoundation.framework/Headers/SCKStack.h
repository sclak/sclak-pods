//
//  SCKStack.h
//  SclakFoundation
//
//  Created by Daniele Poggi on 17/04/2020.
//  Copyright © 2020 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCKStack : NSObject <NSFastEnumeration>

@property (nonatomic, assign, readonly) NSUInteger count;

- (id) initWithArray:(NSArray*)array;
- (id) objectAtIndex:(NSUInteger)index;
- (void) pushObject:(id)object;
- (void) pushObjects:(NSArray*)objects;
- (id) popObject;
- (id) peekObject;
- (id) bottomObject;

@end
