//
//  SCKAirbnb.h
//  SclakFacade
//
//  Created by Daniele Poggi on 19/05/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import "SCKFacadeCallbacks.h"
#import "SCKAirbnbUser.h"
#import "SCKAirbnbListing.h"
#import "SCKAirbnbListingKey.h"
#import "SCKAirbnbReservation.h"

@class Privilege;

@interface SCKAirbnb : NSObject

+ (void) getAuthorizationCallback:(ResponseObjectCallback)callback;
+ (void) postAuthorization:(NSURL*)airbnbResponseURL callback:(ResponseObjectCallback)callback;
+ (void) deleteAuthorizationCallback:(ResponseObjectCallback)callback;

+ (void) getUserCallback:(ResponseObjectCallback)callback;
+ (void) getListingsCallback:(ResponseObjectCallback)callback;
+ (void) getReservationsCallback:(ResponseObjectCallback)callback;

+ (void) pairListing:(SCKAirbnbListing*)listing withKey:(SCKAirbnbListingKey*)key callback:(ResponseObjectCallback)callback;
+ (void) unpairListingKey:(SCKAirbnbListing*)listing callback:(ResponseObjectCallback)callback;

+ (void) pairReservation:(SCKAirbnbReservation*)reservation withPrivilege:(Privilege*)privilege callback:(ResponseObjectCallback)callback;
+ (void) unpairReservation:(SCKAirbnbReservation*)reservation callback:(ResponseObjectCallback)callback;

+ (void) sendMessage:(NSString*)message privilege:(Privilege*)privilege callback:(ResponseObjectCallback)callback;

@end
