//
//  PeripheralType.h
//  SclakFacade
//
//  Created by Daniele Poggi on 16/06/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "PeripheralCharacteristic.h"
#import "PeripheralProtocols.h"

@class Peripheral;

@protocol SCKPeripheralType;

@interface SCKPeripheralType : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *code;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *fwCode;
@property (nonatomic, strong) NSNumber<Optional> *pairable;
@property (nonatomic, strong) NSNumber<Optional> *hasBattery;
@property (nonatomic, strong) NSString<Optional> *beaconUuid;
@property (nonatomic, strong) NSArray<Optional> *supportedAccessories;
@property (nonatomic, strong) NSNumber<Optional> *hasExternalMemory;
@property (nonatomic, strong) NSNumber<Optional> *mergeable;
@property (nonatomic, strong) NSArray<PeripheralCharacteristic*><PeripheralCharacteristic, Optional> *characteristics;

- (BOOL) supportAccessoryWithTypeCode:(NSString*)type;

+ (BOOL) isCRLock:(Peripheral*)peripheral;
+ (BOOL) isSclakDriver:(Peripheral*)peripheral;
+ (BOOL) isGearFamily:(Peripheral*)peripheral;
+ (BOOL) isGearDesi:(Peripheral*)peripheral;
+ (BOOL) isGearA:(Peripheral*)peripheral;
+ (BOOL) isGearR:(Peripheral*)peripheral;
+ (BOOL) isSclakSafe:(Peripheral*)peripheral;
+ (BOOL) isCylinder:(Peripheral*)peripheral;
+ (BOOL) isENTR:(Peripheral*)peripheral;
+ (BOOL) isBWG:(Peripheral*)peripheral;
+ (BOOL) isNello:(Peripheral*)peripheral;
+ (BOOL) isKone:(Peripheral*)peripheral;
+ (BOOL) isSclak2Rele:(Peripheral*)peripheral;
+ (BOOL) isSclakFleet:(Peripheral*)peripheral;
+ (BOOL) isKDE:(Peripheral*)peripheral;

+ (BOOL) isSclakHandleFamily:(Peripheral*)peripheral;

@end
