//
//  Action.h
//  SclakApp
//
//  Created by albi on 17/12/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "Constraint.h"
#import "Option.h"

@protocol Action
@end

@interface Action : JSONModel

@property (nonatomic, strong) NSNumber <Optional> *id;
@property (nonatomic, strong) NSString <Optional> *action;
@property (nonatomic, strong) NSArray <Constraint, Optional> *constraints;

@end
