//
//  Privilege.h
//  SclakApp
//
//  Created by Daniele Poggi on 20/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@class Peripheral, PeripheralGroup, Privilege;

@interface PrivilegeRequest : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSNumber<Optional> *requesterId;
@property (nonatomic, strong) NSNumber<Optional> *privilegeId;
@property (nonatomic, strong) NSNumber<Optional> *peripheralQrcodeId;
@property (nonatomic, strong) NSNumber<Optional> *status;

@property (nonatomic, strong) Peripheral<Optional> *peripheral;
@property (nonatomic, strong) PeripheralGroup<Optional> *peripheralGroup;
@property (nonatomic, strong) Privilege<Optional> *privilege;

@end
