//
//  User.h
//  Sclak2
//
//  Created by albi on 05/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "Email.h"
#import "Device.h"
#import "PhoneNumber.h"
#import "SCKFacadeCallbacks.h"

@class Constraint, Privilege;

@interface User : ResponseObject

@property (nonatomic, strong) NSNumber <Optional> *id;
@property (nonatomic, strong) NSString <Optional> *name;
@property (nonatomic, strong) NSString <Optional> *surname;
@property (nonatomic, strong) NSString <Optional> *phoneNumber;
@property (nonatomic, strong) NSString <Optional> *primaryEmail;
@property (nonatomic, strong) NSString <Optional> *password;
@property (nonatomic, strong) NSNumber <Optional> *randomPassword;
@property (nonatomic, strong) NSNumber <Optional> *otauBetaEnabled;
@property (nonatomic, strong) NSNumber <Optional> *logAccessEnabled;
@property (nonatomic, strong) NSNumber <Optional> *twoFactorEnabled;
@property (nonatomic, strong) NSNumber <Optional> *opcodeSetted;
@property (nonatomic, strong) NSString <Optional> *opcodeHash;
@property (nonatomic, strong) NSNumber <Optional> *ghost;

@property (nonatomic, strong) NSArray <Optional> *actions;
@property (nonatomic, strong) NSArray <Email, Optional> *emails;
@property (nonatomic, strong) NSArray <Device, Optional> *devices;
@property (nonatomic, strong) NSArray <PhoneNumber, Optional> *phoneNumbers;

@property (nonatomic, strong) NSString <Optional> *userPrivateKey;
@property (nonatomic, strong) NSString <Optional> *userPublicKey;

- (NSString*) getName;
- (NSString*) getSurname;
- (NSString*) fullName;
- (NSString*) nameOrSurname;

- (NSString*) primaryEmail;
- (NSString*) primaryPhoneNumber;

- (NSString*) getUsername;

- (BOOL) emailVerified;
- (BOOL) hasRandomPassword;

#pragma mark - User Permissions

- (BOOL) can:(NSString*)action;
- (BOOL) can:(NSString*)action withData:(Constraint*)values;
- (BOOL) can:(NSString*)action withData:(Constraint*)values privilege:(Privilege**)aPrivilege;

#pragma mark - APIs

/**
 * change user password, providing old and new one
 */
+ (void) changePassword:(NSString*)oldPassword newPassword:(NSString*)newPassword callback:(ResponseObjectCallback)callback;

/**
 * generate a reset password request.
 * provide an email or a phone number where the server will send a reset code
 * if both are provided, then two identical reset codes will be sent
 */
+ (void) generateResetPasswordCodeWithEmail:(NSString*)email phoneNumber:(NSString*)phoneNumber callback:(ResponseObjectCallback)callback;
+ (void) resetPasswordForEmail:(NSString*)email phoneNumber:(NSString*)phoneNumber code:(NSString*)code password:(NSString*)password callback:(ResponseObjectCallback)callback;

@end
