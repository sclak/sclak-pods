//
//  NSCalendar+Utils.h
//  Calendar
//
//  Created by albi on 15/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCalendar (Utils)

- (NSArray*) datesForWeekday:(NSInteger)weekday forMonth:(NSInteger)month andYear:(NSInteger)year;
- (NSDate*) dateWithMonth:(NSInteger)month day:(NSInteger)day andYear:(NSInteger)year;
- (NSUInteger) numberOfDaysWithDate:(NSDate *)date;
+ (NSDate*) convertDate:(NSDate*)date withTimezone:(NSString*)timeZone;
+ (NSString*) getTimeFrom:(NSTimeInterval)myTimeInterval;

@end
