//
//  Sender.h
//  SclakApp
//
//  Created by albi on 25/02/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "Email.h"

@protocol Sender
@end

@interface Sender : JSONModel

@property (nonatomic, strong) NSNumber <Optional> *id;
@property (nonatomic, strong) NSString <Optional> *name;
@property (nonatomic, strong) NSString <Optional> *surname;
@property (nonatomic, strong) Email <Optional> *email;
@property (nonatomic, strong) NSString <Optional> *phoneNumber;

- (NSString*) getName;
- (NSString*) getSurname;
- (NSString*) fullName;
- (NSString*) fullNameOrContact;

@end
