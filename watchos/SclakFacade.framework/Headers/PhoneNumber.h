//
//  Email.h
//  Sclak2
//
//  Created by albi on 05/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol PhoneNumber
@end

@interface PhoneNumber : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *phoneCountryCode;
@property (nonatomic, strong) NSString<Optional> *phoneInternationalNumber;
@property (nonatomic, strong) NSString<Optional> *phoneInternationalPrefix;
@property (nonatomic, strong) NSString<Optional> *phoneLocalNumber;
@property (nonatomic, strong) NSString<Optional> *projectApiKey;
@property (nonatomic, strong) NSNumber<Optional> *userId;

@end
