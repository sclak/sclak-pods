//
//  Peripheral.h
//  Sclak2
//
//  Created by albi on 06/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "PinSync.h"
#import "Features.h"
#import "Firmware.h"
#import "ScanToOpen.h"
#import "MemorySetting.h"
#import "PeripheralUser.h"
#import "SCKPeripheralType.h"
#import "PeripheralVersion.h"
#import "PeripheralSetting.h"
#import "AccessoryProtocol.h"
#import "PeripheralBattery.h"
#import "PeripheralBaseModel.h"
#import "PeripheralProtocols.h"
#import "PeripheralStatistics.h"
#import "PeripheralReplacement.h"

#define edit_peripheral                             @"edit_peripheral"
#define add_peripheral                              @"add_peripheral"
#define get_peripheral                              @"get_peripheral"
#define use_peripheral                              @"use_peripheral"
#define create_user                                 @"create_user"
#define add_privilege                               @"add_privilege"
#define manage_groups                               @"manage_groups"
#define test_peripheral                             @"test_peripheral"
#define admin_peripheral                            @"admin_peripheral"
#define purchase_keys                               @"purchase_keys"
#define purchase_guest_packs                        @"purchase_guest_packs"
#define get_peripheral_usages                       @"get_peripheral_usages"
#define manage_pin_codes                            @"manage_pin_codes"
#define pair_peripheral                             @"pair_peripheral"
#define manage_peripherals                          @"manage_peripherals"
#define manage_users                                @"manage_users"
#define replace_peripheral                          @"replace_peripheral"
#define manage_firmwares                            @"manage_firmwares"
#define bulk_add_coupons                            @"bulk_add_coupons"
#define otau_peripheral                             @"otau_peripheral"

#define kGroupTagSuperAdmin                         @"super_admin"
#define kGroupTagInstaller                          @"installer"
#define kGroupTagAdmin                              @"admin"
#define kGroupTagOwner                              @"owner"
#define kGroupTagGuest                              @"guest"

// PERIPHERAL TYPES (PRIMARY)
#define PERIPHERAL_TYPE_SCLAK                                       @"sclak"
#define PERIPHERAL_TYPE_SCLAK_BATTERY                               @"sclakb"
#define PERIPHERAL_TYPE_SCLAK_HANDLE_LEGACY                         @"handle"
#define PERIPHERAL_TYPE_ENTR                                        @"entr"
#define PERIPHERAL_TYPE_SCLAK_HANDLE_KEYPAD                         @"h_keyb"
#define PERIPHERAL_TYPE_SCLAK_HANDLE_TAG                            @"h_tag"
#define PERIPHERAL_TYPE_SCLAK_1C                                    @"sclak_1c"
#define PERIPHERAL_TYPE_SCLAK_1C_BATTERY                            @"sclak_1cb"
#define PERIPHERAL_TYPE_SCLAK_ENTR_BRIDGE                           @"s_bridge"
#define PERIPHERAL_TYPE_CR_LOCK                                     @"crlock"
#define PERIPHERAL_TYPE_CR_LOCK_M                                   @"yaz"
#define PERIPHERAL_TYPE_TIM_BOLT                                    @"tim_bolt"
#define PERIPHERAL_TYPE_KEY_DEPOSIT_LOCK                            @"kde"
#define PERIPHERAL_TYPE_CYLINDER                                    @"cylinder"
#define PERIPHERAL_TYPE_CYLINDER_V2                                 @"cylinderv2"
#define PERIPHERAL_TYPE_CYLINDER_E                                  @"cylinder_e"
#define PERIPHERAL_TYPE_SCLAK_LOCK                                  @"s_lock"
#define PERIPHERAL_TYPE_DHAND                                       @"dhand"
#define PERIPHERAL_TYPE_S2R                                         @"s2r"
#define PERIPHERAL_TYPE_SAM                                         @"sam"
#define PERIPHERAL_TYPE_LOCKER                                      @"lkr"
#define PERIPHERAL_TYPE_SCLAK_GEAR                                  @"gear"
#define PERIPHERAL_TYPE_SCLAK_UFOe                                  @"ufoe"
#define PERIPHERAL_TYPE_GIVIK                                       @"givik"
#define PERIPHERAL_TYPE_SCLAK_GEAR_A                                @"gra"
#define PERIPHERAL_TYPE_SCLAK_GEAR_R                                @"grr"
#define PERIPHERAL_TYPE_SCLAK_TAG_PRESENCE                          @"tagp"
#define PERIPHERAL_TYPE_SCLAK_DRIVER                                @"sdr"
#define PERIPHERAL_TYPE_SCLAK_PADLOCK                               @"plk"
#define PERIPHERAL_TYPE_NELLO                                       @"nello_one"
#define PERIPHERAL_TYPE_KONE                                        @"kone"

// PERIPHERAL TYPES (SECONDARY)
#define PERIPHERAL_TYPE_ULOCK                                       @"ulock"
#define PERIPHERAL_TYPE_PARKEY                                      @"parkey"
#define PERIPHERAL_TYPE_NEAR                                        @"near"
#define PERIPHERAL_TYPE_WITTKOPP                                    @"wittkopp"
#define PERIPHERAL_TYPE_SAFESCLAK_BS                                @"sclaklock_bs"
#define PERIPHERAL_TYPE_SCLAK_KEYPAD                                @"keyb"
#define PERIPHERAL_TYPE_SCLAK_FOB                                   @"keyfob"
#define PERIPHERAL_TYPE_SCLAK_SAFE                                  @"sclaksafe"
#define PERIPHERAL_TYPE_SCLAK_LIGHT_DIMMER                          @"lightdimm"
#define PERIPHERAL_TYPE_ZTF                                         @"ZTF"
#define PERIPHERAL_TYPE_SAFESCLAK_MS                                @"sclaklock_ms"
#define PERIPHERAL_TYPE_CEMO                                        @"cemo"
#define PERIPHERAL_TYPE_ODL                                         @"odl"
#define PERIPHERAL_TYPE_SCLAK_TAG                                   @"s_tag"
#define PERIPHERAL_TYPE_SCLAK_MIFARE                                @"mifare"
#define PERIPHERAL_TYPE_WIEGAND_SCLAK                               @"wieg"
#define PERIPHERAL_TYPE_ENTREMATIC                                  @"ditec"
#define PERIPHERAL_TYPE_BWG                                         @"bwg"

// PERIPHERAL TYPES (VIRTUAL)
#define PERIPHERAL_TYPE_INTEGRATED_KEYPAD                           @"50001"
#define PERIPHERAL_TYPE_INTEGRATED_READER                           @"50002"

#define PERIPHERAL_FAMILY_SCLAK                                     [NSArray arrayWithObjects:PERIPHERAL_TYPE_SCLAK,PERIPHERAL_TYPE_SCLAK_BATTERY,PERIPHERAL_TYPE_SCLAK_1C,PERIPHERAL_TYPE_SCLAK_1C_BATTERY,PERIPHERAL_TYPE_WIEGAND_SCLAK,nil]

#define PERIPHERAL_FAMILY_GEAR                                      [NSArray arrayWithObjects:PERIPHERAL_TYPE_SCLAK_GEAR,PERIPHERAL_TYPE_SCLAK_GEAR_A,PERIPHERAL_TYPE_SCLAK_GEAR_R,nil]

// THIRD-PARTY CUSTOMIZATION
#define SCLAK_PERIPHERAL_VERSION_CODE_DIERRE                        @"dierre"
#define SCLAK_PERIPHERAL_VERSION_CODE_VALNES                        @"valnes"
#define SCLAK_PERIPHERAL_VERSION_CODE_CEMO                          @"cemo"
#define SCLAK_PERIPHERAL_VERSION_CODE_GARDESA_PORTE                 @"gardesa_porte"
#define SCLAK_PERIPHERAL_VERSION_CODE_AGB                           @"agb"
#define SCLAK_PERIPHERAL_VERSION_CODE_KAITHRON                      @"kaithron_air"

#define violetTag                                                   @"violet"
#define lightVioletTag                                              @"lightViolet"
#define oliveTag                                                    @"olive"
#define lightGreenTag                                               @"lightGreen"
#define blueTag                                                     @"blue"
#define turquoiseTag                                                @"turquoise"

@interface Peripheral : PeripheralBaseModel

@property (nonatomic, strong) NSString<Optional> *btcode;
@property (nonatomic, strong) NSString<Optional> *hexCode;
@property (nonatomic, strong) NSString<Optional> *lot;

@property (nonatomic, strong) SCKPeripheralType<Optional> *peripheralType;
@property (nonatomic, strong) NSNumber<Optional> *peripheralTypeId;
@property (nonatomic, strong) NSString<Optional> *peripheralTypeCode;

@property (nonatomic, strong) MemorySetting<Optional> *memorySetting;

@property (nonatomic, strong) PeripheralVersion<Optional> *peripheralVersion;

@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *address;
@property (nonatomic, strong) NSNumber<Optional> *enabled;
@property (nonatomic, strong) NSString<Optional> *desc;
@property (nonatomic, strong) NSNumber<Optional> *insertTime;
@property (nonatomic, strong) NSNumber<Optional> *activationTime;
@property (nonatomic, strong) NSNumber<Optional> *editTime;

// SHARED
@property (nonatomic, strong) NSNumber<Optional> *shared;

// ADMIN, INSTALLER
@property (nonatomic, strong) Privilege<Optional> *admin;
@property (nonatomic, strong) Privilege<Optional> *installer;

// SECRET CODE
@property (nonatomic, strong) NSString<Optional> *secretCode;
@property (nonatomic, strong) NSNumber<Optional> *secretSyncNeeded;

// PUK - AVAILABLE WHEN "SIMPLE PUK" MODE IS ON
@property (nonatomic, strong) NSString<Optional> *puk;

// PERIPHERAL GROUPS
@property (nonatomic, strong) NSArray<PeripheralGroup, Optional> *peripheralGroups;

// INSTALL
@property (nonatomic, strong) NSNumber<Optional> *longitude;
@property (nonatomic, strong) NSNumber<Optional> *latitude;

// AUTO-OPEN, TOC TOC, TWIST AND OPEN
@property (nonatomic, strong) NSNumber<Optional> *autoOpen;
@property (nonatomic, strong) NSNumber<Optional> *autoOpenProximity;
@property (nonatomic, strong) NSNumber<Optional> *knockKnock;
@property (nonatomic, strong) NSNumber<Optional> *autoOpenFired;
@property (nonatomic, strong) NSDate<Optional> *autoOpenTime;
@property (nonatomic, strong) NSNumber<Optional> *knockKnockFired;
@property (nonatomic, strong) NSDate<Optional> *knockKnockTime;
@property (nonatomic, strong) NSNumber<Optional> *twistAndOpen;
@property (nonatomic, strong) NSNumber<Optional> *shakeAndOpen;

// FIRMWARE
@property (nonatomic, strong) NSString<Optional> *peripheralFirmwareVersion; // used for PUT
@property (nonatomic, strong) Firmware<Optional> *peripheralFirmware;
@property (nonatomic, strong) Firmware<Optional> *peripheralFirmwareUpdate;

// FEATURES
@property (nonatomic, strong) Features <Optional> *features;
@property (nonatomic, strong) NSString<Optional> *timeZone;

// SETTINGS
@property (nonatomic, strong) NSMutableArray<PeripheralSetting, Optional> *peripheralSettings;

// ACCESSORIES
@property (nonatomic, strong) NSNumber<Optional> *pairedPeripheralsCount;
@property (nonatomic, strong) NSArray<Accessory, Optional> *accessories;

// MASTER & SLAVES
@property (nonatomic, strong) NSString<Optional> *masterPeripheralId;
@property (nonatomic, strong) NSMutableArray<Peripheral, Optional> *slaves;

// PIN CODES
@property (nonatomic, strong) NSNumber<Optional> *pinSyncNeeded;
@property (nonatomic, strong) PinSync<Optional> *pinSync;

// STATISTICS
@property (nonatomic, strong) PeripheralStatistics<Optional> *peripheralStatistics;

// PERIPHERAL REPLACEMENT
@property (nonatomic, strong) PeripheralReplacement<Optional> *peripheralReplacement;

// peripheral "check_only" flag, used in Peripheral PUT
@property (nonatomic, strong) NSNumber<Optional> *checkOnly;

// COLORS
@property (nonatomic, strong) NSString<Optional> *colorTag;

// MODIFIED DICTIONARY
@property (nonatomic, strong) NSDictionary <Ignore> *modifiedFieldsDictionary;

// BATTERY
@property (nonatomic, strong) PeripheralBattery<Optional> *battery;

// PLACE
@property (nonatomic, strong) NSNumber<Optional> *placeId;

// HAS ADMIN
@property (nonatomic, strong) NSNumber<Optional> *hasAdmin;

#pragma mark - Constructors

- (instancetype) initWithId:(NSNumber*)pid;
- (instancetype) initWithBtcode:(NSString*)btcode;

#pragma mark - Generic Methods

- (NSString*) presentationName;
- (NSString*) presentationShortName;
- (NSString*) halfBtcode;

/**
 * this method removes from sentPrivileges my invitation
 * and put it in mySentPrivilege property
 */
- (void) removeSentPrivilegesWithUserId:(NSNumber*)userId;

- (BOOL) added; // 0
- (BOOL) inMaintenance; // 1
- (BOOL) used; // 3
- (BOOL) active; // 6
- (BOOL) tested; // 9

/**
 * check that this peripheral is an accessory of another peripheral
 */
- (BOOL) isPaired;

- (NSUInteger) countPrivilegesForGroupTag:(NSString*)groupTag;
- (NSUInteger) countPrivilegesForGroupTag:(NSString*)groupTag privileges:(NSArray*)privileges;

- (UIColor*) getColorForTag:(NSString*)colorTag;

#pragma mark - Getter for peripheral + group sent privileges

/**
 * return an array containing all received privileges from other users
 * self-sent privileges are not returned
 * @param activeOnly filter active privileges only (exclude pending, deleted)
 */
- (NSArray*) allReceivedPrivileges:(BOOL)activeOnly;

/**
 * return an array containing all sent privileges for this specific peripheral
 * and all sent privileges for all the peripheral groups that contain this peripheral
 */
- (NSArray*) allSentPrivileges;

/**
* return an array containing  all sent privileges for all the peripheral groups that contain this peripheral
*/
- (NSArray*) allSentGroupPrivileges;

/**
 * return the peripheral group that has specific ID from the array or peripheral groups
 * in the property "peripheralGroups" of this model
 * this array is present in the peripherals received by the guests, when part of a peripheral group
 */
- (PeripheralGroup*) peripheralGroupWithId:(NSNumber*)pid;

#pragma mark - Getters for specific peripheral usages

- (BOOL) isPinPukSupported;
- (BOOL) isAutoCloseSupported;
- (BOOL) isLatchingSupported;
- (BOOL) isAutocloseTimeSupported;
- (BOOL) isLatching;
- (BOOL) isSlowBluetoothAnnounce;
- (BOOL) isENTR;
- (BOOL) isVirtualKeypadEnabled;

- (BOOL) supportPeripheralModes;
- (BOOL) hasPairedAccessories;

#pragma mark Remote Use

- (BOOL) remoteUseEnabled;
- (BOOL) privilegeRemoteUseEnabled;

#pragma mark Support Checks

- (BOOL) supportRemoteUse;
- (BOOL) supportAccessLogSync;
- (BOOL) supportAccessCounterSync;

#pragma mark Battery

- (BOOL) canCheckBattery;

#pragma mark Track Location

- (BOOL) isTrackLocationGPSNeeded;

#pragma mark - Firmware Update

- (BOOL) isFirmwareUpdateNeeded;
- (BOOL) isFirmwareUpdateMandatory;
- (BOOL) isFirmwareUpdateBroken;

#pragma mark - Remote Use

- (NSString*) btcodeForRemoteUse;
- (NSArray<NSString*>*) btcodesForRemoteUse;

#pragma mark - Accessories

- (NSArray*) getPairedAccessories;

- (BOOL) hasIntegratedKeypad;
- (BOOL) hasIntegratedTagReader;
- (BOOL) hasIntegratedAccessories;

- (BOOL) hasPairedKeypad;
- (BOOL) hasPairedTagReader;
- (BOOL) hasPairedRemote;

#pragma mark - Slaves

- (NSArray*) getSlaves;

#pragma mark - Generic Peripheral Settings

- (NSString*) getSettingWithKey:(NSString*)key;
- (NSString*) getSettingWithKey:(NSString*)key defaultValue:(NSObject*)value;
- (NSNumber*) getNumberSettingWithKey:(NSString*)key defaultValue:(NSNumber*)value;
- (void) setSettingWithKey:(NSString*)key value:(NSObject*)value;

#pragma mark - Peripheral installation settings

/**
 * check if this peripheral requires an installation with a test
 * if so, the Installer will go through a wizard that starts with testing the product and then
 * finish with the Admin invitation
 */
- (BOOL) requiresTestInstallation;

/**
 * check if this peripheral requires that the Installer becomes automatically Admin (and Owner)
 */
- (BOOL) requiresAutomaticInstallerIsAdmin;

#pragma mark - Scan To Open

- (BOOL) isScanToOpenPeripheralGroupType:(ScanToOpenType)type;
- (BOOL) isScanToOpenPeripheralGroupExclusiveUse;

#pragma mark - Peripheral Settings

- (NSNumber*) hasBuzzer;
- (NSNumber*) buzzer;
- (void) setBuzzer:(NSNumber<Ignore>*)value;

- (NSNumber*) hasLed;
- (NSNumber*) led;
- (void) setLed:(NSNumber<Ignore>*)value;

- (NSNumber*) autocloseTime;
- (void) setAutocloseTime:(NSNumber<Ignore>*)value;

- (NSString*) peripheralMode;
- (void) setPeripheralMode:(NSString<Ignore>*)value;

- (BOOL) ownerPostPrivilegeEnabled;
- (void) setOwnerPostPrivilegeEnabled:(BOOL)value;

- (BOOL) ownerPostPrivilegePush;
- (void) setOwnerPostPrivilegePush:(BOOL)value;

- (BOOL) adminManageOwnerPrivilegesEnabled;
- (void) setAdminManageOwnerPrivilegesEnabled:(BOOL)value;

- (BOOL) supportOwnerPostPrivilege;
- (BOOL) supportAutoOpen;
- (BOOL) supportTocToc;

- (BOOL) hasDoorSensor;

#pragma mark - Specific Handle Settings

- (NSNumber*) buzzOnHandleEnabled;
- (void) setBuzzOnHandleEnabled:(NSNumber<Ignore>*)value;

- (NSNumber*) buzzOnHandleDisabled;
- (void) setBuzzOnHandleDisabled:(NSNumber<Ignore>*)value;

#pragma mark - Requests

- (void) getPinSyncCallback:(ResponseObjectCallback)callback;

#pragma mark - Peripheral type

/**
 Retrieve the full peripheral type object fetched from the cache for this peripheral.
 */
- (SCKPeripheralType*) getFullPeripheralType;

#pragma mark - Log Model

- (NSDictionary*) logModel;

#pragma mark - Server APIs

+ (void) getPeripheralStatus:(NSString*)btcode callback:(ResponseObjectCallback)callback;
+ (void) resetPeripheral:(NSString*)btcode callback:(ResponseObjectCallback)callback;
+ (void) checkVirtualKeypad:(NSString*)btcode callback:(ResponseObjectCallback)callback;

@end
