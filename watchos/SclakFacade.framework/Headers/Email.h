//
//  Email.h
//  Sclak2
//
//  Created by albi on 05/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol Email
@end

@interface Email : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSNumber<Optional> *status;

- (BOOL) isActive;

@end
