//
//  FeaturePrivilege.h
//  SclakApp
//
//  Created by albi on 16/02/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol FeaturePrivilege
@end

@interface FeaturePrivilege : JSONModel

@property (nonatomic, strong) NSString <Optional> *groupTag;
@property (nonatomic, strong) NSString <Optional> *btcode;
@property (nonatomic, strong) NSNumber <Optional> *availability;
@property (nonatomic, strong) NSNumber <Optional> *quantity;
@property (nonatomic, strong) NSNumber <Optional> *expireTime;
@property (nonatomic, strong) NSNumber <Optional> *purchaseId;
@property (nonatomic, strong) NSNumber <Optional> *insertTime;

- (BOOL) includedLicense;
- (BOOL) purchased;
- (BOOL) infiniteQuantity;
- (BOOL) hasNoExpiration;
- (BOOL) isExpiringIn:(NSUInteger)days fromNow:(NSDate*)now;
- (BOOL) isExpired:(NSDate*)now;

@end
