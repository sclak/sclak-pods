//
//  PeripheralVersion.h
//  SclakFacade
//
//  Created by Daniele Poggi on 12/01/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface PeripheralVersion : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *code;
@property (nonatomic, strong) NSNumber<Optional> *inAppPurchase;
@property (nonatomic, strong) NSNumber<Optional> *trial;

- (BOOL) isTrialEnabled;

@end
