//
//  Day.h
//  SclakApp
//
//  Created by albi on 02/01/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "TimeRange.h"

#define kFromHourDefault    @"08.00"
#define kToHourDefault      @"19.00"

#define kFromHourMaxDefault    @"00.00"
#define kToHourMaxDefault      @"24.00"

@protocol Day <NSObject>
@end

@interface Day : JSONModel

@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) TimeRange *timeRange;

// only used from app
@property (nonatomic, strong) NSString <Ignore> *rectString;
@property (nonatomic, strong) NSIndexPath <Ignore> *indexPath;

- (BOOL) isPartial;
- (BOOL) isFull;

- (BOOL) hasSameMonthAndTimeRange:(Day*)object;
- (BOOL) hasSameTimeRange:(Day*)day;

- (NSString*)description;

@end
