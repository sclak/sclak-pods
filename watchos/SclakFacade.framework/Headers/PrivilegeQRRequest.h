//
//  PrivilegeQRRequest.h
//  SclakApp
//
//  Created by Daniele Poggi on 20/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "ResponseObject.h"

//@class Peripheral, PeripheralGroup, Privilege;

@interface PrivilegeQRRequest : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *requesterEmail;
@property (nonatomic, strong) NSString<Optional> *requesterPhoneNumber;
@property (nonatomic, strong) NSString<Optional> *requesterName;
@property (nonatomic, strong) NSString<Optional> *requesterSurname;
@property (nonatomic, strong) NSNumber<Optional> *privilegeRequestTime;
@property (nonatomic, strong) NSString<Optional> *targetType;
@property (nonatomic, strong) NSString<Optional> *targetField;
@property (nonatomic, strong) NSNumber<Optional> *targetValue;

@end
