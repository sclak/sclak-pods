//
//  Place.h
//  SclakFacade
//
//  Created by Daniele Poggi on 19/11/2018.
//  Copyright © 2018 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "ScanToOpen.h"

@protocol Place;
@protocol PlaceItem;

@interface Places : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *totalCount;
@property (nonatomic, strong) NSArray<Place> *list;

@end

@interface PlaceItem : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *desc;
@property (nonatomic, strong) NSNumber<Optional> *order;
@property (nonatomic, strong) NSNumber<Optional> *peripheralId;

@end

@interface Place : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *desc;
@property (nonatomic, strong) NSString<Optional> *address;
@property (nonatomic, strong) NSNumber<Optional> *latitude;
@property (nonatomic, strong) NSNumber<Optional> *longitude;

@property (nonatomic, strong) NSMutableArray<PlaceItem, Optional> *peripherals;

@property (nonatomic, strong) NSMutableArray<ScanToOpen, Optional> *qrcodes;

- (BOOL) isActive;
- (BOOL) isDeleted;

#pragma mark - Serialization APIs

+ (BOOL) setPlacesCache:(Places*)places;
+ (Place*) getPlaceWithId:(NSNumber*)id;
+ (BOOL) updatePlacesCache:(Place*)place;
+ (BOOL) updatePlacesCache:(Place*)place writeCache:(BOOL)write;
+ (BOOL) removePlaceFromCache:(Place*)place;
+ (void) updatePlacesCacheRemoveDeleted:(NSArray*)allPlaces;

+ (void) writePlacesCache;
+ (void) deletePlacesCache;

#pragma mark - Peripherals

- (NSArray*) peripheralsInPlace;

#pragma mark - Privileges

- (NSArray*) sentPrivilegesInPlace;

@end
