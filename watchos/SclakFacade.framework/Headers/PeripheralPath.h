//
//  PeripheralPath.h
//  SclakFacade
//
//  Created by Daniele Poggi on 15/06/2018.
//  Copyright © 2018 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@protocol PeripheralPath
@end

@interface PeripheralPath : ResponseObject

@property (nonatomic, strong) NSString *sequence;

- (NSArray*) sequenceBtcodes;

@end

@interface PeripheralPaths : ResponseObject

@property (nonatomic, strong) NSArray<PeripheralPath> *paths;

@end
