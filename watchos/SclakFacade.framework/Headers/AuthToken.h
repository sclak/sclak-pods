//
//  Token.h
//  Sclak2
//
//  Created by albi on 05/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "User.h"

@interface AuthToken : ResponseObject

@property (nonatomic, strong) NSString<Optional> *token;
@property (nonatomic, strong) NSNumber<Optional> *userId;
@property (nonatomic, strong) NSNumber<Optional> *expiryDate;
@property (nonatomic, strong) NSNumber<Optional> *creationDate;
@property (nonatomic, strong) NSNumber<Optional> *tokenDuration;

@property (nonatomic, strong) User<Optional> *user;

@property (nonatomic, strong) NSString<Optional> *serverPublicKey;

/**
 * Smartphone authentication
 */
+ (NSURLSessionDataTask*) getAuthTokenCallback:(ResponseObjectCallback)callback;

/**
 * Username / Password authentication
 */
+ (NSURLSessionDataTask*) getAuthTokenWithUsername:(NSString*)username password:(NSString*)password callback:(ResponseObjectCallback)callback;

/**
 * Two Factor authentication
 */
+ (NSURLSessionDataTask*) getAuthTokenWithUsername:(NSString*)username password:(NSString*)password twoFactorCode:(NSString*)code callback:(ResponseObjectCallback)callback;


@end
