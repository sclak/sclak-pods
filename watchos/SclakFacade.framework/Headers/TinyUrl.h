//
//  TinyUrl.h
//  SclakFacade
//
//  Created by albi on 25/09/15.
//  Copyright © 2015 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@interface TinyUrl : ResponseObject

@property (nonatomic, strong) NSString *url;

@end
