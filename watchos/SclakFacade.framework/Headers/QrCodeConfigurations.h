//
//  QrCodeConfigurations.h
//  Pods
//
//  Created by Rico Crescenzio on 31/01/2020.
//

#import "ResponseObject.h"
#import "ScanToOpen.h"

@interface QrCodeConfigurations : ResponseObject

@property (nonatomic, strong) NSArray<ScanToOpen, Optional> *list;

-(BOOL) hasConfigurations;

-(ScanToOpen*) getFirstConfiguration;

@end
