//
//  Badge.h
//  SclakFacade
//
//  Created by Daniele Poggi on 21/06/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "SCKFacadeCallbacks.h"

@class PinCode;
@class Privilege;
@class Peripheral;
@class PeripheralGroup;

@protocol Badge

@end

@interface Badge : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSNumber<Optional> *uniqueId;
@property (nonatomic, strong) NSNumber<Optional> *pinCodeId;
@property (nonatomic, strong) NSNumber<Optional> *privilegeId;
@property (nonatomic, strong) NSNumber<Optional> *written;
@property (nonatomic, strong) NSString<Optional> *serialCode;
@property (nonatomic, strong) NSString<Optional> *qrCode;
@property (nonatomic, strong) NSNumber<Optional> *insertTime;

@property (nonatomic, strong) PinCode<Optional> *pinCode;
@property (nonatomic, strong) Privilege<Optional> *privilege;
@property (nonatomic, strong) Privilege<Optional> *previousPrivilege;
@property (nonatomic, strong) Peripheral<Optional> *peripheral;
@property (nonatomic, strong) PeripheralGroup<Optional> *peripheralGroup;

+ (void) getBadges:(NSDictionary*)params callback:(ResponseObjectCallback)callback;
+ (void) getProductionBadge:(NSString*)uniqueId callback:(ResponseObjectCallback)callback;
+ (void) createBadge:(Badge*)badge callback:(ResponseObjectCallback)callback;
+ (void) activateProductionBadge:(Badge*)badge overwrite:(BOOL)overwrite callback:(ResponseObjectCallback)callback;

@end

@interface Badges : ResponseObject

@property (nonatomic, strong) NSArray<Badge, Optional> *list;

@end
