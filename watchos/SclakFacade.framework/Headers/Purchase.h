//
//  Purchased.h
//  SclakApp
//
//  Created by albi on 30/01/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "ResponseObject.h"

@protocol Purchase
@end

@interface Purchase : JSONModel

@property (nonatomic, strong) NSNumber <Optional> *id;
@property (nonatomic, strong) NSNumber <Optional> *purchasableItemId;
@property (nonatomic, strong) NSString <Optional> *ios;
@property (nonatomic, strong) NSString <Optional> *transactionCode;

@end

@interface Purchases : ResponseObject

@property (nonatomic, strong) NSArray <Purchase> *list;

@end
