//
//  PeripheralStatistics.h
//  SclakFacade
//
//  Created by Daniele Poggi on 10/03/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@interface PeripheralStatisticsCounterPrivilege : ResponseObject

@property (nonatomic, strong) NSNumber *pending;
@property (nonatomic, strong) NSNumber *active;

@end

@interface PeripheralStatisticsCounters : ResponseObject

@property (nonatomic, strong) PeripheralStatisticsCounterPrivilege *owner;
@property (nonatomic, strong) PeripheralStatisticsCounterPrivilege *guest;

@end

@interface PeripheralStatistics : ResponseObject

@property (nonatomic, strong) PeripheralStatisticsCounters *counters;

@end
