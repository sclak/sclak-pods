//
//  ScanToOpen.h
//  SclakFacade
//
//  Created by Daniele Poggi on 06/04/2018.
//  Copyright © 2018 Sclak. All rights reserved.
//

#import "ResponseObject.h"

typedef NS_ENUM(NSUInteger, ScanToOpenType) {
    ScanToOpenTypeAuto,
    ScanToOpenTypeSemiAuto,
    ScanToOpenTypeOpen,
};

@protocol ScanToOpen;
@protocol ScanToOpenItem;
@protocol Peripheral;

@class Place;
@class Calendar;
@class Peripheral;
@class PeripheralGroup;

@interface ScanToOpenItem : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *code;
@property (nonatomic, strong) NSNumber<Optional> *peripheralId;
@property (nonatomic, strong) NSString<Optional> *qrCode;
@property (nonatomic, strong) NSString<Optional> *qrcodeImageData;

- (NSData*) qrcodeImage;
- (void) reset;

@end

@interface ScanToOpen : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSNumber<Optional> *userId;

@property (nonatomic, strong) NSString<Optional> *tinyUrl;

@property (nonatomic, strong) NSString<Optional> *code;
@property (nonatomic, strong) NSString<Optional> *qrcode;
@property (nonatomic, strong) NSString<Optional> *qrcodeImageData;

@property (nonatomic, strong) NSString<Optional> *targetType;
@property (nonatomic, strong) NSString<Optional> *targetField;
@property (nonatomic, strong) NSString<Optional> *targetValue;
@property (nonatomic, strong) NSNumber<Optional> *groupId;
@property (nonatomic, strong) NSString<Optional> *groupTag;
@property (nonatomic, strong) NSString<Optional> *purchaseType;

@property (nonatomic, strong) Calendar<Optional> *timeConstraints;

@property (nonatomic, strong) NSString<Optional> *type;

@property (nonatomic, strong) NSNumber<Optional> *forceScan;
@property (nonatomic, strong) NSNumber<Optional> *exclusiveUse;
@property (nonatomic, strong) NSNumber<Optional> *authorizedUsers;

@property (nonatomic, strong) Peripheral<Optional> *peripheral;
@property (nonatomic, strong) PeripheralGroup<Optional> *peripheralGroup;
@property (nonatomic, strong) Place<Optional> *place;

@property (nonatomic, strong) NSArray<NSString*><Optional> *privileges;

/**
 * can be taken|free
 * used for keyrings and places
 */
@property (nonatomic, strong) NSString<Optional> *peripheralStatus;

/**
 * used for keyrings and places, describes other free peripherals when the scanned one is taken
 */
@property (nonatomic, strong) NSArray<Peripheral, Optional> *freePeripherals;

/**
 * used for keyrings, describes all the peripheral in the keyring
 */
@property (nonatomic, strong) NSMutableArray<ScanToOpenItem, Optional> *peripheralQrcodes;

/**
 * used only by app to keep track of btcode
 */
@property (nonatomic, strong) NSString<Optional> *btcode;

- (BOOL) isType:(ScanToOpenType)type;
- (BOOL) isTypeAuto;
- (BOOL) isTypeSemiAuto;
- (BOOL) isTypeOpen;

- (BOOL) isStatusTaken;
- (BOOL) isStatusFree;
- (BOOL) isStatusLimitReached;

- (BOOL) isExclusiveUse;
- (BOOL) isAuthorizedUsers;
- (BOOL) isForceScan;

- (BOOL) hasFreePeripherals;

- (BOOL) isGuest;

@end
