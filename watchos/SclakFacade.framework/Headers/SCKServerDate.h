//
//  SCKServerDate.h
//  SclakCore
//
//  Created by Francesco Mantello on 08/07/18.
//  Copyright (c) 2018 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LAST_UPDATE_DATE                    @"LAST_UPDATE_DATE"
#define TIME_INTERVAL                       @"TIME_INTERVAL"

#define DATE_SERVER                         @"https://api.sclak.com/time"
#define DATE_FORMAT                         @"EEE, dd MMM yyyy HH:mm:ss Z"

#define SERVER_DATE_CHANGED_NOTIFICATION    @"SERVER_DATE_CHANGED_NOTIFICATION"

typedef void(^ServerDateChangedCallback)(NSDate *serverDate);

@interface SCKServerDate : NSObject

+ (NSDate*) serverDateNow;
+ (NSDate*) serverDateWithTimeZone:(NSString*)timeZone;
+ (NSDate*) serverDateWithTimeZone:(NSString*)timeZone callback:(ServerDateChangedCallback)callback;
+ (NSDate*) lastUpdateDateWithTimeZone:(NSString*)timeZone;

+ (BOOL) trusted;
+ (void) invalidateServerDate;

/**
 * utility to invalidate the server date and get it again from server (if reachability allows it)
 */
+ (void) invalidateAndReload;
+ (void) invalidateAndReloadNetwork;

@end
