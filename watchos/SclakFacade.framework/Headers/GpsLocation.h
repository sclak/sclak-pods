//
//  AccessCounter.h
//  SclakFacade
//
//  Created by Daniele Poggi on 25/03/2019.
//  Copyright © 2019 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "SCKFacadeCallbacks.h"
#import "Peripherals.h"

@protocol GpsLocation;

@interface GpsLocation : JSONModel

@property (nonatomic, strong) NSString <Optional> *btcode;
@property (nonatomic, strong) NSNumber *timestamp;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, strong) NSString *address;

+ (void) syncToServer:(NSArray*)logs callback:(ResponseObjectCallback)callback;
+ (void) syncFromServer:(Peripheral*)peripheral callback:(ResponseObjectCallback)callback;

@end

@interface GpsLocations : ResponseObject
@property (nonatomic, strong) NSArray<GpsLocation> *list;
@end
