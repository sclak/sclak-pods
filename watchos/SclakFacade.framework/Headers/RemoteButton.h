//
//  RemoteButton.h
//  SclakFacade
//
//  Created by Daniele Poggi on 21/06/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "SCKFacadeCallbacks.h"

@class Accessory;
@class Privilege;
@class PinCode;

@protocol RemoteButton

@end

@interface RemoteButton : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSNumber<Optional> *accessoryId;
@property (nonatomic, strong) NSNumber<Optional> *privilegeId;
@property (nonatomic, strong) NSNumber<Optional> *pinCodeId;

@property (nonatomic, strong) NSNumber<Optional> *remoteButtonIndex;

@property (nonatomic, strong) NSNumber<Optional> *insertTime;
@property (nonatomic, strong) NSNumber<Optional> *editTime;

@property (nonatomic, strong) Accessory<Optional> *accessory;
@property (nonatomic, strong) Privilege<Optional> *privilege;
@property (nonatomic, strong) PinCode<Optional> *pinCode;

+ (instancetype) remoteButtonWithAccessoryId:(NSNumber*)accessoryId
                                 privilegeId:(NSNumber*)privilegeId
                           remoteButtonIndex:(NSNumber*)remoteButtonIndex;

+ (void) createForAccessory:(Accessory*)accessory buttons:(NSArray<RemoteButton>*)remoteButtons callback:(ResponseObjectCallback)callback;

@end

@interface RemoteButtons : ResponseObject

@property (nonatomic, strong) NSArray<RemoteButton, Optional> *list;

@end
