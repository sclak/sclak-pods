//
//  TimeLimit.h
//  SclakApp
//
//  Created by albi on 05/01/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class Day;

@interface TimeRange : JSONModel

@property (nonatomic, strong) NSString *fromHour;
@property (nonatomic, strong) NSString *toHour;

/**
 * get begin time range time in seconds from 00:00
 */
- (NSTimeInterval) getFromTime;
- (NSInteger) getFromHour;
- (NSInteger) getFromMinutes;

- (NSInteger) getToHour;
- (NSInteger) getToMinutes;

- (BOOL) isPartial;
- (BOOL) isFull;

-(NSDictionary*) toDictionary;

- (TimeRange*) copy;

@end
