//
//  Privilege.h
//  SclakApp
//
//  Created by Daniele Poggi on 20/11/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "Action.h"
#import "Badge.h"
#import "Coupon.h"
#import "Sender.h"
#import "Calendar.h"
#import "ScanToOpen.h"
#import "RemoteButton.h"
#import "PrivilegeOption.h"
#import "SCKFacadeCallbacks.h"
#import "SCKAirbnbReservation.h"

#define PrivilegeStatusPendingAcceptance 4
#define PrivilegeStatusPendingReset      3
#define PrivilegeStatusActive            2
#define PrivilegeStatusPending           1
#define PrivilegeStatusDisabled          0
#define PrivilegeStatusDeleted           -1

#define kSecurityLevelLow                0
#define kSecurityLevelMedium             1
#define kSecurityLevelHigh               2

#define PrivilegePurchaseTypeSender     @"sender"
#define PrivilegePurchaseTypeUser       @"user"

@protocol Privilege

@end

@interface Privileges : ResponseObject

@property (nonatomic, strong) NSArray<Privilege> *list;

@end

@class Peripheral;
@class PeripheralGroup;

@interface Privilege : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *id;
@property (nonatomic, strong) NSNumber<Optional> *userId;
@property (nonatomic, strong) NSString<Optional> *targetType;
@property (nonatomic, strong) NSString<Optional> *targetField;
@property (nonatomic, strong) NSString<Optional> *targetValue;
@property (nonatomic, strong) NSNumber<Optional> *numTargets;
@property (nonatomic, strong) NSString<Optional> *groupTag;
@property (nonatomic, strong) NSString<Optional> *purchaseType;
@property (nonatomic, strong) NSString<Optional> *userEmail;
@property (nonatomic, strong) NSString<Optional> *userName;
@property (nonatomic, strong) NSString<Optional> *userSurname;
@property (nonatomic, strong) NSString<Optional> *userPhoneNumber;
@property (nonatomic, strong) NSString<Optional> *internationalPhoneNumber;
@property (nonatomic, strong) NSNumber<Optional> *couponId;
@property (nonatomic, strong) NSNumber<Optional> *oneTime;
@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, strong) NSNumber<Optional> *appEnabled;
@property (nonatomic, strong) Calendar<Optional> *timeConstraints;
@property (nonatomic, strong) Coupon<Optional> *coupon;
@property (nonatomic, strong) NSArray<Action, Optional> *actions;
@property (nonatomic, strong) NSNumber<Optional> *confirmRequired;
@property (nonatomic, strong) NSNumber<Optional> *expireTime;
@property (nonatomic, strong) NSNumber<Optional> *modifiedTime;
@property (nonatomic, strong) NSNumber<Optional> *invitationTime;
@property (nonatomic, strong) NSNumber<Optional> *activationTime;
@property (nonatomic, strong) NSNumber<Optional> *lastUsageTime;
@property (nonatomic, strong) NSNumber<Optional> *deleteTime;
@property (nonatomic, strong) NSString <Optional> *pukCode;

// password used to operate accessory pins
@property (nonatomic, strong) NSString <Optional> *password;

// generate keypad pin
@property (nonatomic, strong) NSNumber<Optional> *generateKeypadPin;

// sclak keypad pin
@property (nonatomic, strong) NSNumber<Optional> *pinCodeId;
@property (nonatomic, strong) NSString<Optional> *pinCode;
@property (nonatomic, strong) PinCode<Optional> *keypadPinCode;

// invite
@property (nonatomic, strong) NSNumber<Optional> *insertTime;
@property (nonatomic, strong) NSNumber<Optional> *inviteExpireTime;

// sender
@property (nonatomic, strong) NSNumber<Optional> *senderId;
@property (nonatomic, strong) Sender<Optional> *sender;
@property (nonatomic, strong) NSNumber<Optional, Ignore> *shared;

// editor
@property (nonatomic, strong) NSNumber<Optional> *editorId;
@property (nonatomic, strong) Sender<Optional> *editor;
@property (nonatomic, strong) NSNumber<Optional> *updateTime;

// peripheral accessory
@property (nonatomic, strong) NSString <Optional> *peripheralName;
@property (nonatomic, strong) NSString <Optional> *peripheralDesc;

// peripheral name, desc, address for trashcan
@property (nonatomic, strong) NSString <Optional> *name;
@property (nonatomic, strong) NSString <Optional> *desc;
@property (nonatomic, strong) NSString <Optional> *address;

// last request time
@property (nonatomic, strong) NSNumber<Optional> *lastRequestTime;

// privilege option
@property (nonatomic, strong) PrivilegeOption<Optional> *privilegeOption;

// privilege "check_only" flag, used in Privilege POST
@property (nonatomic, strong) NSNumber<Optional> *checkOnly;
@property (nonatomic, strong) NSNumber<Optional> *generateCoupon;

// AIRBNB
@property (nonatomic, strong) SCKAirbnbReservation<Optional> *airbnbReservation;

// BADGE
@property (nonatomic, strong) NSNumber<Optional> *badgeId;
@property (nonatomic, strong) Badge<Optional> *badge;

// REMOTE BUTTON
@property (nonatomic, strong) RemoteButton<Optional> *remoteButton;

// SCAN TO OPEN
@property (nonatomic, strong) ScanToOpen<Optional> *peripheralQrcode;

/**
 * retrieve own cached privileges with specific group tag
 */
+ (NSArray<Privilege>*) cachedPrivilegesWithGroupTag:(NSString*)groupTag;

/**
 * get the peripheral target of this privilege if available in Facade cache
 * return nil otherwise
 */
- (Peripheral*) peripheral;

/**
 * get the peripheral group target of this privilege if available in Facade cache
 * return nil otherwise
 */
- (PeripheralGroup*) peripheralGroup;

/**
 * get the target quantity
 * if the privilege target a single peripheral, 1 will be returned
 * if the target is a peripheral group, the count of the peripherals in the group will be returned
 */
- (NSUInteger) getTargetQuantity;

/**
 * returns the peripheral list that target this privilege
 * peripherals returned has this privilege in their "privileges" or "sentPrivileges" arrays
 */
- (NSArray*) targetingPeripherals;

/**
 * checks that both target field and target value are setted
 */
- (BOOL) isValid;

- (BOOL) isNewPrivilege;

- (BOOL) isPrivilegeTypePeripheral;
- (BOOL) isPrivilegeTypePeripheralGroup;

- (BOOL) isPrivilegeSuperAdmin;
- (BOOL) isPrivilegeGuest;
- (BOOL) isPrivilegeOwner;
- (BOOL) isPrivilegeAdmin;
- (BOOL) isPrivilegeInstaller;

- (BOOL) isActive; // 2 active
- (BOOL) isPending;
- (BOOL) isDisabled;
- (BOOL) isPendingReset;
- (BOOL) isPendingAcceptance;

- (BOOL) isAppEnabled;
- (BOOL) isAppInvitationAccepted;
- (BOOL) inviteExpired;

- (BOOL) isKeypadEnabled;
- (BOOL) isBadgeEnabled;
- (BOOL) isAccessoryEnabled;

- (NSString*) fullName;

- (NSDate*) futureCheckinDate:(NSDate*)atDate timeZone:(NSTimeZone*)timeZone;
- (NSString*) clearPinCode;

- (BOOL) isEnabledForDate:(NSDate*)date withPeripheral:(Peripheral*)peripheral;

- (NSDate*) lastRequestDate;

- (void) setActive;
- (void) setPending;
- (void) setDisabled;
- (void) setPendingReset;

#pragma mark - Privileges API

+ (Privilege*) getPrivilegeWithId:(NSNumber*)privilegeId;
+ (Privilege*) getSentPrivilegeWithId:(NSNumber*)privilegeId;

+ (Privilege*) getPrivilegeWithId:(NSNumber*)privilegeId peripheral:(Peripheral*)peripheral;
+ (Privilege*) getSentPrivilegeWithId:(NSNumber*)privilegeId peripheral:(Peripheral*)peripheral;

+ (void) activatePrivilege:(NSNumber*)privilegeId activationCode:(NSString*)code userParams:(NSDictionary*)userParams callback:(ResponseObjectCallback)callback;

+ (void) getSentPrivilegesCallback:(ResponseObjectCallback)callback;
+ (void) getSentPrivileges:(NSDictionary*)params callback:(ResponseObjectCallback)callback;

- (BOOL) can:(NSString*)action;
- (BOOL) can:(NSString*)action withData:(Constraint*)data;

@end
