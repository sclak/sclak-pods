//
//  PeripheralCharacteristic.h
//  SclakFacade
//
//  Created by Daniele Poggi on 03/03/2020.
//  Copyright © 2020 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResponseObject.h"

typedef NS_ENUM(NSInteger, PeripheralCharacteristicType)  {
    PeripheralCharacteristicTypeBoolConstant,
    PeripheralCharacteristicTypeStringConstant,
    PeripheralCharacteristicTypeIntConstant,
    PeripheralCharacteristicTypeBool,
    PeripheralCharacteristicTypeInteger,
    PeripheralCharacteristicTypeString,
};

typedef NS_ENUM(NSInteger, PeripheralCharacteristicKey)  {
    PeripheralCharacteristicKeyLed,
    PeripheralCharacteristicKeyBuzzer,
    PeripheralCharacteristicKeyActivationTime,
    PeripheralCharacteristicKeyPeriodOfActivation,
    PeripheralCharacteristicKeyExtensionType,
    PeripheralCharacteristicKeyDoorStatus,
    PeripheralCharacteristicKeyButtonsEnabled,
    PeripheralCharacteristicKeyReverseRotation,
    PeripheralCharacteristicKeyBistable,
    PeripheralCharacteristicKeyImmobilizerInstalled,
    PeripheralCharacteristicKeyIdentify,
    PeripheralCharacteristicKeyExternalId
};

@protocol PeripheralCharacteristic;

@interface PeripheralCharacteristic : JSONModel

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString<Optional> *defaultValue;
@property (nonatomic, strong) NSString<Optional> *min;
@property (nonatomic, strong) NSString<Optional> *max;
@property (nonatomic, strong) NSString<Optional> *acceptableValues;

- (BOOL) getBoolValue;
- (NSNumber*) getIntValue;
- (NSString*) getStringValue;
- (NSArray<NSString*>*)getArrayAcceptableValues;
- (PeripheralCharacteristicType) type;
- (PeripheralCharacteristicKey) key;

@end
