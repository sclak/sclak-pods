//
//  Project.h
//  SclakFacade
//
//  Created by Daniele Poggi on 26/01/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import "ResponseObject.h"
#import "SCKFacadeCallbacks.h"

@interface Project : ResponseObject

@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *apiKey;
@property (nonatomic, strong) NSString *apiSecret;
@property (nonatomic, strong) NSNumber *insertTime;

+ (Project*) restoreProject;

+ (void) getProjectWithApiKey:(NSString*)apiKey apiSecret:(NSString*)apiSecret callback:(ResponseObjectCallback)callback;

@end
