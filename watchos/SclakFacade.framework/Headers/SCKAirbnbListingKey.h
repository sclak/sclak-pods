//
//  SCKAirbnbListingTarget.h
//  SclakFacade
//
//  Created by Daniele Poggi on 19/05/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@class Peripheral;
@class PeripheralGroup;

@interface SCKAirbnbListingKey : ResponseObject

@property (nonatomic, strong) NSString *targetType;
@property (nonatomic, strong) NSString *targetField;
@property (nonatomic, strong) NSString *targetValue;

- (Peripheral*) peripheral;
- (PeripheralGroup*) peripheralGroup;

@end
