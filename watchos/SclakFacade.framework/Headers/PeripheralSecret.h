//
//  PeripheralSecret.h
//  SclakFacade
//
//  Created by albi on 12/11/15.
//  Copyright © 2015 Sclak. All rights reserved.
//

#import "ResponseObject.h"

@protocol PeripheralSecret

@end

@class Peripheral;

@interface PeripheralSecret : ResponseObject

@property (nonatomic, strong) NSNumber<Optional> *peripheralId;
@property (nonatomic, strong) NSString<Optional> *secretCode;

@property (nonatomic, strong) NSNumber<Optional> *masterPeripheralId;
@property (nonatomic, strong) NSString<Optional> *masterSecretCode;

@property (nonatomic, strong) Peripheral<Optional> *peripheral;
@property (nonatomic, strong) Peripheral<Optional> *masterPeripheral;

@end

@interface PeripheralSecrets : ResponseObject

@property (nonatomic, strong) NSArray<Optional, PeripheralSecret> *list;

@end
