//
//  SCKPinManager.h
//  SclakFacade
//
//  Created by Daniele Poggi on 01/07/15.
//  Copyright (c) 2015 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Keychain.h"

@interface SCKPinManager : NSObject

@property (nonatomic, readonly, strong) Keychain *keychain;

typedef NS_ENUM(NSUInteger, CipherType) {
    CipherTypeNone,
    CipherTypeXOR,
    CipherTypeAES,
};

+ (instancetype) getInstance;

#pragma mark - OPERATIVE CODE Management

- (BOOL) hasOperativeCode;
- (BOOL) isOperativeCode:(NSString*)operativeCode;
- (BOOL) setOperativeCode:(NSString*)operativeCode;
- (BOOL) changeOperativeCode:(NSString*)operativeCode;
- (BOOL) removeOperativeCode;

- (NSString*) getDefaultOperativeCode;
- (NSString*) cypheredOperativeCodeForCheck:(NSString*)userPassword;
- (NSString*) cypheredSha3OperativeCodeForCheck:(NSString*)operativeCode;

#pragma mark - PUK Management

- (BOOL) testAes;

#pragma mark - PIN & PUK Validity Tests

+ (BOOL) isPUKValid:(NSString*)puk;

#pragma mark - APIs

/**
 * check PUK setted for specific btcode
 *
 * @param btcode - the btcode for which the PUK is set
 * @return the requested PUK, cyphered with type, or nil if not found
 */
- (BOOL) hasPUKForBtcode:(NSString*)btcode;

/**
 * check if PUK is decryptable with current operative code
 */
- (BOOL) canDecryptPUKForBtcode:(NSString*)btcode;

/**
 * Get the PUK for a specific peripheral btcode
 * the PUK could be a plain code, or could have been ciphered with a CipherType method.
 *
 * @param btcode - the btcode for which the PUK is set
 * @param type - the PUK will be encrypted with XOR or AES, or no encryption will be used
 * @return the requested PUK, cyphered with type, or nil if not found
 */
- (NSString*) PUKForBtcode:(NSString*)btcode encryption:(CipherType)type;

/**
 * Set new PUK for a specific peripheral btcode
 * the PUK could be a plain code, or could have been ciphered with a CipherType method.
 *
 * @param PUK - the PUK code
 * @param btcode - the btcode for which the PUK is set
 * @param type - the PUK could se passed to method in plain string, or ciphered with XOR or AES
 * @return Success or Failure
 */
- (BOOL) setPUK:(NSString*)PUK forBtcode:(NSString*)btcode encryption:(CipherType)type;

/**
 * remove PUK for btcode
 */
- (BOOL) removePUKForBtcode:(NSString*)btcode;

/**
 * remove all PUKs
 */
- (BOOL) removeAllPUKs;

#pragma mark - PIN Management

/**
 * check at least one PIN is setted for specific btcode
 *
 * @param btcode - the btcode for which the PIN is set
 * @return true if at least one PIN is found, false otherwise
 */
- (BOOL) hasPINForBtcode:(NSString*)btcode;

/**
 * check a PIN is setted for specific btcode and pid
 *
 * @param btcode - the btcode for which the PIN is set
 * @param pid - the privilege id for which the PIN is set
 * @return true if at least one PIN is found, false otherwise
 */
- (BOOL) hasPINForBtcode:(NSString*)btcode privilege:(NSNumber*)pid;

/**
 * get the first PIN setted for specific btcode
 *
 * @param btcode - the btcode for which the PIN is set
 * @return the first found PIN if at least one PIN has been setted, nil otherwise
 */
- (NSString*) PINForBtcode:(NSString*)btcode encryption:(CipherType)type;

/**
 * get the PIN setted for specific btcode and pid
 *
 * @param btcode - the btcode for which the PIN is set
 * @param pid - the privilege id for which the PIN is set
 * @return the found PIN if has been setted, nil otherwise
 */
- (NSString*) PINForBtcode:(NSString*)btcode privilege:(NSNumber*)pid encryption:(CipherType)type;

/**
 * Set new PIN for a specific peripheral btcode and pid
 * the PIN could be a plain code, or could have been ciphered with a CipherType method.
 *
 * @param PIN - the PIN code
 * @param btcode - the btcode for which the PIN is set
 * @param type - the PIN could se passed to method in plain string, or ciphered with XOR or AES
 * @return Success or Failure
 */
- (BOOL) setPIN:(NSString*)clearPIN forBtcode:(NSString*)btcode privilege:(NSNumber*)pid encryption:(CipherType)type;

/**
 * remove any PIN for btcode
 */
- (BOOL) removePINForBtcode:(NSString*)btcode;

/**
 * remove PIN for btcode and pid
 */
- (BOOL) removePINForBtcode:(NSString*)btcode privilege:(NSNumber*)pid;

/**
 * remove all PINs
 */
- (BOOL) removeAllPINs;

#pragma mark - Cypher

/**
 * Cypher PIN with Cipher type
 * the PUK could be a plain code, or could have been ciphered with a CipherType method.
 *
 * @param clearPIN - PIN, clear
 * @param clearPUK - PUK, clear
 * @param type - the encryption type (AES, XOR)
 * @return the cyphered PIN
 */
- (NSString*) cypherPIN:(NSString*)clearPIN withPUK:(NSString*)clearPUK encryption:(CipherType)type;

/**
 * Cypher PIN with Cipher type
 * the PUK could be a plain code, or could have been ciphered with a CipherType method.
 *
 * @param cypheredPIN - PIN, cyphered with AES or XOR
 * @param clearPUK - PUK, clear
 * @param type - the encryption type or PIN (AES, XOR)
 * @return the cyphered PIN
 */
- (NSString*) decypherPIN:(NSString*)cypheredPIN withPUK:(NSString*)clearPUK encryption:(CipherType)type;

#pragma mark - Accessory

- (NSString*) generateRandomCode:(int)numDigits;

@end
