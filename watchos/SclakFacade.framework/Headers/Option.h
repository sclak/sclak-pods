//
//  Option.h
//  SclakApp
//
//  Created by albi on 23/12/14.
//  Copyright (c) 2014 Sclak. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol Option
@end

@interface Option : JSONModel

@property (strong, nonatomic) NSString <Optional> *name;
@property (strong, nonatomic) id <Optional> value;

@end
