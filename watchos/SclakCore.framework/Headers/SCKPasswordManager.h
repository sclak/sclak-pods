//
//  SCKPasswordManager.h
//  SclakCore
//
//  Created by Daniele Poggi on 13/06/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCKManagerProtocol.h"

@interface SCKPasswordManager : NSObject<SCKManagerProtocol>

#pragma mark - Shared Instance

+ (SCKPasswordManager*) sharedInstance;

#pragma mark - Password Accessories

- (NSUInteger) countPasswords;
- (NSString*) getPassword:(NSString*)username;
- (void) setUsername:(NSString*)username password:(NSString*)password;
- (void) deletePassword:(NSString*)username;

@end
