//
//  SCKPrivilegeActivationLinkManager.h
//  SclakApp
//
//  Created by Daniele Poggi on 20/05/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

typedef void (^PrivilegeActivationLinkCallback)(BOOL success, NSString *tinyURL, NSString *webURL);

@interface SCKPrivilegeActivationLinkManager : NSObject

- (void) generateTinyPrivilegeActivationLink:(Privilege*)privilege callback:(PrivilegeActivationLinkCallback)callback;

@end
