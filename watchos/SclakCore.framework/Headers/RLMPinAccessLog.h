//
//  SCKPinAccessLog.h
//  SclakFacade
//
//  Created by Daniele Poggi on 15/05/2018.
//  Copyright © 2018 Sclak. All rights reserved.
//

//#import "Realm.h"
#import <Realm/Realm.h>

// @protocol RLMInt;

@interface RLMPinAccessLog : RLMObject

/**
 * Peripheral btcode on which the access log has been saved
 */
@property (nonatomic, strong) NSString *btcode;

/**
 * Codice valido (0) o non Valido (1) default 0
 */
@property (nonatomic, strong) NSNumber<RLMInt> *invalidCode;

/**
 * Tipo di evento loggato:
 * 0 Evento di accesso con esito positivo.
 * 1 Evento di accesso negato errore codice PIN (pin non presente).
 * 2 Evento di accesso negato PIN non valido da accessorio.
 * 3 Evento di accesso negato errore validazione temporale PIN.
 */
@property (nonatomic, strong) NSNumber<RLMInt> *logType;

/**
 * Data LOG evento costruita unendo i campi date e time con la funzione di SclakPeripheralPin106
 */
@property (nonatomic, strong) NSDate *logDate;

/**
 * codice di accesso ricevuto ed utilizzato per la validazione accesso.
 */
@property (nonatomic, strong) NSString *pinCode;

@end
