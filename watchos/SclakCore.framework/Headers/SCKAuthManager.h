//
//  SCKAuthManager.h
//  SclakCore
//
//  Created by Daniele Poggi on 30/11/18.
//

#import <Foundation/Foundation.h>
#import "SCKAuthCommon.h"
#import "SCKAuthCallback.h"
#import "SCKManagerProtocol.h"
#import "PXAlertView.h"

// STANDARD USER DEFAULTS
#define kLastUsedCouponCode                     @"lastUsedCouponCode"

// NOTIFICATIONS

@class AuthToken;
@class Email;
@class ResponseObject;

@protocol SCKAuthManagerDelegate;

@interface SCKAuthManager : NSObject<SCKManagerProtocol>

#pragma mark - Properties

@property (nonatomic, weak) id<SCKAuthManagerDelegate> delegate;

#pragma mark - UI Properties

@property (nonatomic, strong) PXAlertView *progressAlert;

#pragma mark - Shared Instance

+ (SCKAuthManager*) sharedInstance;

#pragma mark - Setup

- (void) setupWithDelegate:(id<SCKAuthManagerDelegate>)delegate;

#pragma mark - Login Accessors

- (void) doLoginWithUsername:(NSString*)username
                    password:(NSString*)password
              rememberOption:(BOOL)rememberOption
                    referrer:(AuthReferrer)referrer
                    callback:(LoginFinishedCallback)callback;

#pragma mark - Register Accessors

- (void) doRegistrationWithName:(NSString*)name
                        surname:(NSString*)surname
                          email:(NSString*)email
                    phoneNumber:(NSString*)phoneNumber
                       password:(NSString*)password
                       referrer:(AuthReferrer)referrer
                       callback:(RegisterFinishedCallback)callback;

- (void) doInstallerRegistrationWithName:(NSString*)name
                                 surname:(NSString*)surname
                                   email:(NSString*)email
                             phoneNumber:(NSString*)phoneNumber
                                password:(NSString*)password
                                referrer:(AuthReferrer)referrer
                                callback:(RegisterFinishedCallback)callback;

#pragma mark - Static Accessors (Edit Email)

+ (void) replaceEmail:(Email*)email withEmail:(NSString*)newEmail callback:(EditEmailFinishedCallback)callback;

#pragma mark - Static Accessors (Change Password)

+ (void) changePassword:(NSString*)oldPassword newPassword:(NSString*)newPassword callback:(ChangePasswordFinishedCallback)callback;
+ (void) resetPasswordForEmail:(NSString*)email phoneNumber:(NSString*)phoneNumber code:(NSString*)code password:(NSString*)password callback:(ResetPasswordFinishedCallback)callback;

#pragma mark - Static Accessors (Forgot Password)

+ (void) sendActivationCodeForEmail:(Email*)email callback:(SendActivationCodeFinishedCallback)callback;
+ (void) generateResetPasswordCodeWithEmail:(NSString*)email phoneNumber:(NSString*)phoneNumber callback:(GenerateResetPasswordCodeFinishedCallback)callback;

#pragma mark - Validations Static Accessors

+ (BOOL) validateEmailWithString:(NSString*)email;
+ (BOOL) validatePhoneWithString:(NSString*)checkString;
+ (BOOL) validatePasswordWithString:(NSString*)password;

#pragma mark - ECC

- (void) manageECC:(AuthToken*)authToken;

@end

@protocol SCKAuthManagerDelegate <NSObject>

@required

- (void) loginSuccessWithReferrer:(AuthReferrer)referrer callback:(LoginFinishedCallback)callback;
- (void) registrationSuccessWithReferrer:(AuthReferrer)referrer callback:(RegisterFinishedCallback)callback;

@optional

- (void) twoFactorChallengeWithUsername:(NSString*)username password:(NSString*)password callback:(TwoFactorChallengeFinishedCallback)callback;
- (void) codeVerificationChallangeWithEmail:(NSString*)email callback:(CodeVerificationChallengeFinishedCallback)callback;

@end
