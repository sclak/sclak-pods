//
//  SCKRealm.h
//  SclakCore
//
//  Created by Daniele Poggi on 25/05/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCKRealm : NSObject

+ (void) setDefaultRealm;
+ (void) setInMemoryRealm;
+ (void) manageMigrations;

@end
