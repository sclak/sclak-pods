//
//  SCKEventLogger.h
//  SclakCore
//
//  Created by Daniele Poggi on 11/03/2019.
//

#import <Foundation/Foundation.h>

@protocol SCKEventLoggerDelegate;

@interface SCKEventLogger : NSObject <SCKEventLoggerDelegate>

@property (nonatomic, strong) id<SCKEventLoggerDelegate> delegate;

+ (SCKEventLogger*) sharedInstance;

@end

@protocol SCKEventLoggerDelegate <NSObject>

@optional
- (void) logCustomEventWithName:(nonnull NSString*)eventName
               customAttributes:(nonnull NSDictionary*)customAttributes;
- (void) logLogin:(nonnull NSDictionary*) customAttributes;
- (void)logSignUp:(nonnull NSDictionary*) customAttributes;

@end
