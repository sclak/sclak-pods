//
//  SCKPeripheralInstallationManager.h
//  SclakCore
//
//  Created by Daniele Poggi on 06/02/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCKManagerProtocol.h"

@class Peripheral;
@class InstallationPost;

typedef void(^InstallationCompleted)(BOOL success, Peripheral *installedPeripheral);

@interface SCKPeripheralInstallationManager : NSObject<SCKManagerProtocol>

#pragma mark - Properties

// Installation properties
@property (nonatomic, strong) Peripheral *installPeripheral; // FM: bad readwrite here, use readonly and make accessory methods to manage association ... !!!
@property (nonatomic, strong) InstallationPost *installation;

#pragma mark - Shared Instance

+ (SCKPeripheralInstallationManager*) sharedInstance;

#pragma mark - Quick installation

- (void) performQuickInstall:(Peripheral*)peripheral activationCode:(NSString*)activationCode callback:(InstallationCompleted)callback;

@end
