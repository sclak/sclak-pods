//
//  SCKPeripheralUsage.h
//  SclakCore
//
//  Created by Daniele Poggi on 09/10/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SclakBle/SclakBle.h>
#import <SclakFacade/LogUsage.h>
#import "SCKPeripheralUsageManagerProtocols.h"

typedef void(^UsageBlock)(SCKPeripheralUsage * _Nonnull usage);

@class Peripheral;
@class Privilege;

@interface SCKPeripheralUsage : PPLPeripheralUsage

/**
 * the usage type: opened with button, or auto open, or toc toc etc.
 */
@property (nonatomic, assign) LogUsageType usageType;

/**
 * the peripheral to check and open
 */
@property (nonatomic, strong) Peripheral *peripheral;

/**
 * the delegate calling this API
 */
@property (nonatomic, weak) id<SCKPeripheralUsageDelegate> delegate;

/**
 * the privilege selected for operating the peripheral
 */
@property (nonatomic, strong) Privilege *privilege;

/**
 * the BLE peripheral selected for operating the usage
 */
@property (nonatomic, strong) PPLDiscoveredPeripheral *discoveredPeripheral;

/**
 * if the usage is configured to open a slave peripheral, it is setted here
 */
@property (nonatomic, assign) BOOL slavePeripheral;

/**
 * true if command requires to track GPS location after operation has been done
 */
@property (nonatomic, assign) BOOL trackLocationGPS;

#pragma mark - Event Blocks

/**
 * event "start blinking"
 */
@property (nonatomic, strong) UsageBlock startBlinkingBlock;

/**
 * event "stop blinking"
 */
@property (nonatomic, strong) UsageBlock stopBlinkingBlock;

/**
 * event "ask permission for remote use"
 */
@property (nonatomic, strong) UsageBlock requestPermissionRemoteUseBlock;

#pragma mark - Build Usage

+ (instancetype) buildWithPeripheral:(Peripheral*)peripheral;

+ (instancetype) buildWithCommand:(SclakCommand)command
                        usageType:(LogUsageType)usageType
                       peripheral:(Peripheral*)peripheral
                         delegate:(id<SCKPeripheralUsageDelegate>)delegate
                       disconnect:(BOOL)disconnect;

- (SclakPeripheral*) getOrRestorePeripheral;

@end
