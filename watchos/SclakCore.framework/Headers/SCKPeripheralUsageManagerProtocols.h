//
//  SCKPeripheralUsageManagerProtocols.h
//  SclakCore
//
//  Created by Daniele Poggi on 17/03/2020.
//  Copyright © 2020 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCKPeripheralUsage.h"
#import <SclakFacade/Peripheral.h>

@protocol SCKPeripheralUsageManagerDelegate <NSObject>

@optional
- (void) afterSuccessUsage:(SCKPeripheralUsage*)usage;
- (void) afterFailureUsage:(SCKPeripheralUsage*)usage error:(NSError*)error;
- (void) showBluetoothOffAlertIfNeeded;
- (void) settingPeripheralAfterUsage:(Peripheral*)peripheral;
- (void) ringTheBell:(SCKPeripheralUsage*)usage;
- (void) requestPermissionForRemoteUse:(SCKPeripheralUsage*)usage;
- (void) securityLevelStartsGetData:(SCKPeripheralUsage*)usage;
- (void) securityLevelEndsGetData:(SCKPeripheralUsage*)usage;
@end

@protocol SCKPeripheralUsageManagerForceScanQrcodeDelegate <NSObject>
@optional
- (void) qrcodeScannerNeeded:(SCKPeripheralUsage*)usage;
@end

@protocol SCKPeripheralUsageDelegate<NSObject>
- (void) onUsageSuccess;
- (void) onUsageError:(NSError*)error;
@optional
- (void) startBlinking:(SCKPeripheralUsage*)usage;
- (void) stopBlinking:(SCKPeripheralUsage*)usage;
- (void) animateSclakButtonProgressView:(float)seconds hideWhenFinished:(BOOL)hide;
- (void) interruptSclakButtonProgressView:(BOOL)hide;
@end
