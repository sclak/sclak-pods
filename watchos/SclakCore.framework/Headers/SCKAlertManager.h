//
//  SCKAlertManager.h
//  SclakCore
//
//  Created by Daniele Poggi on 28/05/2018.
//  Copyright © 2018 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCKManagerProtocol.h"

#if defined SCLAK_CORE_WATCH
#import <WatchKit/WatchKit.h>
#elif defined SCLAK_CORE_APP
#import "PXAlertView.h"
#endif

@interface SCKAlertManager : NSObject<SCKManagerProtocol>

#pragma mark - Shared Instance

+ (SCKAlertManager*) sharedInstance;

#pragma mark - Alerts & Notifications

#if defined SCLAK_CORE_WATCH
    
- (void) alertWithController:(WKInterfaceController*)controller message:(NSString*)message;
    
#elif defined SCLAK_CORE_TODAY
    
- (void) alert:(NSString*)message;
- (void) showErrorAlertViewForResult:(id)result;
    
#elif defined SCLAK_CORE_APP

- (void) alert:(NSString*)message;

- (void) showOrNotifySclakErrorWithTitle:(NSString*)title message:(NSString*)message callback:(PXAlertViewCompletionBlock)callback;

- (void) showErrorAlertViewForResult:(id)result;

- (void) showErrorAlertViewForResult:(id)result andDispatchProgressAlert:(PXAlertView*)progressAlert completion:(PXAlertViewCompletionBlock)completion;

- (void) showErrorAlertWithTitle:(NSString*)title message:(NSString*)message andDispatchProgressAlert:(PXAlertView*)progressAlert;

- (void) showErrorAlertWithTitle:(NSString*)title message:(NSString*)message andDispatchProgressAlert:(PXAlertView*)progressAlert completion:(PXAlertViewCompletionBlock)completion;

- (PXAlertView*) showProgressAlertViewWithTitle:(NSString*)title message:(NSString*)message;

- (PXAlertView*) showProgressAlertViewWithTitle:(NSString*)title message:(NSString*)message cancelTitle:(NSString*)cancelTitle;

- (PXAlertView*) showProgressAlertViewWithTitle:(NSString*)title message:(NSString*)message cancelTitle:(NSString*)cancelTitle completion:(PXAlertViewCompletionBlock)completion;

#endif

@end
