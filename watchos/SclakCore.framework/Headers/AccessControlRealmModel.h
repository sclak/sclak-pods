//
//  AccessControlRealmModel.h
//  SclakCore
//
//  Created by Daniele Poggi on 18/05/16.
//  Copyright © 2016 Sclak. All rights reserved.
//

#import <Realm/Realm.h>

@interface AccessControlRealmModel : RLMObject

@property (nonatomic, strong) NSString *btcode;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSNumber<RLMInt> *openingType;
@property (nonatomic, strong) NSString *apiLevel;
@property (nonatomic, strong) NSString *device;
@property (nonatomic, assign) BOOL checkInOut;
@property (nonatomic, assign) BOOL synced;

@end

