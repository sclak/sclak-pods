//
//  AccessCounterRealmModel.h
//  SclakCore
//
//  Created by Daniele Poggi on 25/03/2019.
//

#import <Realm/Realm.h>

@interface AccessCounterRealmModel : RLMObject

@property (nonatomic, strong) NSString *btcode;
@property (nonatomic, strong) NSNumber<RLMDouble> *datetime;
@property (nonatomic, strong) NSNumber<RLMInt> *accessCounter;

@end

