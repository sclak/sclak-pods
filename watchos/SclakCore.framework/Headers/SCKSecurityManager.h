//
//  SCKSecurityManager.h
//  SclakCore
//
//  Created by albi on 01/12/15.
//  Copyright © 2015 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCKManagerProtocol.h"

// error domain
#define SECURITY_MANAGER_ERROR_DOMAIN                       @"com.sclak.security.error"

// error codes
#define SECURITY_NO_ERRORS                                  1000
#define SECURITY_ERROR_NO_CONNECTION                        1001
#define SECURITY_ERROR_CONNECTION_CANNOT_BE_VERIFIED        1002
#define SECURITY_ERROR_MAX_SEVEN_DAYS                       1003
#define SECURITY_ERROR_MAX_EIGHT_HOURS                      1004
#define SECURITY_ERROR_MAX_ONE_HOUR                         1005

// notifications
#define SECURITY_LEVEL_COUNTDOWN_FINISHED_NOTIFICATION      @"SECURITY_LEVEL_COUNTDOWN_FINISHED_NOTIFICATION"
#define SECURITY_TIMER_FIRED_NOTIFICATION                   @"SECURITY_TIMER_FIRED_NOTIFICATION"

// timer
#define SECURITY_TIMER_TIME_STRING                          @"timeString"

typedef NS_ENUM(NSUInteger, SecurityLevel) {
    SecurityLevelLow,
    SecurityLevelMedium,
    SecurityLevelHigh,
};

@interface SCKSecurityManager : NSObject<SCKManagerProtocol>

#pragma mark - Shared Instance

// + (instancetype) defaultManager;
+ (SCKSecurityManager*) sharedInstance;

#pragma mark - Check Availability Security Level

+ (NSError*) checkAvailabilityForSecurityLevel:(SecurityLevel)securityLevel
                                lastUpdateDate:(NSDate*)updateDate
                                   currentDate:(NSDate*)currentDate
                                      timeZone:(NSString*)timeZone;

#pragma mark - Security Timers

- (BOOL) startSecurityTimerForBtcode:(NSString*)btcode;
- (BOOL) stopSecurityTimerForBtcode:(NSString*)btcode;

@end
