//
//  SCKPeripheralUsageManager.h
//  SclakCore
//
//  Created by Daniele Poggi on 11/02/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCKActionError.h"
#import "SCKManagerProtocol.h"
#import "SCKPeripheralUsage.h"
#import "SCKPeripheralUsageManagerProtocols.h"
#import <SclakBle/PPLBluetoothCallbacks.h>

// notification center events
#define SCKPeripheralUsageManager_afterSuccessUsage @"SCKPeripheralUsageManager_afterSuccessUsage"
#define SCKPeripheralUsageManager_afterFailureUsage @"SCKPeripheralUsageManager_afterFailureUsage"

#define kUsageError @"usage_error"

@class Peripheral, Privilege, SclakPeripheral;

typedef void (^PrivilegeEvaluationCallback) (BOOL success, Privilege *privilege, SclakActionError error, NSString *localizedErrorMessage);

typedef enum : NSUInteger {
    UsageErrorPeripheralNeverScanned,
    UsageErrorPeripheralOutOfRange,
    UsageErrorMQTTError,
    UsageErrorInternalError,
} UsageError;

@interface SCKPeripheralUsageManager : NSObject <SCKManagerProtocol>

#pragma mark - Properties

@property (nonatomic, weak) id<SCKPeripheralUsageManagerDelegate> delegate;
@property (nonatomic, weak) id<SCKPeripheralUsageManagerForceScanQrcodeDelegate> forceScanQrcodeDelegate;

/**
 * enable / disable usage of alerts to communicate problems with the User
 */
@property BOOL enableAlerts;

/**
 * enable / disable BLE proximity check before requesting the peripheral BLE connection
 */
@property BOOL enableProximityCheck;

#pragma mark - Singleton instance

+ (SCKPeripheralUsageManager*) sharedInstance;

#pragma mark - Setup

- (void) setupWithDelegate:(id<SCKPeripheralUsageManagerDelegate>)delegate;

#pragma mark - Peripheral Usages

- (SCKPeripheralUsage*) usageWithBtcode:(NSString*)btcode;
- (SCKPeripheralUsage*) removeUsageWithBtcode:(NSString*)btcode;

#pragma mark - Remote Usage

/**
 * when the execution of a PeripheralUsage requires a remote control (e.g. with MQTT protocol)
 * the delegate will be called requestPermissionForRemoteUse:
 * in not implemented, it is assumed the permission is automatically granted
 * when the delegate is setted, it must then confirm or deny remote usage by calling this method
 */
- (void) confirmRemoteUse:(SCKPeripheralUsage*)usage deny:(BOOL)deny;

#pragma mark - Open Lock Success Hook

/**
 * evaluate what can be done after a successfull open/close command exection
 */
- (void) evaluateOpenLockSuccessHook:(SCKPeripheralUsage*)usage;

/**
 * check access control for peripheral with privilege at date, offline
 */
- (BOOL) accessControlCheck:(Privilege*)privilege peripheral:(Peripheral*)peripheral now:(NSDate*)now;
- (BOOL) checkConditionsForOpenDenied:(Peripheral*)peripheral privilege:(Privilege*)privilege now:(NSDate*)now;

#pragma mark - SCLAK

/**
 *  evaluate permission and then start a sclak command
 *  @param usage               the usage model to evaluate, contains the usage information
 */
- (void) evaluateSclakUsage:(SCKPeripheralUsage*)usage;

/**
 *  evaluate permission and then start a sclak command
 *
 *  @param usage               the usage model to evaluate, contains the usage information
 *  @param startProgressBlock  callback called when the command has started
 *  @param evaluationCallback  callback called when the evaluation is completed
 *  @param responseCallback    callback called when a response is received
 *  @param disconnectCallback  callback called when the command has been executed and a disconnaction has been made
 */
- (void) evaluateSclakUsage:(SCKPeripheralUsage*)usage
      startProgressCallback:(void (^)(void))startProgressBlock
         evaluationCallback:(PrivilegeEvaluationCallback)evaluationCallback
           responseCallback:(BluetoothResponseErrorCallback)responseCallback
         disconnectCallback:(BluetoothResponseCallback)disconnectCallback;

/**
 *  evaluate a peripheral usage, report if the peripheral can be operated in responseCallback success/failure response
 *
 *  @param usage the peripheral usage to evaluate
 *  @param callback the evaluation callback
 */
- (void) evaluatePeripheralUsage:(SCKPeripheralUsage*)usage callback:(PrivilegeEvaluationCallback)callback;

#pragma mark - Operations

/**
 * the sclak command (0x90)
 */
- (void) sclakCommand:(SCKPeripheralUsage*)usage
startProgressCallback:(void (^)(void))startProgressBlock
     responseCallback:(BluetoothResponseErrorCallback)responseCallback
   disconnectCallback:(BluetoothResponseCallback)disconnectCallback;

/**
 * interrupt all operations
 */
- (void) interrupt;

/**
 * invoked when a SCKPeripheralUsage ends on success
 */
- (void) success:(SCKPeripheralUsage*)usage;

/**
 * invoked when a SCKPeripheralUsage ends on failure, with given error
 */
- (void) failure:(SCKPeripheralUsage*)usage error:(NSError*)error;

#pragma mark - Utilities

/**
 * check the provided sclak peripheral has been discovered recently
 */
- (BOOL) checkProximity:(SclakPeripheral*)sclak;

@end
