//
//  SCKSyncAccessManager.h
//  SclakUnit
//
//  Created by Daniele Poggi on 10/05/2018.
//  Copyright © 2018 SCLAK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SclakFacade/LogUsage.h>
#import "SCKManagerProtocol.h"

#if !TARGET_OS_WATCH
#import <UIKit/UIKit.h>
#endif


extern NSUInteger const ERROR_RETRIEVE_GPS_FAILED;

@class Peripheral;
@class SclakPeripheral;

typedef void(^LocationGpsCompleted)(BOOL success, NSError *error);

// Notification Center
#define TRACK_LOCATION_GPS_STARTED        @"TRACK_LOCATION_GPS_STARTED"
#define TRACK_LOCATION_GPS_PROGRESS       @"TRACK_LOCATION_GPS_PROGRESS"
#define TRACK_LOCATION_GPS_FINISHED       @"TRACK_LOCATION_GPS_FINISHED"

// FM: to be renamed in SCKAccessSyncManager
@interface SCKGPSManager : NSObject<SCKManagerProtocol>

#pragma mark - Shared Instance

+ (SCKGPSManager*) sharedInstance;

/**
 * delete LocationGps models
 */
- (void) deleteTrackLocationGps;

#pragma mark - Track Location GPS

#if !TARGET_OS_WATCH
/**
 * read from device the access counter, save on local database
 */
- (void) trackLocationGPS:(Peripheral*)peripheral
               peripheral:(SclakPeripheral*)discoveredPeripheral
             updateServer:(BOOL)updateServer
                  context:(UIViewController*)context
                 callback:(LocationGpsCompleted)callback;

- (void) startTrackLocationGPS:(Peripheral*)peripheral
               peripheral:(SclakPeripheral*)discoveredPeripheral
             updateServer:(BOOL)updateServer
                  context:(UIViewController*)context
                 callback:(LocationGpsCompleted)callback;

- (void) getAllLocationGpsFromServer:(Peripheral*)peripheral callback:(LocationGpsCompleted)callback;
#endif
/**
 * read from local database access counters and send to sclak server
 */
- (void) trackLocationGpsToServerCallback:(LocationGpsCompleted)callback;
/**
 * read from local database access counters and send to sclak server
 */
- (NSString*) getLastLocationGps:(Peripheral*)peripheral;
- (NSArray*) getAllLocationGps:(Peripheral*)peripheral;
@end
