//
//  EntrematicSyncManager.h
//  SclakUnit
//
//  Created by Daniele Poggi on 11/04/2017.
//  Copyright © 2017 SCLAK. All rights reserved.
//

#import "SCKFacadeCallbacks.h"
#import <Foundation/Foundation.h>
#import "SCKSyncManagerProtocol.h"

@interface EntrematicSyncManager : NSObject<SCKSyncManagerProtocol>

#pragma mark - Sync Peripheral

- (void) syncPeripheral:(EntrematicPeripheral*)entrematicPeripheral callback:(ResponseObjectCallback)callback;

@end
