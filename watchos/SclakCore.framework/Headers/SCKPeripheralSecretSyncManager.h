//
//  SCKPeripheralSecretSyncManager.h
//  SclakApp
//
//  Created by Daniele Poggi on 10/06/2017.
//  Copyright © 2017 Sclak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCKSyncManagerProtocol.h"
#import <SclakBle/PPLBluetoothCallbacks.h>

@class Peripheral;
@class SclakPeripheral;

// Notification Center
#define PERIPHERAL_SECRET_SYNC_MANAGER_STARTED        @"PERIPHERAL_SECRET_SYNC_MANAGER_STARTED"
#define PERIPHERAL_SECRET_SYNC_MANAGER_FINISHED       @"PERIPHERAL_SECRET_SYNC_MANAGER_FINISHED"

// FM: to be renamed in SCKPeripheralSecretSyncUsageManager
@interface SCKPeripheralSecretSyncManager : NSObject<SCKSyncManagerProtocol>

#pragma mark - Sync Peripheral

/**
 * start the sync process using model and peripheral to retrive secret sync from server
 * and then sync with bluetooth APIs
 * update server when sync is complete
 * does NOT disconnect with peripheral at the end of procedure
 */
- (void) syncPeripheralSecret:(Peripheral*)peripheral
         discoveredPeripheral:(SclakPeripheral*)discoveredPeripheral
                     callback:(BluetoothResponseErrorCallback)callback;

@end
