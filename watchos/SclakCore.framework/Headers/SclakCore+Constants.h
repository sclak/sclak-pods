//
//  SclakCore+Constants.h
//  SclakCore
//
//  Created by Francesco Mantello on 12/12/18.
//  Copyright (c) 2018 Sclak. All rights reserved.
//

// Network reachability AFNetworking enabled
#ifndef ENABLE_AFNETWORKING_REACHABILITY_CHECK
    #define ENABLE_AFNETWORKING_REACHABILITY_CHECK  NO
#endif

// SCLAK SDK STRINGS TABLE
#ifndef SCLAK_SDK_STRINGS
    #define SCLAK_SDK_STRINGS                       @"SclakSDK"
#endif

#ifndef ENABLE_ACCESS_COUNTER_SYNC
    #define ENABLE_ACCESS_COUNTER_SYNC              NO
#endif

#ifndef ENABLE_TRACK_LOCATION_GPS
    #define ENABLE_TRACK_LOCATION_GPS               YES
#endif

// New Feature (Experimental)
#ifndef ENABLE_LOW_RSSI_AUTH_CHECK
    #define ENABLE_LOW_RSSI_AUTH_CHECK              NO
#endif

#ifndef LOW_RSSI_LIMIT
    #define LOW_RSSI_LIMIT                         -85
#endif

#ifndef ENABLE_LEGACY_PUK_PIN_BACKUP
    #define ENABLE_LEGACY_PUK_PIN_BACKUP            NO
#endif

#ifndef ENABLE_INSTALLATION_LOG
    #define ENABLE_INSTALLATION_LOG                 NO
#endif
